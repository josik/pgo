﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline
{
    public delegate void ExportCompletedEventHandler();

    interface IExportManager
    {
        event ExportCompletedEventHandler ExportCompleted;

        void ExportDataAsync();
        void ExportDataCancelAsync();
    }
}
