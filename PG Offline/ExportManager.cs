﻿using PGOffline.Model;

namespace PGOffline
{
    public class ExportUnit
    {
        DiscussionGroup _group;

        public ExportUnit()
        {
        }

        public ExportUnit(DiscussionGroup group)
        {
            _group = group;

            StartExportFromNumber = FirstMessageNumber;
            EndExportAtNumber = LastMessageNumber;
        }

        public DiscussionGroup Group { get { return _group; } }
        public long FirstMessageNumber { get { return _group.FirstMessageNumber; } }
        public long LastMessageNumber { get { return _group.LastMessageNumber; } }
        public long StartExportFromNumber { get; set; }
        public long EndExportAtNumber { get; set; }
        public bool SetToExport { get; set; }
    }

    public struct ExportSettings
    {
        public bool CopyAttachments { get; set; }
        public bool ExportFavoriteStatus { get; set; }
        public ExportType ExportType { get; set; }
    }

    public enum ExportType
    {
        AllMessages = 0,
        OnlyFavorites,
        OnlyFavoritesNumbers
    }

    public class ExportManager
    {
    }
}
