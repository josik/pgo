﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline
{
    class StatisticsPair<T>
    {
        T _firstObject;
        T _secondObject;

        public T FirstObject
        {
            get 
            {
                return _firstObject; 
            }
            set 
            {
                _firstObject = value; 
            }
        }

        public T SecondObject
        {
            get
            {
                return _secondObject; 
            }
            set 
            {
                _secondObject = value;
            }
        }
    }
}
