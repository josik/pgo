﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using PGOffline.Model;

namespace PGOffline
{
    class StatisticsUtils
    {
        private static Regex regexIsReply = new Regex(@"(?i)^Re(\[\d*\])?:\s*");

        public static List<Message> GetReplies(List<Message> messages)
        {
            List<Message> replies = new List<Message>();

            foreach (Message message in messages)
            {
                if (MessageIsReply(message.Subject))
                {
                    replies.Add(message);
                }
            }

            return replies;
        }

        public static bool MessageIsReply(string messageSubject)
        {
            Match match = regexIsReply.Match(messageSubject);

            return match.Value != String.Empty;
        }

        public static string GetOriginalSubject(string subject)
        {
            string originalSubject = subject;

            if (MessageIsReply(originalSubject))
            {
                Match match = regexIsReply.Match(originalSubject);
                originalSubject = originalSubject.Replace(match.Value, "");
            }

            return originalSubject;
        }
    }
}
