﻿using PGOffline.Model;

namespace PGOffline
{
    struct CompareUnit
    {
        public StatisticsForGroup unit1;
        public StatisticsForGroup unit2;
    }
}