﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline
{
    class ReportBuilder
    {
        public static string BuildReport(List<CompareUnit> units, StatisticsOptions options)
        {
            string reportBody = String.Empty;

            if (options.postings)
            {
                reportBody += GetPostings(units);
            }

            if (options.repliers)
            {
                reportBody += GetReplies(units);
            }

            if (options.topTenSubjects)
            {
                reportBody += GetTopTenSubject(units);
            }

            if (options.threadInitiators)
            {
                reportBody += GetThreadInitiators(units);
            }

            if (options.newMembers)
            {
                reportBody += GetNewMembers(units);
            }

            if (options.lurkers)
            {
                reportBody += GetLurkers(units);
            }

            string report = TemplatesManager.GetTemplate(111);
            report = report.Replace("@@report_date@@", DateTime.Now.ToShortDateString());
            report = report.Replace("@@reports@@", reportBody);

            return report;
        }

        private static string GetPostings(List<CompareUnit> units)
        {
            string postingsBody = String.Empty;

            for (int i = 0; i < units.Count; i++)
            {
                postingsBody += GroupStatisticsHtmlPresenter.GetPostings(units[i].unit1);
                if (units[i].unit2 != null)
                    postingsBody += GroupStatisticsHtmlPresenter.GetPostings(units[i].unit2);
            }

            string postings = TemplatesManager.GetTemplate(112);

            postings = postings.Replace("@@groups@@", postingsBody);

            return postings;
        }

        private static string GetReplies(List<CompareUnit> units)
        {
            string repliersBody = String.Empty;

            foreach (CompareUnit unit in units)
            {
                string repliesSkeleton = TemplatesManager.GetTemplate(132);
                repliesSkeleton = repliesSkeleton.Replace("@@header1@@", GroupStatisticsHtmlPresenter.GroupHeader(unit.unit1));
                repliesSkeleton = repliesSkeleton.Replace("@@part1@@", GroupStatisticsHtmlPresenter.GetReplies(unit.unit1));

                if (unit.unit2 != null)
                {
                    repliesSkeleton = repliesSkeleton.Replace("@@header2@@", GroupStatisticsHtmlPresenter.GroupHeader(unit.unit2));
                    repliesSkeleton = repliesSkeleton.Replace("@@part2@@", GroupStatisticsHtmlPresenter.GetReplies(unit.unit2));
                }
                else
                {
                    repliesSkeleton = repliesSkeleton.Replace("@@header2@@", "");
                    repliesSkeleton = repliesSkeleton.Replace("@@part2@@", "");
                }

                repliersBody += repliesSkeleton;
            }

            string repliers = TemplatesManager.GetTemplate(131);
            repliers = repliers.Replace("@@groups@@", repliersBody);

            return repliers;
        }

        private static string GetTopTenSubject(List<CompareUnit> units)
        {
            string subjectsBody = String.Empty;

            foreach (CompareUnit unit in units)
            {
                string subjectsSkeleton = TemplatesManager.GetTemplate(122);
                subjectsSkeleton = subjectsSkeleton.Replace("@@header1@@", GroupStatisticsHtmlPresenter.GroupHeader(unit.unit1));
                subjectsSkeleton = subjectsSkeleton.Replace("@@part1@@", GroupStatisticsHtmlPresenter.GetSubjects(unit.unit1));

                if (unit.unit2 != null)
                {
                    subjectsSkeleton = subjectsSkeleton.Replace("@@header2@@", GroupStatisticsHtmlPresenter.GroupHeader(unit.unit2));
                    subjectsSkeleton = subjectsSkeleton.Replace("@@part2@@", GroupStatisticsHtmlPresenter.GetSubjects(unit.unit2));
                }
                else
                {
                    subjectsSkeleton = subjectsSkeleton.Replace("@@header2@@", "");
                    subjectsSkeleton = subjectsSkeleton.Replace("@@part2@@", "");
                }

                subjectsBody += subjectsSkeleton;
            }

            string subjects = TemplatesManager.GetTemplate(121);
            subjects = subjects.Replace("@@groups@@", subjectsBody);

            return subjects;
        }

        private static string GetThreadInitiators(List<CompareUnit> units)
        {
            string threadInitiatorsBody = String.Empty;

            foreach (CompareUnit unit in units)
            {
                string threadInitiatorsSkeleton = TemplatesManager.GetTemplate(142);
                threadInitiatorsSkeleton = threadInitiatorsSkeleton.Replace("@@header1@@", GroupStatisticsHtmlPresenter.GroupHeader(unit.unit1));
                threadInitiatorsSkeleton = threadInitiatorsSkeleton.Replace("@@part1@@", GroupStatisticsHtmlPresenter.GetThreadInitiators(unit.unit1));

                if (unit.unit2 != null)
                {
                    threadInitiatorsSkeleton = threadInitiatorsSkeleton.Replace("@@header2@@", GroupStatisticsHtmlPresenter.GroupHeader(unit.unit2));
                    threadInitiatorsSkeleton = threadInitiatorsSkeleton.Replace("@@part2@@", GroupStatisticsHtmlPresenter.GetThreadInitiators(unit.unit2));
                }
                else
                {
                    threadInitiatorsSkeleton = threadInitiatorsSkeleton.Replace("@@header2@@", "");
                    threadInitiatorsSkeleton = threadInitiatorsSkeleton.Replace("@@part2@@", "");
                }

                threadInitiatorsBody += threadInitiatorsSkeleton;
            }

            string threadInitiators = TemplatesManager.GetTemplate(141);
            threadInitiators = threadInitiators.Replace("@@groups@@", threadInitiatorsBody);

            return threadInitiators;
        }

        private static string GetNewMembers(List<CompareUnit> units)
        {
            string newMembersBody = String.Empty;

            foreach (CompareUnit unit in units)
            {
                string newMembersSkeleton = TemplatesManager.GetTemplate(152);
                newMembersSkeleton = newMembersSkeleton.Replace("@@header1@@", GroupStatisticsHtmlPresenter.GroupHeader(unit.unit1));
                newMembersSkeleton = newMembersSkeleton.Replace("@@part1@@", GroupStatisticsHtmlPresenter.GetNewMembers(unit.unit1));

                if (unit.unit2 != null)
                {
                    newMembersSkeleton = newMembersSkeleton.Replace("@@header2@@", GroupStatisticsHtmlPresenter.GroupHeader(unit.unit2));
                    newMembersSkeleton = newMembersSkeleton.Replace("@@part2@@", GroupStatisticsHtmlPresenter.GetNewMembers(unit.unit2));
                }
                else
                {
                    newMembersSkeleton = newMembersSkeleton.Replace("@@header2@@", "");
                    newMembersSkeleton = newMembersSkeleton.Replace("@@part2@@", "");
                }

                newMembersBody += newMembersSkeleton;
            }

            string newMembers = TemplatesManager.GetTemplate(151);
            newMembers = newMembers.Replace("@@groups@@", newMembersBody);

            return newMembers;
        }

        private static string GetLurkers(List<CompareUnit> units)
        {
            string lurkersBody = String.Empty;

            foreach (CompareUnit unit in units)
            {
                string lurkersSkeleton = TemplatesManager.GetTemplate(162);
                lurkersSkeleton = lurkersSkeleton.Replace("@@header1@@", GroupStatisticsHtmlPresenter.GroupHeader(unit.unit1));
                lurkersSkeleton = lurkersSkeleton.Replace("@@part1@@", GroupStatisticsHtmlPresenter.GetLurkers(unit.unit1));

                if (unit.unit2 != null)
                {
                    lurkersSkeleton = lurkersSkeleton.Replace("@@header2@@", GroupStatisticsHtmlPresenter.GroupHeader(unit.unit2));
                    lurkersSkeleton = lurkersSkeleton.Replace("@@part2@@", GroupStatisticsHtmlPresenter.GetLurkers(unit.unit2));
                }
                else
                {
                    lurkersSkeleton = lurkersSkeleton.Replace("@@header2@@", "");
                    lurkersSkeleton = lurkersSkeleton.Replace("@@part2@@", "");
                }

                lurkersBody += lurkersSkeleton;
            }

            string lurkers = TemplatesManager.GetTemplate(161);
            lurkers = lurkers.Replace("@@groups@@", lurkersBody);

            return lurkers;
        }
    }
}
