﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.Model;

namespace PGOffline
{
    class GroupStatisticsHtmlPresenter
    {
        public static string GroupHeader(StatisticsForGroup statisticsForGroup)
        {
            string header = TemplatesManager.GetTemplate(133);
            header = header.Replace("@@group_name@@", statisticsForGroup.Group.Name);
            header = header.Replace("@@range@@", statisticsForGroup.Range.ToString());

            return header;
        }

        public static string GetPostings(StatisticsForGroup statisticsForGroup)
        {
            string rows = String.Empty;
            string template = TemplatesManager.GetTemplate(114);

            foreach (StatisticsForUser unit in statisticsForGroup.UserStats)
            {
                if (unit.postCount == 0)
                    continue;

                string row = template;
                row = row.Replace("@@from@@", unit.userName);
                row = row.Replace("@@first_Post@@", unit.first.ToShortDateString());
                row = row.Replace("@@last_post@@", unit.last.ToShortDateString());
                row = row.Replace("@@posts@@", unit.postCount.ToString());
                row = row.Replace("@@percents@@", unit.postPercentage.ToString("0.#") + " %");

                rows += row;
            }

            string tableSkeleton = TemplatesManager.GetTemplate(113);
            tableSkeleton = tableSkeleton.Replace("@@rows@@", rows);
            tableSkeleton = tableSkeleton.Replace("@@group_name@@", statisticsForGroup.Group.Name);
            tableSkeleton = tableSkeleton.Replace("@@range@@", statisticsForGroup.Range.ToString());

            if (rows == String.Empty)
            {
                string noMessages = TemplatesManager.GetTemplate(115);
                tableSkeleton = tableSkeleton.Replace("@@rows@@", noMessages);
            }

            return tableSkeleton;
        }

        public static string GetReplies(StatisticsForGroup statisticsForGroup)
        {
            string values = TemplatesManager.GetTemplate(134);
            values = values.Replace("@@total_number@@", statisticsForGroup.RepliesCount.ToString());
            values = values.Replace("@@average_number@@", statisticsForGroup.RepliesPerMessage.ToString("0.#"));

            return values;
        }

        public static string GetSubjects(StatisticsForGroup statisticsForGroup)
        {
            string template = TemplatesManager.GetTemplate(125);

            string rows = String.Empty;

            Dictionary<string, int>.Enumerator enumerator = statisticsForGroup.RepliesPerSubject.GetEnumerator();
            enumerator.MoveNext();

            if (enumerator.Current.Value == 0)
                return TemplatesManager.GetTemplate(126);

            for (int i = 1; i <= statisticsForGroup.RepliesPerSubject.Count && i <= 10; i++)
            {
                if (enumerator.Current.Value == 0)
                    break;

                string row = template;
                row = row.Replace("@@number@@", i.ToString());
                row = row.Replace("@@subject@@", enumerator.Current.Key);
                row = row.Replace("@@replies@@", enumerator.Current.Value.ToString());

                enumerator.MoveNext();

                rows += row + '\n';
            }

            string table = TemplatesManager.GetTemplate(124);
            table = table.Replace("@@rows@@", rows);

            return table;
        }

        public static string GetThreadInitiators(StatisticsForGroup statisticsForGroup)
        {
            statisticsForGroup.UserStats = (from entry in statisticsForGroup.UserStats orderby entry.threadsStarted descending select entry).ToList<StatisticsForUser>();

            string rows = String.Empty;
            string template = TemplatesManager.GetTemplate(145);

            foreach (StatisticsForUser unit in statisticsForGroup.UserStats)
            {
                if (unit.threadsStarted == 0)
                    continue;

                string row = template;
                row = row.Replace("@@name@@", unit.userName);
                row = row.Replace("@@count@@", unit.threadsStarted.ToString());
                row = row.Replace("@@percents@@", unit.threadsPercentage.ToString("0.#") + "%");

                rows += row;
            }

            string table = TemplatesManager.GetTemplate(144);
            table = table.Replace("@@rows@@", rows);

            if (rows == String.Empty)
            {
                string noSubjects = TemplatesManager.GetTemplate(146);
                table = table.Replace("@@rows@@", noSubjects);
            }

            return table;
        }

        public static string GetNewMembers(StatisticsForGroup statisticsForGroup)
        {
            if (statisticsForGroup.Range.NumericRange)
                return TemplatesManager.GetTemplate(155);

            string values = TemplatesManager.GetTemplate(154);
            values = values.Replace("@@new_members_number@@", statisticsForGroup.NewMembersCount.ToString());
            values = values.Replace("@@percentage_change@@", statisticsForGroup.NewMembersPercentageChange.ToString("0.#"));

            if (statisticsForGroup.Group.GetMembersCount() == 0)
                values = values.Replace("@@warning@@", TemplatesManager.GetTemplate(157));
            else
                values = values.Replace("@@warning@@", "");

            return values;
        }

        public static string GetLurkers(StatisticsForGroup statisticsForGroup)
        {
            if (statisticsForGroup.Group.GetMembersCount() == 0)
                return TemplatesManager.GetTemplate(168);

            if (statisticsForGroup.Range.NumericRange)
                return TemplatesManager.GetTemplate(167);

            string rows = String.Empty;
            string template = TemplatesManager.GetTemplate(165);

            foreach (Person unit in statisticsForGroup.Lurkers)
            {
                string row = template;
                row = row.Replace("@@name@@", unit.Name);
                row = row.Replace("@@joined@@", unit.Joined.ToShortDateString());

                rows += row;
            }

            if (rows == String.Empty)
            {
                return TemplatesManager.GetTemplate(166);
            }

            string lurkersPercentage = TemplatesManager.GetTemplate(164);
            lurkersPercentage = lurkersPercentage.Replace("@@rows@@", rows);
            lurkersPercentage = lurkersPercentage.Replace("@@percent_lurkers@@", statisticsForGroup.LurkersPercentage.ToString("0.#"));

            return lurkersPercentage;
        }
    }
}
