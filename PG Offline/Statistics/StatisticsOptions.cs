﻿using System;
using PGOffline.Model;
using PGOffline.Exceptions;
using System.Windows.Forms;

namespace PGOffline
{
    public struct StatisticsOptions
    {
        public StatisticsType statisticsType;

        public bool postings;
        public bool repliers;
        public bool topTenSubjects;
        public bool threadInitiators;
        public bool newMembers;
        public bool lurkers;

        public DateTime dateTimeStart;
        public DateTime dateTimeEnd;

        public long messageNumberFirst;
        public long messageNumberLast;

        public DateTime compare1StartDate;
        public DateTime compare1EndDate;

        public DateTime compare2StartDate;
        public DateTime compare2EndDate;

        public long compare1StartNumber;
        public long compare1EndNumber;

        public long compare2StartNumber;
        public long compare2EndNumber;

        public string compareGroup1Name;
        public string compareGroup2Name;

        public StatisticsOptions SetStatisticsOptions()
        {
            StatisticsOptions options = new StatisticsOptions();

            try
            {
                options.compare1EndDate = Options.Instance.GetAsDateTime("statistics_options_compare_1_end_date");
            }
            catch (FormatException)
            {
                throw new StatisticsOptionsFormatException("Statistics options compare_1_end_date isn't correct.");
            }

            try
            {
                options.compare1StartDate = Options.Instance.GetAsDateTime("statistics_options_compare_1_start_date");
            }
            catch (FormatException)
            {
                throw new StatisticsOptionsFormatException("Statistics options compare_1_start_date isn't correct.");
            }

            try
            {
                options.compare2EndDate = Options.Instance.GetAsDateTime("statistics_options_compare_2_end_date");
            }
            catch (FormatException)
            {
                throw new StatisticsOptionsFormatException("Statistics options compare_2_end_date isn't correct.");
            }

            try
            {
                options.compare2StartDate = Options.Instance.GetAsDateTime("statistics_options_compare_2_start_date");
            }
            catch (FormatException)
            {
                throw new StatisticsOptionsFormatException("Statistics options compare_2_start_date isn't correct.");
            }

            try
            {
                options.dateTimeEnd = Options.Instance.GetAsDateTime("statistics_options_date_time_end");
            }
            catch (FormatException)
            {
                throw new StatisticsOptionsFormatException("Statistics options date_time_end isn't correct.");
            }

            try
            {
                options.dateTimeStart = Options.Instance.GetAsDateTime("statistics_options_date_time_start");
            }
            catch (FormatException)
            {
                throw new StatisticsOptionsFormatException("Statistics options date_time_start isn't correct.");
            }

            options.compare1EndNumber = Options.Instance.GetStatisticsNumber("statistics_options_compare_1_end_number");
            options.compare1StartNumber = Options.Instance.GetStatisticsNumber("statistics_options_compare_1_start_number");
            options.compare2EndNumber = Options.Instance.GetStatisticsNumber("statistics_options_compare_2_end_number");
            options.compare2StartNumber = Options.Instance.GetStatisticsNumber("statistics_options_compare_2_start_number");

            options.compareGroup1Name = Options.Instance.GetAsString("statistics_options_compare_group_1_name");
            options.compareGroup2Name = Options.Instance.GetAsString("statistics_options_compare_group_2_name");

            options.lurkers = Options.Instance.GetAsBoolean("statistics_options_lurkers");
            options.messageNumberFirst = Options.Instance.GetStatisticsNumber("statistics_options_message_number_first");
            options.messageNumberLast = Options.Instance.GetStatisticsNumber("statistics_options_message_number_last");
            options.newMembers = Options.Instance.GetAsBoolean("statistics_options_new_members");
            options.postings = Options.Instance.GetAsBoolean("statistics_options_postings");
            options.repliers = Options.Instance.GetAsBoolean("statistics_options_repliers");
            options.statisticsType = (StatisticsType)Options.Instance.GetAsInt("statistics_options_statistics_type");
            options.threadInitiators = Options.Instance.GetAsBoolean("statistics_options_thread_initiators");
            options.topTenSubjects = Options.Instance.GetAsBoolean("statistics_options_top_ten_subjects");

            return options;
        }

        public StatisticsOptions SetStatisticsOptions(Options option)
        {
            StatisticsOptions statisticsOptions = new StatisticsOptions();

            try
            {
                statisticsOptions.compare1EndDate = option.GetAsDateTime("statistics_options_compare_1_end_date");
            }
            catch (FormatException)
            {
                throw new StatisticsOptionsFormatException("Statistics options compare_1_end_date isn't correct.");
            }

            try
            {
                statisticsOptions.compare1StartDate = option.GetAsDateTime("statistics_options_compare_1_start_date");
            }
            catch (FormatException)
            {
                throw new StatisticsOptionsFormatException("Statistics options compare_1_start_date isn't correct.");
            }

            try
            {
                statisticsOptions.compare2EndDate = option.GetAsDateTime("statistics_options_compare_2_end_date");
            }
            catch (FormatException)
            {
                throw new StatisticsOptionsFormatException("Statistics options compare_2_end_date isn't correct.");
            }

            try
            {
                statisticsOptions.compare2StartDate = option.GetAsDateTime("statistics_options_compare_2_start_date");
            }
            catch (FormatException)
            {
                throw new StatisticsOptionsFormatException("Statistics options compare_2_start_date isn't correct.");
            }

            try
            {
                statisticsOptions.dateTimeEnd = option.GetAsDateTime("statistics_options_date_time_end");
            }
            catch (FormatException)
            {
                throw new StatisticsOptionsFormatException("Statistics options date_time_end isn't correct.");
            }

            try
            {
                statisticsOptions.dateTimeStart = option.GetAsDateTime("statistics_options_date_time_start");
            }
            catch (FormatException)
            {
                throw new StatisticsOptionsFormatException("Statistics options date_time_start isn't correct.");
            }

            statisticsOptions.compare1EndNumber = Options.Instance.GetStatisticsNumber("statistics_options_compare_1_end_number");
            statisticsOptions.compare1StartNumber = Options.Instance.GetStatisticsNumber("statistics_options_compare_1_start_number");
            statisticsOptions.compare2EndNumber = Options.Instance.GetStatisticsNumber("statistics_options_compare_2_end_number");
            statisticsOptions.compare2StartNumber = Options.Instance.GetStatisticsNumber("statistics_options_compare_2_start_number");

            statisticsOptions.compareGroup1Name = Options.Instance.GetAsString("statistics_options_compare_group_1_name");
            statisticsOptions.compareGroup2Name = Options.Instance.GetAsString("statistics_options_compare_group_2_name");

            statisticsOptions.lurkers = Options.Instance.GetAsBoolean("statistics_options_lurkers");
            statisticsOptions.messageNumberFirst = Options.Instance.GetStatisticsNumber("statistics_options_message_number_first");
            statisticsOptions.messageNumberLast = Options.Instance.GetStatisticsNumber("statistics_options_message_number_last");
            statisticsOptions.newMembers = Options.Instance.GetAsBoolean("statistics_options_new_members");
            statisticsOptions.postings = Options.Instance.GetAsBoolean("statistics_options_postings");
            statisticsOptions.repliers = Options.Instance.GetAsBoolean("statistics_options_repliers");
            statisticsOptions.statisticsType = (StatisticsType)Options.Instance.GetAsInt("statistics_options_statistics_type");
            statisticsOptions.threadInitiators = Options.Instance.GetAsBoolean("statistics_options_thread_initiators");
            statisticsOptions.topTenSubjects = Options.Instance.GetAsBoolean("statistics_options_top_ten_subjects");

            return statisticsOptions;
        }
    }
}