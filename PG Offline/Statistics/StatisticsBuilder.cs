﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel;

using PGOffline.Forms;
using PGOffline.Model;

namespace PGOffline
{
    class StatisticsBuilder
    {
        private List<StatisticsPair<StatisticsSubject>> _statisticsSubjects;
        private StatisticsOptions _statisticsOptions;

        private BackgroundWorker _worker;
        private FormDeterminateProgress _formIndeterminateProgress;

        public delegate void StatisticsCalculationCompleteEventHandler(string statisticsReport);
        public event StatisticsCalculationCompleteEventHandler StatisticsCalculationComplete;

        public StatisticsBuilder()
        {
            _worker = new BackgroundWorker();
            _worker.WorkerSupportsCancellation = true;
            _worker.WorkerReportsProgress = true;
            _worker.DoWork += new DoWorkEventHandler((sender, e) => CalculateStatistics(e));
            _worker.ProgressChanged += new ProgressChangedEventHandler(OnWorkerProgressChanged);
            _worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(OnStatisticsCalculationComplete);
        }

        public void SetStatisticsSubjects(List<StatisticsPair<StatisticsSubject>> statisticsSubjects, StatisticsOptions statisticsOptions)
        {
            _statisticsSubjects = statisticsSubjects;
            _statisticsOptions = statisticsOptions;            
        }

        private void OnWorkerProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            _formIndeterminateProgress.SetProgress(e.ProgressPercentage);

            if (e.UserState != null)
                _formIndeterminateProgress.Title = "Processing " + e.UserState as string;
        }

        public void BuildReportAsync()
        {
            if (_worker.IsBusy)
                return;

            StatisticsForGroupCalculator calculator = new StatisticsForGroupCalculator();

            List<CompareUnit> statisticsForGroups = new List<CompareUnit>();

            _formIndeterminateProgress = new FormDeterminateProgress();
            _formIndeterminateProgress.Show();

            _worker.RunWorkerAsync(statisticsForGroups);
        }

        public void OnStatisticsCalculationComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            _formIndeterminateProgress.Close();

            if(e.Cancelled == false)
                StatisticsCalculationComplete(e.Result as string);
        }

        private void CalculateStatistics(DoWorkEventArgs e)
        {
            List<CompareUnit> statisticsForGroups = e.Argument as List<CompareUnit>;
            StatisticsForGroupCalculator calculator = new StatisticsForGroupCalculator();

            foreach (StatisticsPair<StatisticsSubject> pair in _statisticsSubjects)
            {
                CompareUnit cu = new CompareUnit();

                cu.unit1 = calculator.CalculateData(pair.FirstObject.ReckoningGroup, pair.FirstObject.RangeForReckoning, _worker);

                if (pair.SecondObject != null)
                    cu.unit2 = calculator.CalculateData(pair.SecondObject.ReckoningGroup, pair.SecondObject.RangeForReckoning, _worker);

                if (_worker.CancellationPending)
                    break;

                statisticsForGroups.Add(cu);
            }

            e.Result = ReportBuilder.BuildReport(statisticsForGroups, _statisticsOptions);

            if (_worker.CancellationPending)
                e.Cancel = true;
        }

        public void CancelStatisticsBuilding()
        {
            _worker.CancelAsync();
        }
    }
}
