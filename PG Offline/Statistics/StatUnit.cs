﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.Model;

namespace PGOffline
{
    public class StatUnit
    {
        public StatUnit()
        {
            _totalPosts = 0;
        }

        private long _id;
        private string _name;
        private int _totalPosts;
        private Nullable<DateTime> _firstMessageDate;
        private Nullable<DateTime> _lastMessageDate;

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int TotalPosts
        {
            get { return _totalPosts; }
            set { _totalPosts = value; }
        }
        
        public Nullable<DateTime> FirstMessageDate
        {
            get { return _firstMessageDate; }
            set { _firstMessageDate = value; }
        }
        
        public Nullable<DateTime> LastMessageDate
        {
            get { return _lastMessageDate; }
            set { _lastMessageDate = value; }
        }
    }
}
