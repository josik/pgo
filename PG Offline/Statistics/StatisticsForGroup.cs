﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.Model;

namespace PGOffline
{
    class StatisticsForGroup
    {
        private int _repliesCount;

        private double _repliesPerMessage;

        private Dictionary<string, int> _repliesPerSubject = new Dictionary<string, int>();

        private List<Person> _lurkers = new List<Person>();

        private List<StatisticsForUser> _userStats = new List<StatisticsForUser>();

        private double _lurkersPercentage;

        private int _newMembersCount;

        private double _newMembersPercentageChange;

        private DiscussionGroup _group;
        private StatisticsRange _range;

        public StatisticsForGroup(DiscussionGroup group, StatisticsRange range)
        {
            _group = group;
            _range = range;
        }
        
        public double NewMembersPercentageChange
        {
            get { return _newMembersPercentageChange; }
            set { _newMembersPercentageChange = value; }
        }

        public int NewMembersCount
        {
            get { return _newMembersCount; }
            set { _newMembersCount = value; }
        }

        public double LurkersPercentage
        {
            get { return _lurkersPercentage; }
            set { _lurkersPercentage = value; }
        }

        public List<Person> Lurkers
        {
            get { return _lurkers; }
            set { _lurkers = value; }
        }

        public Dictionary<string, int> RepliesPerSubject
        {
            get { return _repliesPerSubject; }
            set { _repliesPerSubject = value; }
        }

        public int RepliesCount
        {
            get { return _repliesCount; }
            set { _repliesCount = value; }
        }

        public double RepliesPerMessage
        {
            get { return _repliesPerMessage; }
            set { _repliesPerMessage = value; }
        }

        public List<StatisticsForUser> UserStats
        {
            get { return _userStats; }
            set { _userStats = value; }
        }

        public DiscussionGroup Group
        {
            get { return _group; }
            set { _group = value; }
        }

        public StatisticsRange Range
        {
            get { return _range; }
            set { _range = value; }
        }
    }
}
