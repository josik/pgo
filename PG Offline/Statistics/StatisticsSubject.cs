﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.Model;

namespace PGOffline
{
    class StatisticsSubject
    {
        private DiscussionGroup _reckoningGroup;

        private StatisticsRange _rangeForReckoning;

        public DiscussionGroup ReckoningGroup
        {
            get 
            { 
                return _reckoningGroup; 
            }

            set 
            {
                _reckoningGroup = value; 
            }
        }

        public StatisticsRange RangeForReckoning
        {
            get 
            {
                return _rangeForReckoning; 
            }
            set
            {
                _rangeForReckoning = value; 
            }
        }
    }
}
