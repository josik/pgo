﻿using System;

namespace PGOffline
{
    class StatisticsForUser
    {
        public string userName;

        public int postCount;
        public double postPercentage;

        public int threadsStarted;
        public double threadsPercentage;

        public DateTime first;
        public DateTime last;
    }
}