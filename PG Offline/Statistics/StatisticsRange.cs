﻿using System;

namespace PGOffline
{
    class StatisticsRange
    {
        public long beginNumber;
        public long endNumber;

        public DateTime beginDate;
        public DateTime endDate;

        private StatisticsType _statisticsType;

        public StatisticsRange(StatisticsType statisticsType)
        {
            _statisticsType = statisticsType;
        }

        public bool NumericRange
        {
            get
            {
                return (_statisticsType == StatisticsType.ComparisonByCustomNumbers || _statisticsType == StatisticsType.StatisticsBasedOnMessagesNumbers);
            }
        }

        public bool DateRange
        {
            get
            {
                return (NumericRange == false && _statisticsType != StatisticsType.GroupVsGroup);
            }
        }

        public override string ToString()
        {
            if (_statisticsType == StatisticsType.GroupVsGroup)
                return "All messages";

            if (_statisticsType == StatisticsType.ComparisonByCustomNumbers || _statisticsType == StatisticsType.StatisticsBasedOnMessagesNumbers)
            {
                if (beginNumber == long.MaxValue && endNumber == long.MaxValue)
                    return "Last - Last";
                if (endNumber == long.MaxValue)
                    return beginNumber.ToString() + " - Last";
                else
                    return beginNumber.ToString() + " - " + endNumber.ToString();
            }

            return beginDate.ToShortDateString() + " - " + endDate.ToShortDateString();
        }
    }
}