﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.ComponentModel;
using System.Text.RegularExpressions;

using PGOffline.Model;

namespace PGOffline
{
    class StatisticsForGroupCalculator
    {
        private List<Person> _members = new List<Person>();
        private List<Message> _allGroupMessages;
        private List<Message> _allGroupReplies;

        public StatisticsForGroup CalculateData(DiscussionGroup group, StatisticsRange range, BackgroundWorker worker)
        {
            worker.ReportProgress(0, group.Name);

            StatisticsForGroup statisticsForGroup = new StatisticsForGroup(group, range);

            long ticks = DateTime.Now.Ticks;
            GetSourceData(ref statisticsForGroup);
            long ticks2 = DateTime.Now.Ticks;
            System.Diagnostics.Trace.WriteLine("GetSourceData completed in " + ((ticks2 - ticks) / 10000).ToString() + " ms.");

            ticks = DateTime.Now.Ticks;
            AnalyzeUsers(ref statisticsForGroup, worker);
            ticks2 = DateTime.Now.Ticks;
            System.Diagnostics.Trace.WriteLine("AnalyzeUsers completed in " + ((ticks2 - ticks) / 10000).ToString() + " ms.");

            ticks = DateTime.Now.Ticks;
            AnalyzeMessages(ref statisticsForGroup, worker);
            ticks2 = DateTime.Now.Ticks;
            System.Diagnostics.Trace.WriteLine("AnalyzeMessages completed in " + ((ticks2 - ticks) / 10000).ToString() + " ms.");

            ticks = DateTime.Now.Ticks;
            SortData(ref statisticsForGroup, worker);
            ticks2 = DateTime.Now.Ticks;
            System.Diagnostics.Trace.WriteLine("SortData completed in " + ((ticks2 - ticks) / 10000).ToString() + " ms.");

            return statisticsForGroup;
        }

        private void SortData(ref StatisticsForGroup statisticsForGroup, BackgroundWorker worker)
        {
            if (worker.CancellationPending)
                return;

            statisticsForGroup.UserStats = (from entry in statisticsForGroup.UserStats orderby entry.postCount descending select entry).ToList<StatisticsForUser>();
            statisticsForGroup.RepliesPerSubject = (from entry in statisticsForGroup.RepliesPerSubject orderby entry.Value descending select entry).ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        private void GetSourceData(ref StatisticsForGroup statisticsForGroup)
        {
            if (statisticsForGroup.Range.DateRange)
                _members = statisticsForGroup.Group.GetMembersJoinedBefore(statisticsForGroup.Range.endDate);
            else
                _members = statisticsForGroup.Group.GetMembers();

            if (statisticsForGroup.Range.NumericRange)
                _allGroupMessages = statisticsForGroup.Group.GetMessagesInRange(statisticsForGroup.Range.beginNumber, statisticsForGroup.Range.endNumber);
            else if (statisticsForGroup.Range.DateRange)
                _allGroupMessages = statisticsForGroup.Group.GetMessagesInRange(statisticsForGroup.Range.beginDate, statisticsForGroup.Range.endDate);
            else
                _allGroupMessages = statisticsForGroup.Group.GetOfflineMessages();

            _allGroupReplies = StatisticsUtils.GetReplies(_allGroupMessages);
        }

        private void AnalyzeUsers(ref StatisticsForGroup statisticsForGroup, BackgroundWorker worker)
        {
            long membersProceed = 0;

            foreach (Person member in _members)
            {
                if (worker.CancellationPending)
                    break;

                StatisticsForUser su = new StatisticsForUser();
                su.userName = member.Name;

                List<Message> userMessages = _allGroupMessages.Where(x => x.Person.Id == member.Id).ToList();
                su.postCount = userMessages.Count;
                su.postPercentage = 100 * (double)userMessages.Count / (double)_allGroupMessages.Count;

                foreach (Message message in userMessages)
                    if (StatisticsUtils.MessageIsReply(message.Subject) == false)
                        su.threadsStarted++;

                su.threadsPercentage = 100 * (double)su.threadsStarted / ((double)_allGroupMessages.Count - (double)_allGroupReplies.Count);

                // Get lurkers.
                if (userMessages.Count == 0 && member.IsMember == true)
                {
                    statisticsForGroup.Lurkers.Add(member);
                }
                else
                {
                    if (userMessages.Count > 0)
                    {
                        su.first = userMessages.First().Date;
                        su.last = userMessages.Last().Date;
                    }
                }

                statisticsForGroup.UserStats.Add(su);

                if (statisticsForGroup.Range.DateRange)
                {
                    if (statisticsForGroup.Range.beginDate <= member.Joined && member.Joined <= statisticsForGroup.Range.endDate)
                        statisticsForGroup.NewMembersCount++;
                }

                membersProceed++;

                if (_members.Count > 0)
                    worker.ReportProgress((int)((double)membersProceed / (double)_members.Count * 100.0f));
            }

            if (_members.Count > 0)
            {
                statisticsForGroup.LurkersPercentage = 100 * ((double)statisticsForGroup.Lurkers.Count / (double)_members.Count);
            }
            if (_members.Count - statisticsForGroup.NewMembersCount > 0)
            {
                statisticsForGroup.NewMembersPercentageChange = 100 * ((double)statisticsForGroup.NewMembersCount) / ((double)_members.Count - (double)statisticsForGroup.NewMembersCount);
            }
            else
            {
                statisticsForGroup.NewMembersPercentageChange = 100;
            }
        }

        private void AnalyzeMessages(ref StatisticsForGroup statisticsForGroup, BackgroundWorker worker)
        {
            statisticsForGroup.RepliesCount = _allGroupReplies.Count;
            int nonReplyMessages = _allGroupMessages.Count - _allGroupReplies.Count;
            if (nonReplyMessages > 0)
                statisticsForGroup.RepliesPerMessage = (double)_allGroupReplies.Count / (double)nonReplyMessages;

            long messagesProceed = 0;

            foreach (Message message in _allGroupReplies)
            {
                if (worker.CancellationPending)
                    break;

                string originalSubject = StatisticsUtils.GetOriginalSubject(message.Subject);

                if (statisticsForGroup.RepliesPerSubject.ContainsKey(originalSubject))
                {
                    statisticsForGroup.RepliesPerSubject[originalSubject]++;
                }
                else
                {
                    statisticsForGroup.RepliesPerSubject.Add(originalSubject, 1);
                }

                messagesProceed++;

                if (_allGroupReplies.Count + _members.Count > 0)
                    worker.ReportProgress((int)((double)(messagesProceed + _members.Count) / (double)(_allGroupReplies.Count + _members.Count) * 100.0f));
            }
        }
    }
}