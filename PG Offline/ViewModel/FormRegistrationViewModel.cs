﻿using System;
using System.Windows;
using MessageBox = System.Windows.Forms.MessageBox;

namespace PGOffline.ViewModel
{
    public class FormRegistrationViewModel : ViewModelBase
    {
        #region Public Properties
        
        private string _displayedLicenseCode;
        private bool _isLicenseValid;
        private bool _isLicenseTypeSubscription;
        private bool _isLicenseTypePerpetual;
        private bool _isLicenseTypeTrial;
        private DateTime _displayLicenseExpirationDate;
        private string _displayedLicenseType;

        public string DisplayedLicenseType
        {
            get { return _displayedLicenseType; }
            private set
            {
                _displayedLicenseType = value != null ? value.Replace("_", " ") : string.Empty;
                NotifyPropertyChanged("DisplayedLicenseType");
            }
        }

        public bool IsLicenseValid
        {
            get { return _isLicenseValid; }
            set
            {
                _isLicenseValid = value;
                NotifyPropertyChanged("IsLicenseValid");
            }
        }

        public bool IsLicenseTypePerpetual
        {
            get { return _isLicenseTypePerpetual; }
            set
            {
                _isLicenseTypePerpetual = value;
                NotifyPropertyChanged("IsLicenseTypePerpetual");
            }
        }

        public bool IsLicenseTypeSubscription
        {
            get { return _isLicenseTypeSubscription; }
            set
            {
                _isLicenseTypeSubscription = value;
                NotifyPropertyChanged("IsLicenseTypeSubscription");
            }
        }

        public bool IsLicenseTypeTrial
        {
            get { return _isLicenseTypeTrial; }
            set
            {
                _isLicenseTypeTrial = value;
                NotifyPropertyChanged("IsLicenseTypeTrial");
            }
        }

        public DateTime DisplayLicenseExpirationDate
        {
            get { return _displayLicenseExpirationDate; }
            set
            {
                _displayLicenseExpirationDate = value;
                NotifyPropertyChanged("DisplayLicenseExpirationDate");
            }
        }
        
        public string DisplayedLicenseCode
        {
            get { return _displayedLicenseCode; }
            set
            {
                _displayedLicenseCode = value;
                NotifyPropertyChanged("DisplayedLicenseCode");
            }
        }

        #endregion Public Properties

        #region Constructor
        public FormRegistrationViewModel()
        {
            RegisterProductCommand = new RelayCommand(RegisterProduct);
            PurchaseProductCommand = new RelayCommand(PurchaseProduct);
            CloseWindowCommand = new RelayCommand(CloseWindow);

            // initialize license display values
            
            DisplayedLicenseCode = License.Instance.GetMaskedLicenseCode();
            
            DisplayedLicenseType = Enum.GetName(typeof(LicenseTypeEnum), License.Instance.LicenseType);

            DisplayLicenseExpirationDate = License.Instance.GetExpirationDate();

            DisplayedLicenseCode = License.Instance.GetMaskedLicenseCode();
            
            switch (License.Instance.LicenseType)
            {
                case LicenseTypeEnum.Perpetual:
                    IsLicenseTypePerpetual = true;
                    break;
                case LicenseTypeEnum.Subscription:
                    IsLicenseTypeSubscription = true;
                    break;
                case LicenseTypeEnum.Trial:
                    IsLicenseTypeTrial = true;
                    break;
            }
        }

        #endregion Constructor

        #region Commands

        public RelayCommand RegisterProductCommand { get; set; }

        public RelayCommand PurchaseProductCommand { get; set; }

        public RelayCommand CloseWindowCommand { get; set; }

        private void RegisterProduct(object obj)
        {
            // tgCAAQGDAYoCrM8BAQNmgpXDzwFPAEVtYWlsPXVzZXIyQGRvbWFpbiNOYW1lPUpvaG4gRG9lI0VuY3J5cHRpb25LZXk9I0RhdGVJc3N1ZWQ9Ny8yOS8yMDE0IDE6Mjc6MzUgUE0BAUBAhYuSyY5FvUugqI8leXQLsdohNg/j8KeCADxoMC/F0t1BzL43IwIGwnVjN5Ghttyxdt4H+e5QBQm/pLBLgx18L5VyYOA7/zlsDxgmkXWdc2uhcfmh2S9sKwFb2Lx6nASZ8lLCeZ01csf2H/FDmtJlUX88lwvmQ0LaUmWq3R2lkBIhJ9F4UUeugReBNypPjsiq0k9LRt2dh6QNDBV4H6Rwv1nvKzxi3uLoyaChZG1DwEBh1RQiqO7ms0fVoWUsciKs+DGXLQDs99xGe1tYqd1liEU7Eh8UQZzzzdazjhi8g9dd8MWfXFFWXiJqVo8W7tq0CSCPdm+IsJ+dFNgAjm8=
            // tgCiA/kzhIN+vc8BSXgLnk/c0AE7AEVtYWlsPSNOYW1lPSNFbmNyeXB0aW9uS2V5PSNEYXRlSXNzdWVkPTgvMjEvMjAxNCA0OjI1OjU3IFBNAQGJ8/HtIp1D68rZxH7/QWxBy1CNOjxaEO1EuSQnkjVJRfFWbWPAWBuBk8FMoPJIPDPl5gJpAEd0oEsjdLE/62XY/7vIpURE7QyE+h2WNqdn7G+WFSb3xJwuc9wUvVqj+ejNTW/++TSVN1laur07eoYhUXMkyOJ8dfhxP8Qol9UXAxnIdpegsATP1DXSGSS60+lMJtkRUKEveXxXSVk3kc8IY4/8BPIR36PGeGkbfQ3uap6/pZuSIGcLmpXqmWNIXJ6cZSwHwH6IlhT3Z5phZOxd9GBOP9yn5V+eriwRfPGGqS4Pi9goFKXX1bOObNtyxXBipg2EDJrbFXEViafhNa9e
            if (DisplayedLicenseCode.Length < 15) return;

            bool isValid = License.Instance.SetLicense(DisplayedLicenseCode, saveToRegistry: true);

            if (!isValid) return;

            DisplayedLicenseType = Enum.GetName(typeof(LicenseTypeEnum), License.Instance.LicenseType);

            DisplayLicenseExpirationDate = License.Instance.GetExpirationDate();

            DisplayedLicenseCode = License.Instance.GetMaskedLicenseCode();

            IsLicenseTypePerpetual = false;
            IsLicenseTypeSubscription = false;
            IsLicenseTypeTrial = false;
            
            switch (License.Instance.LicenseType)
            {
                case LicenseTypeEnum.Perpetual:
                    IsLicenseTypePerpetual = true;
                    break;
                case LicenseTypeEnum.Subscription:
                    IsLicenseTypeSubscription = true;
                    break;
                case LicenseTypeEnum.Trial:
                    IsLicenseTypeTrial = true;
                    break;
            }

            
            //MessageBox.Show("Registration applied.");


        }

        private void PurchaseProduct(object obj)
        {
            throw new NotImplementedException();
        }

        private void CloseWindow(object obj)
        {
            Window wind = (Window) obj;
            wind.Close();

        }

        #endregion Commands

    }
}
