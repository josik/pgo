﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Windows.Input;
using PGOffline.Model;
using Xceed.Wpf.Toolkit;

namespace PGOffline.ViewModel
{
    public class FormSearchViewModel : ViewModelBase
    {
        private MessagesSearchCriteria _criteria;

        private List<Message> _foundMessages;

        private BackgroundWorker _searchBackgroundWorker;

        public ICommand ShowMessageCommand { get; set; }

        public ICommand SearchCommand { get; set; }

        public ICommand AddTestDataCommand { get; set; }

        private string _persons;
        public string Persons
        {
            get { return _persons ?? string.Empty; }
            set { _persons = value; }
        }
        
        private string _subject = "Drill";
        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }

        public string Message { get; set; }
        public long NumberFrom { get; set; }
        public long NumberTo { get; set; }

        public FormSearchViewModel()
        {
            ShowMessageCommand = new RelayCommand(ShowMessage);
            SearchCommand = new RelayCommand(Search);
            AddTestDataCommand = new RelayCommand(AddTestData);
        }

        private void AddTestData(object obj)
        {
            //MessageBox.Show("Adding test data...");

            //FormMainViewModel.MyMessageCollection.Add(new MyMessage("A7", "S7", "T7"));
            //FormMainViewModel.MyMessageCollection.Add(new MyMessage("A8", "S8", "T8"));
            //FormMainViewModel.MyMessageCollection.Add(new MyMessage("A9", "S9", "T9"));
        }

        public void ShowMessage(object obj)
        {
            MessageBox.Show(obj.ToString());
        }
        public void Search(object obj)
        {
            //MessageBox.Show("Searching...");
            
            var mWorker = new BackgroundWorker();
            mWorker.DoWork += new DoWorkEventHandler(worker_Search_DoWork);
            mWorker.WorkerSupportsCancellation = true;
            mWorker.WorkerReportsProgress = true;
            mWorker.ProgressChanged += OnProgressChanged;
            mWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            mWorker.RunWorkerAsync();
            //mWorker.RunWorkerAsync(SiteURLTextBox.Text);
        }
        private void worker_Search_DoWork(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            var selectedGroups = new List<DiscussionGroup>();
            var group = DiscussionGroup.GetByName("7x10minilathe");
            if (group != null) { selectedGroups.Add(group); }

            var groupsToSearch = new List<string> {"7x10minilathe"};

            _criteria = new MessagesSearchCriteria
            {
                persons = Persons,
                subject = Subject,
                message = Message,
                numberFrom = NumberFrom,
                numberTo = NumberTo,
                groupNames = groupsToSearch
            };
            
            _foundMessages = new MessagesFinder().SearchMessages(_criteria);


            FormMainViewModel.MyMessageCollection.Clear();

            foreach (var foundMessage in _foundMessages)
            {
                FormMainViewModel.MyMessageCollection.Add(foundMessage);
            }
            
            System.Diagnostics.Trace.WriteLine("Messages found = " + _foundMessages.Count);
        }

        private void OnProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

    }

}
