﻿using PGOffline.View;
using PGOffline.Model;
using System.ComponentModel;

namespace PGOffline.ViewModel
{
    public class FormImportFromFolderViewModel : ViewModelBase
    {
        public event BrowseDatabaseRequestEventHandler BrowseDatabaseRequest;
        public event GetGroupsListRequestEventHandler GetGroupsListRequest;
        public event ImportDataRequestEventHandler ImportDataRequest;
        public event ChangeRangeRequestEventHandler ChangeRangeRequest;
        public event ImportCancelRequestEventHander ImportCancelRequest;
        public event SaveImportFromMdbOptionsRequestEventHandler SaveImportFromMdbOptionsRequest;
        public event LoadImportFromMdbOptionsRequestEventHandler LoadImportFromMdbOptionsRequest;
        

        public event ProgressChangedEventHandler UpdateProgressChanged;
        public int ProgressVisibility { get; set; }

        private readonly TaskProgress _updateTask;
        public TaskProgress UpdateTask
        {
            get { return _updateTask; }
        }
        
        public FormImportFromFolderViewModel()
        {
            //EnableImportButton();

            _updateTask = new TaskProgress(ref UpdateProgressChanged);
        }

        public void OnUpdateProgressChanged(int progressPercentage)
        {
            if (UpdateProgressChanged != null)
            {
                UpdateProgressChanged(this, new ProgressChangedEventArgs(progressPercentage, null));
            }
        }







    }
}
