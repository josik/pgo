﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using PGOffline.Model;

namespace PGOffline.ViewModel
{
    public class MessageViewModel : ViewModelBase
    {
        private Message _message;
        private ObservableCollection<Message> _messages;
        private ICommand _SubmitCommand;

        public Message Message
        {
            get { return _message; }
            set
            {
                _message = value;
                NotifyPropertyChanged("Message");
            }
        }

        public ObservableCollection<Message> Messages
        {
            get { return _messages; }
            set
            {
                _messages = value;
                NotifyPropertyChanged("Messages");
            }
        }
        public ICommand SubmitCommand
        {
            get
            {
                if (_SubmitCommand == null)
                {
                    _SubmitCommand = new RelayCommand(param => this.Submit(), null);
                }
                return _SubmitCommand;
            }
        } 

        public MessageViewModel()
        {
            Message = new Message();
            Messages = new ObservableCollection<Message>();
            Messages.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Messages_CollectionChanged);            
        }
        //Whenever new item is added to the collection, am explicitly calling notify property changed
        void Messages_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            NotifyPropertyChanged("Messages");
        }        
        private void Submit()
        {
            Message.Favorite = true;
            Messages.Add(Message);
            Message = new Message();
        }

    }
}
