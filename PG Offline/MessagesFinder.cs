﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using PGOffline.Model;

namespace PGOffline
{
    public class MessagesFinder
    {
        public List<Message> SearchMessages(MessagesSearchCriteria criteria)
        {
            var messages = new List<Message>();

            var groupIds = GetSearchGroupsIds(criteria);

            if (groupIds.Count <= 0) return messages;

            var personsIds = GetSearchPersonsIds(criteria, groupIds);

            if (personsIds.Count <= 0 && (personsIds.Count != 0 || criteria.persons.Length != 0)) return messages;

            var searchQuery = BuildSearchQuery(criteria, personsIds, groupIds);

            SQLiteCommand sqliteCmdMessages = CreateSearchCommand(criteria, searchQuery);

            List<object[]> valuesList = DatabaseManager.Instance.ExecuteCommandAndGetValuesList(sqliteCmdMessages);
            messages.AddRange(valuesList.Select(valuesSet => new Message(valuesSet)));

            return messages;
        }

        public List<Message> FindMessages(MessagesSearchCriteria criteria)
        {
            var messages = new List<Message>();

            var groupIds = GetSearchGroupsIds(criteria);

            if (groupIds.Count > 0)
            {
                var personsIds = GetSearchPersonsIds(criteria, groupIds);

                if (personsIds.Count > 0 || (personsIds.Count == 0 && criteria.persons.Length == 0))
                {
                    var searchQuery = BuildSearchQuery(criteria, personsIds, groupIds);

                    SQLiteCommand sqliteCmdMessages = CreateSearchCommand(criteria, searchQuery);

                    List<object[]> valuesList = DatabaseManager.Instance.ExecuteCommandAndGetValuesList(sqliteCmdMessages);

                    FillMessagesList(messages, sqliteCmdMessages);
                }
            }

            return messages;
        }

        private static void FillMessagesList(List<Message> messages, SQLiteCommand sqliteCmdMessages)
        {
            List<object[]> valuesList = DatabaseManager.Instance.ExecuteCommandAndGetValuesList(sqliteCmdMessages);

            foreach (object[] valuesSet in valuesList)
            {
                messages.Add(new Message(valuesSet));
            }
        }

        private SQLiteCommand CreateSearchCommand(MessagesSearchCriteria criteria, string searchQuery)
        {
            var sqliteCmdMessages = DatabaseManager.Instance.CreateCommand(searchQuery);

            if (!string.IsNullOrEmpty(criteria.subject))
            {
                sqliteCmdMessages.Parameters.AddWithValue("@subject", string.Format("%{0}%", criteria.subject));
            }

            if (!string.IsNullOrEmpty(criteria.message))
            {
                sqliteCmdMessages.Parameters.AddWithValue("@content", string.Format("%{0}%", criteria.message));
            }

            if (criteria.numberFrom != long.MinValue)
            {
                sqliteCmdMessages.Parameters.AddWithValue("@number_from", criteria.numberFrom);
            }

            if (criteria.numberTo > 0 && criteria.numberTo != long.MaxValue)
            {
                sqliteCmdMessages.Parameters.AddWithValue("@number_to", criteria.numberTo);
            }
            return sqliteCmdMessages;
        }

        private string BuildSearchQuery(MessagesSearchCriteria criteria, ICollection<long> personsIds, IEnumerable<long> groupIds)
        {
            var searchRequestBuilder = new StringBuilder("SELECT id, number, date, subject, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time FROM group_message WHERE discussion_group IN (" + String.Join(",", groupIds) + ")");

            if (!string.IsNullOrEmpty(criteria.persons) && personsIds != null && personsIds.Count > 0)
            {
                searchRequestBuilder.Append(" AND person IN (" + String.Join(",", personsIds) + ")");
            }

            if (!string.IsNullOrEmpty(criteria.subject))
            {
                searchRequestBuilder.Append(" AND subject LIKE @subject");
            }

            if (!string.IsNullOrEmpty(criteria.message))
            {
                searchRequestBuilder.Append(" AND content LIKE @content");
            }

            if (criteria.numberFrom != long.MinValue)
            {
                searchRequestBuilder.Append(" AND number >= @number_from");
            }

            if (criteria.numberTo > 0 && criteria.numberTo != long.MaxValue)
            {
                searchRequestBuilder.Append(" AND number <= @number_to");
            }
            return searchRequestBuilder.ToString();
        }

        private List<long> GetSearchPersonsIds(MessagesSearchCriteria criteria, IEnumerable<long> groupIds)
        {
            string[] personsNames = criteria.persons.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var personsIds = new List<long>();

            foreach (long groupId in groupIds)
            {
                foreach (string personName in personsNames)
                {
                    var sqliteCmdPersons = DatabaseManager.Instance.CreateCommand("SELECT id FROM person WHERE discussion_group = @group AND (name LIKE @name OR email LIKE @name)");
                    sqliteCmdPersons.Parameters.AddWithValue("group", groupId);
                    sqliteCmdPersons.Parameters.AddWithValue("name", string.Format("%{0}%", personName));

                    List<object[]> rawPersonsId = DatabaseManager.Instance.ExecuteCommandAndGetValuesList(sqliteCmdPersons);

                    foreach (object[] rawPersonId in rawPersonsId)
                    {
                        personsIds.Add((long)rawPersonId[0]);
                    }
                }
            }

            return personsIds;
        }

        private List<long> GetSearchGroupsIds(MessagesSearchCriteria criteria)
        {
            var groupIds = new List<long>();

            foreach (string groupName in criteria.groupNames)
            {
                var group = DiscussionGroup.GetByName(groupName);

                if (group != null)
                {
                    groupIds.Add(group.Id);
                }
            }

            return groupIds;
        }
    }
}
