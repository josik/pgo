﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace PGOffline
{
    public delegate void ItemsProviderItemAddedEventHandler<T>(T item);

    /// <summary>
    /// Represents a provider of collection details.
    /// </summary>
    /// <typeparam name="T">The type of items in the collection.</typeparam>
    public interface IItemsProvider<T>
    {
        event ItemsProviderItemAddedEventHandler<T> ItemsProviderItemAdded;

        int IndexOf(T item);

        bool Contains(T item);

        /// <summary>
        /// Fetches the total number of items available.
        /// </summary>
        /// <returns></returns>
        int FetchCount();

        /// <summary>
        /// Fetches a range of items.
        /// </summary>
        /// <param name="startIndex">The start index.</param>
        /// <param name="count">The number of items to fetch.</param>
        /// <returns></returns>
        IList<T> FetchRange(int startIndex, int count);
    }
}
