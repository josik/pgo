﻿using System.Windows.Input;

namespace PGOffline.Commands
{
    public class FormFindAllCommands
    {
        public static readonly RoutedUICommand FindAll = new RoutedUICommand("Find All", "FindAll", typeof(FormFindAll));
    }
}
