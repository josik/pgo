﻿using System.Windows.Input;

namespace PGOffline.Commands
{
    public static class FormMainCommands
    {
        public static readonly RoutedUICommand MenuItemRegisterProduct = new RoutedUICommand("Menu Item Register Product", "MenuItemRegisterProduct", typeof(FormMain));
    }
}
