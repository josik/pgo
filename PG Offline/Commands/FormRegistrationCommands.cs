﻿using System.Windows.Input;
using PGOffline.Forms;

namespace PGOffline.Commands
{
    public static class FormRegistrationCommands
    {
        public static readonly RoutedUICommand RegisterProduct = new RoutedUICommand("Register Product", "RegisterProduct", typeof(FormRegistration));

        public static readonly RoutedUICommand PurchaseProduct = new RoutedUICommand("Purchase Product", "PurchaseProduct", typeof(FormRegistration));

        public static readonly RoutedUICommand CloseWindow = new RoutedUICommand("Close Window", "CloseWindow", typeof(FormRegistration));
    }
}
