﻿using PGOffline.Exceptions;
using System;
using System.Data.SQLite;
using System.IO;
using System.Text;

namespace PGOffline
{
    public class SchemaUpdater
    {
        //private const int DATABASE_VERSION = 12;

        private SQLiteConnection _connection;

        private string _databasePath;

        public SchemaUpdater(SQLiteConnection connection, string databasePath)
        {
            _connection = connection;

            _databasePath = databasePath;
        }

        public void TryDatabaseUpgrade()
        {
            try
            {
                int databaseVersion = LoadDatabaseVersion();

                if (ShouldUpgradeDatabase(databaseVersion))
                {
                    UpgradeDatabase(databaseVersion);
                }
            }
            catch (DatabaseWrongFormatException e)
            {
                throw; 
            }
            catch (SchemaMigrationException e)
            {
                throw;
            }
        }

        private int LoadDatabaseVersion()
        {
            try
            {
                int databaseVersion = 0;

                string strVersion = "";
                
                using (SQLiteCommand cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT value FROM options WHERE key = 'database_version'";

                    try
                    {
                        strVersion = (string)cmd.ExecuteScalar();
                    }
                    catch (SQLiteException e)
                    {
                        throw new DatabaseWrongFormatException(_databasePath, e.Message);
                    }
                }

                if (strVersion == "2.0")
                {
                    databaseVersion = 2;
                }
                else
                {
                    if (!int.TryParse(strVersion, out databaseVersion))
                    {
                        throw new DatabaseWrongVersionException(strVersion);
                    }
                }

                return databaseVersion;
            }
            catch (SQLiteException e)
            {
                throw new DatabaseWrongFormatException(_databasePath, e.Message);
            }
        }

        private bool ShouldUpgradeDatabase(int databaseVersion)
        {
            var expectedDbVersion = new UpgradeDatabaseManager().GetExpectedDatabaseVersion();

            return databaseVersion < expectedDbVersion;
        }

        private void UpgradeDatabase(int databaseVersion)
        {
            var isDbCurrent = UpgradeDatabaseManager.Instance.IsDatabaseVersionCurrent();

            if (isDbCurrent) return;
            
            System.Windows.MessageBox.Show("Your database version is obsolete. Please press OK and wait. It will be upgraded.", "Database conversion", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);

            var upgradeDbResult = UpgradeDatabaseManager.Instance.UpgradeDatabase();

            if (!upgradeDbResult)
            {
                System.Windows.MessageBox.Show("Database upgrade failed.", "Database message", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);

                return;
            }

            DatabaseManager.Instance.ResetConnection();
            
            System.Windows.MessageBox.Show("Database upgraded.", "Database message", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
        }

        private void VacuumDatabase()
        {
            try
            {
                using (var cmd = new SQLiteCommand("VACUUM;", _connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {

            }
        }
        
        private void AppendUnreadRecountToQuery(StringBuilder queryBuilder)
        {
            using (SQLiteCommand cmd1 = _connection.CreateCommand())
            {
                cmd1.CommandText = "SELECT id FROM discussion_group";

                using (SQLiteDataReader reader1 = cmd1.ExecuteReader())
                {
                    while (reader1.Read())
                    {
                        var groupId = reader1.GetValue(0);

                        using (SQLiteCommand cmd2 = _connection.CreateCommand())
                        {
                            cmd2.CommandText = "SELECT COUNT(id) FROM group_message WHERE unread = 1 AND discussion_group = " + groupId;

                            var unreadCount = cmd2.ExecuteScalar();

                            queryBuilder.AppendLine(String.Format("UPDATE discussion_group SET unread_count = {0} WHERE id = {1};", unreadCount, groupId));
                        }
                    }
                    reader1.Close();
                }
            }
        }
        
        private string createEmptyDatabase()
        {
            string newDBName = string.Format("{0}_new_sqlite_db" + ".db3", _connection.DataSource);

            string newDBPath = string.Format("{0}\\{1}", Path.GetDirectoryName(_databasePath), newDBName);

            SQLiteConnection.CreateFile(newDBPath);

            using (SQLiteConnection sqliteExportConnection = new SQLiteConnection("Data source=" + newDBPath))
            {
                if (sqliteExportConnection.State != System.Data.ConnectionState.Open)
                {
                    sqliteExportConnection.Open();
                }

                using (StreamReader reader = File.OpenText(string.Format("{0}\\Data\\ygroups_script_production.sql", AppDomain.CurrentDomain.BaseDirectory)))
                {
                    using (SQLiteCommand cmd = sqliteExportConnection.CreateCommand())
                    {
                        cmd.CommandText = reader.ReadToEnd();

                        cmd.ExecuteNonQuery();
                    }
                }

                sqliteExportConnection.Close();
            }

            return newDBPath;
        }

        private bool copyData(string databasePath, string newDatabasePath)
        {
            bool result = true;

            try
            {
                DatabaseManager.Instance.ExecuteQuery(string.Format("ATTACH DATABASE '{0}' AS new_db", newDatabasePath));

                StringBuilder queryBuilder = new StringBuilder();

                queryBuilder.AppendLine("INSERT INTO new_db.attachment SELECT * FROM main.attachment;");
                queryBuilder.AppendLine("INSERT INTO new_db.email SELECT * FROM main.email;");
                queryBuilder.AppendLine("INSERT INTO new_db.email_account SELECT * FROM main.email_account;");
                queryBuilder.AppendLine("INSERT INTO new_db.email_message SELECT * FROM main.email_message;");
                queryBuilder.AppendLine("INSERT INTO new_db.email_message_attachment SELECT * FROM main.email_message_attachment;");
                queryBuilder.AppendLine("INSERT INTO new_db.address_book SELECT * FROM main.address_book;");
                queryBuilder.AppendLine("INSERT INTO new_db.user_account SELECT * FROM main.user_account;");
                queryBuilder.AppendLine("INSERT INTO new_db.discussion_group SELECT * FROM main.discussion_group;");
                queryBuilder.AppendLine("INSERT INTO new_db.person SELECT * FROM main.person;");
                queryBuilder.AppendLine("INSERT INTO new_db.file SELECT * FROM main.file;");
                queryBuilder.AppendLine("INSERT INTO new_db.group_message SELECT id, number, date, subject, content, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time FROM main.group_message;");
                queryBuilder.AppendLine("INSERT INTO new_db.group_message_attachment SELECT * FROM main.group_message_attachment;");
                queryBuilder.AppendLine("INSERT INTO new_db.group_settings SELECT * FROM main.group_settings;");
                queryBuilder.AppendLine("INSERT INTO new_db.message_copy_email SELECT * FROM main.message_copy_email;");
                queryBuilder.AppendLine("INSERT INTO new_db.message_to_email SELECT * FROM main.message_to_email;");
                queryBuilder.AppendLine("INSERT INTO new_db.photo SELECT * FROM main.photo;");

                using (SQLiteTransaction transaction = _connection.BeginTransaction())
                {
                    using (var cmd = new SQLiteCommand(queryBuilder.ToString(), _connection))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }

                DatabaseManager.Instance.ExecuteQuery("DETACH new_db");
            }
            catch (Exception)
            {
                result = false;
            }
            finally
            {
                DatabaseManager.Instance.CloseConnection();
            }

            return result;
        }

        private bool checkDiskSpace(string path)
        {
            bool result = true;

            FileInfo databasePath = new FileInfo(path);
            string bdDriveName = Path.GetPathRoot(databasePath.FullName);

            string system = Environment.GetFolderPath(Environment.SpecialFolder.System);
            string systemDriveName = Path.GetPathRoot(system);

            DriveInfo bdDrive = new DriveInfo(bdDriveName);
            double freeSpace = (bdDrive.AvailableFreeSpace / 1024) / 1024;

            DriveInfo systemDrive = new DriveInfo(systemDriveName);
            double freeSpaceOnSystem = (systemDrive.AvailableFreeSpace / 1024) / 1024;

            long fileSizeBytes = databasePath.Length;
            float fileSizeMbytes = ((fileSizeBytes) / 1024) / 1024;
            string fileSizeString =string.Format("{0:f}", fileSizeMbytes) + " megabytes";

            string message = "";

            if (bdDriveName.Equals(systemDriveName))
            {
                if (freeSpace <= (fileSizeMbytes * 2))
                {
                    result = false;

                    message = " You need approximately " + fileSizeString + " on disk " + bdDriveName;
                }
            }
            else
            {
                if ((freeSpace <= fileSizeMbytes) && (freeSpaceOnSystem <= fileSizeMbytes))
                {
                    result = false;

                    message = " You need approximately " + fileSizeString + " on disk " + bdDriveName + " and " + systemDriveName;
                }
                else if (freeSpace <= fileSizeMbytes)
                {
                    result = false;

                    message = " You need approximately " + fileSizeString + " on disk " + bdDriveName;
                }
                else if (freeSpaceOnSystem <= fileSizeMbytes)
                {
                    result = false;

                    message = "You need approximately " + fileSizeString + " on disk " + systemDriveName;
                }
            }

            if (result == true) return true;

            System.Windows.MessageBox.Show("This is a large update. You don't have enough disk space." + message + ".", "Database conversion", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Exclamation);

            throw new SchemaMigrationException(path, message);
        }



        #region Old DB Upgrade Code


        //private void UpgradeDatabase(int databaseVersion)
        //{
        //    System.Windows.MessageBox.Show("Your database version is obsolete. Please press OK and wait. It will be upgraded.", "Database conversion", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);

        //    // 2 -> 3
        //    if (databaseVersion == 2)
        //    {
        //        upgradeFrom2To3();

        //        databaseVersion = 3;
        //    }

        //    // 3 -> 4
        //    if (databaseVersion == 3)
        //    {
        //        upgradeFrom3To4();

        //        databaseVersion = 4;
        //    }

        //    // 4 -> 5
        //    if (databaseVersion == 4)
        //    {
        //        upgradeFrom4To5();

        //        databaseVersion = 5;
        //    }

        //    // 5 -> 6
        //    if (databaseVersion == 5)
        //    {
        //        upgradeFrom5To6();

        //        databaseVersion = 6;
        //    }

        //    // 6 -> 7
        //    if (databaseVersion == 6)
        //    {
        //        upgradeFrom6To7();

        //        VacuumDatabase();

        //        databaseVersion = 7;
        //    }

        //    // 7 -> 8
        //    if (databaseVersion == 7)
        //    {
        //        upgradeFrom7To8();

        //        databaseVersion = 8;
        //    }

        //    // 8 -> 9
        //    if (databaseVersion == 8)
        //    {
        //        upgradeFrom8To9();

        //        databaseVersion = 9;
        //    }

        //    // 9 -> 10
        //    if (databaseVersion == 9)
        //    {
        //        upgradeFrom9To10();

        //        databaseVersion = 10;
        //    }

        //    // 10 -> 11
        //    if (databaseVersion == 10)
        //    {
        //        upgradeFrom10to11();

        //        databaseVersion = 11;
        //    }

        //    // 11 -> 12
        //    if (databaseVersion == 11)
        //    {
        //        upgradeFrom11to12();

        //        databaseVersion = 12;
        //    }

        //    DatabaseManager.Instance.ResetConnection();

        //    System.Windows.MessageBox.Show("Database upgraded.", "Database message", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);              
        //}

        private void upgradeFrom2To3()
        {
            var queryBuilder = new StringBuilder();

            queryBuilder.Append("PRAGMA auto_vacuum=FULL;");

            queryBuilder.Append("ALTER TABLE discussion_group RENAME TO discussion_group_old;");

            queryBuilder.Append("CREATE TABLE discussion_group (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, charset TEXT NOT NULL DEFAULT 'UTF-8', actual_email INTEGER NOT NULL,");
            queryBuilder.Append("group_settings INTEGER NOT NULL,user_account INTEGER NOT NULL,username TEXT,password TEXT,last_members_update DATETIME,");
            queryBuilder.Append("FOREIGN KEY (actual_email) REFERENCES email_account(id), FOREIGN KEY (group_settings) REFERENCES group_settings(id)");
            queryBuilder.Append("FOREIGN KEY (user_account) REFERENCES user_account(id));");

            queryBuilder.Append("INSERT INTO discussion_group (id, name, actual_email, group_settings, user_account, username, password, last_members_update) SELECT id, name, actual_email, group_settings, user_account, username, password, last_members_update FROM discussion_group_old;");

            queryBuilder.Append("DROP TABLE discussion_group_old;");

            queryBuilder.Append("CREATE INDEX IF NOT EXISTS discussion_group_name ON discussion_group (name);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS discussion_group_charset ON discussion_group (charset);");

            queryBuilder.Append("ALTER TABLE group_message RENAME TO group_message_old;");

            queryBuilder.Append("CREATE TABLE group_message (id INTEGER PRIMARY KEY AUTOINCREMENT,number INTEGER NOT NULL,date DATETIME,subject TEXT NOT NULL DEFAULT '',content BLOB,");
            queryBuilder.Append("favorite BOOLEAN NOT NULL,unread BOOLEAN NOT NULL,deleted BOOLEAN NOT NULL,person INTEGER NOT NULL,charset TEXT,discussion_group INTEGER NOT NULL,");
            queryBuilder.Append("FOREIGN KEY (discussion_group) REFERENCES discussion_group(id),FOREIGN KEY (person) REFERENCES person(id),");
            queryBuilder.Append("UNIQUE (number, discussion_group) ON CONFLICT REPLACE);");

            queryBuilder.Append("INSERT INTO group_message (id, number, date, subject, content, favorite, unread, deleted, person, discussion_group) SELECT id, number, date, subject, content, favorite, unread, deleted, person, discussion_group FROM group_message_old;");

            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_id ON group_message (id);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_number ON group_message (number);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_number_and_discussion_group ON group_message (discussion_group, number);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_discussion_group ON group_message (discussion_group);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_unread ON group_message (unread);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_person ON group_message (person);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_charset ON group_message (charset);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_unread_group ON group_message (unread, discussion_group);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_charset_group ON group_message (discussion_group, charset);");

            queryBuilder.Append("DROP TABLE group_message_old;");

            queryBuilder.Append("DROP TABLE IF EXISTS charset;");
            queryBuilder.Append("DROP INDEX IF EXISTS charset_name;");

            queryBuilder.Append("UPDATE options SET value = '3' WHERE key = 'database_version';");

            queryBuilder.Append("INSERT OR REPLACE INTO options (key, value) VALUES ('fizzler_photo_attachment_node', 'div.ygrp-photo');");

            using (SQLiteTransaction transaction = _connection.BeginTransaction())
            {
                using (var cmd = new SQLiteCommand(queryBuilder.ToString(), _connection))
                {
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }

        private void upgradeFrom3To4()
        {
            StringBuilder queryBuilder = new StringBuilder();

            queryBuilder.Append("ALTER TABLE discussion_group RENAME TO discussion_group_old;");

            queryBuilder.Append("CREATE TABLE discussion_group (");
            queryBuilder.Append("id INTEGER PRIMARY KEY AUTOINCREMENT,");
            queryBuilder.Append("name TEXT NOT NULL,");
            queryBuilder.Append("charset TEXT NOT NULL DEFAULT 'UTF-8',");
            queryBuilder.Append("actual_email INTEGER,");
            queryBuilder.Append("user_account INTEGER,");
            queryBuilder.Append("username TEXT,");
            queryBuilder.Append("password TEXT,");
            queryBuilder.Append("last_members_update DATETIME,");
            queryBuilder.Append("FOREIGN KEY (actual_email) REFERENCES email_account(id),");
            queryBuilder.Append("FOREIGN KEY (user_account) REFERENCES user_account(id));");

            queryBuilder.Append("INSERT INTO discussion_group (id, name, actual_email, user_account, username, password, last_members_update) SELECT id, name, actual_email, user_account, username, password, last_members_update FROM discussion_group_old;");

            queryBuilder.Append("DROP TABLE discussion_group_old;");

            queryBuilder.Append("CREATE INDEX IF NOT EXISTS discussion_group_name ON discussion_group (name);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS discussion_group_charset ON discussion_group (charset);");

            queryBuilder.Append("UPDATE options SET value = '4' WHERE key = 'database_version';");

            using (SQLiteTransaction transaction = _connection.BeginTransaction())
            {
                using (var cmd = new SQLiteCommand(queryBuilder.ToString(), _connection))
                {
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }

        private void upgradeFrom4To5()
        {
            StringBuilder queryBuilder = new StringBuilder();

            queryBuilder.Append("UPDATE options SET value = 'Tahoma' WHERE key = 'miscellaneous_options_main_window_panes_font_name';");

            queryBuilder.Append("UPDATE options SET value = '5' WHERE key = 'database_version';");

            using (SQLiteTransaction transaction = _connection.BeginTransaction())
            {
                using (var cmd = new SQLiteCommand(queryBuilder.ToString(), _connection))
                {
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }

        private void upgradeFrom5To6()
        {
            StringBuilder queryBuilder = new StringBuilder();

            queryBuilder.Append("INSERT INTO options (key, value) VALUES ('miscellaneous_options_minimize_to_system_tray', 'false');");

            queryBuilder.Append("UPDATE options SET value = '6' WHERE key = 'database_version';");

            using (SQLiteTransaction transaction = _connection.BeginTransaction())
            {
                using (var cmd = new SQLiteCommand(queryBuilder.ToString(), _connection))
                {
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }

        private void upgradeFrom6To7()
        {
            long fileSizeBytes = new FileInfo(_databasePath).Length;
            float fileSizeMbytes = ((fileSizeBytes * 3) / 1024) / 1024;
            string fileSizeString = string.Format("{0:f}", fileSizeMbytes) + " megabytes.";

            System.Windows.MessageBox.Show("This is huge update. Please make sure that you have enough disk space. Approximately " + fileSizeString, "Database conversion", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Exclamation);

            StringBuilder queryBuilder = new StringBuilder();

            // =========

            queryBuilder.Append("ALTER TABLE group_settings RENAME TO group_settings_old;");

            queryBuilder.Append("CREATE TABLE group_settings (");
            queryBuilder.Append("key TEXT,");
            queryBuilder.Append("value TEXT,");
            queryBuilder.Append("discussion_group INTEGER,");
            queryBuilder.Append("PRIMARY KEY (key, discussion_group),");
            queryBuilder.Append("FOREIGN KEY (discussion_group) REFERENCES discussion_group(id) ON DELETE CASCADE);");

            queryBuilder.Append("INSERT INTO group_settings SELECT * FROM group_settings_old;");

            queryBuilder.Append("DROP TABLE group_settings_old;");

            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_settings_key ON group_settings (key);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_settings_value ON group_settings (value);");

            // =========

            queryBuilder.Append("ALTER TABLE person RENAME TO person_old;");

            queryBuilder.Append("CREATE TABLE person (");
            queryBuilder.Append("id INTEGER PRIMARY KEY AUTOINCREMENT,");
            queryBuilder.Append("name TEXT NOT NULL,");
            queryBuilder.Append("joined DATETIME,");
            queryBuilder.Append("email TEXT,");
            queryBuilder.Append("is_member BOOLEAN,");
            queryBuilder.Append("discussion_group INTEGER NOT NULL,");
            queryBuilder.Append("FOREIGN KEY (discussion_group) REFERENCES discussion_group(id) ON DELETE CASCADE);");

            queryBuilder.Append("INSERT INTO person SELECT * FROM person_old;");

            queryBuilder.Append("DROP TABLE person_old;");

            queryBuilder.Append("CREATE INDEX IF NOT EXISTS person_email ON person (email);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS person_name ON person (name);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS person_id ON person (id);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS person_discussion_group ON person (discussion_group);");

            // =========

            queryBuilder.Append("ALTER TABLE photo RENAME TO photo_old;");

            queryBuilder.Append("CREATE TABLE photo (");
            queryBuilder.Append("id INTEGER PRIMARY KEY AUTOINCREMENT,");
            queryBuilder.Append("content BLOB,");
            queryBuilder.Append("discussion_group INTEGER NOT NULL,");
            queryBuilder.Append("FOREIGN KEY (discussion_group) REFERENCES discussion_group(id) ON DELETE CASCADE);");

            queryBuilder.Append("INSERT INTO photo SELECT * FROM photo_old;");

            queryBuilder.Append("DROP TABLE photo_old;");

            // =========

            queryBuilder.Append("ALTER TABLE file RENAME TO file_old;");

            queryBuilder.Append("CREATE TABLE file");
            queryBuilder.Append("(id INTEGER PRIMARY KEY AUTOINCREMENT,");
            queryBuilder.Append("content BLOB,");
            queryBuilder.Append("discussion_group INTEGER NOT NULL,");
            queryBuilder.Append("FOREIGN KEY (discussion_group) REFERENCES discussion_group(id) ON DELETE CASCADE);");

            queryBuilder.Append("INSERT INTO file SELECT * FROM file_old;");

            queryBuilder.Append("DROP TABLE file_old;");

            // =========

            queryBuilder.Append("ALTER TABLE group_message RENAME TO group_message_old;");

            queryBuilder.Append("CREATE TABLE group_message (");
            queryBuilder.Append("id INTEGER PRIMARY KEY AUTOINCREMENT,");
            queryBuilder.Append("number INTEGER NOT NULL,");
            queryBuilder.Append("date DATETIME,");
            queryBuilder.Append("subject TEXT NOT NULL DEFAULT '',");
            queryBuilder.Append("content BLOB,");
            queryBuilder.Append("favorite BOOLEAN NOT NULL,");
            queryBuilder.Append("unread BOOLEAN NOT NULL,");
            queryBuilder.Append("deleted BOOLEAN NOT NULL,");
            queryBuilder.Append("person INTEGER NOT NULL,");
            queryBuilder.Append("charset TEXT,");
            queryBuilder.Append("discussion_group INTEGER NOT NULL,");
            queryBuilder.Append("FOREIGN KEY (discussion_group) REFERENCES discussion_group(id) ON DELETE CASCADE,");
            queryBuilder.Append("FOREIGN KEY (person) REFERENCES person(id),");
            queryBuilder.Append("UNIQUE (number, discussion_group) ON CONFLICT REPLACE);");

            queryBuilder.Append("INSERT INTO group_message SELECT * FROM group_message_old;");

            queryBuilder.Append("DROP TABLE group_message_old;");

            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_id ON group_message (id);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_number ON group_message (number);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_number_and_discussion_group ON group_message (discussion_group, number);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_discussion_group ON group_message (discussion_group);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_unread ON group_message (unread);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_person ON group_message (person);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_charset ON group_message (charset);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_unread_group ON group_message (unread, discussion_group);");
            queryBuilder.Append("CREATE INDEX IF NOT EXISTS group_message_charset_group ON group_message (discussion_group, charset);");

            // =========

            queryBuilder.Append("ALTER TABLE group_message_attachment RENAME TO group_message_attachment_old;");

            queryBuilder.Append("CREATE TABLE group_message_attachment (");
            queryBuilder.Append("group_message INTEGER,");
            queryBuilder.Append("attachment INTEGER,");
            queryBuilder.Append("PRIMARY KEY(group_message, attachment),");
            queryBuilder.Append("FOREIGN KEY (group_message) REFERENCES group_message(id) ON DELETE CASCADE,");
            queryBuilder.Append("FOREIGN KEY (attachment) REFERENCES attachment(id));");

            queryBuilder.Append("INSERT INTO group_message_attachment SELECT * FROM group_message_attachment_old;");

            queryBuilder.Append("DROP TABLE group_message_attachment_old;");

            // =========

            queryBuilder.Append("ALTER TABLE group_message_search_result RENAME TO group_message_search_result_old;");

            queryBuilder.Append("CREATE TABLE group_message_search_result (");
            queryBuilder.Append("group_message INTEGER,");
            queryBuilder.Append("search_result INTEGER,");
            queryBuilder.Append("PRIMARY KEY (group_message, search_result),");
            queryBuilder.Append("FOREIGN KEY (group_message) REFERENCES group_message(id) ON DELETE CASCADE,");
            queryBuilder.Append("FOREIGN KEY (search_result) REFERENCES search_result(id) ON DELETE CASCADE);");

            queryBuilder.Append("INSERT INTO group_message_search_result SELECT * FROM group_message_search_result_old;");

            queryBuilder.Append("DROP TABLE group_message_search_result_old;");

            // =========

            queryBuilder.Append("ALTER TABLE search_params_options RENAME TO search_params_options_old;");

            queryBuilder.Append("CREATE TABLE search_params_options (");
            queryBuilder.Append("search_params INTEGER,");
            queryBuilder.Append("key TEXT,");
            queryBuilder.Append("value TEXT,");
            queryBuilder.Append("PRIMARY KEY (search_params, key),");
            queryBuilder.Append("FOREIGN KEY (search_params) REFERENCES search_params(id) ON DELETE CASCADE);");

            queryBuilder.Append("INSERT INTO search_params_options SELECT * FROM search_params_options_old;");

            queryBuilder.Append("DROP TABLE search_params_options_old;");

            // =========

            queryBuilder.Append("ALTER TABLE discussion_group_search_params RENAME TO discussion_group_search_params_old;");

            queryBuilder.Append("CREATE TABLE discussion_group_search_params (");
            queryBuilder.Append("discussion_group INTEGER,");
            queryBuilder.Append("search_params INTEGER,");
            queryBuilder.Append("PRIMARY KEY (discussion_group, search_params),");
            queryBuilder.Append("FOREIGN KEY (discussion_group) REFERENCES discussion_group(id) ON DELETE CASCADE,");
            queryBuilder.Append("FOREIGN KEY (search_params) REFERENCES search_params(id) ON DELETE CASCADE);");

            queryBuilder.Append("INSERT INTO discussion_group_search_params SELECT * FROM discussion_group_search_params_old;");

            queryBuilder.Append("DROP TABLE discussion_group_search_params_old;");

            // =========

            queryBuilder.Append("UPDATE options SET value = '7' WHERE key = 'database_version';");

            using (SQLiteTransaction transaction = _connection.BeginTransaction())
            {
                using (var cmd = new SQLiteCommand(queryBuilder.ToString(), _connection))
                {
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }

        private void upgradeFrom7To8()
        {
            if (!File.Exists(PathResolver.GetOptionsPath()))
            {
                using (SQLiteCommand cmd1 = new SQLiteCommand("ATTACH @databasePath AS optionsDatabase", _connection))
                {
                    cmd1.Parameters.AddWithValue("@databasePath", PathResolver.GetOptionsPath());
                    cmd1.ExecuteNonQuery();
                }

                using (SQLiteTransaction transaction = _connection.BeginTransaction())
                {
                    using (SQLiteCommand cmd2 = new SQLiteCommand("INSERT OR REPLACE INTO optionsDatabase.options SELECT * FROM main.options WHERE key <> 'database_version'", _connection))
                    {
                        cmd2.ExecuteNonQuery();
                    }

                    using (SQLiteCommand cmd3 = new SQLiteCommand("DELETE FROM main.options WHERE key <> 'database_version'", _connection))
                    {
                        cmd3.ExecuteNonQuery();
                    }

                    using (SQLiteCommand cmd5 = new SQLiteCommand("UPDATE main.options SET value = '8' WHERE key = 'database_version';", _connection))
                    {
                        cmd5.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }

                using (SQLiteCommand cmd4 = new SQLiteCommand("DETACH optionsDatabase", _connection))
                {
                    cmd4.ExecuteNonQuery();
                }
            }
            else
            {
                using (SQLiteTransaction transaction = _connection.BeginTransaction())
                {
                    using (SQLiteCommand cmd3 = new SQLiteCommand("DELETE FROM main.options WHERE key <> 'database_version'", _connection))
                    {
                        cmd3.ExecuteNonQuery();
                    }

                    using (SQLiteCommand cmd5 = new SQLiteCommand("UPDATE main.options SET value = '8' WHERE key = 'database_version';", _connection))
                    {
                        cmd5.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
            }
        }

        private void upgradeFrom8To9()
        {
            StringBuilder queryBuilder = new StringBuilder();

            queryBuilder.AppendLine("ALTER TABLE discussion_group ADD COLUMN unread_count INTEGER NOT NULL DEFAULT 0;");

            AppendUnreadRecountToQuery(queryBuilder);

            // INSERT INTO group_message (number, discussion_group, favorite, unread, deleted, person) VALUES (96, 1, 0, 1, 0, 1)

            queryBuilder.AppendLine("CREATE TRIGGER after_unread_group_message_insert AFTER INSERT ON group_message WHEN new.unread = 1 AND new.deleted = 0 BEGIN");
            queryBuilder.AppendLine("UPDATE discussion_group SET unread_count=unread_count+1 WHERE discussion_group.id=new.discussion_group;");
            queryBuilder.AppendLine("END;");

            queryBuilder.AppendLine("CREATE TRIGGER after_unread_group_message_delete AFTER DELETE ON group_message WHEN old.unread = 1 AND old.deleted = 0 BEGIN");
            queryBuilder.AppendLine("UPDATE discussion_group SET unread_count=unread_count-1 WHERE discussion_group.id=old.discussion_group;");
            queryBuilder.AppendLine("END;");

            queryBuilder.AppendLine("CREATE TRIGGER after_group_message_update_set_deleted AFTER UPDATE OF deleted ON group_message WHEN new.deleted = 1 AND old.deleted = 0 AND new.unread = 1 BEGIN");
            queryBuilder.AppendLine("UPDATE discussion_group SET unread_count=unread_count-1 WHERE discussion_group.id=old.discussion_group;");
            queryBuilder.AppendLine("END;");

            queryBuilder.AppendLine("CREATE TRIGGER after_group_message_update_set_undeleted AFTER UPDATE OF deleted ON group_message WHEN new.deleted = 0 AND old.deleted = 1 AND new.unread = 1 BEGIN");
            queryBuilder.AppendLine("UPDATE discussion_group SET unread_count=unread_count+1 WHERE discussion_group.id=old.discussion_group;");
            queryBuilder.AppendLine("END;");

            queryBuilder.AppendLine("CREATE TRIGGER after_group_message_update_set_unread AFTER UPDATE OF unread ON group_message WHEN new.unread = 1 AND old.unread = 0 AND new.deleted = 0 BEGIN");
            queryBuilder.AppendLine("UPDATE discussion_group SET unread_count=unread_count+1 WHERE discussion_group.id=new.discussion_group;");
            queryBuilder.AppendLine("END;");

            queryBuilder.AppendLine("CREATE TRIGGER after_group_message_update_set_read AFTER UPDATE OF unread ON group_message WHEN new.unread = 0 AND old.unread = 1 AND new.deleted = 0 BEGIN");
            queryBuilder.AppendLine("UPDATE discussion_group SET unread_count=unread_count-1 WHERE discussion_group.id=new.discussion_group;");
            queryBuilder.AppendLine("END;");

            queryBuilder.AppendLine("UPDATE options SET value = '9' WHERE key = 'database_version';");

            using (SQLiteTransaction transaction = _connection.BeginTransaction())
            {
                using (var cmd = new SQLiteCommand(queryBuilder.ToString(), _connection))
                {
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }
        
        private void upgradeFrom9To10()
        {
            bool result = checkDiskSpace(_databasePath);

            if (result)
            {
                string newDatabasePath = createEmptyDatabase();

                bool isCopy = copyData(_databasePath, newDatabasePath);

                if (isCopy)
                {
                    File.Delete(_databasePath);

                    File.Move(newDatabasePath, _databasePath);

                    _connection = new SQLiteConnection(@"Data source=" + _databasePath);
                    _connection.Open();
                }
            }
        }

        private void upgradeFrom10to11()
        {
            StringBuilder queryBuilder = new StringBuilder();

            queryBuilder.Append("CREATE TABLE search_param (");
            queryBuilder.Append("version INTEGER,");
            queryBuilder.Append("name TEXT,");
            queryBuilder.Append("value TEXT,");
            queryBuilder.Append("UNIQUE (name) ON CONFLICT REPLACE);");

            queryBuilder.AppendLine("UPDATE options SET value = '11' WHERE key = 'database_version';");

            using (SQLiteTransaction transaction = _connection.BeginTransaction())
            {
                using (var cmd = new SQLiteCommand(queryBuilder.ToString(), _connection))
                {
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }

        // There is no actual upgrade. Required to restore unread messages counter.
        private void upgradeFrom11to12()
        {
            StringBuilder queryBuilder = new StringBuilder();

            AppendUnreadRecountToQuery(queryBuilder);

            queryBuilder.AppendLine("UPDATE options SET value = '12' WHERE key = 'database_version';");

            using (SQLiteTransaction transaction = _connection.BeginTransaction())
            {
                using (var cmd = new SQLiteCommand(queryBuilder.ToString(), _connection))
                {
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }

        #endregion Old DB Upgrade Code
    }
}
