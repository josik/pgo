﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.View
{
    public delegate void ApplyDisplayOptionsRequestEventHandler();
    public delegate void ToggleQuotesRequestEventHandler();
    public delegate void PrintDigestRequestEventHandler();
    public delegate void ShowNextMessagesRequestEventHandler();
    public delegate void ShowPrevMessagesRequestEventHandler();
    public delegate void ForwardRequestEventHandler();
    public delegate void ChangeMarkAsReadStatusEventHandler(bool? isChecked);

    interface IFormDigestView
    {
        event ApplyDisplayOptionsRequestEventHandler ApplyDisplayOptionsRequest;
        event ToggleQuotesRequestEventHandler ToggleQuotesRequest;
        event PrintDigestRequestEventHandler PrintDigestRequest;
        event ShowNextMessagesRequestEventHandler ShowNextMessagesRequest;
        event ShowPrevMessagesRequestEventHandler ShowPrevMessagesRequest;
        event ForwardRequestEventHandler ForwardRequest;
        event ChangeMarkAsReadStatusEventHandler ChangeMarkAsReadStatus;

        void SetGroupName(string groupName);

        int MessagesPerPage { get; }
        bool? MarkMessagesAsRead { get; }

        void DisplayDigest(string digest);
    }
}
