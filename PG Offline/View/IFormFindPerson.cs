﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.Model;

namespace PGOffline.View
{
    public delegate void FindingUsersCompleteEventHandler(List<Person> selectedPersons);
    public delegate void FormFindPersonLoadCompleteEventHandler();

    public interface IFormFindPerson
    {
        event FindingUsersCompleteEventHandler FindingUsersComplete;
        event FormFindPersonLoadCompleteEventHandler FormFindPersonLoadComplete;

        void SetPesons(List<Person> persons);
    }
}
