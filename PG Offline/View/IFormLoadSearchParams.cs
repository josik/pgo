﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.View
{
    public delegate void LoadSearhParamHandler(string name);

    public delegate void RemoveSearchParamHandler(string name);

    interface IFormLoadSearchParams : IBaseFormView
    {
        event LoadSearhParamHandler LoadSearchParam;

        event RemoveSearchParamHandler RemoveSearchParam;

        IEnumerable<string> SearchParamsNames { set; }
    }
}
