﻿using PGOffline.Model;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls.Primitives;

namespace PGOffline.View
{
    #region Menu delegates
    public delegate void OpenDatabaseRequestEventHandler(string path);
    public delegate void ShowMailClientRequestEventHandler();
    public delegate void ShowAddressBookRequestEventHandler();
    public delegate void ShowStatisticsBuilderRequestEventHandler();
    public delegate void ShowStatsRequestEventHandler(DiscussionGroup discussionGroup);
    public delegate void ShowMasterLoginSettingsRequestEventHandler();
    public delegate void ShowDigestRequestEventHandler();
    public delegate void ShowSelectionAsDigestRequestEventHandler(IList messages);
    public delegate void OpenMessageInYahooRequestEventHandler(Message message);
    public delegate void FullScreenViewRequestEventHandler();
    public delegate void ImportFromFolderRequestEventHandler();
    public delegate void ExportToFolderRequestEventHandler();
    public delegate void OpenDataFolderRequestEventHandler();
    public delegate void ShowOptionsRequestEventHandler();
    public delegate void ShowGroupSettingsRequestEventHander();
    public delegate void NewGroupRequestEventHandler();
    public delegate void ShowDownloadGroupFilesRequestEventHandler();
    public delegate void ShowDownloadGroupPhotosRequestEventHandler();
    public delegate void DeleteDiscussionGroupRequestEventHandler(DiscussionGroup group);
    public delegate void ReplyMessageRequestEventHandler(Message message);
    public delegate void HideQuoteStringRequestEventHandler(bool hide);
    public delegate void RecentDatabasesRequestEventHandler();
    #endregion

    public delegate void WindowActivatedEventHandler();
    public delegate void ExitRequestEventHandler();
    public delegate void LoadGroupsListRequestEventHandler();
    public delegate void LoadMessagesRequestEventHandler(DiscussionGroup discussionGroup);
    public delegate void RefreshAllGroupsRequestEventHander();
    public delegate void LoadMembersRequestEventHandler(DiscussionGroup discussionGroup);
    public delegate void LoadMembersForAllGroupsRequestEventHandler();
    public delegate void DisplayGroupsRequestEventHandler();
    public delegate void DiscussionGroupSelectedEventHandler(DiscussionGroup discussionGroup);
    public delegate void CancelProcedureRequestEventHandler(DiscussionGroup discussionGroup);
    public delegate void SearchRequestEventHandler();
    
    public delegate void MarkAllGroupsAsReadRequestEventHandler();

    public delegate void ShowAllMessagesRequestEventHandler();
    public delegate void ShowOnlyNewMessagesRequestEventHandler();
    public delegate void ShowOnlyFavoritesMessagesRequestEventHandler();

    public delegate void ToolbarCaptionsVisibilityChangedHandler(bool visible);
    public delegate void ToolbarButtonsSizeChangedHandler(bool small);

    public delegate void SetEncodingHandler(Encoding encoding);

    public interface IFormMainView
    {
        #region Menu events
        event OpenDatabaseRequestEventHandler OpenDatabaseRequest;
        event ShowAddressBookRequestEventHandler ShowAddressBookRequest;
        event ShowMailClientRequestEventHandler ShowMailClientRequest;
        event ShowStatisticsBuilderRequestEventHandler ShowStatisticsBuilderRequest;
        event ShowStatsRequestEventHandler ShowStatsRequest;
        event ShowMasterLoginSettingsRequestEventHandler ShowMasterLoginSettingsRequest;
        event ShowDigestRequestEventHandler ShowDigestRequest;
        event ShowSelectionAsDigestRequestEventHandler ShowSelectionAsDigestRequest;
        event OpenMessageInYahooRequestEventHandler OpenMessageInYahooRequest;
        event FullScreenViewRequestEventHandler FullScreenViewRequest;
        event ImportFromFolderRequestEventHandler ImportFromFolderRequest;
        event ExportToFolderRequestEventHandler ExportToFolderRequest;
        event OpenDataFolderRequestEventHandler OpenDataFolderRequest;
        event ShowOptionsRequestEventHandler ShowOptionsRequest;
        event ShowGroupSettingsRequestEventHander ShowGroupSettingsRequest;
        event NewGroupRequestEventHandler NewGroupRequest;
        event ShowDownloadGroupFilesRequestEventHandler ShowDownloadGroupFilesRequest;
        event ShowDownloadGroupPhotosRequestEventHandler ShowDownloadGroupPhotosRequest;
        event DeleteDiscussionGroupRequestEventHandler DeleteDiscussionGroupRequest;
        event ReplyMessageRequestEventHandler ReplyMessageRequest;
        event HideQuoteStringRequestEventHandler HideQuoteStringRequest;
        event RecentDatabasesRequestEventHandler RecentDatabasesRequest;
        #endregion

        event WindowActivatedEventHandler WindowActivated;
        event ExitRequestEventHandler ExitRequest;
        event LoadGroupsListRequestEventHandler LoadGroupsListRequest;
        event LoadMessagesRequestEventHandler LoadMessagesRequest;
        event RefreshAllGroupsRequestEventHander RefreshAllGroupsRequest;
        event LoadMembersRequestEventHandler LoadMembersRequest;
        event LoadMembersForAllGroupsRequestEventHandler LoadMembersForAllGroupsRequest;
        event DisplayGroupsRequestEventHandler DisplayGroupsRequest;
        event DiscussionGroupSelectedEventHandler DiscussionGroupSelected;
        event CancelProcedureRequestEventHandler CancelProcedureRequest;
        event SearchRequestEventHandler SearchRequest;

        event MarkAllGroupsAsReadRequestEventHandler MarkAllGroupsAsReadRequest;

        event ShowAllMessagesRequestEventHandler ShowAllMessagesRequest;
        event ShowOnlyNewMessagesRequestEventHandler ShowOnlyNewMessagesRequest;
        event ShowOnlyFavoritesMessagesRequestEventHandler ShowOnlyFavoritesMessagesRequest;

        event ToolbarCaptionsVisibilityChangedHandler ToolbarCaptionsVisibilityChanged;
        event ToolbarButtonsSizeChangedHandler ToolbarButtonsSizeChanged;

        event SetEncodingHandler SetEncoding;

        string WindowTitle { get; set; }
        string DatabaseTitle { get; set; }

        System.Windows.Controls.TextBox LogArea { get; }

        StatusBar StatusBar1 { get; }

        void SetMenuItemGroupsListIsEnabled(bool enabled);

        void SetMenuItemStopIsEnabled(bool enabled);

        void AddMessage(Message message);
        void DisplayMessage(Message message);
        void SetDiscussionGroupsList(List<DiscussionGroup> discussionGroups);
        void UpdateDiscussionGroupsList();
        void UpdateMessagesList();
        void DisplayStats(DiscussionGroup group);
        void SetFont(string fontName);
        void TrayMinimize(bool status);

        void EnableMenuItemFullscreen(bool value);

        void EnableControls();
        void DisableControls();

        void SelectNextMessage();
        void SelectPreviousMessage();
        void SelectNextDiscussionGroup();
        void SelectPreviousDiscussionGroup();

        void SetToolbarCaptionsVisibility(bool visible);
        void SetToolbarSmallButtons(bool small);
        void SetQuoteFiltering(bool value);

        void SetTitle(string title);
        void UpdateWindowTitle();
        void SetDatabaseTitle(string title);

        int DisplayedMessagesCount();

        IMessagesListView MessagesListView { get; }
    }
}
