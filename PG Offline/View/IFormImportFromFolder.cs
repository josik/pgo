﻿using System.Collections.Generic;

namespace PGOffline.View
{
    public delegate void BrowseDatabaseRequestEventHandler();
    public delegate void GetGroupsListRequestEventHandler(string mdbLocation);
    public delegate void ImportDataRequestEventHandler(List<ImportUnit> groups, string mdbLocation, ImportSettings settings);
    public delegate void ChangeRangeRequestEventHandler(ImportUnit importUnit);
    public delegate void ImportCancelRequestEventHander();
    public delegate void SaveImportFromMdbOptionsRequestEventHandler();
    public delegate void LoadImportFromMdbOptionsRequestEventHandler();

    public interface IFormImportFromFolder
    {
        event BrowseDatabaseRequestEventHandler BrowseDatabaseRequest;
        event GetGroupsListRequestEventHandler GetGroupsListRequest;
        event ImportDataRequestEventHandler ImportDataRequest;
        event ChangeRangeRequestEventHandler ChangeRangeRequest;
        event ImportCancelRequestEventHander ImportCancelRequest;
        event SaveImportFromMdbOptionsRequestEventHandler SaveImportFromMdbOptionsRequest;
        event LoadImportFromMdbOptionsRequestEventHandler LoadImportFromMdbOptionsRequest;

        bool CopyAttachments { get; set; }
        bool RememberLastMessageNumber { get; set; }
        bool ImportFavoriteStatus { get; set; }
        ImportType ImportType { get; set; }
        string DatabasePath { get; set; }

        void UpdateProgress(double progressValue);
        void DisplayGroups(IEnumerable<ImportUnit> groups);
        void SetDatabasePath(string path);
        void RefreshImportUnits();
        void DisableImportButton();
        void EnableImportButton();
        void DisableProgressBar();
        void EnableProgressBar();
    }
}
