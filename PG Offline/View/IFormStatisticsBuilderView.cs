﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.Model;

namespace PGOffline.View
{
    public delegate void BuildStatisticsRequestHandler(List<DiscussionGroup> groups, StatisticsOptions options);
    public delegate void CancelStatisticsCalculationRequestEventHandler();
    public delegate void BaseStatisticsOnDateSelectedEventHandler();
    public delegate void BaseStatisticsOnNumberSelectedEventHandler();
    public delegate void CompareByWeekSelectedEventHandler();
    public delegate void CompareByMonthSelectedEventHandler();
    public delegate void CompareByYearSelectedEventHandler();
    public delegate void CompareByCustomDateSelectedEventHandler();
    public delegate void CompareByMessageNumberSelectedEventHandler();
    public delegate void GroupVsGroupSelectedEventHandler();
    public delegate void DisplayGroupsOnStatsRequestEventHandler();
    public delegate void SaveStatisticsOptionsRequestEventHandler(StatisticsOptions options);
    public delegate void SetStatisticsOptionsRequestEventHandler();

    public interface IFormStatisticsBuilderView
    {
        event BuildStatisticsRequestHandler BuildStatisticsRequest;
        event CancelStatisticsCalculationRequestEventHandler CancelStatisticsCalculationRequest;
        event BaseStatisticsOnDateSelectedEventHandler BaseStatisticsOnDateSelected;
        event BaseStatisticsOnNumberSelectedEventHandler BaseStatisticsOnNumberSelected;
        event CompareByWeekSelectedEventHandler CompareByWeekSelected;
        event CompareByMonthSelectedEventHandler CompareByMonthSelected;
        event CompareByYearSelectedEventHandler CompareByYearSelected;
        event CompareByCustomDateSelectedEventHandler CompareByCustomDateSelected;
        event CompareByMessageNumberSelectedEventHandler CompareByMessageNumberSelected;
        event GroupVsGroupSelectedEventHandler GroupVsGroupSelected;
        event DisplayGroupsOnStatsRequestEventHandler DisplayGroupsRequest;
        event SaveStatisticsOptionsRequestEventHandler SaveStatisticsOptionsRequest;
        event SetStatisticsOptionsRequestEventHandler SetStatisticsOptionsRequest;

        void SelectDateInputForms();
        void SelectMessageNumberInputForms();
        void SelectComparingDateInputForms();
        void SelectComparingMessageNumbersInputForms();
        void SelectGroupsComboboxes();
        void DisableAllInputForms();
        void ShowReport(string report);
        void DisplayGroups(List<DiscussionGroup> groups);

        StatisticsOptions StatisticsOptions { get; set; }
    }
}
