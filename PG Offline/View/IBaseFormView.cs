﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.View
{
    interface IBaseFormView
    {
        void Close();
        void Show();
        bool? ShowDialog();
    }
}
