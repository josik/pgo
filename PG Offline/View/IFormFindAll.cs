﻿using System.Collections.Generic;
using PGOffline.Model;

namespace PGOffline.View
{
    public delegate void FormFindAllLoadCompleteEventHandler();
    public delegate void FindAllRequestEventHandler();
    public delegate void FindPersonRequestEventHandler();
    public delegate void SaveSearchParamsRequestEventHandler();
    public delegate void LoadSearchParamsRequestEventHandler();
    public delegate void StartSearchRequestEventHandler();
    public delegate void StopSearchRequestEventHandler();
    
    public interface IFormFindAll
    {
        event FormFindAllLoadCompleteEventHandler FormFindAllLoadComplete;
        event FindPersonRequestEventHandler FindPersonRequest;
        event SaveSearchParamsRequestEventHandler SaveSearchParamsRequest;
        event LoadSearchParamsRequestEventHandler LoadSearchParamsRequest;
        event StartSearchRequestEventHandler StartSearchRequest;
        event StopSearchRequestEventHandler StopSearchRequest;

        string Persons { get; set; }
        string Subject { get; set;  }
        string Message { get; set; }
        long? FromMessage { get; set; }
        long? ToMessage { get; set; }

        List<DiscussionGroup> GetDiscussionGroups();

        void SetControlsReadyToSearch();
        void SetControlsSearchInProgress();
        void PopulateGroupsList(List<DiscussionGroup> groups);
        void AddUsers(string usersNames);
        void SetSelectedGroups(List<DiscussionGroup> groups);
    }
}
