﻿
namespace PGOffline.View
{
    public delegate void SaveImportOptionsRequestEventHandler();

    interface IFormOptionsView
    {
        event SaveImportOptionsRequestEventHandler SaveOptionsRequest;

        #region Download options

            bool EnableDownloadLimitSpeed { get; set; }

            int DownloadLimitSpeedType { get; set; }

            int MessagesPerMinute { get; set; }

            int InitialDelay { get; set; }

            int Download { get; set; }

            int ThenWait { get; set; }

            bool EnableCacheServer { get; set; }

            string CacheServerName { get; set; }

            bool EnableMessagesCaching { get; set; }

            int ReceiveMessagesPerRequest { get; set; }
        
        #endregion

        #region WorkflowOptions
            int OnFailureReRequestMessageTimes { get; set; }

            bool OnLockoutResumeAfterMinutesEnabled { get; set; }
            int OnLockoutResumeAfterMinutes { get; set; }

            bool StopAfterReceivingInvalidPagesEnabled { get; set; }
            int StopAfterReceivingInvalidPages { get; set; }

            bool SkipAuthorization { get; set; }
        #endregion

        #region MailOptions
            string SmtpServer { get; set; }
            int SmtpPort { get; set; }
            string SmtpLogin { get; set; }
            string SmtpPassword { get; set; }
            string From { get; set; }
            bool EnableSsl { get; set; }

            bool UseExternalMailClient { get; set; }
            string ExternalMailClientPath { get; set; }
        #endregion

        #region MiscellaneousOptions
            int MarkMessagesAsReadAfterSeconds { get; set; }

            bool MinimizeToSystemTray { get; set; }

            int ShowEventsOnMainFormMessagesPaneCount { get; set; }

            bool ShowMembersListOutOfDateWarning { get; set; }
            bool SaveSortOrderInMessagesPane { get; set; }

            //MessagesPaneSortOrder MessagesPaneSortOrder { get; set; }
            int SortedMessagesPaneIndex { get; set; }

            bool UseSelectedSortOrderForNewMessagesShow { get; set; }

            bool LogEventsToFile { get; set; }
            string LogEventsFilePath { get; set; }

            string MainWindowPanesFontName { get; set; }
        #endregion

        #region RefreshGroupOptions
            int RefreshGroupType { get; set; }

            int HoursPeriod { get; set; }

            int MinutesPeriod { get; set; }

            bool DownloadMessagesPerGroupLimit { get; set; }

            int DownloadMessagesPerGroupLimitCount { get; set; }

            int RefreshGroupLoopType { get; set; }

            int LoopCount { get; set; }
        #endregion

        #region RefreshMembersOptions
            int RefreshMembersType { get; set; }

            int RefreshPeriod { get; set; }
        #endregion

        #region ParsingOptions
            string FizzlerMessageTitle { get; set; }
            string FizzlerMessageEmail { get; set; }
            string FizzlerMessageSenderName { get; set; }
            string FizzlerMessageSenderNameAlternative { get; set; }
            string FizzlerMessageDate { get; set; }
            string FizzlerMessageContents { get; set; }
            string RegexMessagesCount { get; set; }

            string FizzlerAttachmentNode { get; set; }
            string FizzlerAttachmentOriginalMessage { get; set; }
            string RegexAttachmentsCount { get; set; }

            string FizzlerMembersTableRow { get; set; }
            string FizzlerMemberName { get; set; }
            string FizzlerMemberNameAlternative { get; set; }
            string FizzlerMemberEmail { get; set; }
            string FizzlerMemberJoinDate { get; set; }
            string RegexMembersCount { get; set; }

            string FizzlerFileUri { get; set; }

            string FizzlerPhotoDiv { get; set; }
            string FizzlerPhotoUri { get; set; }
            string FizzlerPhotoThumbnail { get; set; }
            string FizzlerAlbumLink { get; set; }
            string FizzlerAlbumLinkSecondary { get; set; }

            string FizzlerUnfoundGroupName { get; set; }
            string FizzlerUnfoundGroupCriterion { get; set; }
        #endregion
    }
}
