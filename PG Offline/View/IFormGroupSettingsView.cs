﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.Model;

namespace PGOffline.View
{
    public delegate void GroupSettingsSaveRequestEventHandler();
    public delegate void GroupSettingsLoadRequestEventHandler();
    public delegate void SaveActiveTabRequestEventHandler(int index);
    public delegate void BrowseAttachmentsFolderRequestEventHandler();
    public delegate void BrowseOutlookFolderRequestEventHandler();

    public interface IFormGroupSettingsView
    {
        event GroupSettingsSaveRequestEventHandler GroupSettingsSaveRequest;
        event GroupSettingsLoadRequestEventHandler GroupSettingsLoadRequest;
        event SaveActiveTabRequestEventHandler SaveActiveTabRequest;
        event BrowseAttachmentsFolderRequestEventHandler BrowseAttachmentsFolderRequest;
        event BrowseOutlookFolderRequestEventHandler BrowseOutlookFolderRequest;

        int StartMessageNumber { get; set; }
        int EndMessageNumber { get; set; }

        string IncludeAttachmentsPattern { get; set; }
        string ExcludeAttachmentsPattern { get; set; }

        string AttachmentsFolder { get; set; }

        string OutlookFolder { get; set; }

        bool DisableAttachementsDownloading { get; set; }
        bool UpdateAttachmentsLinks { get; set; }
        int AttachmentsLimitationSize { get; set; }
        string AttachmentsLimitationType { get; set; }

        bool SkipDuringRefreshAllGroups { get; set; }
        bool SkipDuringGetMembersForAllGroups { get; set; }
        bool UseOldDesignFormat { get; set; }

        string GroupLogin { get; set; }
        string GroupPassword { get; set; }
        string GroupName { get; }

        int ActiveTab { get; set; }

        void SetGroupName(string groupName);
        void SetHeader(string header);
        void CloseWindow();
        void SetGroupNameEditability(bool isEditable);
    }
}
