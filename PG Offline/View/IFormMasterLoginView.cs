﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.View
{
    public delegate void MasterCredentialsChangedRequestHandler();
    public delegate void CloseRequestEventHandler();

    interface IFormMasterLoginView
    {
        event MasterCredentialsChangedRequestHandler MasterCredentialsChanged;
        event CloseRequestEventHandler CloseRequest;
             
        string Username { get; set; }
        string Password { get; set; }
    }
}
