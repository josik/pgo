﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.View
{
    public delegate void ContactSaveRequestEventHandler();

    interface IFormContactEditorView
    {
        event ContactSaveRequestEventHandler ContactSaveRequest;

        string Name { get; set; }
        string Email { get; set; }
        string Comment { get; set; }
    }
}
