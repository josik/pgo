﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.View
{
    public delegate void GetContentListRequestEventHandler();
    public delegate void DownloadContentRequestEventHandler();
    public delegate void CloseDownloadContentFormRequestEventHandler();

    interface IFormDownloadContentView
    {
        event GetContentListRequestEventHandler GetContentListRequest;
        event DownloadContentRequestEventHandler DownloadContentRequest;
        event CloseDownloadContentFormRequestEventHandler CloseDownloadContentFormRequest;

        void PopulateTreeView(List<object> items);
        void CloseForm();
        void DisableDownloadControls();
        void EnableDownloadControls();
        List<object> GetSelectedItems();
        
        void SetWindowTitle(string title);
    }
}
