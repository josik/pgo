﻿using System.Collections.Generic;


namespace PGOffline.View
{
    public delegate void AddLocalDiscussionGroupRequestEventHandler(string groupName);

    public delegate void RemoveLocalDiscussionGroupRequestEventHandler(string groupName);

    public delegate void SaveGroupsListChangesRequestEventHandler();

    interface IFormSelectGroupsView
    {
        event AddLocalDiscussionGroupRequestEventHandler AddLocalDiscussionGroupRequest;

        event RemoveLocalDiscussionGroupRequestEventHandler RemoveLocalDiscussionGroupRequest;

        event SaveGroupsListChangesRequestEventHandler SaveGroupsListChangesRequest;

        void SetLocalGroups(SortedList<string, string> localGroups);

        void SetRemoteGroups(SortedList<string, string> remoteGroups);
    }
}
