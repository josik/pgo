﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.View
{
    public delegate void FullScreenPreviousMessageRequestEventHandler();
    public delegate void FullScreenNextMessageRequestEventHandler();

    public delegate void FullScreenUpGroupRequestEventHandler(object sender);
    public delegate void FullScreenDownGroupRequestEventHandler(object sender);

    public delegate void FullScreenReplyMessageRequestEventHandler();

    public interface IFormFullScreenView
    {
        event FullScreenPreviousMessageRequestEventHandler FullScreenPreviousMessageRequest;
        event FullScreenNextMessageRequestEventHandler FullScreenNextMessageRequest;

        event FullScreenUpGroupRequestEventHandler FullScreenUpGroupRequest;
        event FullScreenDownGroupRequestEventHandler FullScreenDownGroupRequest;

        event AddMessagesToFavoritesRequestEventHandler AddMessagesToFavoritesRequest;
        event RemoveMessagesFromFavoritesRequestEventHandler RemoveMessagesFromFavoritesRequest;
        event DeleteMessagesRequestEventHandler DeleteMessagesRequest;

        void SetMessage(PGOffline.Model.Message message);
        void SetDiscussionGroup(PGOffline.Model.DiscussionGroup group);
        void SetEmptyContent();
    }
}
