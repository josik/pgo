﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.View
{
    public delegate void SaveSearchParamsHandler(string name);

    interface IFormSaveSearchParams : IBaseFormView
    {
        event SaveSearchParamsHandler SaveSearchParams;
    }
}
