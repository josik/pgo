﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.View
{
    public delegate void SetRangeRequestEventHandler(int leftEdge, int rightEdge);

    public interface IFormRanges
    {
        event SetRangeRequestEventHandler SetRangeRequest;
    }
}
