﻿using System.Windows.Input;

namespace PGOffline.View
{
    public interface IFormRegistrationView
    {
        void CommandBinding_PurchaseProduct(object sender, ExecutedRoutedEventArgs e);

        void CommandBinding_CloseWindow(object sender, ExecutedRoutedEventArgs e);
    }
}
