﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.Model;
using System.ComponentModel;

namespace PGOffline.View
{
    public delegate void AddMessagesToFavoritesRequestEventHandler(IList messages);
    public delegate void RemoveMessagesFromFavoritesRequestEventHandler(IList messages);
    public delegate void MarkMessagesAsReadRequestEventHandler(IList messages);
    public delegate void MarkMessagesAsUnreadRequestEventHandler(IList messages);
    public delegate void MarkAllMessagesAsReadRequestEventHandler();
    public delegate void MarkAllMessagesAsUnreadRequestEventHandler();
    public delegate void DeleteMessagesRequestEventHandler(IList messages);
    public delegate void ExportSelectedMessagesRequestEventHandler(string databasePath);

    public delegate void MessageSelectedEventHandler(Message message);

    public interface IMessagesListView
    {
        event AddMessagesToFavoritesRequestEventHandler AddMessagesToFavoritesRequest;
        event RemoveMessagesFromFavoritesRequestEventHandler RemoveMessagesFromFavoritesRequest;
        event MarkMessagesAsReadRequestEventHandler MarkMessagesAsReadRequest;
        event MarkMessagesAsUnreadRequestEventHandler MarkMessagesAsUnreadRequest;
        event MarkAllMessagesAsReadRequestEventHandler MarkAllMessagesAsReadRequest;
        event MarkAllMessagesAsUnreadRequestEventHandler MarkAllMessagesAsUnreadRequest;
        event DeleteMessagesRequestEventHandler DeleteMessagesRequest;
        event ExportSelectedMessagesRequestEventHandler ExportSelectedMessagesRequest;

        event MessageSelectedEventHandler MessageSelected;

        void RefreshMessagesList();

        void SetFilter(Predicate<object> filter);
        void SetMessages(IList<Message> messages);
        IEnumerable<Message> GetMessages();

        List<long> GetSelectedMessagesNumbers();
    }
}
