﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGOffline.Model;

namespace PGOffline.View
{
    public delegate void ExportRequestEventHandler(String databasePath, List<ExportUnit> units, ExportSettings settings);

    interface IFormExportToFolder
    {
        event ExportRequestEventHandler ExportRequest;

        void SetDiscussionGroups(List<DiscussionGroup> groups);
    }
}
