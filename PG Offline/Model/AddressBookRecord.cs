﻿using System.Collections.Generic;
using System.Data.SQLite;

namespace PGOffline.Model
{
    public class AddressBookRecord
    {
        const int ID_INDEX = 0;
        const int NAME_INDEX = 1;
        const int COMMENT_INDEX = 2;
        const int EMAIL_INDEX = 3;

        object[] _rawData;

        public AddressBookRecord(object[] rawData)
        {
            _rawData = rawData;
        }

        public static List<AddressBookRecord> GetAll()
        {
            List<object[]> rawRecords = DatabaseManager.Instance.ExecuteQueryAndGetValuesList("SELECT * FROM address_book");

            List<AddressBookRecord> records = new List<AddressBookRecord>();

            foreach (object[] rawRecord in rawRecords)
            {
                records.Add(new AddressBookRecord(rawRecord));
            }

            return records;
        }

        public static AddressBookRecord Create(string name, string email, string comment)
        {
            SQLiteCommand cmd = DatabaseManager.Instance.GetConnection().CreateCommand();

            cmd.CommandText = "INSERT INTO address_book (name, email, comment) VALUES(@name, @email, @comment)";
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@comment", comment);

            cmd.ExecuteNonQuery();

            long id = DatabaseManager.Instance.GetConnection().LastInsertRowId;

            return new AddressBookRecord(new object[] { id, name, email, comment });
        }

        public void Delete()
        {
            SQLiteCommand cmd = DatabaseManager.Instance.GetConnection().CreateCommand();

            cmd.CommandText = "DELETE FROM address_book WHERE id = @id";
            cmd.Parameters.AddWithValue("@id", Id);

            cmd.ExecuteNonQuery();
        }

        public void Save()
        {
            SQLiteCommand cmd = DatabaseManager.Instance.GetConnection().CreateCommand();

            cmd.CommandText = "UPDATE address_book SET name = @name, email = @email, comment = @comment WHERE id = @id";
            cmd.Parameters.AddWithValue("@name", Name);
            cmd.Parameters.AddWithValue("@email", Email);
            cmd.Parameters.AddWithValue("@comment", Comment);
            cmd.Parameters.AddWithValue("@id", Id);

            cmd.ExecuteNonQuery();
        }

        public long Id
        {
            get { return (long)_rawData[ID_INDEX]; }
        }

        public string Name
        {
            get { return (string)_rawData[NAME_INDEX]; }
            set { _rawData[NAME_INDEX] = value; }
        }

        public string Email
        {
            get { return (string)_rawData[EMAIL_INDEX]; }
            set { _rawData[EMAIL_INDEX] = value; }
        }

        public string Comment
        {
            get { return (string)_rawData[COMMENT_INDEX]; }
            set { _rawData[COMMENT_INDEX] = value; }
        }
    }
}
