﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace PGOffline.Model
{
    public class UserAccount
    {
        const int ID_INDEX = 0;
        const int NAME_INDEX = 1;
        const int PASSWORD_INDEX = 2;

        object[] _rawData;

        public UserAccount(string username, string password)
        {
            _rawData = new object[] {0, username, password };

        }

        private UserAccount(object[] rawData)
        {
            _rawData = rawData;
        }

        public static UserAccount Create(string username, string password)
        {
            SQLiteCommand cmd = DatabaseManager.Instance.GetConnection().CreateCommand();

            cmd.CommandText = "INSERT INTO user_account (name, password) VALUES(@name, @password)";
            cmd.Parameters.AddWithValue("@name", username);
            cmd.Parameters.AddWithValue("@password", password);

            cmd.ExecuteNonQuery();

            long id = DatabaseManager.Instance.GetConnection().LastInsertRowId;

            return new UserAccount(new object[] { id, username, password });
        }

        public static UserAccount GetFirst()
        {
            List<object[]> rawValues = DatabaseManager.Instance.ExecuteQueryAndGetValuesList("SELECT * FROM user_account");

            return rawValues.Select(rawValue => new UserAccount(rawValue)).FirstOrDefault();
        }

        public void Save()
        {
            // TODO: encrypt user credentials

            //if (File.Exists(@"c:\temp\pwd.txt"))
            //{
            //    var enc1 = File.ReadAllText(@"c:\temp\pwd.txt");
            //    SecureString sec1 = Utils.Encryption.DecryptString(enc1);
            //    string clear1 = Utils.Encryption.ToInsecureString(sec1);
            //}

            //string enc2 = Utils.Encryption.EncryptString(Utils.Encryption.ToSecureString(Password));
            //File.WriteAllText(@"c:\temp\pwd.txt", enc2);
            //SecureString sec2 = Utils.Encryption.DecryptString(enc2);
            //string clear2 = Utils.Encryption.ToInsecureString(sec2);
            
            


            SQLiteCommand cmd = DatabaseManager.Instance.GetConnection().CreateCommand();

            cmd.CommandText = "UPDATE user_account SET name = @name, password = @password WHERE id = @id";
            cmd.Parameters.AddWithValue("@id", Id);
            cmd.Parameters.AddWithValue("@name", Name);
            cmd.Parameters.AddWithValue("@password", Password);

            cmd.ExecuteNonQuery();
        }

        public long Id
        {
            get { return (long)_rawData[ID_INDEX]; }
        }

        public string Name
        {
            get { return (string)_rawData[NAME_INDEX]; }
            set { _rawData[NAME_INDEX] = value; }
        }

        public string Password
        {
            get { return (string)_rawData[PASSWORD_INDEX]; }
            set { _rawData[PASSWORD_INDEX] = value; }
        }
    }
}
