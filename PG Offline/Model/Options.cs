﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using PGOffline.Exceptions;

namespace PGOffline.Model
{
    public class Options : Singleton<Options>
    {
        Dictionary<string, string> _options = new Dictionary<string, string>();

        SQLiteConnection _connection;

        private string SchemaPath { get; set; }

        public string DatabasePath { get; set; }

        // HACK. Used to create 'options.db3' if not exists.
        public void Initialize()
        {
        }

        public Options()
        {
            CreateConnection();

            using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM `options`", _connection))
            {
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        _options[reader.GetString(0)] = reader.GetString(1);
                    }
                }
            }
        }
        
        public Options(SQLiteConnection connection, string schemaPath)
        {
            _connection = connection;

            CreateDatabase(schemaPath);

            using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM `options`", _connection))
            {
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        _options[reader.GetString(0)] = reader.GetString(1);
                    }
                }
            }
        }

        public SQLiteTransaction TransactionBegin()
        {
            return _connection.BeginTransaction();
        }

        public bool BackupDatabase(string sourceName, string destName)
        {
            File.Copy(sourceName, destName, overwrite: true);

            return File.Exists(destName);
        }

        public SQLiteConnection GetConnection()
        {
            return _connection;
        }

        public void CloseConnection()
        {
            if (_connection == null) return;

            _connection.Dispose();

            _connection = null;

            GC.Collect();
        }

        private void CreateConnection()
        {
            DatabasePath = PathResolver.GetOptionsPath();

            try
            {
                _connection = new SQLiteConnection(@"Data source=" + DatabasePath);

                _connection.Open();
            }
            catch (IOException ex)
            {
                throw new DatabaseNoAccessException(DatabasePath, ex.Message);
            }
            catch (SQLiteException ex)
            {
                throw new DatabaseWrongFormatException(DatabasePath, ex.Message);
            }

            SchemaPath = PathResolver.GetOptionsSchemaScriptPath();

            CreateDatabase(SchemaPath);
        }
        
        private void CreateDatabase(string schemaPath)
        {
            using (StreamReader reader = File.OpenText(schemaPath))
            {
                try
                {
                    using (SQLiteCommand cmd = _connection.CreateCommand())
                    {
                        cmd.CommandText = reader.ReadToEnd();

                        int count = cmd.ExecuteNonQuery();
                    }

                    SchemaPath = schemaPath;
                }
                catch (IOException)
                {
                }
            }
        }

        public bool GetAsBoolean(string key)
        {
            string value = GetAsString(key);

            if (value == null) return false;

            return value.Length != 0 && bool.Parse(value);
        }

        public int GetAsInt(string key)
        {
            string value = GetAsString(key);

            if (value == null) return 0;

            return value.Length == 0 ? 0 : int.Parse(value);
        }

        public uint GetAsUInt(string key)
        {
            string value = GetAsString(key);

            return value.Length == 0 ? 0 : uint.Parse(value);
        }

        public float GetAsFloat(string key)
        {
            string value = GetAsString(key);

            if (value == null) return 0;

            return value.Length == 0 ? 0.0f : float.Parse(value);
        }

        public double GetAsDouble(string key)
        {
            string value = GetAsString(key);

            if (value == null) return 0.0f;

            return value.Length == 0 ? 0.0f : double.Parse(value);
        }

        public long GetAsLong(string key)
        {
            string value = GetAsString(key);

            if (value == null) return 0;

            return value.Length == 0 ? 0 : long.Parse(value);
        }

        public long GetStatisticsNumber(string key)
        {
            string value = GetAsString(key);

            if (value == null) return 0;

            long number;

            return long.TryParse(value, out number) == false ? long.MaxValue : number;
        }

        public string GetAsString(string key)
        {
            return _options.Keys.Contains(key) ? _options[key] : null;
        }

        public DateTime GetAsDateTime(string key)
        {
            string value = GetAsString(key);

            return value.Length == 0 ? DateTime.MinValue : DateTime.Parse(value);
        }

        public void Set(string key, string value)
        {
            _options[key] = value;
        }

        public void Set(string key, bool value)
        {
            Set(key, value.ToString());
        }

        public void Set(string key, int value)
        {
            Set(key, value.ToString());
        }

        public void Set(string key, uint value)
        {
            Set(key, value.ToString());
        }

        public void Set(string key, float value)
        {
            Set(key, value.ToString());
        }

        public void Set(string key, double value)
        {
            Set(key, value.ToString());
        }

        public void Set(string key, DateTime value)
        {
            Set(key, value.ToString());
        }

        public void SetStatisticsNumber(string key, long value)
        {
            Set(key, value == long.MaxValue ? "Last" : value.ToString());
        }

        public Dictionary<string, string> GetAllOptions()
        {
            return _options;
        }

        public Dictionary<string, string> GetParsingOptions()
        {
            var retval = new Dictionary<string, string>();

            using (SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM `options` WHERE `key` LIKE 'regex%' OR `key` LIKE 'fizzler%'", _connection))
            {
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        retval[reader.GetString(0)] = reader.GetString(1);
                    }

                }
            }

            return retval;
        }

        public void SaveOptions()
        {
            Dictionary<string, string>.Enumerator it = _options.GetEnumerator();

            using (SQLiteTransaction transaction = _connection.BeginTransaction())
            {
                while (it.MoveNext())
                {
                    using (SQLiteCommand cmd = new SQLiteCommand("INSERT OR REPLACE INTO `options` (`key`, `value`) VALUES (@key, @value)", _connection))
                    {
                        cmd.Parameters.AddWithValue("@key", it.Current.Key);

                        cmd.Parameters.AddWithValue("@value", it.Current.Value);

                        cmd.ExecuteNonQuery();
                    }
                }

                transaction.Commit();
            }
        }

        public void ExecuteQuery(string query)
        {
            using (SQLiteCommand command = new SQLiteCommand(query, GetConnection()))
            {
                command.ExecuteNonQuery();
            }
        }
        
        public object ExecuteQueryAndGetValue(string query, int fieldIndex)
        {
            using (SQLiteCommand command = new SQLiteCommand(query, GetConnection()))
            {
                using (SQLiteDataReader dataReader = command.ExecuteReader())
                {
                    dataReader.Read();

                    object value = dataReader.GetValue(fieldIndex);

                    dataReader.Close();

                    return value;
                }
            }
        }
    }
}
