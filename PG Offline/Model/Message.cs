﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Data.SQLite;
using System.IO;
using System.Text.RegularExpressions;

namespace PGOffline.Model
{
    public enum MessageStatus
    {
        Unread = 0,
        Read
    }

    public class Message : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private const int DBINDEX_ID = 0;
        private const int DBINDEX_NUMBER = 1;
        private const int DBINDEX_DATE = 2;
        private const int DBINDEX_SUBJECT = 3;
        private const int DBINDEX_FAVORITE = 4;
        private const int DBINDEX_UNREAD = 5;
        private const int DBINDEX_PERSON = 6;
        private const int DBINDEX_CHARSET = 7;
        private const int DBINDEX_GROUP = 8;
        private const int DBINDEX_TOPICID = 9;
        private const int DBINDEX_PARENTID = 10;
        private const int DBINDEX_THREADLEVEL = 11;
        private const int DBINDEX_PREVINTOPIC = 12;
        private const int DBINDEX_NEXTINTOPIC = 13;
        private const int DBINDEX_PREVINTIME = 14;
        private const int DBINDEX_NEXTINTIME = 15;

	    protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null && !String.IsNullOrWhiteSpace(propertyName))
            {
                 handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private List<Attachment> _attachments;

        private string _content;

        private string _attachmentContent;

        private object[] _rawData;

        public Message(object[] rawData)
        {
            _rawData = rawData;
        }

        public Message(long number, DateTime? date, string subject, bool favorite, bool unread, long personId, long discussionGroupId, long topicId, long parentId, long threadLevel, long prevInTopic, long nextInTopic, long prevInTime, long nextInTime, string charset = null)
        {
            _rawData = new object[20];

            _rawData[DBINDEX_ID] = -1;

            _rawData[DBINDEX_NUMBER] = number;
            _rawData[DBINDEX_DATE] = date;
            _rawData[DBINDEX_SUBJECT] = subject;
            _rawData[DBINDEX_FAVORITE] = favorite;
            _rawData[DBINDEX_UNREAD] = unread;
            _rawData[DBINDEX_PERSON] = personId;
            _rawData[DBINDEX_CHARSET] = charset;
            _rawData[DBINDEX_GROUP] = discussionGroupId;
            _rawData[DBINDEX_TOPICID] = topicId;
            _rawData[DBINDEX_PARENTID] = parentId;
            _rawData[DBINDEX_THREADLEVEL] = threadLevel;
            _rawData[DBINDEX_PREVINTOPIC] = prevInTopic;
            _rawData[DBINDEX_NEXTINTOPIC] = nextInTopic;
            _rawData[DBINDEX_PREVINTIME] = prevInTopic;
            _rawData[DBINDEX_NEXTINTIME] = nextInTopic;
        }

        public Message()
        {
            
        }

        public static Message GetMessageByNumber(long number)
        {
            object[] rawData = DatabaseManager.Instance.ExecuteQueryAndGetValues(String.Format("select id, number, date, subject, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time from group_message where number = {0}", number));

            if (rawData == null)
            {
                var t = "";
            }

            return rawData == null ? null : new Message(rawData);
        }

        public static Message GetMessageById(long id)
        {
            object[] rawData = DatabaseManager.Instance.ExecuteQueryAndGetValues(String.Format("select id, number, date, subject, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time from group_message where id = {0}", id));

            return new Message(rawData);
        }

        public long Id
        {
            get { return (long)_rawData[DBINDEX_ID]; }
        }

        private string ReEncode(
                System.Text.Encoding fromEncoding,
                System.Text.Encoding toEncoding,
                string text)
        {
            return fromEncoding.GetString(
                    System.Text.Encoding.Convert(
                            fromEncoding,
                            toEncoding,
                            fromEncoding.GetBytes(text)));
        }

        public string Subject
        {
            get
            {
                string encodingName = Options.Instance.GetAsString("global_encoding");

                Encoding encoding = null;

                if (encodingName != null)
                {
                    encoding = Encoding.GetEncoding(encodingName);
                }

                if (encoding == null)
                {
                    encoding = Encoding.Default;
                }

                string tmpString = (string)_rawData[DBINDEX_SUBJECT];

                byte[] encodingBytes = Encoding.GetEncoding("windows-1252").GetBytes(tmpString);

                return encoding.GetString(encodingBytes);
            }
        }

        public long Number
        {
            get { return (long)_rawData[DBINDEX_NUMBER]; }
        }

        public long TopicId
        {
            get { return (long)_rawData[DBINDEX_TOPICID]; }
        }
        public long ParentId
        {
            get { return (long)_rawData[DBINDEX_PARENTID]; }
        }
        public long ThreadLevel
        {
            get { return (long)_rawData[DBINDEX_THREADLEVEL]; }
        }
        public long PrevInTopic
        {
            get { return (long)_rawData[DBINDEX_PREVINTOPIC]; }
        }
        public long NextInTopic
        {
            get { return (long)_rawData[DBINDEX_NEXTINTOPIC]; }
        }
        public long PrevInTime
        {
            get { return (long)_rawData[DBINDEX_PREVINTIME]; }
        }
        public long NextInTime
        {
            get { return (long)_rawData[DBINDEX_NEXTINTIME]; }
        }

        public string Author
        {
            get 
            {
                try
                {
                    return Person.GetById((long)_rawData[DBINDEX_PERSON]).Name;
                }
                catch (Exception)
                {
                    return "";
                }
            }
        }

        public Person Person
        {
            get
            {
                return Person.GetById((long)_rawData[DBINDEX_PERSON]);
            }
        }

        public DateTime Date
        {
            get 
            {
                try
                {
                    return (DateTime)_rawData[DBINDEX_DATE];
                }
                catch (InvalidCastException)
                {
                    return DateTime.MinValue;
                }
            }
        }

        public string Content
        {
            get
            {
                if (_content == null)
                {
                    SQLiteCommand cmd = DatabaseManager.Instance.CreateCommand("SELECT content FROM group_message WHERE id = @id");
                    cmd.Parameters.AddWithValue("@id", Id);

                    byte[] rawMessageBody;

                    try
                    {
                        rawMessageBody = (byte[])cmd.ExecuteScalar();
                    }
                    catch (Exception)
                    {
                        return "";
                    }

                    if (rawMessageBody == null || rawMessageBody.Length == 0)
                    {
                        return "";
                    }

                    string encodingName = Options.Instance.GetAsString("global_encoding");

                    Encoding encoding = null;

                    if (encodingName != null)
                    {
                        encoding = Encoding.GetEncoding(encodingName);
                    }

                    if (encoding == null)
                    {
                        encoding = Encoding.Default;
                    }

                    string tmpString = Encoding.UTF8.GetString(rawMessageBody);

                    byte[] encodingBytes = Encoding.GetEncoding("windows-1252").GetBytes(tmpString);

                    _content = encoding.GetString(encodingBytes);

                    using (var stringWriter = new StringWriter(new StringBuilder()))
                    {
                        var htmlDoc = new HtmlAgilityPack.HtmlDocument
                        {
                            OptionCheckSyntax = true,
                            OptionFixNestedTags = true
                        };

                        htmlDoc.LoadHtml(_content);

                        htmlDoc.Save(stringWriter);

                        _content = stringWriter.GetStringBuilder().ToString();

                        if (Options.Instance.GetAsBoolean("hide_quote_string"))
                        {
                            Regex regexLessThan = new Regex("&gt;.*");

                            _content = regexLessThan.Replace(_content, "");
                        }
                    }
                }

                _content = string.Format("<html><head><meta http-equiv='Content-Type' content='text/html;charset=utf-8'></head>{0}</html>", _content);

                return _content;
            }
            set
            {
                _content = value;
            }
        }

        public string AttachmentLinks
        {
            get
            {
                if (_attachmentContent != null) return _attachmentContent;

                var attachmentLinks = new StringBuilder();

                attachmentLinks.AppendLine("<html><head><meta http-equiv='Content-Type' content='text/html;charset=utf-8'></head><br/>");

                attachmentLinks.AppendLine("<span>Attachments:<br/>");

                foreach (var attachment in this.Attachments)
                {
                    attachmentLinks.AppendLine(string.Format(@"<a href='file:///{0}'>{1}</a></br>", attachment.Filename.Replace(@"\", @"/"), attachment.DisplayName));
                }

                attachmentLinks.AppendLine("</span>");

                attachmentLinks.AppendLine("</html>");

                _attachmentContent = attachmentLinks.ToString();

                return _attachmentContent;
            }
            set
            {
                _attachmentContent = value;
                
            }
        }

        public List<Attachment> Attachments
        {
            get 
            {
                try
                {
                    return Attachment.GetByGroupMessageId((long)_rawData[DBINDEX_ID]);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public int AttachmentsCount
        {
            get { return Attachments != null ? Attachments.Count : 0; }
        }

        public bool Unread
        {
            get { return (bool)_rawData[DBINDEX_UNREAD]; }
        }

        public MessageStatus MessageStatus
        {
            get
            {
                return Unread == true ? Model.MessageStatus.Unread : Model.MessageStatus.Read;
            }

            set
            {
                SetMessageStatus(value);

                Save();

                RaisePropertyChanged("MessageStatus");

                RaisePropertyChanged("MessageStatusString");
            }
        }

        public void NotifyStatusChanged()
        {
            RaisePropertyChanged("MessageStatus");

            RaisePropertyChanged("MessageStatusString");            
        }

        /// <summary>
        /// Set message status but does not save changes to the database.
        /// </summary>
        /// <param name="status">Message status.</param>
        public void SetMessageStatus(MessageStatus status)
        {
            switch (status)
            {
                case Model.MessageStatus.Unread:
                    _rawData[DBINDEX_UNREAD] = true;
                    break;

                case Model.MessageStatus.Read:
                    _rawData[DBINDEX_UNREAD] = false;
                    break;

                default:
                    return;
            }

            Save();
        }

        public string MessageStatusString
        {
            get
            {
                switch (MessageStatus)
                {
                    case Model.MessageStatus.Read:
                        return "";
                        break;

                    case Model.MessageStatus.Unread:
                        return "New!";
                        break;
                }

                return MessageStatus.ToString();
            }
        }

        public bool Favorite
        {
            get
            {
                return (bool)_rawData[DBINDEX_FAVORITE];
            }

            set
            {
                if (_rawData == null) return;

                // TODO: add exception handling for when rawData is null

                _rawData[DBINDEX_FAVORITE] = value;
                
                Save();

                RaisePropertyChanged("Favorite");
            }
        }

        public DiscussionGroup DiscussionGroup
        {
            get
            {
                return DiscussionGroup.GetById((long)_rawData[DBINDEX_GROUP]);
            }
        }

        public long Insert(byte[] content, SQLiteConnection connection = null)
        {
            if (connection == null)
            {
                connection = DatabaseManager.Instance.GetConnection();
            }

            if (!DatabaseManager.Instance.IsConnectionIsReady())
            {
                return DatabaseManager.IntFailedReturnValue;
            }

            try
            {
                //var cmd1Text = "";

                //using (SQLiteCommand cmd1 = new SQLiteCommand(cmd1Text, connection))
                //{
                //    cmd1.ExecuteScalar();
                //}

                const string columns = "number, date, subject, content, favorite, unread, discussion_group, charset, person, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time";
                
                const string values = "@number, @date, @subject, @content, @favorite, @unread, @discussion_group, @charset, @person, @topic_id, @parent_id, @thread_level, @prev_in_topic, @next_in_topic, @prev_in_time, @next_in_time";

                //string commandText = String.Format("INSERT INTO group_message ({0}) VALUES ({1});", columns, values);
                string commandText = String.Format("INSERT OR IGNORE INTO group_message ({0}) VALUES ({1});", columns, values);

                using (SQLiteCommand cmd = new SQLiteCommand(commandText, connection))
                {
                    cmd.Parameters.Add("@number", System.Data.DbType.Int64).Value = _rawData[DBINDEX_NUMBER];
                    cmd.Parameters.Add("@date", System.Data.DbType.DateTime).Value = _rawData[DBINDEX_DATE];
                    cmd.Parameters.Add("@subject", System.Data.DbType.String).Value = _rawData[DBINDEX_SUBJECT];
                    cmd.Parameters.Add("@content", System.Data.DbType.Binary).Value = content;
                    cmd.Parameters.Add("@favorite", System.Data.DbType.Boolean).Value = _rawData[DBINDEX_FAVORITE];
                    cmd.Parameters.Add("@unread", System.Data.DbType.Boolean).Value = _rawData[DBINDEX_UNREAD];
                    cmd.Parameters.Add("@person", System.Data.DbType.Int64).Value = _rawData[DBINDEX_PERSON];
                    cmd.Parameters.Add("@charset", System.Data.DbType.String).Value = _rawData[DBINDEX_CHARSET];
                    cmd.Parameters.Add("@discussion_group", System.Data.DbType.Int64).Value = _rawData[DBINDEX_GROUP];
                    cmd.Parameters.Add("@topic_id", System.Data.DbType.Int64).Value = _rawData[DBINDEX_TOPICID];
                    cmd.Parameters.Add("@parent_id", System.Data.DbType.Int64).Value = _rawData[DBINDEX_PARENTID];
                    cmd.Parameters.Add("@thread_level", System.Data.DbType.Int64).Value = _rawData[DBINDEX_THREADLEVEL];
                    cmd.Parameters.Add("@prev_in_topic", System.Data.DbType.Int64).Value = _rawData[DBINDEX_PREVINTOPIC];
                    cmd.Parameters.Add("@next_in_topic", System.Data.DbType.Int64).Value = _rawData[DBINDEX_NEXTINTOPIC];
                    cmd.Parameters.Add("@prev_in_time", System.Data.DbType.Int64).Value = _rawData[DBINDEX_PREVINTIME];
                    cmd.Parameters.Add("@next_in_time", System.Data.DbType.Int64).Value = _rawData[DBINDEX_NEXTINTIME];

                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }

                long id = DatabaseManager.Instance.GetLastInsertRowId(connection);

#if DEBUG
                //File.AppendAllText("c:/temp/debug_import.log", string.Format("id == {0} {1}", id, Environment.NewLine));
#endif

                _rawData[DBINDEX_ID] = id;

#if DEBUG
                //if ((long)_rawData[DBINDEX_ID] == -1)
                //{
                //    File.AppendAllText("c:/temp/debug_import.log", string.Format("id == -1"));
                //}                
                //if (id != Id)
                //{
                //    //string IdValue = Id == null ? "null" : Id.ToString();

                //    File.AppendAllText("c:/temp/debug_import.log", string.Format("id != Id :: {0} != {1} {2}", id, Id, Environment.NewLine));
                //}
#endif
                
                return Id;
            }
            catch (Exception ex)
            {
                return -1;
            }

        }

        private void Save()
        {
            SQLiteConnection connection = DatabaseManager.Instance.GetConnection();

            string commandText = String.Format("UPDATE group_message SET favorite=@favorite, unread=@unread, charset=@charset WHERE id=@id");

            using (SQLiteCommand command = new SQLiteCommand(commandText, connection))
            {
                command.Parameters.Add("@id", DbType.Int64).Value = _rawData[DBINDEX_ID];
                command.Parameters.Add("@favorite", DbType.Boolean).Value = _rawData[DBINDEX_FAVORITE];
                command.Parameters.Add("@unread", DbType.Boolean).Value = _rawData[DBINDEX_UNREAD];
                command.Parameters.Add("@charset", DbType.String).Value = _rawData[DBINDEX_CHARSET] == DBNull.Value
                    ? string.Empty
                    : _rawData[DBINDEX_CHARSET];

                command.ExecuteNonQuery();
            }
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(Message)) return false;

            var message = obj as Message;

            return message != null && message.Id.Equals(Id);
        }

        public static void Delete(Message message)
        {
            DeleteById(message.Id);
        }

        public static bool DeleteById(long id)
        {
            try
            {
                SQLiteConnection connection = DatabaseManager.Instance.GetConnection();

                List<object> attachmentIdList = DatabaseManager.Instance.ExecuteQueryAndGetValueList(string.Format("SELECT attachment FROM group_message_attachment WHERE group_message = {0}", id), 0);

                SQLiteTransaction tr = DatabaseManager.Instance.TransactionBegin();

                using (SQLiteCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "DELETE FROM group_message_attachment WHERE group_message = @message_id";
                    cmd.Parameters.AddWithValue("@message_id", id);
                    cmd.ExecuteNonQuery();

                    foreach (var attachmentId in attachmentIdList)
                    {
                        cmd.CommandText = "DELETE FROM attachment WHERE id = @attachment_id";
                        cmd.Parameters.AddWithValue("@attachment_id", attachmentId);
                        cmd.ExecuteNonQuery();
                    }

                    cmd.CommandText = "DELETE FROM group_message WHERE id = @message_id";
                    cmd.Parameters.AddWithValue("@message_id", id);
                    cmd.ExecuteNonQuery();
                }

                tr.Commit();

                return true;
            }
            catch (Exception ex)
            {
                return false;
                //throw;
            }

        }

        public override int GetHashCode()
        {
            return (int)Id;
        }
    }
}
