﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace PGOffline.Model
{
    public class Person
    {


        public static List<Person> GetAll()
        {
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM person", DatabaseManager.Instance.GetConnection());
            SQLiteDataReader dataReader = command.ExecuteReader();
            List<Person> persons = new List<Person>();
            while (dataReader.Read())
            {
                object[] values = new object[6];
                dataReader.GetValues(values);
                Person person = new Person(values);
                LocalCache.Instance.Add(person);
                persons.Add(person);
            }
            dataReader.Close();

            return persons;
        }

        public static List<Person> GetByDiscussionGroup(DiscussionGroup group)
        {
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM person WHERE discussion_group = @group", DatabaseManager.Instance.GetConnection());
            command.Parameters.AddWithValue("@group", group.Id);
            SQLiteDataReader dataReader = command.ExecuteReader();
            List<Person> persons = new List<Person>();
            while (dataReader.Read())
            {
                object[] values = new object[6];
                dataReader.GetValues(values);
                Person person = new Person(values);
                LocalCache.Instance.Add(person);
                persons.Add(person);
            }
            dataReader.Close();

            return persons;
        }

        public static Person GetById(long id)
        {
            Person person = LocalCache.Instance.GetPersonById(id);

            if (person != null)
            {
                return person;
            }

            SQLiteCommand command = new SQLiteCommand("SELECT * FROM person WHERE id = @id", DatabaseManager.Instance.GetConnection());
            command.Parameters.AddWithValue("@id", id);
            SQLiteDataReader dataReader = command.ExecuteReader();
            dataReader.Read();
            object[] values = new object[6];
            dataReader.GetValues(values);
            dataReader.Close();

            person = new Person(values);

            LocalCache.Instance.Add(person);

            return person;
        }

        public static Person GetByName(string name)
        {
            Person person = LocalCache.Instance.GetPersonByName(name);

            if (person != null)
            {
                return person;
            }

            SQLiteCommand command = new SQLiteCommand("SELECT * FROM person WHERE name = @name", DatabaseManager.Instance.GetConnection());
            command.Parameters.Add("@name", System.Data.DbType.String).Value = name;
            SQLiteDataReader dataReader = command.ExecuteReader();
            dataReader.Read();
            if (dataReader.HasRows)
            {
                object[] values = new object[6];
                dataReader.GetValues(values);
                dataReader.Close();

                person = new Person(values);

                LocalCache.Instance.Add(person);

                return person;
            }
            else
            {
                return null;
            }
        }

        public static Person GetByEmail(string email)
        {
            Person person = LocalCache.Instance.GetPersonByEmail(email);

            if (person != null)
            {
                return person;
            }

            SQLiteCommand command = new SQLiteCommand("SELECT * FROM person WHERE email = @email", DatabaseManager.Instance.GetConnection());
            command.Parameters.Add("@email", System.Data.DbType.String).Value = email;
            SQLiteDataReader dataReader = command.ExecuteReader();
            dataReader.Read();

            if (dataReader.HasRows)
            {
                object[] values = new object[6];
                dataReader.GetValues(values);
                dataReader.Close();

                person = new Person(values);

                LocalCache.Instance.Add(person);

                return person;
            }
            else
            {
                return null;
            }
        }

        public static Person GetByEmailForGroup(string email, int groupId)
        {
            Person person = LocalCache.Instance.GetPersonByEmail(email);

            if (person != null)
            {
                return person;
            }

            SQLiteCommand command = new SQLiteCommand("SELECT * FROM person WHERE discussion_group = @groupId AND email = @email", DatabaseManager.Instance.GetConnection());
            
            command.Parameters.AddWithValue("@groupId", groupId);

            command.Parameters.Add("@email", System.Data.DbType.String).Value = email;

            SQLiteDataReader dataReader = command.ExecuteReader();

            dataReader.Read();

            if (dataReader.HasRows)
            {
                object[] values = new object[6];
                dataReader.GetValues(values);
                dataReader.Close();

                person = new Person(values);

                LocalCache.Instance.Add(person);

                return person;
            }
            else
            {
                return null;
            }
        }
        
        private object[] _rawData;

        public Person(object[] rawData)
        {
            _rawData = rawData;
        }

        public Person(string name, DateTime? joined, string email, bool isMember, long discussionGroupId)
        {
            _rawData = new object[6];

            _rawData[1] = name;

            if (joined != DateTime.MinValue)
            {
                _rawData[2] = joined;
            }
            else
            {
                _rawData[2] = null;
            }

            _rawData[3] = email;
            _rawData[4] = isMember;
            _rawData[5] = discussionGroupId;
        }

        public long Id
        {
            get { return (long)_rawData[0]; }
        }

        public string Name
        {
            get { return (string)_rawData[1]; }
        }

        public DateTime Joined
        {
            get { return (DateTime)_rawData[2]; }
        }

        public string Email
        {
            get { return (string)_rawData[3]; }
        }

        public bool IsMember
        {
            get { return (bool)_rawData[4]; }
        }
        public long DiscussionGroupId
        {
            get { return (long)_rawData[5]; }
        }
        
        public DiscussionGroup DiscussionGroup
        {
            get { return DiscussionGroup.GetById((long)_rawData[5]); }
        }

        public long Insert(SQLiteConnection connection = null)
        {
            if (connection == null)
            {
                connection = DatabaseManager.Instance.GetConnection();
            }

            // check by email if the person record already exists
            Person foundPerson = GetByEmailForGroup(Convert.ToString(_rawData[3]), Convert.ToInt32(_rawData[5]));
            
            if (foundPerson != null)
            {
                return foundPerson.Id;
            }
            
            // person record does not exist, add it

            const string columns = "name, discussion_group, joined, email, is_member";

            const string values = "@name, @discussion_group, @joined, @email, @is_member";

            var commandText = string.Format("INSERT INTO person ({0}) VALUES ({1});", columns, values);

            using (SQLiteCommand cmd = new SQLiteCommand(commandText, connection))
            {
                cmd.Parameters.Add("@name", System.Data.DbType.String).Value = _rawData[1];

                cmd.Parameters.Add("@discussion_group", System.Data.DbType.Int64).Value = _rawData[5];

                cmd.Parameters.Add("@joined", System.Data.DbType.DateTime).Value = _rawData[2];

                cmd.Parameters.Add("@email", System.Data.DbType.String).Value = _rawData[3];

                cmd.Parameters.Add("@is_member", System.Data.DbType.Boolean).Value = _rawData[4];

                cmd.ExecuteNonQuery();
            }

            _rawData[0] = DatabaseManager.Instance.GetLastInsertRowId(connection);

            LocalCache.Instance.Add(this);

            return Id;
        }

        public void Update()
        {
            SQLiteConnection connection = DatabaseManager.Instance.GetConnection();

            string commandText = String.Format("UPDATE person SET name=@name, discussion_group=@discussion_group, joined=@joined, email=@email, is_member=@is_member WHERE id=@id");
            SQLiteCommand command = new SQLiteCommand(commandText, connection);

            command.Parameters.Add("@id", System.Data.DbType.Int64).Value = _rawData[0];
            command.Parameters.Add("@name", System.Data.DbType.String).Value = _rawData[1];
            command.Parameters.Add("@joined", System.Data.DbType.DateTime).Value = _rawData[2];
            command.Parameters.Add("@email", System.Data.DbType.String).Value = _rawData[3];
            command.Parameters.Add("@is_member", System.Data.DbType.Boolean).Value = _rawData[4];
            command.Parameters.Add("@discussion_group", System.Data.DbType.Int64).Value = _rawData[5];

            command.ExecuteNonQuery();
        }

        public void UpdateOrInsert()
        {
            if ((long)_rawData[0] == 0)
            {
                Insert();
            }
            else
            {
                Update();
            }
        }
    }
}
