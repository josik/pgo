﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;

namespace PGOffline.Model
{
    public class Attachment
    {
        const int ID_INDEX = 0;
        const int FILENAME_INDEX = 1;
        const int ID_MESSAGE = 2;

        object[] _rawData;

        public long Id
        {
            get
            {
                return (long)_rawData[ID_INDEX];
            }
        }

        public string DisplayName
        {
            get
            {
                var fullPath = (string) _rawData[FILENAME_INDEX];
                return Path.GetFileName(fullPath);
            }
        }

        public string Filename
        {
            get
            {
                return (string)_rawData[FILENAME_INDEX];
            }
        }

        public long MessageId
        {
            get
            {
                return (long)_rawData[ID_MESSAGE];
            }
        }

        public Message Message
        {
            get { return Message.GetMessageById((long)_rawData[2]); }
        }

        public Attachment(object[] rawData)
        {
            _rawData = rawData;
        }

        public Attachment(string filename, long messageId)
        {
            _rawData = new object[3];

            _rawData[1] = filename;
            _rawData[2] = messageId;
        }

        public static List<Attachment> GetByEmailMessageId(long id)
        {
            string query = String.Format("SELECT attachment.* FROM email_message_attachment INNER JOIN attachment ON email_message_attachment.attachment = attachment.id WHERE email_message_attachment.email_message = {0}", id);

            List<object[]> rawAttachments = DatabaseManager.Instance.ExecuteQueryAndGetValuesList(query);

            List<Attachment> retval = new List<Attachment>();

            foreach (object[] rawAttachment in rawAttachments)
            {
                retval.Add(new Attachment(rawAttachment));
            }

            return retval;
        }

        public static List<Attachment> GetByGroupMessageId(long messageId)
        {
            string query = String.Format("SELECT attachment.* FROM group_message_attachment INNER JOIN attachment ON group_message_attachment.attachment = attachment.id WHERE group_message_attachment.group_message = {0}", messageId);

            List<object[]> rawAttachments = DatabaseManager.Instance.ExecuteQueryAndGetValuesList(query);

            List<Attachment> retval = new List<Attachment>();

            foreach (object[] rawAttachment in rawAttachments)
            {
                retval.Add(new Attachment(rawAttachment));
            }

            return retval;
        }

        public long Insert(SQLiteConnection connection = null)
        {
            if (connection == null)
            {
                connection = DatabaseManager.Instance.GetConnection();
            }

            SQLiteCommand cmd = connection.CreateCommand();

            cmd.CommandText = "INSERT INTO attachment (filename) VALUES(@filename)";

            cmd.Parameters.AddWithValue("@filename", Filename);

            cmd.ExecuteNonQuery();

            long id = DatabaseManager.Instance.GetConnection().LastInsertRowId;

            cmd.CommandText = "INSERT INTO group_message_attachment (group_message, attachment) VALUES(@group_message, @attachment)";

            cmd.Parameters.AddWithValue("@group_message", MessageId);

            cmd.Parameters.AddWithValue("@attachment", id);

            cmd.ExecuteNonQuery();

            return id;
        }
    }
}
