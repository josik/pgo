﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Model
{
    public class Email
    {
        const int ADDRESS_INDEX = 0;

        object[] _rawData;

        public Email(object[] rawData)
        {
            _rawData = rawData;
        }

        public static Email GetOrCreate(string address)
        {
            DatabaseManager.Instance.ExecuteQuery(String.Format("INSERT INTO email (address) VALUES('{0}')", address));

            return new Email(new [] { address });
        }

        public static List<Email> GetByEmailMessageId(long id)
        {
            List<object[]> rawEmails = DatabaseManager.Instance.ExecuteQueryAndGetValuesList(String.Format("SELECT email FROM message_to_email WHERE message = {0}", id));

            List<Email> retval = new List<Email>();

            foreach (object[] rawEmail in rawEmails)
            {
                retval.Add(new Email(rawEmail));
            }

            return retval;
        }

        public static List<Email> GetEmailCopyByEmailMessageId(long id)
        {
            List<object[]> rawEmails = DatabaseManager.Instance.ExecuteQueryAndGetValuesList(String.Format("SELECT email FROM message_copy_email WHERE message = {0}", id));

            List<Email> retval = new List<Email>();

            foreach (object[] rawEmail in rawEmails)
            {
                retval.Add(new Email(rawEmail));
            }

            return retval;
        }

        public string Address
        {
            get
            {
                return (string)_rawData[ADDRESS_INDEX];
            }
        }

        public override string ToString()
        {
            return Address;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(Email))
                return false;

            return ((Email)obj).Address == Address;
        }

        public override int GetHashCode()
        {
            return Address.GetHashCode();
        }
    }
}
