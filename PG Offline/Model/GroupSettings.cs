﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace PGOffline.Model
{
    public class GroupSettings
    {
        Dictionary<string, string> _groupSettings;

        DiscussionGroup _discussionGroup;

        public GroupSettings(DiscussionGroup parentGroup)
        {
            _groupSettings = new Dictionary<string, string>();

            _discussionGroup = parentGroup;
        }

        public static Dictionary<string, string> GetDefaultGroupSettings()
        {
            Dictionary<string, string> defaultSettings = new Dictionary<string, string>
            {
                {"attachments_folder", Options.Instance.GetAsString("group_settings_default_attachments_folder").Replace("APPDATA", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData))},
                {"include_attachments_pattern", "*.*"},
                {"start_download_from_message", "1"},
                {"end_download_at_message", Int32.MaxValue.ToString()},
                {"attachments_limitation_type", "at_least"},
                {"attachments_limitation_size", "0"},
                {"outlook_folder", ""},
                {"use_old_design_format", "False"},
                {"skip_during_get_members_for_all_groups", "False"},
                {"skip_during_refresh_all_groups", "False"},
                {"disable_attachements_downloading", "False"}
            };

            return defaultSettings;
        }

        public string GetValue(string key)
        {
            return _groupSettings.ContainsKey(key) ? _groupSettings[key] : String.Empty;
        }

        public long GetAsLong(string key)
        {
            string value = GetValue(key);

            return value.Length == 0 ? 0 : long.Parse(value);
        }

        public int GetAsInt(string key)
        {
            string value = GetValue(key);

            return value.Length == 0 ? 0 : int.Parse(value);
        }

        public bool GetAsBoolean(string key)
        {
            string value = GetValue(key);

            if (!string.IsNullOrEmpty(value))
            {
                return Boolean.Parse(value);
            }

            return false;
        }

        public void SetValue(string key, string value)
        {
            if (_groupSettings.ContainsKey(key))
            {
                _groupSettings[key] = value;
            }
            else
            {
                _groupSettings.Add(key, value);
            }
        }

        public void SetValues(Dictionary<string, string> settings)
        {
            foreach (var setting in settings)
            {
                switch (setting.Key)
                {
                    case "attachments_limitation_type":
                        // (4.0.811) note: "0" is to handle a previous coding error which was saving "0" as the "mode" rather than "at_least"
                        SetValue(setting.Key, setting.Value == "0" ? "at_least" : setting.Value);
                        break;
                    default:
                        SetValue(setting.Key, setting.Value);
                        break;
                }
            }
        }

        public void SaveSettings()
        {
            // Transaction temporarily turned off because of possible nesting.
            // SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin();

            foreach (var setting in _groupSettings)
            {
                const string cmdText = "INSERT OR REPLACE INTO group_settings (key, value, discussion_group) VALUES (@key, @value, @discussion_group)";

                SQLiteCommand cmd = DatabaseManager.Instance.CreateCommand(cmdText);

                cmd.Parameters.AddWithValue("@key", setting.Key);

                cmd.Parameters.AddWithValue("@value", setting.Value);

                cmd.Parameters.AddWithValue("@discussion_group", _discussionGroup.Id);

                cmd.ExecuteNonQuery();
            }

            // transaction.Commit();
        }

        public void SetDefaultValues()
        {
            string defaultAttachmentsFolder = Options.Instance.GetAsString("group_settings_default_attachments_folder");

            if (defaultAttachmentsFolder == null)
            {
                return;
            }

            defaultAttachmentsFolder = defaultAttachmentsFolder.Replace("APPDATA", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));

            Dictionary<string, string> defaultSettings = new Dictionary<string, string>
            {
                {"attachments_folder", defaultAttachmentsFolder},
                {"include_attachments_pattern", "*.*"},
                {"start_download_from_message", "1"},
                {"end_download_at_message", Int32.MaxValue.ToString()},
                {"attachments_limitation_type", "at_least"},
                {"attachments_limitation_size", "0"},
                {"outlook_folder", ""},
                {"use_old_design_format", "False"},
                {"skip_during_get_members_for_all_groups", "False"},
                {"skip_during_refresh_all_groups", "False"},
                {"disable_attachements_downloading", "False"}
            };

            SetValues(defaultSettings);
        }

        public void RefreshValuesForDiscussionGroup()
        { 
            string queryText = String.Format("SELECT * FROM `group_settings` WHERE `discussion_group` = '{0}';", _discussionGroup.Id);

            List<object[]> groupSettings = DatabaseManager.Instance.ExecuteQueryAndGetValuesList(queryText);

            if (groupSettings.Count == 0)
            {
                this.SetDefaultValues();
                return;
            }

            var rawSettings = new Dictionary<string, string>();

            foreach (object[] setting in groupSettings)
	        {
                rawSettings.Add((string)setting[0], (string)setting[1]);
	        }

            SetValues(rawSettings);
        }

        public bool SkipDuringRefreshAllGroups
        {
            get { return GetAsBoolean("skip_during_refresh_all_groups"); }
        }

        public string AttachmentsLimitMode
        {
            get { return GetValue("attachments_limitation_type"); }
        }

        public int AttachmentLimitSize
        {
            get { return GetAsInt("attachments_limitation_size"); }
        }

        public int MinimumAttachmentSize
        {
            // (4.0.811) note: "0" is to handle a previous coding error which was saving "0" as the "mode" rather than "at_least"
            //get { return (AttachmentsLimitMode == "at_least" || AttachmentsLimitMode == "0") ? AttachmentLimitSize : 0; }
            get { return AttachmentsLimitMode == "at_least" ? AttachmentLimitSize : 0; }
        }

        public int MaximumAttachmentSize
        {
            // (4.0.811) note: "0" is to handle a previous coding error which was saving "0" as the "mode" rather than "at_least"
            //get { return (AttachmentsLimitMode == "at_least" || AttachmentsLimitMode == "0") ? int.MaxValue : AttachmentLimitSize; }
            get { return AttachmentsLimitMode == "at_least" ? int.MaxValue : AttachmentLimitSize; }
        }
    }
}
