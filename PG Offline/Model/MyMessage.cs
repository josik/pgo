﻿using System.Collections.Specialized;

namespace PGOffline.Model
{
    public class MyMessage
    {
        public MyMessage(string author, string subject, string topic)
        {
            Author = author;
            Subject = subject;
            Topic = topic;
        }

        public string Author { get; set; }
        public string Subject { get; set; }
        public string Topic { get; set; }
    }
}
