﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.Collections.ObjectModel;

namespace PGOffline.Model
{
    public class EmailMessage
    {
        const int ID_INDEX = 0;
        const int SUBJECT_INDEX = 1;
        const int CONTENT_INDEX = 2;
        const int SENT_INDEX = 3;
        const int DATE_INDEX = 4;

        object[] _rawData;

        public EmailMessage(object[] rawData)
        {
            _rawData = rawData;
        }

        public static EmailMessage GetById(long id)
        {
            return new EmailMessage(DatabaseManager.Instance.ExecuteQueryAndGetValues(String.Format("SELECT * FROM email_message WHERE id = {0}", id)));
        }

        public static ObservableCollection<EmailMessage> GetDrafts()
        {
            List<object[]> rawMessages = DatabaseManager.Instance.ExecuteQueryAndGetValuesList("SELECT * FROM email_message WHERE sent = 0");

            ObservableCollection<EmailMessage> retval = new ObservableCollection<EmailMessage>();

            foreach (object[] rawMessage in rawMessages)
            {
                retval.Add(new EmailMessage(rawMessage));
            }

            return retval;
        }

        public static ObservableCollection<EmailMessage> GetSent()
        {
            List<object[]> rawMessages = DatabaseManager.Instance.ExecuteQueryAndGetValuesList("SELECT * FROM email_message WHERE sent = 1");

            ObservableCollection<EmailMessage> retval = new ObservableCollection<EmailMessage>();

            foreach (object[] rawMessage in rawMessages)
            {
                retval.Add(new EmailMessage(rawMessage));
            }

            return retval;
        }

        public static EmailMessage Create(DateTime date)
        {
            SQLiteCommand command = DatabaseManager.Instance.GetConnection().CreateCommand();

            command.CommandText = "INSERT INTO email_message (date) VALUES (?)";
            command.Parameters.Add(date);

            command.ExecuteNonQuery();

            return GetById(DatabaseManager.Instance.GetConnection().LastInsertRowId);
        }

        public static void DeleteById(long id)
        {
            DatabaseManager.Instance.ExecuteQuery(String.Format("DELETE FROM email_message WHERE id = {0}", id));
        }

        public void Delete()
        {
            DeleteById(Id);
        }

        public void Save()
        {
            SQLiteCommand cmd = DatabaseManager.Instance.GetConnection().CreateCommand();

            cmd.CommandText = "UPDATE email_message SET subject = @subject, content = @content, sent = @sent, date = @date WHERE id = @id";
            cmd.Parameters.AddWithValue("@subject", Subject);
            cmd.Parameters.AddWithValue("@content", Content);
            cmd.Parameters.AddWithValue("@sent", Sent);
            cmd.Parameters.AddWithValue("@date", Date);
            cmd.Parameters.AddWithValue("@id", Id);

            cmd.ExecuteNonQuery();
        }

        public void AddEmail(string email)
        {
            DatabaseManager.Instance.ExecuteQuery(String.Format("INSERT INTO message_to_email (message, email) VALUES ({0}, '{1}')", Id, email));
        }

        public void AddEmail(Email email)
        {
            AddEmail(email.Address);
        }

        public void AddEmailCopy(string email)
        {
            DatabaseManager.Instance.ExecuteQuery(String.Format("INSERT INTO message_copy_email (message, email) VALUES ({0}, '{1}')", Id, email));
        }

        public void AddEmailCopy(Email email)
        {
            AddEmailCopy(email.Address);
        }

        public long Id
        {
            get
            {
                return (long)_rawData[ID_INDEX];
            }
        }

        public List<Email> Emails
        {
            get
            {
                return Email.GetByEmailMessageId(Id);
            }
        }

        public List<Email> EmailsCopy
        {
            get
            {
                return Email.GetEmailCopyByEmailMessageId(Id);
            }
        }

        public string EmailsString
        {
            get
            {
                StringBuilder emailBuilder = new StringBuilder();

                foreach (Email email in Emails)
                {
                    emailBuilder.Append(email.Address);
                    emailBuilder.Append(',');
                }

                return emailBuilder.ToString();
            }
        }

        public string Content
        {
            get
            {
                return (string)_rawData[CONTENT_INDEX];
            }

            set
            {
                _rawData[CONTENT_INDEX] = value;
            }
        }

        public string Subject
        {
            get
            {
                return (string)_rawData[SUBJECT_INDEX];
            }

            set
            {
                _rawData[SUBJECT_INDEX] = value;
            }
        }

        public List<Attachment> Attachments
        {
            get
            {
                return Attachment.GetByEmailMessageId(Id);
            }
        }

        public int AttachmentsCount
        {
            get
            {
                return Attachments.Count;
            }
        }

        public bool Sent
        {
            get
            {
                return (bool)_rawData[SENT_INDEX];
            }

            set
            {
                _rawData[SENT_INDEX] = value;
            }
        }

        public DateTime? Date
        {
            get
            {
                return (DateTime?)_rawData[DATE_INDEX];
            }

            set
            {
                _rawData[DATE_INDEX] = value;
            }
        }
    }
}
