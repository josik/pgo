﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Text;
using Asfdfdfd.Api.Yahoo.Groups;

namespace PGOffline.Model
{
    public class DiscussionGroup : INotifyPropertyChanged
    {
        private const int DBINDEX_ID = 0;
        private const int DBINDEX_NAME = 1;
        private const int DBINDEX_CHARSET = 2;
        private const int DBINDEX_ACTUAL_EMAIL = 3;
        private const int DBINDEX_USER_ACCOUNT = 4;
        private const int DBINDEX_USERNAME = 5;
        private const int DBINDEX_PASSWORD = 6;
        private const int DBINDEX_LAST_MEMBER_UPDATE = 7;

        public static List<DiscussionGroup> GetAll()
        {
            List<object[]> rawGroups = DatabaseManager.Instance.ExecuteQueryAndGetValuesList("SELECT * FROM discussion_group");

            List<DiscussionGroup> groups = new List<DiscussionGroup>();

            foreach (object[] rawGroup in rawGroups)
            {
                DiscussionGroup group = LocalCache.Instance.GetDiscussionGroupById((long)rawGroup[0]);

                if (group != null)
                {
                    groups.Add(group);

                    continue;
                }

                group = new DiscussionGroup(rawGroup);

                LocalCache.Instance.Add(group);

                groups.Add(group);
            }

            return groups;
        }

        public static DiscussionGroup GetById(long id)
        {
            DiscussionGroup group = LocalCache.Instance.GetDiscussionGroupById(id);

            if (group != null)
            {
                return group;
            }

            object[] groupFromDb = DatabaseManager.Instance.ExecuteQueryAndGetValues(String.Format("SELECT * FROM discussion_group WHERE id = {0}", id));

            return LocalCache.Instance.Add(new DiscussionGroup(groupFromDb));
        }

        public static DiscussionGroup GetByName(string name)
        {
            DiscussionGroup group = LocalCache.Instance.GetDiscussionGroupByName(name);

            if (group != null)
            {
                return group;
            }

            object[] groupFromDb = DatabaseManager.Instance.ExecuteQueryAndGetValues(String.Format("SELECT * FROM discussion_group WHERE name = '{0}'", name));

            if (groupFromDb != null)
            {
                return LocalCache.Instance.Add(new DiscussionGroup(groupFromDb));
            }

            return null;
        }

        public static void Delete(DiscussionGroup group)
        {
            DeleteById(group.Id);
        }

        public static void DeleteById(long id)
        {
            using (SQLiteCommand cmd = DatabaseManager.Instance.CreateCommand("DELETE FROM discussion_group WHERE id = @group"))
            {
                cmd.Parameters.AddWithValue("@group", id);

                cmd.ExecuteNonQuery();
            }

            LocalCache.Instance.Remove(id);

            LocalCache.Instance.DropAllCaches();

            //DatabaseManager.Instance.ResetConnection();
        }

        public static void DeleteByName(string name)
        {
            using (SQLiteCommand cmd = DatabaseManager.Instance.CreateCommand("DELETE FROM discussion_group WHERE name = @name"))
            {
                cmd.Parameters.AddWithValue("@name", name);

                cmd.ExecuteNonQuery();
            }

            LocalCache.Instance.Remove(name);

            LocalCache.Instance.DropAllCaches();

            //DatabaseManager.Instance.ResetConnection();
        }

        private object[] _rawData;

        private GroupSettings _groupSettings;

        private const int MESSAGES_BUFFER_LENGTH = 10;

        public DiscussionGroup(object[] rawData)
        {
            _rawData = rawData;

            _groupSettings = new GroupSettings(this);

            _groupSettings.RefreshValuesForDiscussionGroup();
        }

        public DiscussionGroup(string name)
        {
            _rawData = new object[8];

            _rawData[DBINDEX_NAME] = name;

            _rawData[DBINDEX_CHARSET] = "UTF-8";

            _groupSettings = new GroupSettings(this);

            _groupSettings.SetDefaultValues();
        }

        public DiscussionGroup(string name, string charset, long actualEmail, long groupSettings, long userAccount, string username, string password, DateTime? lastMemberUpdate)
        {
            _rawData = new object[8];
            _rawData[DBINDEX_NAME] = name;
            _rawData[DBINDEX_CHARSET] = charset;
            _rawData[DBINDEX_ACTUAL_EMAIL] = actualEmail;
            _rawData[DBINDEX_USER_ACCOUNT] = userAccount;
            _rawData[DBINDEX_USERNAME] = username;
            _rawData[DBINDEX_PASSWORD] = password;
            _rawData[DBINDEX_LAST_MEMBER_UPDATE] = lastMemberUpdate;

            _groupSettings = new GroupSettings(this);
            _groupSettings.SetDefaultValues();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null && !String.IsNullOrWhiteSpace(propertyName))
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public bool IsHandlerSet
        {
            get
            {
                return this.PropertyChanged != null;
            }
        }

        public override string ToString()
        {
            return Name;
        }

        public GroupSettings GroupSettings
        {
            get
            {
                return _groupSettings;
            }
        }

        public List<Message> GetOfflineMessages()
        {
            string commandText = String.Format("SELECT id, number, date, subject, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time FROM group_message WHERE discussion_group = {0};", Id);

            List<object[]> valuesList = DatabaseManager.Instance.ExecuteQueryAndGetValuesList(commandText);

            List<Message> messages = new List<Message>();

            foreach (object[] valuesSet in valuesList)
            {
                messages.Add(new Message(valuesSet));
            }

            return messages;
        }

        public List<Message> GetAllFavoritesMessages()
        {
            string commandText = String.Format("select id, number, date, subject, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time from group_message where discussion_group = {0} AND favorite = 1;", Id);

            List<object[]> valuesList = DatabaseManager.Instance.ExecuteQueryAndGetValuesList(commandText);

            List<Message> messages = new List<Message>();

            foreach (object[] valuesSet in valuesList)
            {
                messages.Add(new Message(valuesSet));
            }

            return messages;
        }

        public List<Message> GetMessagesInRange(long startNumber, long stopNumber)
        {
            List<object[]> valuesList = DatabaseManager.Instance.ExecuteQueryAndGetValuesList(String.Format("SELECT id, number, date, subject, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time FROM group_message WHERE discussion_group = {0} AND number BETWEEN {1} AND {2}", Id, startNumber, stopNumber));

            List<Message> messages = new List<Message>();

            foreach (object[] valuesSet in valuesList)
            {
                messages.Add(new Message(valuesSet));
            }

            return messages;
        }

        public long GetMembersCount()
        {
            using (SQLiteCommand cmd = DatabaseManager.Instance.CreateCommand("SELECT COUNT(*) FROM person WHERE discussion_group = @id AND is_member = 'true'"))
            {
                cmd.Parameters.AddWithValue("@id", Id);

                return (long)cmd.ExecuteScalar();
            }
        }
        
        public long GetMessageCount()
        {
            using (SQLiteCommand cmd = DatabaseManager.Instance.CreateCommand("SELECT COUNT(*) FROM group_message WHERE discussion_group = @id"))
            {
                cmd.Parameters.AddWithValue("@id", Id);

                return (long)cmd.ExecuteScalar();
            }
        }

        public List<Message> GetMessagesInRange(DateTime startDate, DateTime stopDate)
        {
            List<Message> messages;

            using (SQLiteCommand cmd = DatabaseManager.Instance.CreateCommand("SELECT id, number, date, subject, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time FROM group_message WHERE discussion_group = @id AND date BETWEEN @startDate AND @stopDate"))
            {
                cmd.Parameters.AddWithValue("@id", Id);
            
                cmd.Parameters.AddWithValue("@stopDate", stopDate);
            
                cmd.Parameters.AddWithValue("@startDate", startDate);

                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    messages = new List<Message>();

                    while (reader.Read())
                    {
                        object[] values = new object[reader.FieldCount];
            
                        reader.GetValues(values);

                        messages.Add(new Message(values));
                    }
                }
            }

            return messages;
        }

        public List<Message> GetAllNewMessages()
        {
            string commandText = String.Format("SELECT id, number, date, subject, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time FROM group_message WHERE discussion_group = {0} AND unread = 1;", Id);

            List<object[]> valuesList = DatabaseManager.Instance.ExecuteQueryAndGetValuesList(commandText);

            List<Message> messages = new List<Message>();

            foreach (object[] valuesSet in valuesList)
            {
                messages.Add(new Message(valuesSet));
            }

            return messages;
        }

        public List<StatUnit> GetStatUnits()
        {
            List<StatUnit> statUnits = new List<StatUnit>();

            SQLiteConnection connection = DatabaseManager.Instance.GetConnection();
            
            string commandText = String.Format("SELECT person, date FROM group_message t1 WHERE t1.discussion_group = {0} ORDER BY person, date;", Id);

            using (SQLiteCommand command = DatabaseManager.Instance.CreateCommand(commandText))
            {
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    long currentPersonId = -1;

                    StatUnit currentStatUnit = null;

                    while (reader.Read())
                    {
                        long readedPersonId = reader.GetInt64(0);

                        DateTime? date;

                        try
                        {
                            date = reader.GetDateTime(1);
                        }
                        catch (InvalidCastException e)
                        {
                            date = null;
                        }

                        if (currentPersonId != readedPersonId)
                        {
                            currentPersonId = readedPersonId;

                            if (currentStatUnit != null)
                            {
                                currentStatUnit.Name = Person.GetById(currentPersonId).Name;

                                statUnits.Add(currentStatUnit);
                            }

                            currentStatUnit = new StatUnit
                            {
                                FirstMessageDate = date,
                                Id = readedPersonId
                            };
                        }

                        if (currentStatUnit == null) continue;

                        currentStatUnit.TotalPosts++;

                        currentStatUnit.LastMessageDate = date;
                    }

                    if (currentStatUnit == null) return statUnits;

                    currentStatUnit.Name = Person.GetById(currentPersonId).Name;

                    statUnits.Add(currentStatUnit);
                }
            }

            return statUnits;
        }

        public long MessagesCount
        {
            get
            {
                return (long)DatabaseManager.Instance.ExecuteQueryAndGetValue(String.Format("SELECT COUNT(*) FROM group_message WHERE discussion_group = {0}", Id), 0);
            }
        }

        public void RefreshMessagesCount()
        {
            RaisePropertyChanged("MessagesCount");
        }

        public List<Person> GetMembers()
        {
            return Person.GetByDiscussionGroup(this);
        }

        public List<Person> GetMembersJoinedBefore(DateTime date)
        {
            List<Person> persons;

            using (SQLiteCommand cmd = DatabaseManager.Instance.CreateCommand("SELECT * FROM person WHERE discussion_group = @id AND joined <= @date"))
            {
                cmd.Parameters.AddWithValue("@id", Id);

                cmd.Parameters.AddWithValue("@date", date);

                using (SQLiteDataReader dataReader = cmd.ExecuteReader())
                {
                    persons = new List<Person>();

                    while (dataReader.Read())
                    {
                        object[] values = new object[6];

                        dataReader.GetValues(values);

                        Person person = new Person(values);

                        LocalCache.Instance.Add(person);

                        persons.Add(person);
                    }
                }
            }

            return persons;
        }

        public long Id
        {
            get { return (long)_rawData[DBINDEX_ID]; }

            set { _rawData[DBINDEX_ID] = Convert.ToInt64(value); }
        }

        public string Name
        {
            get { return (string)_rawData[DBINDEX_NAME]; }

            set { _rawData[DBINDEX_NAME] = value; }
        }
        
        public string Charset
        {
            get { return (string)_rawData[DBINDEX_CHARSET]; }

            set { _rawData[DBINDEX_CHARSET] = value; }
        }

        public DateTime? LastMembersUpdate
        {
            get
            {
                try
                {
                    return (DateTime?) _rawData[DBINDEX_LAST_MEMBER_UPDATE];
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

            set { _rawData[DBINDEX_LAST_MEMBER_UPDATE] = value; }
        }

        public long FirstMessageNumber
        {
            get
            {
                try
                {
                    return (long)DatabaseManager.Instance.ExecuteQueryAndGetValue(
                        string.Format("SELECT number FROM group_message WHERE discussion_group = {0} ORDER BY number ASC LIMIT 1", Id), 0);
                }
                catch (InvalidCastException e)
                {
                    return 0;
                }
            }
        }

        public long LastMessageNumber
        {
            get
            {
                try
                {
                    return (long)DatabaseManager.Instance.ExecuteQueryAndGetValue(
                        string.Format("SELECT number FROM group_message WHERE discussion_group = {0} ORDER BY number DESC LIMIT 1", Id), 0);
                }
                catch (InvalidCastException e)
                {
                    return 0;
                }
            }
        }

        public UserAccount GetUserAccount()
        {
            string groupLogin = _groupSettings.GetValue("group_login");

            string groupPassword = _groupSettings.GetValue("group_password");

#if DEBUG
            Trace.Write(string.Format("/DEBUG/[{0}] ", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));
#endif
            Trace.Write(string.Format("[{0}] Logging in with account ", Name));

            if (groupLogin == String.Empty)
            {
                Trace.WriteLine(UserAccount.GetFirst().Name);

                return UserAccount.GetFirst();
            }

            Trace.WriteLine(groupLogin);

            return new UserAccount(groupLogin, groupPassword);
        }

        public Person AddMember(YahooGroupMember member)
        {
            Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Adding member {1}", Name, member.Name));

            //return AddMember(member.Name, _discussion_group.id, member.Joined, member.Email, true);

            return null;
        }
        public Person AddMember(string name, long discussionGroup, DateTime joined, string email, bool isMember)
        {
            var person = new Person(name, joined, email, isMember, discussionGroup);

            person.Insert();

            /*
            _membersBuffer.Add(person);
            
            if (_membersListCapacity <= _membersBuffer.Count)
                SaveMembers();
            */

            return person;
        }

        public void SaveMembers()
        {
            /*
            foreach (Person person in _membersBuffer)
            {
                person.UpdateOrInsert();
            }
            
            _membersBuffer.Clear();
            */
        }

        public Message AddMessage(YahooGroupMessage yahooGroupMessage)
        {
            return AddMessage(
                yahooGroupMessage.Author, yahooGroupMessage.Email, yahooGroupMessage.Number,
                yahooGroupMessage.Date, yahooGroupMessage.Title, Encoding.UTF8.GetBytes(yahooGroupMessage.Content),
                false, true, yahooGroupMessage.TopicId, yahooGroupMessage.ParentId, yahooGroupMessage.ThreadLevel,
                yahooGroupMessage.PrevInTopic, yahooGroupMessage.NextInTopic, yahooGroupMessage.PrevInTime,
                yahooGroupMessage.NextInTime, true, yahooGroupMessage.Attachments);
        }

        public delegate void MessageAddedEventHandler(Message message);

        public event MessageAddedEventHandler MessageAdded;

        public Message AddMessage(string author, string email, long number, DateTime? date, string subject, byte[] content, bool favorite, bool unread, long topicId, long parentId, long threadLevel, long prevInTopic, long nextInTopic, long prevInTime, long nextInTime, bool callMessageEdit = true, List<YahooGroupAttachment> attachments = null, string charset = null)
        {
            try
            {
                Person person = AddPerson(author, email, date);

                var message = new Message(number, date, subject, favorite, unread, person.Id, Id, topicId, parentId, threadLevel, prevInTopic, nextInTopic, prevInTime, nextInTime, charset);

                message.Insert(content);

                try
                {
                    AddAtachments(message.Id, attachments);
                }
                catch (Exception ex)
                {
                    throw;
                }
                
                return message;
            }
            catch (Exception ex)
            {
                throw;
            }
            
        }

        private Person AddPerson(string author, string email, DateTime? date)
        {
            //File.AppendAllText("c:/temp/debug_rawmessage.log", string.Format("Entered AddPerson{0}", Environment.NewLine));

            try
            {
                if (author == string.Empty && email == string.Empty)
                {
                    author = email = "(unknown)";
                };

                if (email == string.Empty)
                {
                    email = author;
                }

                Person existingSender = email.Length > 0 ? Person.GetByEmail(email) : Person.GetByName(author);

                if (existingSender != null) return existingSender;
                
                Person newSender = new Person(author, date, email, false, Id);

                newSender.Insert();

                return newSender;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void AddAtachments(long messageId, IEnumerable<YahooGroupAttachment> attachments = null)
        {
            if (attachments == null) return;

            foreach (YahooGroupAttachment attachment in attachments)
            {
                var existingAttachment = new Attachment(attachment.PathOnDisk, messageId);

                existingAttachment.Insert();
            }
        }

        public void MarkAllMessagesAsRead()
        {
            DatabaseManager.Instance.ExecuteQuery(string.Format("UPDATE group_message SET unread = 0 WHERE discussion_group = {0}", Id));

            RaisePropertyChanged("UnreadMessagesCount");
        }

        public void MarkAllMessagesAsUnread()
        {
            DatabaseManager.Instance.ExecuteQuery(String.Format("UPDATE group_message SET unread = 1 WHERE discussion_group = {0}", Id));

            RaisePropertyChanged("UnreadMessagesCount");
        }

        public int UnreadNonDeletedMessagesCount
        {
            get
            {
                return DatabaseManager.Instance.ExecuteQueryAndGetInt(string.Format("SELECT unread_count FROM discussion_group WHERE id = {0}", Id));
            }
        }

        public long Insert()
        {
            using (SQLiteCommand cmd = DatabaseManager.Instance.CreateCommand("INSERT INTO discussion_group (name, charset, actual_email, user_account, username, password, last_members_update) VALUES (@name, @charset, @actual_email, @user_account, @username, @password, @last_members_update)"))
            {
                try
                {
                    cmd.Parameters.AddWithValue("@name", _rawData[DBINDEX_NAME]);
                    cmd.Parameters.AddWithValue("@charset", _rawData[DBINDEX_CHARSET]);
                    cmd.Parameters.AddWithValue("@actual_email", _rawData[DBINDEX_ACTUAL_EMAIL]);
                    cmd.Parameters.AddWithValue("@user_account", _rawData[DBINDEX_USER_ACCOUNT]);
                    cmd.Parameters.AddWithValue("@username", _rawData[DBINDEX_USERNAME]);
                    cmd.Parameters.AddWithValue("@password", _rawData[DBINDEX_PASSWORD]);
                    cmd.Parameters.AddWithValue("@last_members_update", _rawData[DBINDEX_LAST_MEMBER_UPDATE] == DBNull.Value ? default(DateTime) : Convert.ToDateTime(_rawData[DBINDEX_LAST_MEMBER_UPDATE]));

                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw;
                }
                
            }

            _rawData[0] = DatabaseManager.Instance.GetLastInsertRowId(DatabaseManager.Instance.GetConnection());

            LocalCache.Instance.Add(this);

            return Id;
        }

        public void SaveGroupSettings()
        {
            _groupSettings.SaveSettings();
        }

        public void MessageAddedNotify(Message message)
        {
            if (MessageAdded != null)
            {
                MessageAdded(message);
            }
        }
    }

}
