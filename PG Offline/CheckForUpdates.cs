﻿using Microsoft.Windows.Controls;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace PGOffline
{
    public class CheckForUpdates
    {
        private static String _updaterModulePath;

        public CheckForUpdates()
        {
            //InitializeComponent();

            // Start the thread that will launch the updater in silent mode with 10 second delay.

            _updaterModulePath = PathResolver.GetAppDir() + "PG Offline 4 Updater.exe";

            Thread thread = new Thread(new ThreadStart(StartSilent));

            thread.Start();
            
            // Compute the updater.exe path relative to the application main module path

            //_updaterModulePath = Path.Combine(Application.StartupPath, "PG Offline 4 Updater.exe");
        }
        
        private void FileClose_Click(object sender, EventArgs e)
        {
            //this.Close();
        }

        /**
         * This handler should be associated with a menu item that launches the
         * updater's configuration dialog.
         */
        public void OptionsAutoUpdaters_Click(object sender, EventArgs e)
        {
            try
            {
                Process process = Process.Start(_updaterModulePath, "/configure");

                process.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to open update configuration window.");
            }
        }

        /**
         * This handler should be associated with a menu item that launches the
         * updater in check now mode (usually from  Help submenu)
         */
        public void HelpCheckForUpdates_Click(object sender, EventArgs e)
        {

#if DEBUG
            //File.AppendAllText(@"c:\temp\PgOffline_debug.log", _updaterModulePath);
#endif

            try
            {
                Process process = Process.Start(_updaterModulePath, "/checknow");

                process.Close();
            }
            catch (Exception)
            {
                var errMsg = "Unable to check for update.";

                if (!File.Exists(_updaterModulePath))
                {
                    errMsg += @" Could not find ""PG Offline 4 Updater.exe"" ";
                }

                Trace.WriteLine(errMsg);

                MessageBox.Show(errMsg);
            }
        }

        private static void StartSilent()
        {
            try
            {
                Thread.Sleep(10000);

                Process process = Process.Start(_updaterModulePath, "/silent");

                process.Close();
            }
            catch (Exception)
            {
                var errMsg = "Unable to check for update.";

                if (!File.Exists(_updaterModulePath))
                {
                    errMsg += @" Could not find ""PG Offline 4 Updater.exe"" ";
                }
                
                Trace.WriteLine(errMsg);
            }
        }

    }
}
