﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGOffline.Model;

namespace PGOffline
{
    [Serializable]
    public class MessagesSearchCriteria
    {
        public string subject;
        public string message;
        public long numberFrom;
        public long numberTo;
        public string persons;
        public List<string> groupNames = new List<string>();
    }
}
