﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Asfdfdfd.Api.Yahoo.Groups;
using PGOffline.Forms;
using PGOffline.Model;
using PGOffline.Properties;
using PGOffline.View;

namespace PGOffline.Presenter
{
    abstract class DownloadContentPresenter
    {
        protected IFormDownloadContentView _formDownloadContent;

        protected YahooGroup _currentYahooGroup;
        protected YahooGroupsServer _yahooServer;
        protected List<object> _rootElements;
        protected BackgroundWorker _downloadContentListWorker;
        protected BackgroundWorker _downloadContentWorker;
        protected FormIndeterminateProgress _formIndeterminateProgress;
        protected FormDeterminateProgress _formDeterminateProgress;

        public DownloadContentPresenter(IFormDownloadContentView formDownloadContent, DiscussionGroup currentDiscussionGroup, YahooGroupsServer yahooServer)
        {
            _formDownloadContent = formDownloadContent;
            _formDownloadContent.DisableDownloadControls();
            _formDownloadContent.GetContentListRequest += new GetContentListRequestEventHandler(OnGetContentListRequest);
            _formDownloadContent.DownloadContentRequest += new DownloadContentRequestEventHandler(OnDownloadContentRequest);
            _formDownloadContent.CloseDownloadContentFormRequest += new CloseDownloadContentFormRequestEventHandler(OnCloseDownloadContentFormRequest);

            _yahooServer = yahooServer;

            _yahooServer._userGroups.Add(_yahooServer.CreateGroup(currentDiscussionGroup.Name));

            _currentYahooGroup = _yahooServer._userGroups.Find(i => i.Name == currentDiscussionGroup.Name);

            _rootElements = new List<object>();
        }

        public void OnGetContentListRequest()
        {
            _downloadContentListWorker = new BackgroundWorker
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };

            _downloadContentListWorker.DoWork += new DoWorkEventHandler((sender, e) => DownloadContentList());

            _downloadContentListWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((sender, e) => OnDownloadContentListComplete());

            _formIndeterminateProgress = new FormIndeterminateProgress();

            _formIndeterminateProgress.SetTitle("Listing...");

            _formIndeterminateProgress.Show();

            _downloadContentListWorker.RunWorkerAsync();

            Trace.WriteLine(System.DateTime.Now.ToLongTimeString() + " " + string.Format("Getting content for group {0}", _currentYahooGroup.Name));
        }

        public void OnDownloadContentRequest()
        {
            _downloadContentWorker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };

            _downloadContentWorker.DoWork += new DoWorkEventHandler((sender, e) => DownloadContent());

            _downloadContentWorker.ProgressChanged += new ProgressChangedEventHandler((sender, e) => DownloadContentProgress(e.ProgressPercentage));

            _downloadContentWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((sender, e) => OnDownloadContentComplete());

            _formDeterminateProgress = new FormDeterminateProgress();

            _formDeterminateProgress.Show();
            
            _formDownloadContent.DisableDownloadControls();

            _downloadContentWorker.RunWorkerAsync();
        }

        public void OnCloseDownloadContentFormRequest()
        {
            if (_downloadContentListWorker != null)
                _downloadContentListWorker.CancelAsync();

            if (_downloadContentWorker != null)
                _downloadContentWorker.CancelAsync();

            if (_formDeterminateProgress != null)
                _formDeterminateProgress.Close();

            if (_formIndeterminateProgress != null)
                _formIndeterminateProgress.Close();

            _formDownloadContent.CloseForm();
        }

        protected abstract void DownloadContentList();

        protected void OnDownloadContentListComplete()
        {
            _formIndeterminateProgress.Close();

            if (_downloadContentListWorker.CancellationPending)
            {
                Trace.WriteLine("Listing interrupted.");

                return;
            }

            _formDownloadContent.PopulateTreeView(_rootElements);            

            if (_rootElements.Count == 0)
            {
                Trace.WriteLine("There are no items available for this group.");

                System.Windows.MessageBox.Show("There are no items available for this group.");
            }
            else
            {
                _formDownloadContent.EnableDownloadControls();
            }

            Trace.WriteLine("Listing complete.");
        }

        protected abstract void DownloadContent();

        protected void OnDownloadContentComplete()
        {
            _formDeterminateProgress.Close();

            _formDownloadContent.EnableDownloadControls();

            _formDownloadContent.CloseForm();

            Trace.WriteLine(System.DateTime.Now.ToLongTimeString() + " " + string.Format("Getting content for group {0} completed", _currentYahooGroup.Name));
        }

        protected void DownloadContentProgress(int progerssPercentage)
        {
            _formDeterminateProgress.SetProgress(progerssPercentage);
        }
    }
}
