﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Asfdfdfd.Api.Yahoo.Groups;
using PGOffline.Model;
using PGOffline.View;

namespace PGOffline.Presenter
{
    class FormDownloadPhotosPresenter : DownloadContentPresenter
    {
        public FormDownloadPhotosPresenter(IFormDownloadContentView formDownloadPhotos, DiscussionGroup currentDiscussionGroup, YahooGroupsServer yahooServer) :
            base(formDownloadPhotos, currentDiscussionGroup, yahooServer)
        {
            _formDownloadContent.SetWindowTitle("Download photos");
        }

        protected override void DownloadContentList()
        {
            try
            {
                UserAccount userAccount = UserAccount.GetFirst();

                if (!_yahooServer.Login(userAccount.Name, userAccount.Password)) return;

                _rootElements.Clear();

                _rootElements.AddRange(_currentYahooGroup.ReceiveAlbums(_downloadContentListWorker));
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error. " + ex.Message);
            }
        }

        protected override void DownloadContent()
        {
            _currentYahooGroup.DownloadPhotos(_formDownloadContent.GetSelectedItems().Cast<YahooGroupAlbumOfPhotos>(), _downloadContentWorker);
        }
    }
}
