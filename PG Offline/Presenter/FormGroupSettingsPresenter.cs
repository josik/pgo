﻿using PGOffline.Model;
using PGOffline.View;
using System;

namespace PGOffline.Presenter
{
    class FormGroupSettingsPresenter
    {
        IFormGroupSettingsView _formGroupSettingsView;
        DiscussionGroup _discussionGroup;

        public FormGroupSettingsPresenter(IFormGroupSettingsView formGroupSettingsView, DiscussionGroup discussionGroup)
        {
            _formGroupSettingsView = formGroupSettingsView;
            _discussionGroup = discussionGroup;

            _formGroupSettingsView.GroupSettingsSaveRequest += new GroupSettingsSaveRequestEventHandler(OnGroupSettingsSaveRequest);
            _formGroupSettingsView.GroupSettingsLoadRequest += new GroupSettingsLoadRequestEventHandler(OnGroupSettingsLoadRequest);
            _formGroupSettingsView.SaveActiveTabRequest += new SaveActiveTabRequestEventHandler(OnSaveAcitveTabRequest);
            _formGroupSettingsView.BrowseAttachmentsFolderRequest += new BrowseAttachmentsFolderRequestEventHandler(OnBrowseAttachmentsFolderRequest);
            _formGroupSettingsView.BrowseOutlookFolderRequest += new BrowseOutlookFolderRequestEventHandler(OnBrowseOutlookFolderRequest);
        }

        void OnGroupSettingsSaveRequest()
        {
            if (_formGroupSettingsView.UpdateAttachmentsLinks == true)
            {
                string messageText = string.Format("This will update attachment links for {0}.\n", _discussionGroup.Name);

                messageText += string.Format("Make sure to move all your attachments for {0} to this folder.\n", _discussionGroup.Name);

                messageText += "Are you sure you want to continue?";

                System.Windows.MessageBoxResult result = System.Windows.MessageBox.Show(messageText, "PG Offline", System.Windows.MessageBoxButton.YesNo);

                if (result == System.Windows.MessageBoxResult.Yes)
                {
                    DatabaseManager.Instance.UpdateAttachmentsLinks(_formGroupSettingsView.AttachmentsFolder, DiscussionGroup.GetById(_discussionGroup.Id));
                }
                else
                {
                    return;
                }
            }

            PrepareGroupSettingsToSave();

            SaveSettings();

            _formGroupSettingsView.CloseWindow();
        }

        void OnGroupSettingsLoadRequest()
        {
            DisplayGroupSettings();
        }

        void DisplayGroupSettings()
        {
            _formGroupSettingsView.StartMessageNumber = Int32.Parse(_discussionGroup.GroupSettings.GetValue("start_download_from_message"));
            _formGroupSettingsView.EndMessageNumber = Int32.Parse(_discussionGroup.GroupSettings.GetValue("end_download_at_message"));
            _formGroupSettingsView.IncludeAttachmentsPattern = _discussionGroup.GroupSettings.GetValue("include_attachments_pattern");
            _formGroupSettingsView.ExcludeAttachmentsPattern = _discussionGroup.GroupSettings.GetValue("exclude_attachments_pattern");
            _formGroupSettingsView.AttachmentsFolder = _discussionGroup.GroupSettings.GetValue("attachments_folder");
            _formGroupSettingsView.OutlookFolder = _discussionGroup.GroupSettings.GetValue("outlook_folder");
            _formGroupSettingsView.AttachmentsLimitationSize = _discussionGroup.GroupSettings.GetAsInt("attachments_limitation_size");
            _formGroupSettingsView.AttachmentsLimitationType = _discussionGroup.GroupSettings.GetValue("attachments_limitation_type");
            _formGroupSettingsView.UseOldDesignFormat = _discussionGroup.GroupSettings.GetAsBoolean("use_old_design_format");
            _formGroupSettingsView.SkipDuringGetMembersForAllGroups = _discussionGroup.GroupSettings.GetAsBoolean("skip_during_get_members_for_all_groups");
            _formGroupSettingsView.SkipDuringRefreshAllGroups = _discussionGroup.GroupSettings.GetAsBoolean("skip_during_refresh_all_groups");
            _formGroupSettingsView.GroupLogin = _discussionGroup.GroupSettings.GetValue("group_login");
            _formGroupSettingsView.GroupPassword = _discussionGroup.GroupSettings.GetValue("group_password");
            _formGroupSettingsView.ActiveTab = _discussionGroup.GroupSettings.GetAsInt("active_tab");
            _formGroupSettingsView.DisableAttachementsDownloading = _discussionGroup.GroupSettings.GetAsBoolean("disable_attachements_downloading");
            _formGroupSettingsView.SetGroupName(_discussionGroup.Name);

            if (_formGroupSettingsView.GroupLogin == "")
                _formGroupSettingsView.GroupLogin = "(default)";
            if (_formGroupSettingsView.GroupPassword == "")
                _formGroupSettingsView.GroupPassword = "(default)";
            }

        void SaveSettings()
        {
            _discussionGroup.GroupSettings.SaveSettings();
        }

        void PrepareGroupSettingsToSave()
        {
            if (_formGroupSettingsView.GroupLogin == "" || _formGroupSettingsView.GroupLogin == "(default)")
                _formGroupSettingsView.GroupLogin = "";
            if (_formGroupSettingsView.GroupPassword == "" || _formGroupSettingsView.GroupPassword == "(default)")
                _formGroupSettingsView.GroupPassword = "";

            _discussionGroup.GroupSettings.SetValue("start_download_from_message", _formGroupSettingsView.StartMessageNumber.ToString());
            _discussionGroup.GroupSettings.SetValue("end_download_at_message", _formGroupSettingsView.EndMessageNumber.ToString());
            _discussionGroup.GroupSettings.SetValue("include_attachments_pattern", _formGroupSettingsView.IncludeAttachmentsPattern);
            _discussionGroup.GroupSettings.SetValue("exclude_attachments_pattern", _formGroupSettingsView.ExcludeAttachmentsPattern);
            _discussionGroup.GroupSettings.SetValue("attachments_folder", _formGroupSettingsView.AttachmentsFolder);
            _discussionGroup.GroupSettings.SetValue("outlook_folder", _formGroupSettingsView.OutlookFolder);
            _discussionGroup.GroupSettings.SetValue("attachments_limitation_size", _formGroupSettingsView.AttachmentsLimitationSize.ToString());
            _discussionGroup.GroupSettings.SetValue("attachments_limitation_type", _formGroupSettingsView.AttachmentsLimitationType);
            _discussionGroup.GroupSettings.SetValue("use_old_design_format", _formGroupSettingsView.UseOldDesignFormat.ToString());
            _discussionGroup.GroupSettings.SetValue("skip_during_get_members_for_all_groups", _formGroupSettingsView.SkipDuringGetMembersForAllGroups.ToString());
            _discussionGroup.GroupSettings.SetValue("skip_during_refresh_all_groups", _formGroupSettingsView.SkipDuringRefreshAllGroups.ToString());
            _discussionGroup.GroupSettings.SetValue("group_login", _formGroupSettingsView.GroupLogin);
            _discussionGroup.GroupSettings.SetValue("group_password", _formGroupSettingsView.GroupPassword);
            _discussionGroup.GroupSettings.SetValue("disable_attachements_downloading", _formGroupSettingsView.DisableAttachementsDownloading.ToString());
        }

        void OnSaveAcitveTabRequest(int index)
        {
            _discussionGroup.GroupSettings.SetValue("active_tab", index.ToString());
            _discussionGroup.GroupSettings.SaveSettings();
        }

        void OnBrowseAttachmentsFolderRequest()
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();

            System.Windows.Forms.DialogResult result = dialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                _formGroupSettingsView.AttachmentsFolder = dialog.SelectedPath;
            }        
        }

        void OnBrowseOutlookFolderRequest()
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();

            System.Windows.Forms.DialogResult result = dialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                _formGroupSettingsView.OutlookFolder = dialog.SelectedPath;
            }        
        }
    }
}
