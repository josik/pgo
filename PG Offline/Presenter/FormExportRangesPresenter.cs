﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.View;

namespace PGOffline.Presenter
{
    class FormExportRangesPresenter
    {
        IFormRanges _formRanges;
        ExportUnit _exportUnit;

        public delegate void SetRangeCompleteEventHandler();
        public event SetRangeCompleteEventHandler SetRangeComplete;

        public FormExportRangesPresenter(IFormRanges formRanges, ExportUnit exportUnit)
    	{
            _formRanges = formRanges;
            _exportUnit = exportUnit;

            _formRanges.SetRangeRequest += new SetRangeRequestEventHandler(OnSetRangeComplete);
	    }

        void OnSetRangeComplete(int leftEdge, int rightEdge)
        {
            if (leftEdge < _exportUnit.FirstMessageNumber)
                _exportUnit.StartExportFromNumber = _exportUnit.FirstMessageNumber;
            else
                _exportUnit.StartExportFromNumber = leftEdge;

            if (_exportUnit.LastMessageNumber < rightEdge)
                _exportUnit.EndExportAtNumber = _exportUnit.LastMessageNumber;
            else
                _exportUnit.EndExportAtNumber = rightEdge;

            SetRangeComplete();
        }
    }
}
