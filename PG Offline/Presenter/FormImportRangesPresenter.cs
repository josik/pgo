﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.View;

namespace PGOffline.Presenter
{
    class FormImportRangesPresenter
    {
        IFormRanges _formRanges;
        ImportUnit _importUnit;

        public delegate void SetRangeCompleteEventHandler();
        public event SetRangeCompleteEventHandler SetRangeComplete;

        public FormImportRangesPresenter (IFormRanges formRanges, ImportUnit importUnit)
    	{
            _formRanges = formRanges;
            _importUnit = importUnit;

            _formRanges.SetRangeRequest += new SetRangeRequestEventHandler(OnSetRangeComplete);
	    }

        void OnSetRangeComplete(int leftEdge, int rightEdge)
        {
            if (leftEdge < _importUnit.FirstMessageNumber)
                _importUnit.StartImportFromNumber = _importUnit.FirstMessageNumber;
            else
                _importUnit.StartImportFromNumber = leftEdge;

            if (_importUnit.LastMessageNumber < rightEdge)
                _importUnit.EndImportAtNumber = _importUnit.LastMessageNumber;
            else
                _importUnit.EndImportAtNumber = rightEdge;

            SetRangeComplete();
        }
    }
}
