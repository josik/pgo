﻿using System.Collections.Generic;
using Asfdfdfd.Api.Yahoo.Groups;
using PGOffline.Model;
using PGOffline.View;

namespace PGOffline.Presenter
{
    class FormSelectGroupsPresenter
    {
        public delegate void GroupsListChangedEventHandler();
        public event GroupsListChangedEventHandler GroupsListChanged;

        SortedList<string, string> _localGroups = new SortedList<string, string>();
        SortedList<string, string> _remoteGroups = new SortedList<string, string>();

        public FormSelectGroupsPresenter(IFormSelectGroupsView formSelectGroupsView, List<YahooGroup> remoteGroups, List<DiscussionGroup> localGroups)
        {
            IFormSelectGroupsView formSelectGroupsView1 = formSelectGroupsView;
            formSelectGroupsView1.AddLocalDiscussionGroupRequest += new AddLocalDiscussionGroupRequestEventHandler(OnAddLocalDiscussionGroupRequest);
            formSelectGroupsView1.RemoveLocalDiscussionGroupRequest += new RemoveLocalDiscussionGroupRequestEventHandler(OnRemoveLocalDiscussionGroupRequest);
            formSelectGroupsView1.SaveGroupsListChangesRequest += new SaveGroupsListChangesRequestEventHandler(OnSaveGroupsListChangesRequest);

            
            

            foreach (var localGroup in localGroups)
            {
                _localGroups.Add(localGroup.Name, localGroup.Name);
            }

            foreach (var remoteGroup in remoteGroups)
            {
                if (!_localGroups.ContainsKey(remoteGroup.Name) && !_remoteGroups.ContainsKey(remoteGroup.Name))
                {
                    _remoteGroups.Add(remoteGroup.Name, remoteGroup.Name);
                }
            }

            formSelectGroupsView1.SetLocalGroups(_localGroups);
            formSelectGroupsView1.SetRemoteGroups(_remoteGroups);
        }

        void OnSaveGroupsListChangesRequest()
        {
            // handle adding groups
            foreach (string localGroupName in _localGroups.Values)
            {
                // record newly added groups to database
                DiscussionGroup discussionGroup = DiscussionGroup.GetByName(localGroupName);

                if (discussionGroup != null) continue;

                discussionGroup = new DiscussionGroup(localGroupName);
                discussionGroup.Insert();
                discussionGroup.SaveGroupSettings();
            }

            // handle removing groups
            foreach (string remoteGroupName in _remoteGroups.Values)
                DiscussionGroup.DeleteByName(remoteGroupName);

            GroupsListChanged();
        }

        void OnAddLocalDiscussionGroupRequest(string groupName)
        {
            if (groupName == null)
                return;

            _remoteGroups.Remove(groupName);
            _localGroups.Add(groupName, groupName);
        }

        void OnRemoveLocalDiscussionGroupRequest(string groupName)
        {
            if (groupName == null)
                return;

            _localGroups.Remove(groupName);
            _remoteGroups.Add(groupName, groupName);
        }
    }
}
