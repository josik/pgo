﻿using PGOffline.Model;
using PGOffline.View;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace PGOffline.Presenter
{
    class FormExportToFolderPresenter
    {
        IFormExportToFolder _view;

        IExportManager _exportManager;

        public FormExportToFolderPresenter(IFormExportToFolder view)
        {
            _view = view;

            _view.SetDiscussionGroups(DiscussionGroup.GetAll());

            _view.ExportRequest += new ExportRequestEventHandler(view_ExportRequest);
        }

        void view_ExportRequest(string databasePath, List<ExportUnit> units, ExportSettings settings)
        {
            if (databasePath == null) return;

            Directory.CreateDirectory(path: Path.GetDirectoryName(databasePath));

            if (!File.Exists(databasePath))
            {
                _exportManager = new ExportDb3Manager(databasePath, units, settings);

                _exportManager.ExportDataAsync();
            }
            else
            {
                MessageBox.Show(@"File already exists. Please input a new file name.", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
