﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;
using Microsoft.Windows.Controls;
using PGOffline.Model;
using PGOffline.View;

namespace PGOffline.Presenter
{
    class FormImportFromFolderPresenter
    {
        public delegate void ImportCompleteEventHandler();
        public delegate void UpdateProgressEventHandler();

        public event ImportCompleteEventHandler ImportComplete;
        public event UpdateProgressEventHandler UpdateProgressRequest;

        public BackgroundWorker ImportDataWorker { get; set; }

        public MediaElement StillWorkingSpinner { get; set; }

        IFormImportFromFolder _formImportFromFolder;

        IImportManager _importManager;

        private ImportDb3Manager _importDb3Manager;

        private ImportMdbManager _importMdbManager;

        public FormImportFromFolderPresenter(IFormImportFromFolder formImportFromFolder)
        {
            _formImportFromFolder = formImportFromFolder;

            _formImportFromFolder.BrowseDatabaseRequest += new BrowseDatabaseRequestEventHandler(OnBrowseDatabaseRequest);
            _formImportFromFolder.GetGroupsListRequest += new GetGroupsListRequestEventHandler(OnGetGroupsListRequest);
            _formImportFromFolder.ImportDataRequest += new ImportDataRequestEventHandler(OnImportDataRequest);
            _formImportFromFolder.ChangeRangeRequest += new ChangeRangeRequestEventHandler(OnChangeRangeRequest);
            _formImportFromFolder.ImportCancelRequest += new ImportCancelRequestEventHander(OnImportCancelRequest);
            _formImportFromFolder.SaveImportFromMdbOptionsRequest += new SaveImportFromMdbOptionsRequestEventHandler(OnSaveOptionsRequet);
            _formImportFromFolder.LoadImportFromMdbOptionsRequest += new LoadImportFromMdbOptionsRequestEventHandler(OnLoadOptionsRequest);
        }


        //void OnUpdateProgressRequest(double progressPercentage)
        //{
        //    _formImportFromFolder.UpdateProgress(progressPercentage);
        //}

        private void OnUpdateProgressRequest(double progressPercentage)
        {
            if (_importManager != null)
            {
                //_importManager
                _formImportFromFolder.UpdateProgress(progressPercentage);
            }
        }

        void OnBrowseDatabaseRequest()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
            {
                FileName = "",
                DefaultExt = ".mdb",
                Filter = "PGOffline 4 database file (.db3)|*.db3|Microsoft Access database file (.mdb)|*.mdb"
            };

            Nullable<bool> result = dlg.ShowDialog();

            if (result != true) return;

            _formImportFromFolder.SetDatabasePath(dlg.FileName);

            OnGetGroupsListRequest(dlg.FileName);
        }

        void OnGetGroupsListRequest(string databasePath)
        {
            List<ImportUnit> units = null;

            if (databasePath.EndsWith(".db3", true, System.Globalization.CultureInfo.CurrentCulture))
            {
                var importManager = new ImportSqliteManager(databasePath);

                units = importManager.GetImportUnits(databasePath);
            }
            else if (databasePath.EndsWith(".mdb", true, System.Globalization.CultureInfo.CurrentCulture))
            {
                var importManager = new ImportMdbManager(databasePath);

                units = importManager.GetImportUnits(databasePath);
            }

            _formImportFromFolder.DisplayGroups(units);
        }

        void OnImportDataRequest(List<ImportUnit> units, string databasePath, ImportSettings settings)
        {
            if (databasePath.EndsWith(".db3", true, System.Globalization.CultureInfo.CurrentCulture))
            {
                _importManager = new ImportSqliteManager(databasePath, units, settings);
            }
            else if (databasePath.EndsWith(".mdb", true, System.Globalization.CultureInfo.CurrentCulture))
            {
                _importManager = new ImportMdbManager(databasePath, units, settings);
            }
            else
            {
                // TODO: or throw exception of unsupported import file type
                return;
            }

            _importManager.ImportComplete += new ImportCompletedEventHandler(OnImportComplete);

            _formImportFromFolder.DisableImportButton();

            // can't do a second import;   doing an import, leaving the window open, and doing another import seems to have the dataworker in a canceled state


            if (ImportDataWorker != null)
            {
                ImportDataCancelAsync();
            }

            _formImportFromFolder.EnableProgressBar();

            ImportDataWorker = new BackgroundWorker { WorkerSupportsCancellation = true, WorkerReportsProgress = true };

            ImportDataWorker.DoWork += new DoWorkEventHandler((sender, e) => _importManager.ImportData());

            ImportDataWorker.ProgressChanged += new ProgressChangedEventHandler((sender, e) => _formImportFromFolder.UpdateProgress(e.ProgressPercentage));
            
            ImportDataWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((sender, e) => _importManager.ImportDataComplete());

            _importManager.ImportDataWorker = ImportDataWorker;

            _importManager.StillWorkingSpinner = StillWorkingSpinner;

            ImportDataWorker.RunWorkerAsync();

            _formImportFromFolder.DisableProgressBar();
        }

        private void ImportDataCancelAsync()
        {
            ImportDataWorker.CancelAsync();
        }

        void OnImportComplete()
        {
            ImportComplete();
            
            _formImportFromFolder.UpdateProgress(0);

            DatabaseManager.Instance.ResetConnection();

            _formImportFromFolder.EnableImportButton();

            MessageBox.Show(_importManager.ProgressPercentage == 100 ? "Import is complete." : "Import canceled.");
        }

        void OnChangeRangeRequest(ImportUnit importUnit)
        {
            if (importUnit == null)
            {
                return;
            }

            FormRanges formRanges = new FormRanges(importUnit.StartImportFromNumber, importUnit.EndImportAtNumber);

            FormImportRangesPresenter formRangesPresenter = new FormImportRangesPresenter(formRanges, importUnit);

            formRangesPresenter.SetRangeComplete += new FormImportRangesPresenter.SetRangeCompleteEventHandler(OnSetRangeComplete);
            
            formRanges.ShowDialog();
        }

        void OnSetRangeComplete()
        {
            _formImportFromFolder.RefreshImportUnits();
        }

        void OnImportCancelRequest()
        {
            if (_importManager != null)
            {
                ImportDataCancelAsync();
            }

            _formImportFromFolder.EnableImportButton();
        }

        void OnSaveOptionsRequet()
        {
            Options.Instance.Set("import_from_mdb_options_copy_attachments", _formImportFromFolder.CopyAttachments);
            Options.Instance.Set("import_from_mdb_options_import_favorite_status", _formImportFromFolder.ImportFavoriteStatus);
            Options.Instance.Set("import_from_mdb_options_remember_last_message_number", _formImportFromFolder.RememberLastMessageNumber);
            Options.Instance.Set("import_from_mdb_options_import_type", (int)_formImportFromFolder.ImportType);
            Options.Instance.Set("import_from_mdb_options_database_path", _formImportFromFolder.DatabasePath);

            Options.Instance.SaveOptions();
        }

        void OnLoadOptionsRequest()
        {
            _formImportFromFolder.CopyAttachments = Options.Instance.GetAsBoolean("import_from_mdb_options_copy_attachments");
            _formImportFromFolder.ImportFavoriteStatus = Options.Instance.GetAsBoolean("import_from_mdb_options_import_favorite_status");
            _formImportFromFolder.RememberLastMessageNumber = Options.Instance.GetAsBoolean("import_from_mdb_options_remember_last_message_number");
            _formImportFromFolder.ImportType = (ImportType)Options.Instance.GetAsInt("import_from_mdb_options_import_type");
            _formImportFromFolder.DatabasePath = Options.Instance.GetAsString("import_from_mdb_options_database_path");
        }
    }
}
