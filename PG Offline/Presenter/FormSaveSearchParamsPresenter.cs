﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.View;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace PGOffline.Presenter
{
    class FormSaveSearchParamsPresenter
    {
        IFormSaveSearchParams _view;

        string _params;

        public FormSaveSearchParamsPresenter(IFormSaveSearchParams view, MessagesSearchCriteria criteria)
        {
            _params = SerializeParams(criteria);

            _view = view;

            _view.SaveSearchParams += new SaveSearchParamsHandler(_view_SaveSearchParams);

            _view.ShowDialog();
        }

        string SerializeParams(MessagesSearchCriteria criteria)
        {
            var serializer = new XmlSerializer(typeof(MessagesSearchCriteria));

            XmlWriterSettings settings = new XmlWriterSettings();

            settings.Encoding = new UnicodeEncoding(false, false); // no BOM in a .NET string

            settings.Indent = false;

            settings.OmitXmlDeclaration = false;

            using (StringWriter textWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(xmlWriter, criteria);
                }

                return textWriter.ToString();
            }
        }

        void _view_SaveSearchParams(string name)
        {
            var cmd = DatabaseManager.Instance.CreateCommand("INSERT INTO `search_param` (`version`, `name`, `value`) VALUES (@version, @name, @value)");

            cmd.Parameters.AddWithValue("@version", 1);

            cmd.Parameters.AddWithValue("@name", name);

            cmd.Parameters.AddWithValue("@value", _params);

            cmd.ExecuteNonQuery();

            _view.Close();
        }
    }
}
