﻿using PGOffline.View;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace PGOffline.Presenter
{
    class FormLoadSearchParamsPresenter
    {
        public delegate void ParamsLoadedHandler(MessagesSearchCriteria criteria);

        public event ParamsLoadedHandler ParamsLoaded;

        IFormLoadSearchParams _view;

        ObservableCollection<string> _names = new ObservableCollection<string>();

        public FormLoadSearchParamsPresenter(IFormLoadSearchParams view)
        {
            var cmd = DatabaseManager.Instance.CreateCommand("SELECT `name` FROM `search_param`");

            DatabaseManager.Instance.ExecuteCommandAndGetValuesList(cmd).ForEach((object[] values) => _names.Add((string)values[0]));

            _view = view;

            _view.LoadSearchParam += new LoadSearhParamHandler(_view_LoadSearchParams);

            _view.RemoveSearchParam += new RemoveSearchParamHandler(_view_RemoveSearchParam);

            _view.SearchParamsNames = _names;
        }

        void _view_RemoveSearchParam(string name)
        {
            if (name == null) return;

            _names.Remove(name);

            var cmd = DatabaseManager.Instance.CreateCommand("DELETE FROM `search_param` WHERE `name` = @name");

            cmd.Parameters.AddWithValue("@name", name);

            cmd.ExecuteNonQuery();
        }

        MessagesSearchCriteria LoadCriteria(string name)
        {
            var cmd = DatabaseManager.Instance.CreateCommand("SELECT `value` FROM `search_param` WHERE `name` = @name");

            cmd.Parameters.AddWithValue("@name", name);

            var xmlValue = (string)cmd.ExecuteScalar();

            XmlSerializer serializer = new XmlSerializer(typeof(MessagesSearchCriteria));

            XmlReaderSettings settings = new XmlReaderSettings();
            // No settings need modifying here

            using (StringReader textReader = new StringReader(xmlValue))
            {
                using (XmlReader xmlReader = XmlReader.Create(textReader, settings))
                {
                    return (MessagesSearchCriteria)serializer.Deserialize(xmlReader);
                }
            }
        }

        void _view_LoadSearchParams(string name)
        {
            if (name == null) return;

            ParamsLoaded(LoadCriteria(name));

            _view.Close();
        }
    }
}
