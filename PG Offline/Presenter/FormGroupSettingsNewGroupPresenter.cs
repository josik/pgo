﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using PGOffline.View;
using PGOffline.Model;

namespace PGOffline.Presenter
{
    class FormGroupSettingsNewGroupPresenter
    {
        public delegate void NewGroupCreatedEventHandler();

        public event NewGroupCreatedEventHandler NewGroupCreated;

        IFormGroupSettingsView _formGroupSettingsView;

        DiscussionGroup _discussionGroup;

        public FormGroupSettingsNewGroupPresenter(IFormGroupSettingsView formGroupSettingsView)
        {
            _discussionGroup = null;

            _formGroupSettingsView = formGroupSettingsView;

            _formGroupSettingsView.SetHeader("New Group");

            _formGroupSettingsView.SetGroupNameEditability(true);

            _formGroupSettingsView.GroupSettingsSaveRequest += new GroupSettingsSaveRequestEventHandler(OnGroupSettingsSaveRequest);

            _formGroupSettingsView.GroupSettingsLoadRequest += new GroupSettingsLoadRequestEventHandler(OnGroupSettingsLoadRequest);

            _formGroupSettingsView.BrowseAttachmentsFolderRequest += new BrowseAttachmentsFolderRequestEventHandler(OnBrowseAttachmentsFolderRequest);

            _formGroupSettingsView.BrowseOutlookFolderRequest += new BrowseOutlookFolderRequestEventHandler(OnBrowseOutlookFolderRequest);
        }

        void OnGroupSettingsSaveRequest()
        {
            if (_formGroupSettingsView.GroupName == String.Empty)
            {
                System.Windows.MessageBox.Show("Group Name can't be empty.", "PG Offline");

                return;
            }

            SaveGroupSettings();

            _formGroupSettingsView.CloseWindow();
        }

        void SaveGroupSettings()
        {
            _discussionGroup = new DiscussionGroup(_formGroupSettingsView.GroupName);

            _discussionGroup.Insert();

            _discussionGroup.GroupSettings.SetValue("start_download_from_message", _formGroupSettingsView.StartMessageNumber.ToString());
            _discussionGroup.GroupSettings.SetValue("end_download_at_message", _formGroupSettingsView.EndMessageNumber.ToString());
            _discussionGroup.GroupSettings.SetValue("include_attachments_pattern", _formGroupSettingsView.IncludeAttachmentsPattern);
            _discussionGroup.GroupSettings.SetValue("exclude_attachments_pattern", _formGroupSettingsView.ExcludeAttachmentsPattern);
            _discussionGroup.GroupSettings.SetValue("attachments_folder", _formGroupSettingsView.AttachmentsFolder);
            _discussionGroup.GroupSettings.SetValue("outlook_folder", _formGroupSettingsView.OutlookFolder);
            _discussionGroup.GroupSettings.SetValue("attachments_limitation_size", _formGroupSettingsView.AttachmentsLimitationSize.ToString());
            _discussionGroup.GroupSettings.SetValue("attachments_limitation_type", _formGroupSettingsView.AttachmentsLimitationType);
            _discussionGroup.GroupSettings.SetValue("use_old_design_format", _formGroupSettingsView.UseOldDesignFormat.ToString());
            _discussionGroup.GroupSettings.SetValue("skip_during_get_members_for_all_groups", _formGroupSettingsView.SkipDuringGetMembersForAllGroups.ToString());
            _discussionGroup.GroupSettings.SetValue("skip_during_refresh_all_groups", _formGroupSettingsView.SkipDuringRefreshAllGroups.ToString());
            _discussionGroup.GroupSettings.SetValue("group_login", _formGroupSettingsView.GroupLogin);
            _discussionGroup.GroupSettings.SetValue("group_password", _formGroupSettingsView.GroupPassword);
            _discussionGroup.GroupSettings.SetValue("disable_attachements_downloading", _formGroupSettingsView.DisableAttachementsDownloading.ToString());

            _discussionGroup.GroupSettings.SaveSettings();

            NewGroupCreated();
        }

        void OnGroupSettingsLoadRequest()
        {
            Dictionary<string, string> defaultSettings = GroupSettings.GetDefaultGroupSettings();
            
            _formGroupSettingsView.StartMessageNumber = Int32.Parse(defaultSettings["start_download_from_message"]);
            _formGroupSettingsView.EndMessageNumber = Int32.Parse(defaultSettings["end_download_at_message"]);
            _formGroupSettingsView.IncludeAttachmentsPattern = defaultSettings["include_attachments_pattern"];
            _formGroupSettingsView.AttachmentsFolder = defaultSettings["attachments_folder"];
            _formGroupSettingsView.AttachmentsLimitationType = defaultSettings["attachments_limitation_type"];
            _formGroupSettingsView.AttachmentsLimitationSize = Int32.Parse(defaultSettings["attachments_limitation_size"]);
            _formGroupSettingsView.OutlookFolder = defaultSettings["outlook_folder"];
            _formGroupSettingsView.UseOldDesignFormat = Boolean.Parse(defaultSettings["use_old_design_format"]);
            _formGroupSettingsView.SkipDuringGetMembersForAllGroups = Boolean.Parse(defaultSettings["skip_during_get_members_for_all_groups"]);
            _formGroupSettingsView.SkipDuringRefreshAllGroups = Boolean.Parse(defaultSettings["skip_during_refresh_all_groups"]);
            _formGroupSettingsView.DisableAttachementsDownloading = Boolean.Parse(defaultSettings["disable_attachements_downloading"]);
        }

        void OnBrowseAttachmentsFolderRequest()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                _formGroupSettingsView.AttachmentsFolder = dialog.SelectedPath;
            }            
        }

        void OnBrowseOutlookFolderRequest()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                _formGroupSettingsView.OutlookFolder = dialog.SelectedPath;
            }        
        }
    }
}
