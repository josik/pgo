﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Threading;

using PGOffline.Model;
using PGOffline.View;
using System.Collections;
using System.Data.SQLite;
using System.ComponentModel;

namespace PGOffline.Presenter
{
    class MessagesListPresenter
    {
        public event MessageSelectedEventHandler MessageSelected;

        private View.IMessagesListView _view;

        private Message _selectedMessage;

        private DiscussionGroup _group;

        private DiscussionGroupMessagesList _messagesList;

        public MessagesListPresenter(View.IMessagesListView view)
        {
            _view = view;

            _view.AddMessagesToFavoritesRequest += new AddMessagesToFavoritesRequestEventHandler(OnAddMessagesToFavoritesRequest);
            _view.RemoveMessagesFromFavoritesRequest += new RemoveMessagesFromFavoritesRequestEventHandler(OnRemoveMessagesFromFavoritesRequest);
            _view.MarkMessagesAsReadRequest += new MarkMessagesAsReadRequestEventHandler(OnMarkMessagesAsReadRequest);
            _view.MarkMessagesAsUnreadRequest += new MarkMessagesAsUnreadRequestEventHandler(OnMarkMessagesAsUnreadRequest);
            _view.MarkAllMessagesAsReadRequest += new MarkAllMessagesAsReadRequestEventHandler(OnMarkAllMessagesAsReadRequest);
            _view.MarkAllMessagesAsUnreadRequest += new MarkAllMessagesAsUnreadRequestEventHandler(OnMarkAllMessagesAsUnreadRequest);
            _view.DeleteMessagesRequest += new DeleteMessagesRequestEventHandler(OnDeleteMessagesRequest);
            _view.ExportSelectedMessagesRequest += new ExportSelectedMessagesRequestEventHandler(OnExportSelectedMessagesRequest);

            _view.MessageSelected += new MessageSelectedEventHandler(OnMessageSelected);

            // Set filter that hides deleted messages.
            UpdateFilter();
        }

        public void AttachFullScreen(View.IFormFullScreenView formFullScreenView)
        {
            formFullScreenView.AddMessagesToFavoritesRequest += new AddMessagesToFavoritesRequestEventHandler(OnAddMessagesToFavoritesRequest);

            formFullScreenView.RemoveMessagesFromFavoritesRequest += new RemoveMessagesFromFavoritesRequestEventHandler(OnRemoveMessagesFromFavoritesRequest);

            formFullScreenView.DeleteMessagesRequest += new DeleteMessagesRequestEventHandler(OnDeleteMessagesRequest);
        }

        public void DetachFullScreen(View.IFormFullScreenView formFullScreenView)
        {
            formFullScreenView.AddMessagesToFavoritesRequest -= OnAddMessagesToFavoritesRequest;

            formFullScreenView.RemoveMessagesFromFavoritesRequest -= OnRemoveMessagesFromFavoritesRequest;

            formFullScreenView.DeleteMessagesRequest -= OnDeleteMessagesRequest;
        }

        void OnExportSelectedMessagesRequest(string databasePath)
        {
            ExportSettings settings = new ExportSettings
            {
                CopyAttachments = true,
                ExportFavoriteStatus = true,
                ExportType = ExportType.AllMessages
            };

            IExportManager exportManager = new ExportDb3Manager(databasePath, _group, _view.GetSelectedMessagesNumbers(), settings);

            exportManager.ExportDataAsync();
        }

        private void OnMessageSelected(Message message)
        {
            ProcessMessageTimer(message);

            _selectedMessage = message;

            if (message != null)
            {
                MessageSelected(message);
            }
        }

        public void SetDiscussionGroup(DiscussionGroup group)
        {
            _group = group;

            if (_group == null)
            {
                return;
            }

            if (_messagesList != null)
            {
                _messagesList.Dispose();
            }

            _messagesList = new DiscussionGroupMessagesList(group);
            
            SetMessages(_messagesList, false);
        }

        public void SetMessages(IList<Message> messages, bool genericList = true)
        {
            if (genericList && _messagesList != null)
            {
                _messagesList.Dispose();

                _messagesList = null;
            }

            _view.SetMessages(messages);
        }

        private DispatcherTimer _markAsReadTimer;

        private void ProcessMessageTimer(Message message)
        {
            if (Equals(message, _selectedMessage)) return;

            if (_markAsReadTimer != null)
            {
                _markAsReadTimer.Stop();

                _markAsReadTimer = null;
            }

            if (message == null)
            {
                return;
            }

            if (message.MessageStatus != MessageStatus.Unread) { return; }

            _markAsReadTimer = new DispatcherTimer();

            _markAsReadTimer.Interval = new TimeSpan(0, 0, Options.Instance.GetAsInt("miscellaneous_options_mark_messages_as_read_after_seconds"));

            _markAsReadTimer.Tick += new EventHandler((sender, e) =>
            {
                message.MessageStatus = MessageStatus.Read;

                if (_group != null)
                {
                    _group.RefreshMessagesCount();
                }

                UpdateFilter();

                if (_markAsReadTimer != null)
                {
                    _markAsReadTimer.Stop();
                }
            });

            _markAsReadTimer.Start();
        }

        private bool _showAllMessages = true;
        private bool _showOnlyFavoritesMessages = false;
        private bool _showOnlyNewMessages = false;

        public bool IsShowAllMessages
        {
            get { return _showAllMessages; }
        }

        public bool IsShowOnlyFavoritesMessages
        {
            get { return _showOnlyFavoritesMessages; }
        }
        
        public bool IsShowOnlyNewMessages
        {
            get { return _showOnlyNewMessages; }
        }
        
        public void ShowOnlyFavoriteMessages()
        {
            _showAllMessages = false;
            _showOnlyFavoritesMessages = true;
            _showOnlyNewMessages = false;

            UpdateFilter();
        }

        public void ShowOnlyNewMessages()
        {
            _showAllMessages = false;
            _showOnlyFavoritesMessages = false;
            _showOnlyNewMessages = true;

            UpdateFilter();
        }

        public void ShowAllMessages()
        {
            _showAllMessages = true;
            _showOnlyFavoritesMessages = false;
            _showOnlyNewMessages = false;

            UpdateFilter();
        }

        void UpdateFilter()
        {
            _view.SetFilter(new Predicate<object>(MessagesFilter));
        }

        bool MessagesFilter(object message)
        {
            if (message == null)
                return false;

            if (_showAllMessages)
                return true;

            if (_showOnlyFavoritesMessages && !((Message)message).Favorite)
                return false;

            if (_showOnlyNewMessages && ((Message)message).MessageStatus != MessageStatus.Unread)
                return false;

            return true;
        }
        
        async void OnDeleteMessagesRequest(IList messages)
        {
            if (!DatabaseManager.Instance.IsConnectionIsReady()) return;

            List<long> messageIds = (from Message message in messages select message.Id).ToList();

            if (messageIds.Count == 0) return;

            // clear message from message detail view
            MessageSelected(null);

            await DeleteMessagesViewTask(messages);

            _view.RefreshMessagesList();

            await DeleteMessageDatabaseTask(messageIds);
            
            UpdateFilter();
        }

        private async Task DeleteMessagesViewTask(IList messages)
        {
            var t = Task.Delay(TimeSpan.FromSeconds(0))
                        .ContinueWith(
                            _ =>
                            {
                                // clear message from message detail view
                                MessageSelected(null);

                                foreach (Message message in messages)
                                {
                                    _messagesList.Remove(message);
                                }

                                _view.RefreshMessagesList();

                                UpdateFilter();
                            },
                        // Specify where to execute the continuation
                        TaskScheduler.FromCurrentSynchronizationContext()
                        );

            await t;
        }

        private async Task DeleteMessageDatabaseTask(IEnumerable<long> messageIds)
        {
            var t = Task.Delay(TimeSpan.FromSeconds(0))
                .ContinueWith(
                    _ =>
                    {
                        var transaction = DatabaseManager.Instance.TransactionBegin();

                        foreach (var messageId in messageIds)
                        {
                            Message.DeleteById(messageId);
                        }

                        transaction.Commit();
                    },
                    // Specify where to execute the continuation
                    TaskScheduler.FromCurrentSynchronizationContext()
                );

            await t;
        }

       void OnRemoveMessagesFromFavoritesRequest(IList messages)
        {
            SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin();

            foreach (Message message in messages)
            {
                message.Favorite = false;
            }

            transaction.Commit();

            UpdateFilter();
        }

        void OnAddMessagesToFavoritesRequest(IList messages)
        {
            SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin();

            foreach (Message message in messages)
            {
                message.Favorite = true;
            }

            transaction.Commit();
        }

        void OnMarkAllMessagesAsUnreadRequest()
        {
            if (_group == null) return;

            _group.MarkAllMessagesAsUnread();

            foreach (Message message in _view.GetMessages())
            {
                message.SetMessageStatus(MessageStatus.Unread);

                message.NotifyStatusChanged();
            }

            _view.RefreshMessagesList();

            UpdateFilter();
        }

        void OnMarkAllMessagesAsReadRequest()
        {
            if (_group == null) return;

            _group.MarkAllMessagesAsRead();

            foreach (Message message in _view.GetMessages())
            {
                message.SetMessageStatus(MessageStatus.Read);

                message.NotifyStatusChanged();
            }

            _view.RefreshMessagesList();

            UpdateFilter();
        }

        void OnMarkMessagesAsUnreadRequest(IList messages)
        {
            SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin();

            foreach (Message message in messages)
            {
                message.MessageStatus = MessageStatus.Unread;
            }

            transaction.Commit();

            _group.RefreshMessagesCount();

            UpdateFilter();
        }

        void OnMarkMessagesAsReadRequest(IList messages)
        {
            SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin();

            foreach (Message message in messages)
            {
                message.MessageStatus = MessageStatus.Read;
            }

            transaction.Commit();

            _group.RefreshMessagesCount();

            UpdateFilter();
        }

        public List<long> GetSelectedMessagesNumbers()
        {
            return _view.GetSelectedMessagesNumbers();
        }
    }
}
