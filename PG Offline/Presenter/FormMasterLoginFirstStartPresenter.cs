﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.Model;
using PGOffline.View;

namespace PGOffline.Presenter
{
    class FormMasterLoginFirstStartPresenter
    {
        IFormMasterLoginView _formMasterLoginView;

        UserAccount _userAccount;

        public FormMasterLoginFirstStartPresenter(IFormMasterLoginView formMasterLoginView)
        {
            _formMasterLoginView = formMasterLoginView;
            _formMasterLoginView.MasterCredentialsChanged += new MasterCredentialsChangedRequestHandler(OnMasterCredentialsChanged);
            _formMasterLoginView.CloseRequest += new CloseRequestEventHandler(OnCloseRequest);
        }

        void OnCloseRequest()
        {
            UserAccount userAccount = UserAccount.GetFirst();

            if (userAccount == null)
            {
                System.Windows.Application.Current.Shutdown();
            }
        }

        void OnMasterCredentialsChanged()
        {
            UserAccount.Create(_formMasterLoginView.Username, _formMasterLoginView.Password);
        }
    }
}
