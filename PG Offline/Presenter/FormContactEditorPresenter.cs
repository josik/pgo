﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.Model;
using PGOffline.View;

namespace PGOffline.Presenter
{
    class FormContactEditorPresenter
    {
        public delegate void AddressBookRecordAddedEventHandler(AddressBookRecord addressBookRecord);
        public event AddressBookRecordAddedEventHandler AddressBookRecordAdded;

        public delegate void AddressBookRecordChangedEventHandler(AddressBookRecord addressBookRecord);
        public event AddressBookRecordChangedEventHandler AddressBookRecordChanged;

        IFormContactEditorView _formContactEditorView;

        AddressBookRecord _addressBookRecord;

        public FormContactEditorPresenter(IFormContactEditorView formContactEditorView)
        {
            _formContactEditorView = formContactEditorView;

            _formContactEditorView.ContactSaveRequest += new ContactSaveRequestEventHandler(OnContactSaveRequest);
        }

        public FormContactEditorPresenter(IFormContactEditorView formContactEditorView, AddressBookRecord addressBookRecord)
        {
            _addressBookRecord = addressBookRecord;

            _formContactEditorView = formContactEditorView;
            _formContactEditorView.ContactSaveRequest += new ContactSaveRequestEventHandler(OnContactSaveRequest);

            _formContactEditorView.Email = _addressBookRecord.Email;
            _formContactEditorView.Name = _addressBookRecord.Name;
            _formContactEditorView.Comment = _addressBookRecord.Comment;
        }

        void OnContactSaveRequest()
        {
            if (_addressBookRecord == null)
            {
                _addressBookRecord = AddressBookRecord.Create(_formContactEditorView.Name, _formContactEditorView.Email, _formContactEditorView.Comment);

                AddressBookRecordAdded(_addressBookRecord);
            }
            else
            {
                _addressBookRecord.Name = _formContactEditorView.Name;
                _addressBookRecord.Email = _formContactEditorView.Email;
                _addressBookRecord.Comment = _formContactEditorView.Comment;

                _addressBookRecord.Save();

                AddressBookRecordChanged(_addressBookRecord);
            }
        }
    }
}
