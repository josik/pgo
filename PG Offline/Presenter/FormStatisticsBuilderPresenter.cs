﻿using PGOffline.Exceptions;
using PGOffline.Model;
using PGOffline.View;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace PGOffline.Presenter
{
    class FormStatisticsBuilderPresenter
    {
        IFormStatisticsBuilderView _formStatisticsBuilderView;

        StatisticsBuilder _statisticsBuilder;

        public FormStatisticsBuilderPresenter(IFormStatisticsBuilderView formStatisticsBuilderView)
        {
            _statisticsBuilder = new StatisticsBuilder();

            _formStatisticsBuilderView = formStatisticsBuilderView;

            _formStatisticsBuilderView.BuildStatisticsRequest += new BuildStatisticsRequestHandler(OnBuildStatisticsRequest);

            _formStatisticsBuilderView.CancelStatisticsCalculationRequest += new CancelStatisticsCalculationRequestEventHandler(OnCancelStatisticsCalculationRequest);

            _formStatisticsBuilderView.BaseStatisticsOnDateSelected += new BaseStatisticsOnDateSelectedEventHandler(OnBaseStatisticsOnDateSelected);
            _formStatisticsBuilderView.BaseStatisticsOnNumberSelected += new BaseStatisticsOnNumberSelectedEventHandler(OnBaseStatisticsOnNumberSelected);
            _formStatisticsBuilderView.CompareByWeekSelected += new CompareByWeekSelectedEventHandler(OnCompareByWeekSelected);
            _formStatisticsBuilderView.CompareByMonthSelected += new CompareByMonthSelectedEventHandler(OnCompareByMonthSelected);
            _formStatisticsBuilderView.CompareByYearSelected += new CompareByYearSelectedEventHandler(OnCompareByYearSelected);
            _formStatisticsBuilderView.CompareByCustomDateSelected += new CompareByCustomDateSelectedEventHandler(OnCompareByCustomDateSelected);
            _formStatisticsBuilderView.CompareByMessageNumberSelected += new CompareByMessageNumberSelectedEventHandler(OnCompareByCustomNumbersSelected);
            _formStatisticsBuilderView.GroupVsGroupSelected += new GroupVsGroupSelectedEventHandler(OnGroupVsGroupSelected);
            _formStatisticsBuilderView.DisplayGroupsRequest += new DisplayGroupsOnStatsRequestEventHandler(OnDisplayGroupsRequest);
            _formStatisticsBuilderView.SaveStatisticsOptionsRequest += new SaveStatisticsOptionsRequestEventHandler(OnSaveStatisticsOptionsRequest);
            _formStatisticsBuilderView.SetStatisticsOptionsRequest += new SetStatisticsOptionsRequestEventHandler(OnSetStatisticsOptionsRequest);
        }

        private void OnDisplayGroupsRequest()
        { 
            List<DiscussionGroup> groups = DiscussionGroup.GetAll();

            _formStatisticsBuilderView.DisplayGroups(groups);
        }

        private void OnBuildStatisticsRequest(List<DiscussionGroup> groups, StatisticsOptions options)
        {
            BuildStatistics(groups, options);
        }

        private void OnCancelStatisticsCalculationRequest()
        {
            if (_statisticsBuilder != null)
            {
                _statisticsBuilder.CancelStatisticsBuilding();
            }
        }

        private void OnBaseStatisticsOnNumberSelected()
        {
            _formStatisticsBuilderView.DisableAllInputForms();

            _formStatisticsBuilderView.SelectMessageNumberInputForms();
        }

        private void OnBaseStatisticsOnDateSelected()
        {
            _formStatisticsBuilderView.DisableAllInputForms();

            _formStatisticsBuilderView.SelectDateInputForms();
        }

        private void OnCompareByWeekSelected()
        {
            _formStatisticsBuilderView.DisableAllInputForms();
        }

        private void OnCompareByMonthSelected()
        {
            _formStatisticsBuilderView.DisableAllInputForms();
        }

        private void OnCompareByYearSelected()
        {
            _formStatisticsBuilderView.DisableAllInputForms();
        }

        private void OnCompareByCustomDateSelected()
        {
            _formStatisticsBuilderView.DisableAllInputForms();

            _formStatisticsBuilderView.SelectComparingDateInputForms();
        }

        private void OnCompareByCustomNumbersSelected()
        {
            _formStatisticsBuilderView.DisableAllInputForms();
            _formStatisticsBuilderView.SelectComparingMessageNumbersInputForms();
        }

        private void OnGroupVsGroupSelected()
        {
            _formStatisticsBuilderView.DisableAllInputForms();

            _formStatisticsBuilderView.SelectGroupsComboboxes();
        }

        private StatisticsPair<StatisticsSubject> GetSingleGroupStatisticsSubjects(DiscussionGroup group, StatisticsOptions options)
        {
            StatisticsPair<StatisticsSubject> pair = new StatisticsPair<StatisticsSubject>
            {
                FirstObject = new StatisticsSubject
                {
                    ReckoningGroup = @group,
                    RangeForReckoning = new StatisticsRange(options.statisticsType)
                }
            };

            if (options.statisticsType == StatisticsType.ComparisonByWeek ||
                options.statisticsType == StatisticsType.ComparisonByMonth ||
                options.statisticsType == StatisticsType.ComparisonByYear ||
                options.statisticsType == StatisticsType.ComparisonByCustomTime ||
                options.statisticsType == StatisticsType.ComparisonByCustomNumbers)
            {
                pair.SecondObject = new StatisticsSubject
                {
                    ReckoningGroup = @group,
                    RangeForReckoning = new StatisticsRange(options.statisticsType)
                };
            }


            if (options.statisticsType == StatisticsType.ComparisonByWeek)
            {
                pair.FirstObject.RangeForReckoning.beginDate = GetStartOfCurrentWeek();

                pair.FirstObject.RangeForReckoning.endDate = GetEndOfCurrentWeek();

                pair.SecondObject.RangeForReckoning.beginDate = pair.FirstObject.RangeForReckoning.beginDate - TimeSpan.FromDays(7);

                pair.SecondObject.RangeForReckoning.endDate = pair.FirstObject.RangeForReckoning.endDate - TimeSpan.FromDays(7);
            }

            if (options.statisticsType == StatisticsType.ComparisonByMonth)
            {
                DateTime beginDate = new DateTime();

                beginDate = beginDate.AddYears(DateTime.Today.Year - 1);
                beginDate = beginDate.AddMonths(DateTime.Today.Month - 1);

                pair.FirstObject.RangeForReckoning.beginDate = beginDate;
                pair.FirstObject.RangeForReckoning.endDate = beginDate.AddDays(DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month));
                pair.FirstObject.RangeForReckoning.endDate -= TimeSpan.FromSeconds(1);

                DateTime monthAgo = new DateTime();

                if (DateTime.Today.Month == 1)
                {
                    monthAgo = monthAgo.AddYears(DateTime.Today.Year - 2);
                    monthAgo = monthAgo.AddMonths(11);
                }
                else
                {
                    monthAgo = monthAgo.AddYears(DateTime.Today.Year - 1);
                    monthAgo = monthAgo.AddMonths(DateTime.Today.Month - 2);
                }

                pair.SecondObject.RangeForReckoning.beginDate = monthAgo;
                pair.SecondObject.RangeForReckoning.endDate = monthAgo.AddDays(DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month));
                pair.SecondObject.RangeForReckoning.endDate -= TimeSpan.FromSeconds(1);
            }

            if (options.statisticsType == StatisticsType.ComparisonByYear)
            {
                pair.FirstObject.RangeForReckoning.beginDate = (new DateTime()).AddYears(DateTime.Today.Year - 1);
                pair.FirstObject.RangeForReckoning.endDate = (new DateTime()).AddYears(DateTime.Today.Year).AddSeconds(-1);

                pair.SecondObject.RangeForReckoning.beginDate = (new DateTime()).AddYears(DateTime.Today.Year - 2);
                pair.SecondObject.RangeForReckoning.endDate = (new DateTime()).AddYears(DateTime.Today.Year - 1).AddSeconds(-1);
            }

            if (options.statisticsType == StatisticsType.ComparisonByCustomTime)
            {
                pair.FirstObject.RangeForReckoning.beginDate = options.compare1StartDate;
                pair.FirstObject.RangeForReckoning.endDate = options.compare1EndDate;

                pair.SecondObject.RangeForReckoning.beginDate = options.compare2StartDate;
                pair.SecondObject.RangeForReckoning.endDate = options.compare2EndDate;
            }

            if (options.statisticsType == StatisticsType.ComparisonByCustomNumbers)
            {
                pair.FirstObject.RangeForReckoning.beginNumber = options.compare1StartNumber;
                pair.FirstObject.RangeForReckoning.endNumber = options.compare1EndNumber;

                pair.SecondObject.RangeForReckoning.beginNumber = options.compare2StartNumber;
                pair.SecondObject.RangeForReckoning.endNumber = options.compare2EndNumber;
            }

            if (options.statisticsType == StatisticsType.StatisticsBasedOnDate)
            {
                pair.FirstObject.RangeForReckoning.beginDate = options.dateTimeStart;
                pair.FirstObject.RangeForReckoning.endDate = options.dateTimeEnd;
            }

            if (options.statisticsType == StatisticsType.StatisticsBasedOnMessagesNumbers)
            {
                pair.FirstObject.RangeForReckoning.beginNumber = options.messageNumberFirst;
                pair.FirstObject.RangeForReckoning.endNumber = options.messageNumberLast;
            }

            return pair;
        }

        private StatisticsPair<StatisticsSubject> GetGroupVsGroupSubjects(DiscussionGroup group1, DiscussionGroup group2, StatisticsOptions options)
        {
            StatisticsPair<StatisticsSubject> pair = new StatisticsPair<StatisticsSubject>
            {
                FirstObject = new StatisticsSubject(),
                SecondObject = new StatisticsSubject()
            };

            pair.FirstObject.ReckoningGroup = group1;

            pair.SecondObject.ReckoningGroup = group2;

            pair.FirstObject.RangeForReckoning = pair.SecondObject.RangeForReckoning = new StatisticsRange(options.statisticsType);

            return pair;
        }

        private void BuildStatistics(List<DiscussionGroup> groups, StatisticsOptions options)
        {
            List<StatisticsPair<StatisticsSubject>> statisticsSubjects = new List<StatisticsPair<StatisticsSubject>>();

            if (options.statisticsType == StatisticsType.GroupVsGroup)
            {
                statisticsSubjects.Add(GetGroupVsGroupSubjects(groups[0], groups[1], options));
            }
            else
            {
                foreach (DiscussionGroup group in groups)
                {
                    statisticsSubjects.Add(GetSingleGroupStatisticsSubjects(group, options));
                }
            }

            _statisticsBuilder.SetStatisticsSubjects(statisticsSubjects, options);

            _statisticsBuilder.StatisticsCalculationComplete += new StatisticsBuilder.StatisticsCalculationCompleteEventHandler(OnStatisticsCalculationComplete);

            _statisticsBuilder.BuildReportAsync();
        }

        private void OnStatisticsCalculationComplete(string report)
        {
            _statisticsBuilder.StatisticsCalculationComplete -= OnStatisticsCalculationComplete;

            _formStatisticsBuilderView.ShowReport(report);
        }

        private DateTime GetStartOfCurrentWeek()
        {
            DateTime startOfWeek = DateTime.Today;

            while (startOfWeek.DayOfWeek != DayOfWeek.Monday)
            {
                startOfWeek = startOfWeek - TimeSpan.FromDays(1);
            }

            return startOfWeek;
        }

        private DateTime GetEndOfCurrentWeek()
        {
            DateTime endOfWeek = DateTime.Today;

            while (endOfWeek.DayOfWeek != DayOfWeek.Sunday)
            {
                endOfWeek = endOfWeek + TimeSpan.FromDays(1) - TimeSpan.FromSeconds(1);
            }

            return endOfWeek;
        }

        private void OnSaveStatisticsOptionsRequest(StatisticsOptions options)
        {
            Options.Instance.Set("statistics_options_compare_1_end_date", options.compare1EndDate);
            Options.Instance.SetStatisticsNumber("statistics_options_compare_1_end_number", options.compare1EndNumber);
            Options.Instance.Set("statistics_options_compare_1_start_date", options.compare1StartDate);
            Options.Instance.SetStatisticsNumber("statistics_options_compare_1_start_number", options.compare1StartNumber);
            Options.Instance.Set("statistics_options_compare_2_end_date", options.compare2EndDate);
            Options.Instance.SetStatisticsNumber("statistics_options_compare_2_end_number", options.compare2EndNumber);
            Options.Instance.Set("statistics_options_compare_2_start_date", options.compare2StartDate);
            Options.Instance.SetStatisticsNumber("statistics_options_compare_2_start_number", options.compare2StartNumber);
            Options.Instance.Set("statistics_options_compare_group_1_name", options.compareGroup1Name);
            Options.Instance.Set("statistics_options_compare_group_2_name", options.compareGroup2Name);
            Options.Instance.Set("statistics_options_date_time_end", options.dateTimeEnd);
            Options.Instance.Set("statistics_options_date_time_start", options.dateTimeStart);
            Options.Instance.Set("statistics_options_lurkers", options.lurkers);
            Options.Instance.SetStatisticsNumber("statistics_options_message_number_first", options.messageNumberFirst);
            Options.Instance.SetStatisticsNumber("statistics_options_message_number_last", options.messageNumberLast);
            Options.Instance.Set("statistics_options_new_members", options.newMembers);
            Options.Instance.Set("statistics_options_postings", options.postings);
            Options.Instance.Set("statistics_options_repliers", options.repliers);
            Options.Instance.Set("statistics_options_statistics_type", (int)options.statisticsType);
            Options.Instance.Set("statistics_options_thread_initiators", options.threadInitiators);
            Options.Instance.Set("statistics_options_top_ten_subjects", options.topTenSubjects);

            Options.Instance.SaveOptions();
        }

        private void OnSetStatisticsOptionsRequest()
        {
            try
            {
                StatisticsOptions options = new StatisticsOptions();

                options.SetStatisticsOptions();

                _formStatisticsBuilderView.StatisticsOptions = options;
            }
            catch (StatisticsOptionsFormatException e)
            {
                MessageBox.Show(e.Message.ToString());
            }
        }

    }
}
