﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using PGOffline.View;
using PGOffline.Model;
using PGOffline.Forms;
using Asfdfdfd.Api.Yahoo.Groups;

namespace PGOffline.Presenter
{
    class FormDownloadFilesPresenter : DownloadContentPresenter
    {
        public FormDownloadFilesPresenter(IFormDownloadContentView formDownloadFiles, DiscussionGroup currentDiscussionGroup, YahooGroupsServer yahooServer) : 
            base(formDownloadFiles, currentDiscussionGroup, yahooServer)
        {
            _formDownloadContent.SetWindowTitle("Download files");
        }

        protected override void DownloadContentList()
        {
            try
            {
                UserAccount userAccount = UserAccount.GetFirst();

                if (!_yahooServer.Login(userAccount.Name, userAccount.Password)) return;

                _rootElements.Clear();

                _rootElements.AddRange(_currentYahooGroup.ReceiveFilesAndDirectories(_downloadContentListWorker));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error. " + ex.Message);
            }
        }

        protected override void DownloadContent()
        {
            _currentYahooGroup.DownloadAllFiles(_formDownloadContent.GetSelectedItems().Cast<YahooGroupFile>(), _downloadContentWorker);            
        }
    }
}
