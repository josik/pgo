﻿using PGOffline.Model;
using PGOffline.View;
using System.Collections.Generic;
using System.ComponentModel;

namespace PGOffline.Presenter
{
    class FormFindAllPresenter
    {
        IFormFindAll _formFindAll;

        public delegate void SearchCompleteEventHandler(List<Message> foundedMessages);

        public event SearchCompleteEventHandler SearchComplete;

        private BackgroundWorker _searchBackgroundWorker;
        
        List<Message> _foundedMessages;

        MessagesSearchCriteria _criteria = new MessagesSearchCriteria();

        public FormFindAllPresenter (IFormFindAll formFindAll)
    	{
            _foundedMessages = new List<Message>();

            _formFindAll = formFindAll;
            
            _formFindAll.FormFindAllLoadComplete += new FormFindAllLoadCompleteEventHandler(OnFormFindAllLoadComplete);
            
            _formFindAll.FindPersonRequest += new FindPersonRequestEventHandler(OnFindPersonRequest);
            
            _formFindAll.SaveSearchParamsRequest += new SaveSearchParamsRequestEventHandler(OnSaveSearchParamsRequest);
            
            _formFindAll.LoadSearchParamsRequest += new LoadSearchParamsRequestEventHandler(OnLoadSearchParamsRequest);
            
            _formFindAll.StartSearchRequest += new StartSearchRequestEventHandler(OnStartSearchRequest);

            _formFindAll.StopSearchRequest += new StopSearchRequestEventHandler(OnStopSearchRequest);
	    }

        private void OnFormFindAllLoadComplete()
        {
            _formFindAll.SetControlsReadyToSearch();

            _formFindAll.PopulateGroupsList(DiscussionGroup.GetAll());
        }

        private void OnFindPersonRequest()
        {
            if (_formFindAll.GetDiscussionGroups().Count == 0)
            {
                System.Windows.MessageBox.Show("Please select one or more groups from the list.", "PG Offline");

                return;
            }

            FormFindPerson formFindPerson = new FormFindPerson();

            FormFindPersonPresenter formFindPersonPresenter = new FormFindPersonPresenter(formFindPerson, _formFindAll.GetDiscussionGroups());

            formFindPersonPresenter.UsersToSearchSelectedEvent += new FormFindPersonPresenter.UsersToSearchSelectedEventHandler(OnFindPersonComplete);

            formFindPerson.ShowDialog();
        }

        private void OnFindPersonComplete(List<Person> persons)
        {
            if(persons.Count == 0) return;

            string usersNames = "";

            foreach (Person person in persons)
            {
                usersNames += person.Name + ", ";
            }

            usersNames = usersNames.Remove(usersNames.Length - 2, 2);

            _formFindAll.AddUsers(usersNames);
        }

        private void OnSaveSearchParamsRequest()
        {
            PrepareSearchParams();

            var presenter = new FormSaveSearchParamsPresenter(new FormSaveSearchParams(), _criteria);
        }

        private void OnLoadSearchParamsRequest()
        {
            var form = new FormLoadSearchParams();

            var presenter = new FormLoadSearchParamsPresenter(form);

            presenter.ParamsLoaded += new FormLoadSearchParamsPresenter.ParamsLoadedHandler(_ParamsLoaded);

            form.ShowDialog();
        }

        void _ParamsLoaded(MessagesSearchCriteria criteria)
        {
            _criteria = criteria;

            _formFindAll.Persons = _criteria.persons;
            
            _formFindAll.Subject = _criteria.subject;
            
            _formFindAll.Message = _criteria.message;
            
            _formFindAll.FromMessage = _criteria.numberFrom;
            
            _formFindAll.ToMessage = _criteria.numberTo;

            var selectedGroups = new List<DiscussionGroup>();

            _criteria.groupNames.ForEach((groupName) =>
            {
                var group = DiscussionGroup.GetByName(groupName);

                if (group != null)
                {
                    selectedGroups.Add(group);
                }
            });

            _formFindAll.SetSelectedGroups(selectedGroups);
        }

        private void OnStartSearchRequest()
        {
            PrepareSearchParams();

            FindMessageAsync();

            _formFindAll.SetControlsSearchInProgress();
        }

        private void PrepareSearchParams()
        {
            _formFindAll.GetDiscussionGroups().ForEach((group) => _criteria.groupNames.Add(group.Name));

            _criteria.subject = _formFindAll.Subject;

            _criteria.persons = _formFindAll.Persons;

            _criteria.message = _formFindAll.Message;

            if (_formFindAll.FromMessage == null)
            {
                _criteria.numberFrom = long.MinValue;
            }
            else
            {
                _criteria.numberFrom = (long) _formFindAll.FromMessage;
            }

            if (_formFindAll.ToMessage == null)
            {
                _criteria.numberTo = long.MaxValue;
            }
            else
            {
                _criteria.numberTo = (long) _formFindAll.ToMessage;
            }
        }

        private void OnStopSearchRequest()
        {
            FindMessagesCancelAsync();

            _formFindAll.SetControlsReadyToSearch();
        }

        private void FindMessages()
        {
            _foundedMessages.Clear();
            
            _foundedMessages = new MessagesFinder().FindMessages(_criteria);

            System.Diagnostics.Trace.WriteLine("Messages found = " + _foundedMessages.Count);
        }

        private void FindMessageAsync()
        {
            if (_searchBackgroundWorker != null)
            {
                FindMessagesCancelAsync();
            }

            _searchBackgroundWorker = new BackgroundWorker
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };

            _searchBackgroundWorker.DoWork += new DoWorkEventHandler((sender, e) => FindMessages());

            _searchBackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((sender, e) => OnSearchComplete());

            _searchBackgroundWorker.RunWorkerAsync();            
        }

        private void OnSearchComplete()
        {
            SearchComplete(_foundedMessages);

            _formFindAll.SetControlsReadyToSearch();
        }

        private void FindMessagesCancelAsync()
        {
            if (_searchBackgroundWorker != null)
            {
                _searchBackgroundWorker.CancelAsync();
            }
        }
    }
}
