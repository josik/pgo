﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGOffline.Exceptions;
using System.Windows;


namespace PGOffline.Presenter
{
    public class FormMainPresenterConnection
    {
        public bool TryOpenConnection(string lastDatabasePath)
        {
            try
            {
                DatabaseManager.Instance.OpenConnection(lastDatabasePath);
                return true;
            }
            catch (DatabaseWrongVersionException e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            catch (DatabaseWrongFormatException e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }
    }
}
