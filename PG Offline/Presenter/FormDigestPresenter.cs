﻿using PGOffline.Model;
using PGOffline.View;
using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Windows;

namespace PGOffline.Presenter
{
    class FormDigestPresenter
    {
        IFormDigestView _formDigestView;

        IList _messages;

        int _startMessageIndex;
        int _messagesPerPage;
        int _pageNumber = 1;

        string _query;

        bool _searchResults;

        string _groupName;

        bool? _autoMarkAsRead = false;

        DiscussionGroup _group;

        public FormDigestPresenter(IFormDigestView formDigestView, IList messages, DiscussionGroup group,  string groupName, string query, bool searchResults)
        {
            _group = group;

            _messages = messages;

            _formDigestView = formDigestView;

            _formDigestView.ApplyDisplayOptionsRequest += new ApplyDisplayOptionsRequestEventHandler(OnApplyDisplayOptionsRequest);
            _formDigestView.ForwardRequest += new ForwardRequestEventHandler(OnForwardRequest);
            _formDigestView.PrintDigestRequest += new PrintDigestRequestEventHandler(OnPrintDigestRequest);
            _formDigestView.ShowNextMessagesRequest += new ShowNextMessagesRequestEventHandler(OnShowNextMessagesRequest);
            _formDigestView.ShowPrevMessagesRequest += new ShowPrevMessagesRequestEventHandler(OnShowPrevMessagesRequest);
            _formDigestView.ToggleQuotesRequest += new ToggleQuotesRequestEventHandler(OnToggleQuotesRequest);
            _formDigestView.ChangeMarkAsReadStatus += new ChangeMarkAsReadStatusEventHandler(ChangeMarkAsReadStatus);

            _messagesPerPage = _formDigestView.MessagesPerPage;
            _startMessageIndex = 0;

            _formDigestView.SetGroupName(groupName);

            _query = query;

            _searchResults = searchResults;

            _groupName = groupName;

            try
            {
                UpdateDigest();
            }
            catch (DirectoryNotFoundException e)
            {
                MessageBox.Show("Could not find template. " + e.Message + ". Please check your installation path.", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        void ChangeMarkAsReadStatus(bool? isChecked)
        {
            _autoMarkAsRead = isChecked;

            if (_autoMarkAsRead == true)
            {
                MarkPageMessagesAsRead();
            }
        }

        void MarkPageMessagesAsRead()
        {
            // Transactions temporarily turned off.
            // SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin();

            for (int messageIndex = _startMessageIndex; messageIndex <= StopMessageIndex; messageIndex++)
            {
                Message message = (Message)_messages[messageIndex];

                message.MessageStatus = MessageStatus.Read;
            }

            // transaction.Commit();

            _group.RefreshMessagesCount();
        }

        void OnToggleQuotesRequest()
        {
        }

        void OnShowPrevMessagesRequest()
        {
            _startMessageIndex -= _messagesPerPage;
            
            if (_startMessageIndex < 0)
                _startMessageIndex = 0;
            
            _pageNumber--;

            if (_pageNumber < 1)
                _pageNumber = 1;

            UpdateDigest();
        }

        void OnShowNextMessagesRequest()
        {
            _startMessageIndex += _messagesPerPage;

            if (_startMessageIndex >= _messages.Count)
                _startMessageIndex = _messages.Count - _messagesPerPage;

            if (_startMessageIndex < 0)
                _startMessageIndex = 0;

            _pageNumber++;

            if (_pageNumber > PagesCount)
                _pageNumber = PagesCount;

            UpdateDigest();
        }

        void OnPrintDigestRequest()
        {
        }

        void OnForwardRequest()
        {
        }

        void OnApplyDisplayOptionsRequest()
        {
            _messagesPerPage = _formDigestView.MessagesPerPage;

            _startMessageIndex = 0;

            UpdateDigest();
        }

        int PagesCount
        {
            get
            {
                return (int)Math.Ceiling(_messages.Count / (double)_messagesPerPage);
            }
        }

        int StopMessageIndex
        {
            get
            {
                int stopMessageIndex = _startMessageIndex + _messagesPerPage;

                if (stopMessageIndex >= _messages.Count)
                    stopMessageIndex = _messages.Count - 1;

                return stopMessageIndex;
            }
        }

        void UpdateDigest()
        {
            if (_autoMarkAsRead == true)
            {
                MarkPageMessagesAsRead();
            }

            /*
            string template103 = TemplatesManager.GetTemplate(103);

            template103 = template103.Replace("@@orig_filename@@", "");
            template103 = template103.Replace("@@size@@", "");
            template103 = template103.Replace("@@file_id@@", "");
            */

            StringBuilder resMsg = new StringBuilder();
            StringBuilder resTitles = new StringBuilder();

            for (int messageIndex = _startMessageIndex; messageIndex <= StopMessageIndex; messageIndex++)
            {
                Message message = (Message)_messages[messageIndex];
                
                string template106107;

                if (message.AttachmentsCount > 0)
                {
                    template106107 = TemplatesManager.GetTemplate(106);
                }
                else
                {
                    template106107 = TemplatesManager.GetTemplate(107);
                }

                template106107 = template106107.Replace("@@title_color@@", "#FFDAB9");
                
                template106107 = template106107.Replace("@@group@@", message.DiscussionGroup.Name);
                
                template106107 = template106107.Replace("@@number@@", message.Number.ToString());
                
                template106107 = template106107.Replace("@@from@@", message.Author);
                
                template106107 = template106107.Replace("@@date@@", message.Date.ToShortDateString());
                
                template106107 = template106107.Replace("@@subject@@", message.Subject);
                
                template106107 = template106107.Replace("@@message@@", "\r\n" + message.Content + "\r\n");
                
                //template106107 = template106107.Replace("@@attachment@@", "\r\n" + message.Content + "\r\n");

                resMsg.Append(template106107);

                string template105 = TemplatesManager.GetTemplate(105);

                if (messageIndex%2 == 0)
                {
                    template105 = template105.Replace("@@title_color@@", "#FFDAB9");
                }
                else
                {
                    template105 = template105.Replace("@@title_color@@", "#C0FFC0");
                }

                template105 = template105.Replace("@@group@@", message.DiscussionGroup.Name);

                template105 = template105.Replace("@@number@@", message.Number.ToString());
                
                template105 = template105.Replace("@@from@@", message.Author);
                
                template105 = template105.Replace("@@date@@", message.Date.ToShortDateString());
                
                template105 = template105.Replace("@@subject@@", message.Subject);

                resTitles.Append(template105);
            }

            StringBuilder digest = new StringBuilder(TemplatesManager.GetTemplate(104));
            
            if (_query.Length == 0)
            {
                if (_searchResults)
                {
                    digest = digest.Replace("@@title@@", "<b>New messages in search results.</b>");
                }
                else
                {
                    digest = digest.Replace("@@title@@", String.Format("<b>New messages in {0} group.</b>", _groupName));
                }
            }
            else
            {
                if (_searchResults)
                {
                    digest = digest.Replace("@@title@@", "<b>Selected messages in search results.</b>");
                }
                else
                {
                    digest = digest.Replace("@@title@@", String.Format("<b>Selected messages in {0} group.</b>", _groupName));
                }
            }

            digest = digest.Replace("@@page_number@@", _pageNumber.ToString());

            digest = digest.Replace("@@pages_total@@", PagesCount.ToString());

            //string[] digestTemplateParts = digest.ToString().Split(new string[] { "@@tytles@@", "@@messages@@" }, StringSplitOptions.None);

            digest = digest.Replace("@@tytles@@", resTitles.ToString());

            digest = digest.Replace("@@messages@@", resMsg.ToString());

            _formDigestView.DisplayDigest(digest.ToString());

            //_formDigestView.DisplayDigest(digestTemplateParts[0] + resTitles.ToString() + digestTemplateParts[1] + resMsg.ToString() + digestTemplateParts[2]);
        }
    }
}
