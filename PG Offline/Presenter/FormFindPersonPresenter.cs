﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PGOffline.View;
using PGOffline.Model;

namespace PGOffline.Presenter
{
    class FormFindPersonPresenter
    {
        IFormFindPerson _formFindPerson;
        List<DiscussionGroup> _groups;

        public delegate void UsersToSearchSelectedEventHandler(List<Person> persons);
        public event UsersToSearchSelectedEventHandler UsersToSearchSelectedEvent;

        public FormFindPersonPresenter(IFormFindPerson formFindPerson, List<DiscussionGroup> groups)
        {
            _formFindPerson = formFindPerson;
            _groups = groups;

            _formFindPerson.FormFindPersonLoadComplete += new FormFindPersonLoadCompleteEventHandler(OnFormFindPersonLoadComplete);
            _formFindPerson.FindingUsersComplete += new FindingUsersCompleteEventHandler(OnFindingUsersComplete);
        }

        private void OnFormFindPersonLoadComplete()
        {
            List<Person> persons = new List<Person>();
            foreach (DiscussionGroup group in _groups)
            {
                persons.AddRange(Person.GetByDiscussionGroup(group));
            }

            _formFindPerson.SetPesons(persons);
        }

        private void OnFindingUsersComplete(List<Person> persons)
        {
            UsersToSearchSelectedEvent(persons);
        }
    }
}
