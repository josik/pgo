﻿using PGOffline.Properties;
using PGOffline.View;
using PGOffline.Model;

namespace PGOffline.Presenter
{
    class FormOptionsPresenter
    {
        IFormOptionsView _formOptionsView;

        public FormOptionsPresenter(IFormOptionsView formOptionsView)
        {
            _formOptionsView = formOptionsView;

            _formOptionsView.SmtpServer = Options.Instance.GetAsString("mail_options_smtp_server");
            _formOptionsView.SmtpPort = Options.Instance.GetAsInt("mail_options_smtp_port");
            _formOptionsView.SmtpLogin = Options.Instance.GetAsString("mail_options_smtp_login");
            _formOptionsView.SmtpPassword = Options.Instance.GetAsString("mail_options_smtp_password");
            _formOptionsView.From = Options.Instance.GetAsString("mail_options_from");
            _formOptionsView.EnableSsl = Options.Instance.GetAsBoolean("mail_options_smtp_ssl");

            _formOptionsView.FizzlerMessageTitle = Options.Instance.GetAsString("fizzler_message_title");
            _formOptionsView.FizzlerMessageEmail = Options.Instance.GetAsString("fizzler_message_email");
            _formOptionsView.FizzlerMessageSenderName = Options.Instance.GetAsString("fizzler_message_sender_name");
            _formOptionsView.FizzlerMessageSenderNameAlternative = Options.Instance.GetAsString("fizzler_message_sender_name_alternative");
            _formOptionsView.FizzlerMessageDate = Options.Instance.GetAsString("fizzler_message_date");
            _formOptionsView.FizzlerMessageContents = Options.Instance.GetAsString("fizzler_message_contents");
            _formOptionsView.RegexMessagesCount = Options.Instance.GetAsString("regex_messages_count");

            _formOptionsView.FizzlerAttachmentNode = Options.Instance.GetAsString("fizzler_attachment_node");
            _formOptionsView.FizzlerAttachmentOriginalMessage = Options.Instance.GetAsString("fizzler_attachment_original_message");
            _formOptionsView.RegexAttachmentsCount = Options.Instance.GetAsString("regex_attachments_count");

            _formOptionsView.FizzlerMembersTableRow = Options.Instance.GetAsString("fizzler_members_table_row");
            _formOptionsView.FizzlerMemberName = Options.Instance.GetAsString("fizzler_member_name");
            _formOptionsView.FizzlerMemberNameAlternative = Options.Instance.GetAsString("fizzler_member_name_alternative");
            _formOptionsView.FizzlerMemberEmail = Options.Instance.GetAsString("fizzler_member_email");
            _formOptionsView.FizzlerMemberJoinDate = Options.Instance.GetAsString("fizzler_member_join_date");
            _formOptionsView.RegexMembersCount = Options.Instance.GetAsString("regex_members_count");

            _formOptionsView.FizzlerFileUri = Options.Instance.GetAsString("fizzler_file_uri");

            _formOptionsView.FizzlerPhotoDiv = Options.Instance.GetAsString("fizzler_photo_div");
            _formOptionsView.FizzlerPhotoUri = Options.Instance.GetAsString("fizzler_photo_uri");
            _formOptionsView.FizzlerPhotoThumbnail = Options.Instance.GetAsString("fizzler_photo_thumbnail");
            _formOptionsView.FizzlerAlbumLink = Options.Instance.GetAsString("fizzler_album_link");
            _formOptionsView.FizzlerAlbumLinkSecondary = Options.Instance.GetAsString("fizzler_album_link_second");

            _formOptionsView.FizzlerUnfoundGroupCriterion = Options.Instance.GetAsString("fizzler_unfound_group_criterion");
            _formOptionsView.FizzlerUnfoundGroupName = Options.Instance.GetAsString("fizzler_unfound_group_name");

            _formOptionsView.MarkMessagesAsReadAfterSeconds = Options.Instance.GetAsInt("miscellaneous_options_mark_messages_as_read_after_seconds");
            _formOptionsView.MainWindowPanesFontName = Options.Instance.GetAsString("miscellaneous_options_main_window_panes_font_name");
            _formOptionsView.MinimizeToSystemTray = Options.Instance.GetAsBoolean("miscellaneous_options_minimize_to_system_tray");
            _formOptionsView.ShowEventsOnMainFormMessagesPaneCount = Options.Instance.GetAsInt("miscellaneous_options_show_events_on_main_form_messages_pane_count");
            _formOptionsView.LogEventsToFile = Options.Instance.GetAsBoolean("miscellaneous_options_log_events_to_file");
            _formOptionsView.LogEventsFilePath = Options.Instance.GetAsString("miscellaneous_options_log_events_file_path");
            _formOptionsView.SaveSortOrderInMessagesPane = Options.Instance.GetAsBoolean("miscellaneous_options_save_sort_order");

            // download options
            _formOptionsView.EnableDownloadLimitSpeed = Options.Instance.GetAsBoolean("download_options_enable_download_limit_speed");
            _formOptionsView.DownloadLimitSpeedType = Options.Instance.GetAsInt("download_options_download_limit_speed_type");
            _formOptionsView.MessagesPerMinute = Options.Instance.GetAsInt("download_options_messages_per_minute");
            _formOptionsView.InitialDelay = Options.Instance.GetAsInt("download_options_initial_delay");
            _formOptionsView.Download =  Options.Instance.GetAsInt("download_options_download");
            _formOptionsView.ThenWait = Options.Instance.GetAsInt("download_options_then_wait");
            _formOptionsView.EnableCacheServer = Options.Instance.GetAsBoolean("download_options_enable_cache_server");
            _formOptionsView.CacheServerName = 
                string.IsNullOrEmpty(Options.Instance.GetAsString("download_options_cache_server_name"))
                    ? Settings.Default.DefaultCacheServerName
                    : Options.Instance.GetAsString("download_options_cache_server_name");
            _formOptionsView.EnableMessagesCaching = Options.Instance.GetAsBoolean("download_options_enable_messages_caching");
            _formOptionsView.ReceiveMessagesPerRequest = Options.Instance.GetAsInt("download_options_receive_messages_per_request");

            _formOptionsView.RefreshMembersType = Options.Instance.GetAsInt("refresh_members_options_refresh_members_type");
            _formOptionsView.RefreshPeriod = Options.Instance.GetAsInt("refresh_members_options_refresh_period");

            _formOptionsView.OnFailureReRequestMessageTimes = Options.Instance.GetAsInt("workflow_options_on_failure_rerequest_message_times");
            _formOptionsView.OnLockoutResumeAfterMinutesEnabled = Options.Instance.GetAsBoolean("workflow_options_on_lockout_resume_after_minutes_enabled");
            _formOptionsView.OnLockoutResumeAfterMinutes = Options.Instance.GetAsInt("workflow_options_on_lockout_resume_after_minutes");
            _formOptionsView.StopAfterReceivingInvalidPagesEnabled = Options.Instance.GetAsBoolean("workflow_options_stop_after_receiving_invalid_pages_enabled");
            _formOptionsView.StopAfterReceivingInvalidPages = Options.Instance.GetAsInt("workflow_options_stop_after_receiving_invalid_pages");
            _formOptionsView.SkipAuthorization = Options.Instance.GetAsBoolean("workflow_options_skip_authorization");

            _formOptionsView.RefreshGroupType = Options.Instance.GetAsInt("refresh_groups_refresh_type");
            _formOptionsView.HoursPeriod = Options.Instance.GetAsInt("refresh_groups_hours_period");
            _formOptionsView.MinutesPeriod = Options.Instance.GetAsInt("refresh_groups_minutes_period");
            _formOptionsView.DownloadMessagesPerGroupLimit = Options.Instance.GetAsBoolean("refresh_groups_download_messages_per_group_limit");
            _formOptionsView.DownloadMessagesPerGroupLimitCount = Options.Instance.GetAsInt("refresh_groups_download_messages_per_group_limit_count");
            _formOptionsView.RefreshGroupLoopType = Options.Instance.GetAsInt("refresh_groups_loop_type");
            _formOptionsView.LoopCount = Options.Instance.GetAsInt("refresh_groups_loop_count");

            _formOptionsView.SaveOptionsRequest += new SaveImportOptionsRequestEventHandler(OnSaveOptionsRequest);
        }

        void OnSaveOptionsRequest()
        {
            Options.Instance.Set("mail_options_smtp_server", _formOptionsView.SmtpServer);
            Options.Instance.Set("mail_options_smtp_port", _formOptionsView.SmtpPort);
            Options.Instance.Set("mail_options_smtp_login", _formOptionsView.SmtpLogin);
            Options.Instance.Set("mail_options_smtp_password", _formOptionsView.SmtpPassword);
            Options.Instance.Set("mail_options_from", _formOptionsView.From);
            Options.Instance.Set("mail_options_smtp_ssl", _formOptionsView.EnableSsl);

            Options.Instance.Set("fizzler_message_title", _formOptionsView.FizzlerMessageTitle);
            Options.Instance.Set("fizzler_message_email", _formOptionsView.FizzlerMessageEmail);
            Options.Instance.Set("fizzler_message_sender_name", _formOptionsView.FizzlerMessageSenderName);
            Options.Instance.Set("fizzler_message_sender_name_alternative", _formOptionsView.FizzlerMessageSenderNameAlternative);
            Options.Instance.Set("fizzler_message_date", _formOptionsView.FizzlerMessageDate);
            Options.Instance.Set("fizzler_message_contents", _formOptionsView.FizzlerMessageContents);
            Options.Instance.Set("regex_messages_count", _formOptionsView.RegexMessagesCount);

            Options.Instance.Set("fizzler_attachment_node", _formOptionsView.FizzlerAttachmentNode);
            Options.Instance.Set("fizzler_attachment_original_message", _formOptionsView.FizzlerAttachmentOriginalMessage);
            Options.Instance.Set("regex_attachments_count", _formOptionsView.RegexAttachmentsCount);
            
            Options.Instance.Set("fizzler_members_table_row", _formOptionsView.FizzlerMembersTableRow);
            Options.Instance.Set("fizzler_member_name", _formOptionsView.FizzlerMemberName);
            Options.Instance.Set("fizzler_member_name_alternative", _formOptionsView.FizzlerMemberNameAlternative);
            Options.Instance.Set("fizzler_member_email", _formOptionsView.FizzlerMemberEmail);
            Options.Instance.Set("fizzler_member_join_date", _formOptionsView.FizzlerMemberJoinDate);
            Options.Instance.Set("regex_members_count", _formOptionsView.RegexMembersCount);

            Options.Instance.Set("fizzler_unfound_group_name", _formOptionsView.FizzlerUnfoundGroupName);
            Options.Instance.Set("fizzler_unfound_group_criterion", _formOptionsView.FizzlerUnfoundGroupCriterion);

            Options.Instance.Set("fizzler_photo_div", _formOptionsView.FizzlerPhotoDiv);
            Options.Instance.Set("fizzler_photo_uri", _formOptionsView.FizzlerPhotoUri);
            Options.Instance.Set("fizzler_photo_thumbnail", _formOptionsView.FizzlerPhotoThumbnail);
            Options.Instance.Set("fizzler_album_link", _formOptionsView.FizzlerAlbumLink);
            Options.Instance.Set("fizzler_album_link_second", _formOptionsView.FizzlerAlbumLinkSecondary);

            Options.Instance.Set("fizzler_file_uri", _formOptionsView.FizzlerFileUri);

            Options.Instance.Set("miscellaneous_options_mark_messages_as_read_after_seconds", _formOptionsView.MarkMessagesAsReadAfterSeconds);
            Options.Instance.Set("miscellaneous_options_main_window_panes_font_name", _formOptionsView.MainWindowPanesFontName);
            Options.Instance.Set("miscellaneous_options_minimize_to_system_tray", _formOptionsView.MinimizeToSystemTray);
            Options.Instance.Set("miscellaneous_options_show_events_on_main_form_messages_pane_count", _formOptionsView.ShowEventsOnMainFormMessagesPaneCount);
            Options.Instance.Set("miscellaneous_options_log_events_to_file", _formOptionsView.LogEventsToFile);
            Options.Instance.Set("miscellaneous_options_log_events_file_path", _formOptionsView.LogEventsFilePath);
            Options.Instance.Set("miscellaneous_options_save_sort_order", _formOptionsView.SaveSortOrderInMessagesPane);

            // download options
            Options.Instance.Set("download_options_enable_download_limit_speed", _formOptionsView.EnableDownloadLimitSpeed);
            Options.Instance.Set("download_options_download_limit_speed_type", _formOptionsView.DownloadLimitSpeedType);
            Options.Instance.Set("download_options_messages_per_minute", _formOptionsView.MessagesPerMinute);
            Options.Instance.Set("download_options_initial_delay", _formOptionsView.InitialDelay);
            Options.Instance.Set("download_options_download", _formOptionsView.Download);
            Options.Instance.Set("download_options_then_wait", _formOptionsView.ThenWait);
            Options.Instance.Set("download_options_enable_cache_server", _formOptionsView.EnableCacheServer);
            Options.Instance.Set("download_options_cache_server_name", _formOptionsView.CacheServerName);
            Options.Instance.Set("download_options_enable_messages_caching", _formOptionsView.EnableMessagesCaching);
            Options.Instance.Set("download_options_receive_messages_per_request", _formOptionsView.ReceiveMessagesPerRequest);

            Options.Instance.Set("refresh_members_options_refresh_members_type", _formOptionsView.RefreshMembersType);
            Options.Instance.Set("refresh_members_options_refresh_period", _formOptionsView.RefreshPeriod);

            Options.Instance.Set("workflow_options_on_failure_rerequest_message_times", _formOptionsView.OnFailureReRequestMessageTimes);
            Options.Instance.Set("workflow_options_on_lockout_resume_after_minutes_enabled", _formOptionsView.OnLockoutResumeAfterMinutesEnabled);
            Options.Instance.Set("workflow_options_on_lockout_resume_after_minutes", _formOptionsView.OnLockoutResumeAfterMinutes);
            Options.Instance.Set("workflow_options_stop_after_receiving_invalid_pages_enabled", _formOptionsView.StopAfterReceivingInvalidPagesEnabled);
            Options.Instance.Set("workflow_options_stop_after_receiving_invalid_pages", _formOptionsView.StopAfterReceivingInvalidPages);
            Options.Instance.Set("workflow_options_skip_authorization", _formOptionsView.SkipAuthorization);

            Options.Instance.Set("refresh_groups_refresh_type", _formOptionsView.RefreshGroupType);
            Options.Instance.Set("refresh_groups_hours_period", _formOptionsView.HoursPeriod);
            Options.Instance.Set("refresh_groups_minutes_period", _formOptionsView.MinutesPeriod);
            Options.Instance.Set("refresh_groups_download_messages_per_group_limit", _formOptionsView.DownloadMessagesPerGroupLimit);
            Options.Instance.Set("refresh_groups_download_messages_per_group_limit_count", _formOptionsView.DownloadMessagesPerGroupLimitCount);
            Options.Instance.Set("refresh_groups_loop_type", _formOptionsView.RefreshGroupLoopType);
            Options.Instance.Set("refresh_groups_loop_count", _formOptionsView.LoopCount);

            Options.Instance.SaveOptions();
        }
    }
}
