﻿using PGOffline.Model;
using PGOffline.View;

namespace PGOffline.Presenter
{
    class FormMasterLoginPresenter
    {
        IFormMasterLoginView _formMasterLoginView;

        UserAccount _userAccount;

        public FormMasterLoginPresenter(IFormMasterLoginView formMasterLoginView)
        {
            _formMasterLoginView = formMasterLoginView;
            _formMasterLoginView.MasterCredentialsChanged += new MasterCredentialsChangedRequestHandler(OnMasterCredentialsChanged);

            _userAccount = UserAccount.GetFirst();

            if (_userAccount != null)
            {
                _formMasterLoginView.Username = _userAccount.Name;
                _formMasterLoginView.Password = _userAccount.Password;
            }
        }

        void OnMasterCredentialsChanged()
        {
            if (_userAccount == null)
            {
                UserAccount.Create(_formMasterLoginView.Username, _formMasterLoginView.Password);
            }
            else
            {
                _userAccount.Name = _formMasterLoginView.Username;
                _userAccount.Password = _formMasterLoginView.Password;
                _userAccount.Save();
            }
        }
    }
}
