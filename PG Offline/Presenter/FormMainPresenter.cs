﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Asfdfdfd.Api.Yahoo.Groups;
using PGOffline.Exceptions;
using PGOffline.Model;
using PGOffline.Properties;
using PGOffline.View;

namespace PGOffline.Presenter
{
    class FormMainPresenter
    {
        IFormMainView _formMainView;

        string _version = string.Empty;

        YahooGroupsServer _yahooServer;

        YahooGroup _currentYahooGroup;

        DiscussionGroup _currentDiscussionGroup;

        private bool _windowActivated = false;

        // #1774
        // Some users have duplicate discussion groups entries. It 
        // shouldn't occur but has happened a couple of times
        List<DiscussionGroup> _discussionGroups = new List<DiscussionGroup>();

        private IMessagesLoader _messagesLoader;

        bool _requestedLoadingMembersForAllGroups = false;

        PGOTraceListener _traceListener;

        private MessagesListPresenter _messagesListPresenter;

        private System.Windows.Forms.Timer _refreshMembersTimer;

        private System.Windows.Forms.Timer _refreshGroupsTimer;

        private bool _isGroupsRefreshInProgress = false;

        public FormMainPresenter(IFormMainView formMainView)
        {
            string lastDatabasePath = null;

            if (File.Exists(PathResolver.GetOptionsPath()))
            {
                lastDatabasePath = Options.Instance.GetAsString("last_database_path");

                if (!File.Exists(lastDatabasePath) && lastDatabasePath != null)
                {
                    string fileName = Path.GetFileName(lastDatabasePath);

                    lastDatabasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "PG Offline 4", fileName);

                    if (!File.Exists(lastDatabasePath))
                    {
                        lastDatabasePath = null;
                    }
                }
            }

            _formMainView = formMainView;

            // CONNECT LAST DATABASE 
            var connection = new FormMainPresenterConnection();

            connection.TryOpenConnection(lastDatabasePath);
            {
                _formMainView.DatabaseTitle = string.Format("[{0}]", Path.GetFileName(lastDatabasePath));
            }

            // ON EACH START OF APPLICATION
            // check if databases need upgrading
            var databaseUpgradeComplete = UpgradeDatabaseManager.Instance.UpgradeDatabase();
            var optionsDatabaseUpgradeComplete = UpgradeOptionsDatabaseManager.Instance.UpgradeDatabase();


            _messagesListPresenter = new MessagesListPresenter(_formMainView.MessagesListView);
            _messagesListPresenter.MessageSelected += new MessageSelectedEventHandler((message) => _formMainView.DisplayMessage(message));
            _messagesListPresenter.MessageSelected += new MessageSelectedEventHandler((message) => _currentMessage = message);

            _traceListener = new PGOTraceListener(_formMainView.LogArea);

            Trace.Listeners.Add(_traceListener);

            _formMainView.SetDiscussionGroupsList(_discussionGroups);

            SetupMenuHandlers();

            _formMainView.OpenDatabaseRequest += new OpenDatabaseRequestEventHandler(OnOpenDatabaseRequest);
            _formMainView.ExitRequest += new ExitRequestEventHandler(OnExitRequest);
            _formMainView.LoadGroupsListRequest += new LoadGroupsListRequestEventHandler(OnLoadGroupsListRequest);
            _formMainView.DisplayGroupsRequest += new DisplayGroupsRequestEventHandler(UpdateDiscussionGroupsList);
            _formMainView.LoadMessagesRequest += new LoadMessagesRequestEventHandler(OnLoadMessagesRequest);
            _formMainView.RefreshAllGroupsRequest += new RefreshAllGroupsRequestEventHander(OnRefreshAllGroupsRequest);
            _formMainView.DiscussionGroupSelected += new DiscussionGroupSelectedEventHandler(OnDiscussionGroupSelected);
            _formMainView.LoadMembersRequest += new LoadMembersRequestEventHandler(OnLoadMembersRequest);
            _formMainView.LoadMembersForAllGroupsRequest += new LoadMembersForAllGroupsRequestEventHandler(OnLoadMembersForAllGroupsRequest);
            _formMainView.CancelProcedureRequest += new CancelProcedureRequestEventHandler(OnCancelProcedureRequest);
            _formMainView.SearchRequest += new SearchRequestEventHandler(OnSearchRequest);
            _formMainView.ShowMasterLoginSettingsRequest += new ShowMasterLoginSettingsRequestEventHandler(() => ShowModalForm(typeof(FormMasterLogin), typeof(FormMasterLoginPresenter)));
            _formMainView.WindowActivated += new WindowActivatedEventHandler(OnWindowActivated);
            _formMainView.MarkAllGroupsAsReadRequest += new MarkAllGroupsAsReadRequestEventHandler(OnMarkAllGroupsAsReadRequest);
            _formMainView.ShowAllMessagesRequest += new ShowAllMessagesRequestEventHandler(_messagesListPresenter.ShowAllMessages);
            _formMainView.ShowOnlyNewMessagesRequest += new ShowOnlyNewMessagesRequestEventHandler(_messagesListPresenter.ShowOnlyNewMessages);
            _formMainView.ShowOnlyFavoritesMessagesRequest += new ShowOnlyFavoritesMessagesRequestEventHandler(_messagesListPresenter.ShowOnlyFavoriteMessages);
            _formMainView.ToolbarCaptionsVisibilityChanged += new ToolbarCaptionsVisibilityChangedHandler(OnToolbarCaptionsVisibilityChanged);
            _formMainView.ToolbarButtonsSizeChanged += new ToolbarButtonsSizeChangedHandler(OnToolbarButtonsSizeChanged);
            _formMainView.ReplyMessageRequest += new ReplyMessageRequestEventHandler(OnReplyMessageRequest);
            _formMainView.HideQuoteStringRequest += new HideQuoteStringRequestEventHandler(OnHideQuoteStringRequest);
            _formMainView.SetEncoding += new SetEncodingHandler(FormMainViewSetEncoding);
            _formMainView.RecentDatabasesRequest += new RecentDatabasesRequestEventHandler(OnRecentDatabasesRequest);

            SetupYahooServer();

            SetupTrialTimer();

            _formMainView.SetToolbarCaptionsVisibility(!Options.Instance.GetAsBoolean("toolbar_captions_disabled"));
            _formMainView.SetToolbarSmallButtons(Options.Instance.GetAsBoolean("toolbar_buttons_small"));

            _formMainView.SetFont(Options.Instance.GetAsString("miscellaneous_options_main_window_panes_font_name"));
            _formMainView.LogArea.MaxLines = Options.Instance.GetAsInt("miscellaneous_options_show_events_on_main_form_messages_pane_count");

            var intMembersOptionsRefreshType = Options.Instance.GetAsInt("refresh_members_options_refresh_members_type");

            switch (intMembersOptionsRefreshType)
            {
                case 2:
                    //_refreshMembersTimer = new System.Windows.Forms.Timer();
                    //_refreshMembersTimer.Interval = Options.Instance.GetAsInt("refresh_members_options_refresh_period") * 24 * 60 * 60 * 1000;
                    //_refreshMembersTimer.Tick += new EventHandler((sender, e) => OnRefreshAllGroupsRequest());
                    //_refreshMembersTimer.Start();
                    break;
                case 1:
                    //OnRefreshAllGroupsRequest();
                    break;
            }

            var intGroupsRefreshType = Options.Instance.GetAsInt("refresh_groups_refresh_type");

            switch (intGroupsRefreshType)
            {
                case 1:
                    OnRefreshAllGroupsRequest();
                    break;

                case 2:
                    int hours = Options.Instance.GetAsInt("refresh_groups_hours_period");

                    int minutes = Options.Instance.GetAsInt("refresh_groups_minutes_period");

                    _refreshGroupsTimer = new System.Windows.Forms.Timer
                    {
                        Interval = (hours * 60 + minutes) * 60 * 1000
                    };

                    _refreshGroupsTimer.Tick += new EventHandler((sender, e) => OnRefreshAllGroupsRequest());

                    _refreshGroupsTimer.Start();

                    break;
            }

            _formMainView.SetQuoteFiltering(Options.Instance.GetAsBoolean("hide_qoute_string"));
        }

        private void OnRecentDatabasesRequest()
        {
        }

        void FormMainViewSetEncoding(Encoding encoding)
        {
            if (encoding == null)
            {
                encoding = Encoding.Default;
            }

            Options.Instance.Set("global_encoding", encoding.WebName);

            Options.Instance.SaveOptions();

            if (_currentDiscussionGroup != null)
            {
                OnDiscussionGroupSelected(_currentDiscussionGroup);
            }
        }

        void OnHideQuoteStringRequest(bool hide)
        {
            Options.Instance.Set("hide_quote_string", hide);

            _formMainView.DisplayMessage(_currentMessage);
        }

        void OnReplyMessageRequest(Message message)
        {
            string url;

            if (message != null)
            {
                url = String.Format("http://groups.yahoo.com/group/{0}/post?act=reply&messageNum={1}", message.DiscussionGroup.Name, message.Number);
            }
            else
            {
                url = String.Format("http://groups.yahoo.com/group/{0}/post?act=reply", message.DiscussionGroup.Name);
            }

            Utils.OpenLinkInExternalBrowser(url);
        }

        void OnToolbarButtonsSizeChanged(bool small)
        {
            Options.Instance.Set("toolbar_buttons_small", small);

            Options.Instance.SaveOptions();
        }

        void OnToolbarCaptionsVisibilityChanged(bool visible)
        {
            Options.Instance.Set("toolbar_captions_disabled", !visible);

            Options.Instance.SaveOptions();
        }

        void OnOpenDatabaseRequest(string path)
        {
            LocalCache.Instance.DropAllCaches();
            
            try
            {
                DatabaseManager.Instance.OpenConnection(path);
            }
            catch (DatabaseWrongVersionException ex)
            {
                Trace.WriteLine(ex.Message);

                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }
            catch (DatabaseWrongFormatException ex)
            {
                Trace.WriteLine(ex.Message);

                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }

            _formMainView.DatabaseTitle = string.Format("[{0}]", Path.GetFileName(path));

            _formMainView.UpdateWindowTitle();

            UpdateDiscussionGroupsList();

            _messagesListPresenter.SetMessages(new List<Message>(), true);

            Options.Instance.Set("last_database_path", path);

            Options.Instance.SaveOptions();

            if (UserAccount.GetFirst() == null)
            {
                ShowModalForm(typeof(FormMasterLogin), typeof(FormMasterLoginFirstStartPresenter));
            }                
        }

        private void SetupYahooServer()
        {
            _yahooServer = new YahooGroupsServer(Utils.GetAppDownloadOptions());

            UpdateDiscussionGroupsList();

            _yahooServer.SetParsingOptions(Options.Instance.GetParsingOptions());
        }

        private void SetupMenuHandlers()
        {
            _formMainView.ShowStatisticsBuilderRequest += new ShowStatisticsBuilderRequestEventHandler(() => ShowModalForm(typeof(FormStatisticsBuilder), typeof(FormStatisticsBuilderPresenter)));
            _formMainView.ShowStatsRequest += new ShowStatsRequestEventHandler(OnShowStatsRequest);
            _formMainView.ShowDigestRequest += new ShowDigestRequestEventHandler(OnShowDigestRequest);
            _formMainView.ShowSelectionAsDigestRequest += new ShowSelectionAsDigestRequestEventHandler(OnShowSelectionAsDigestRequest);
            _formMainView.OpenMessageInYahooRequest += new OpenMessageInYahooRequestEventHandler(OnOpenMessageInYahooRequest);
            _formMainView.FullScreenViewRequest += new FullScreenViewRequestEventHandler(OnFullScreenViewRequest);
            _formMainView.ImportFromFolderRequest += new ImportFromFolderRequestEventHandler(OnImportFromFolderRequest);
            _formMainView.ExportToFolderRequest += new ExportToFolderRequestEventHandler(() => ShowModalForm(typeof(FormExportToFolder), typeof(FormExportToFolderPresenter)));
            _formMainView.OpenDataFolderRequest += new OpenDataFolderRequestEventHandler(OnOpenDataFolderRequest);
            _formMainView.ShowOptionsRequest += new ShowOptionsRequestEventHandler(OnShowOptionsRequest);
            _formMainView.ShowGroupSettingsRequest += new ShowGroupSettingsRequestEventHander(OnShowGroupSettingsRequest);
            _formMainView.NewGroupRequest += new NewGroupRequestEventHandler(OnNewGroupRequest);
            _formMainView.ShowDownloadGroupFilesRequest += new ShowDownloadGroupFilesRequestEventHandler(OnShowDownloadGroupFilesRequest);
            _formMainView.ShowDownloadGroupPhotosRequest += new ShowDownloadGroupPhotosRequestEventHandler(OnShowDownloadGroupPhotosRequest);
            _formMainView.DeleteDiscussionGroupRequest += new DeleteDiscussionGroupRequestEventHandler(OnDeleteDiscussionGroupRequest);
        }

        void OnDeleteDiscussionGroupRequest(DiscussionGroup group)
        {
            DiscussionGroup.Delete(group);

            UpdateDiscussionGroupsList();
        }

        void OnWindowActivated()
        {
            if (_windowActivated) return;

            _windowActivated = true;

            var assembly = Assembly.GetExecutingAssembly();

            var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);

            _version = fvi.ProductVersion;

            _formMainView.SetTitle("PG Offline " + _version);
            
            Trace.WriteLine("PG Offline " + _version);
            
            if (UserAccount.GetFirst() == null)
            {
                ShowModalForm(typeof(FormMasterLogin), typeof(FormMasterLoginFirstStartPresenter));
            }
            else
            {
                try
                {
                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Days left for trial use - {0}", GetTrialDaysLeft()));
                }
                catch (TrialException ex)
                {
                    Trace.WriteLine(ex.Message);

                    MessageBox.Show(ex.Message, "Trial message", MessageBoxButton.OK, MessageBoxImage.Stop);

                    Environment.Exit(0);
                }
            }
        }

        private void SetupTrialTimer()
        {
            var trialTimer = new System.Windows.Forms.Timer
            {
                Interval = 300000
            };

            trialTimer.Tick += new EventHandler((sender, e) => GetTrialDaysLeft());

            trialTimer.Start();
        }

        int GetTrialDaysLeft()
        {
            try
            {
                return TrialVerifier.DaysLeft;
            }
            catch (TrialException ex)
            {
                Trace.WriteLine(ex.Message);

                MessageBox.Show(ex.Message, "Trial message", MessageBoxButton.OK, MessageBoxImage.Stop);

                Environment.Exit(0);
            }

            return 0;
        }

        void OnMarkAllGroupsAsReadRequest()
        {
            List<DiscussionGroup> discussionGroups = DiscussionGroup.GetAll();

            foreach (DiscussionGroup discussionGroup in discussionGroups)
            {
                discussionGroup.MarkAllMessagesAsRead();
            }

            _formMainView.UpdateMessagesList();

            _formMainView.UpdateDiscussionGroupsList();
        }

        void OnShowOptionsRequest()
        {
            int prevRefreshMemberType = Options.Instance.GetAsInt("refresh_members_options_refresh_members_type");

            int prevRefreshMemberTypeInterval = Options.Instance.GetAsInt("refresh_members_options_refresh_period");

            ShowModalForm(typeof(FormOptions), typeof(FormOptionsPresenter));

            _formMainView.SetFont(Options.Instance.GetAsString("miscellaneous_options_main_window_panes_font_name"));

            _formMainView.TrayMinimize(Options.Instance.GetAsBoolean("miscellaneous_options_minimize_to_system_tray"));

            _formMainView.LogArea.MaxLines = Options.Instance.GetAsInt("miscellaneous_options_show_events_on_main_form_messages_pane_count");

            if (Options.Instance.GetAsBoolean("miscellaneous_options_log_events_to_file"))
            {
                _traceListener.SetOutputFile(Options.Instance.GetAsString("miscellaneous_options_log_events_file_path"));
            }
            else
            {
                _traceListener.SetOutputFile(null);
            }

            if (!(prevRefreshMemberType == 2 && Options.Instance.GetAsInt("refresh_members_options_refresh_members_type") == 2 && prevRefreshMemberTypeInterval == Options.Instance.GetAsInt("refresh_members_options_refresh_period")))
            {
                if (Options.Instance.GetAsInt("refresh_members_options_refresh_members_type") == 2)
                {
                    if (_refreshMembersTimer != null)
                    {
                        _refreshMembersTimer.Stop();
                    }

                    _refreshMembersTimer = new System.Windows.Forms.Timer
                    {
                        Interval = Options.Instance.GetAsInt("refresh_members_options_refresh_period")*24*60*60*1000
                    };

                    _refreshMembersTimer.Tick += new EventHandler((sender, e) => OnRefreshAllGroupsRequest());

                    _refreshMembersTimer.Start();
                }
            }


            switch (Options.Instance.GetAsInt("refresh_groups_refresh_type"))
            {
                case 0:
                case 1:
                    if (_refreshGroupsTimer != null)
                    {
                        _refreshGroupsTimer.Stop();
                    }

                    break;

                case 2:
                    int hours = Options.Instance.GetAsInt("refresh_groups_hours_period");

                    int minutes = Options.Instance.GetAsInt("refresh_groups_minutes_period");

                    int msec = (hours * 60 + minutes) * 60 * 1000;

                    if (_refreshGroupsTimer == null || _refreshGroupsTimer.Interval != msec)
                    {
                        if (_refreshGroupsTimer != null)
                        {
                            _refreshGroupsTimer.Stop();
                        }

                        _refreshGroupsTimer = new System.Windows.Forms.Timer
                        {
                            Interval = msec
                        };

                        _refreshGroupsTimer.Tick += new EventHandler((sender, e) => OnRefreshAllGroupsRequest());

                        _refreshGroupsTimer.Start();
                    }
                    break;
            }
        }

        private Message _currentMessage;

        void OnFullScreenViewRequest()
        {
            var formFullScreen = new FormFullScreen();

            formFullScreen.Closing += new CancelEventHandler(OnFullScreenClosing);

            formFullScreen.FullScreenNextMessageRequest += new FullScreenNextMessageRequestEventHandler(OnFullScreenNextMessageRequest);
            formFullScreen.FullScreenPreviousMessageRequest += new FullScreenPreviousMessageRequestEventHandler(OnFullScreenPreviousMessageRequest);
            formFullScreen.FullScreenUpGroupRequest += new FullScreenUpGroupRequestEventHandler(OnFullScreenUpGroupRequest);
            formFullScreen.FullScreenDownGroupRequest += new FullScreenDownGroupRequestEventHandler(OnFullScreenDownGroupRequest);

            _formMainView.DiscussionGroupSelected += new DiscussionGroupSelectedEventHandler(formFullScreen.SetDiscussionGroup);
            _messagesListPresenter.MessageSelected += new MessageSelectedEventHandler(formFullScreen.SetMessage);

            _messagesListPresenter.AttachFullScreen(formFullScreen);

            if (_currentMessage == null)
                OnFullScreenNextMessageRequest();
            else
                formFullScreen.SetMessage(_currentMessage);

            formFullScreen.ShowDialog();
        }

        void OnFullScreenClosing(object sender, EventArgs e)
        {
            var formFullScreen = (FormFullScreen)sender;

            formFullScreen.Closing -= OnFullScreenClosing;

            formFullScreen.FullScreenNextMessageRequest -= OnFullScreenNextMessageRequest;
            formFullScreen.FullScreenPreviousMessageRequest -= OnFullScreenPreviousMessageRequest;
            formFullScreen.FullScreenUpGroupRequest -= OnFullScreenUpGroupRequest;
            formFullScreen.FullScreenDownGroupRequest -= OnFullScreenDownGroupRequest;

            _formMainView.DiscussionGroupSelected -= formFullScreen.SetDiscussionGroup;
            _messagesListPresenter.MessageSelected -= formFullScreen.SetMessage;

            _messagesListPresenter.DetachFullScreen(formFullScreen);
        }

        void OnFullScreenNextMessageRequest()
        {
            _formMainView.SelectNextMessage();
        }

        void OnFullScreenPreviousMessageRequest()
        {
            _formMainView.SelectPreviousMessage();
        }

        void OnFullScreenUpGroupRequest(object sender)
        {
            _formMainView.SelectPreviousDiscussionGroup();

            if (_formMainView.DisplayedMessagesCount() == 0)
            {
                ((FormFullScreen) sender).SetEmptyContent();
            }

            OnFullScreenNextMessageRequest();
        }

        void OnFullScreenDownGroupRequest(object sender)
        {
            _formMainView.SelectNextDiscussionGroup();

            if (_formMainView.DisplayedMessagesCount() == 0)
            {
                ((FormFullScreen) sender).SetEmptyContent();
            }

            OnFullScreenNextMessageRequest();
        }
        void OnOpenDataFolderRequest()
        {
            Process.Start("explorer.exe", PathResolver.GetApplicationDataPath());
        }

        void OnOpenMessageInYahooRequest(Message message)
        {
            Utils.OpenLinkInExternalBrowser(string.Format("http://groups.yahoo.com/group/{0}/message/{1}", message.DiscussionGroup.Name, message.Number));
        }

        void OnShowDigestRequest()
        {
            if (_currentDiscussionGroup == null) return;

            var formDigest = new FormDigest();

            var formDigestPresenter = new FormDigestPresenter(formDigest, _currentDiscussionGroup.GetAllNewMessages(), _currentDiscussionGroup, _currentDiscussionGroup.Name, "", false);

            formDigest.ShowDialog();
        }

        void OnShowSelectionAsDigestRequest(IList messages)
        {
            if (_currentDiscussionGroup == null) return;

            var formDigest = new FormDigest();

            var formDigestPresenter = new FormDigestPresenter(formDigest, messages, _currentDiscussionGroup, _currentDiscussionGroup.Name, " ", false);

            formDigest.ShowDialog();
        }

        void ShowModalForm(Type formType, Type presenterType)
        {
            var form = Activator.CreateInstance(formType);

            var presenter = Activator.CreateInstance(presenterType, new object[] { form });

            (form as Window).ShowDialog();
        }

        void OnShowStatsRequest(DiscussionGroup discussionGroup)
        {
            if (discussionGroup != null)
            {
                _formMainView.DisplayStats(discussionGroup);
            }
        }

        void OnSearchRequest()
        {
            var formFindAll = new FormFindAll();

            var formFindAllPresenter = new FormFindAllPresenter(formFindAll);

            formFindAllPresenter.SearchComplete += new FormFindAllPresenter.SearchCompleteEventHandler(OnSearchComplete);

            formFindAll.Show();
        }

        void OnSearchComplete(List<Message> messages)
        {
            _messagesListPresenter.SetMessages(messages);
        }

        void OnShowGroupSettingsRequest()
        {
            if (_currentDiscussionGroup == null) return;

            var formGroupSettings = new FormGroupSettings();

            var formGroupSettingsPresenter = new FormGroupSettingsPresenter(formGroupSettings, _currentDiscussionGroup);

            formGroupSettings.ShowDialog();
        }

        void OnNewGroupRequest()
        {
            var formGroupSettings = new FormGroupSettings();

            var formGroupSettingsNewGroupPresenter = new FormGroupSettingsNewGroupPresenter(formGroupSettings);

            formGroupSettingsNewGroupPresenter.NewGroupCreated += new FormGroupSettingsNewGroupPresenter.NewGroupCreatedEventHandler(UpdateDiscussionGroupsList);

            formGroupSettings.ShowDialog();
        }

        void OnShowDownloadGroupFilesRequest()
        {
            if (_currentDiscussionGroup == null) return;

            var formDownloadFiles = new FormDownloadContent();

            var formDownloadFilesPresenter = new FormDownloadFilesPresenter(formDownloadFiles, _currentDiscussionGroup, _yahooServer);

            formDownloadFiles.ShowDialog();
        }

        void OnShowDownloadGroupPhotosRequest()
        {
            if (_currentDiscussionGroup == null) return;

            var formDownloadPhotos = new FormDownloadContent();

            var formDownloadPhotosPresenter = new FormDownloadPhotosPresenter(formDownloadPhotos, _currentDiscussionGroup, _yahooServer);

            formDownloadPhotos.ShowDialog();
        }

        void _formMainView_OnLoadMessagesRequest(string groupName)
        {
            throw new NotImplementedException();
        }

        void OnExitRequest()
        {
            Application.Current.Shutdown();
        }

        void OnLoadGroupsListRequest()
        {
            //_formMainView.DisableControls();

            _formMainView.SetMenuItemGroupsListIsEnabled(false);

            Trace.WriteLine("Getting list of user groups");

            var loader = new DiscussionGroupsListLoader(_yahooServer);

            loader.DiscussionGroupsListLoaded += new DiscussionGroupsListLoader.DiscussionGroupsListLoadedEventHandler(OnReceiveYahooGroupsListCompleted);

            loader.DiscussionGroupsListLoadError += new DiscussionGroupsListLoader.DiscussionGroupsListLoadErrorEventHandler(OnDiscussionGroupsListLoadError);

            loader.LoadAsync();
        }

        void OnDiscussionGroupsListLoadError(string message)
        {
            //_formMainView.EnableControls();
            
            _formMainView.SetMenuItemGroupsListIsEnabled(true);

            Trace.WriteLine("List load error: " + message);
        }

        void OnImportFromFolderRequest()
        {
            var formImportFromFolder = new FormImportFromFolder();

            var formImportFromFolderPresenter = new FormImportFromFolderPresenter(formImportFromFolder);

            formImportFromFolderPresenter.ImportComplete += new FormImportFromFolderPresenter.ImportCompleteEventHandler(UpdateDiscussionGroupsList);

            formImportFromFolder.ShowDialog();
        }

        void OnReceiveYahooGroupsListCompleted(List<YahooGroup> yahooGroups)
        {
            //_formMainView.EnableControls();

            _formMainView.SetMenuItemStopIsEnabled(false);

            var formSelectGroups = new FormSelectGroups();

            var formSelectGroupsPresenter = new FormSelectGroupsPresenter(formSelectGroups, yahooGroups, DiscussionGroup.GetAll());

            formSelectGroupsPresenter.GroupsListChanged += new FormSelectGroupsPresenter.GroupsListChangedEventHandler(UpdateDiscussionGroupsList);

            formSelectGroups.Closed += new EventHandler(CloseGroupsListEventHandler);
            
            formSelectGroups.ShowDialog();
        }

        private void CloseGroupsListEventHandler(object sender, EventArgs e)
        {
            _formMainView.SetMenuItemGroupsListIsEnabled(true);
        }


        /*
        void OnReceiveYahooGroupsCompleted(List<YahooGroup> yahooGroups)
        {
            foreach (YahooGroup group in yahooGroups)
            {
                DiscussionGroup discussionGroup = DiscussionGroup.GetByName(group.Name);
                if (discussionGroup == null)
                {
                    DiscussionGroup newDiscussionGroup = DiscussionGroup.Create(group.Name);
                }
            }

            OnDisplayGroupsRequest();
        }
        */
        void UpdateDiscussionGroupsList()
        {
            _discussionGroups = DiscussionGroup.GetAll();

            _discussionGroups.Sort((group1, group2) => String.Compare(group1.Name, group2.Name, StringComparison.CurrentCultureIgnoreCase));

            _formMainView.SetDiscussionGroupsList(_discussionGroups);

            _formMainView.UpdateDiscussionGroupsList();
        }

        void OnCancelProcedureRequest(DiscussionGroup discussionGroup)
        {
            if (_downloader == null) return;

            _downloader.CancelAsync();

            _downloader = null;
        }

        private void OnGroupMessagesLoadingCompleted()
        {
            _formMainView.UpdateDiscussionGroupsList();

            //_formMainView.EnableControls();

            _formMainView.SetMenuItemStopIsEnabled(false);

            _messagesLoader = null;
        }

        DiscussionGroupsMessagesDownloader _downloader;

        private void OnLoadMessagesRequest(DiscussionGroup discussionGroup)
        {
            if (discussionGroup == null) return;
            //_formMainView.DisableControls();

            _formMainView.SetMenuItemStopIsEnabled(true);
            
            UserAccount currentUser = discussionGroup.GetUserAccount();

            if (!_yahooServer.Login(currentUser.Name, currentUser.Password)) return;

            Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Receiving messages for group {0}", discussionGroup.Name));

            _downloader = new DiscussionGroupsMessagesDownloader(_yahooServer);

            _downloader.DownloadNewMessagesAsync(discussionGroup);

            _downloader.Finished += new DiscussionGroupsMessagesDownloader.FinishedEventHandler(() =>
            {
                OnGroupMessagesLoadingCompleted();

                Trace.WriteLine("Downloading completed.");
            });

            _downloader.Canceled += new DiscussionGroupsMessagesDownloader.CanceledEventHandler(() =>
            {
                OnCanceled();

                OnGroupMessagesLoadingCompleted();

                Trace.WriteLine("Downloading canceled.");
            });
        }

        void OnRefreshAllGroupsRequest()
        {
            // select the first discussion group otherwise we can't seem to stop the process
            _formMainView.SelectNextDiscussionGroup();

            _formMainView.SetMenuItemStopIsEnabled(true);

            _discussionGroups = DiscussionGroup.GetAll();

            if (_discussionGroups.Count == 0 || _isGroupsRefreshInProgress)
            {
                return;
            }

            _isGroupsRefreshInProgress = true;

            //_formMainView.DisableControls();

            _formMainView.SetMenuItemStopIsEnabled(false);

            Trace.WriteLine("Receiving messages for all groups.");

            _downloader = new DiscussionGroupsMessagesDownloader(_yahooServer);

            _downloader.DownloadNewMessagesAsync(_discussionGroups);

            _downloader.Finished += new DiscussionGroupsMessagesDownloader.FinishedEventHandler(() =>
            {
                OnGroupsMessagesLoadingCompleted();

                Trace.WriteLine("Downloading completed.");
            });

            _downloader.Canceled += new DiscussionGroupsMessagesDownloader.CanceledEventHandler(() =>
            {
                OnCanceled();

                OnGroupsMessagesLoadingCompleted();

                Trace.WriteLine("Downloading canceled.");
            });
        }

        private void OnCanceled()
        {
            if (_currentYahooGroup != null)
            {
                _requestedLoadingMembersForAllGroups = false;

                // _currentYahooGroup.ReceiveMembersCancelAsync();

                if (_refreshingDiscussionGroup != null)
                {
                    _refreshingDiscussionGroup.SaveMembers();
                }
            }

            _isGroupsRefreshInProgress = false;

            //_formMainView.EnableControls();

            _formMainView.SetMenuItemStopIsEnabled(false);
        }

        private void OnGroupsMessagesLoadingCompleted()
        {
            _isGroupsRefreshInProgress = false;

            _formMainView.UpdateDiscussionGroupsList();

            //_formMainView.EnableControls();

            _formMainView.SetMenuItemStopIsEnabled(false);

            _messagesLoader = null;
        }

        #region Old groups members loading code. Refactor!

        private DiscussionGroup _refreshingDiscussionGroup;

        private DiscussionGroup GetNextDiscussionGroup(DiscussionGroup previousDiscussionGroup)
        {
            int index = _discussionGroups.IndexOf(previousDiscussionGroup);

            for (int i = index; i < _discussionGroups.Count - 1; i++)
            {
                DiscussionGroup nextGroup = _discussionGroups[i + 1];

                if (nextGroup.GroupSettings.GetAsBoolean("skip_during_refresh_all_groups") == false)
                {
                    return nextGroup;
                }

                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Group {0} skipped", nextGroup.Name));

                continue;
            }

            return null;
        }

        private void OnLoadMembersRequest(DiscussionGroup discussionGroup)
        {
            if (_refreshingDiscussionGroup == null)
            {
                _refreshingDiscussionGroup = discussionGroup;
            }

            try
            {
                UserAccount currentUser = _refreshingDiscussionGroup.GetUserAccount();

                if (!_yahooServer.Login(currentUser.Name, currentUser.Password)) return;

                _currentYahooGroup = _yahooServer.CreateGroup(discussionGroup.Name);

                _currentYahooGroup.ReceiveMembersProgress += new YahooGroup.ReceiveMembersProgressEventHandler((percent, member) => discussionGroup.AddMember(member));

                _currentYahooGroup.ReceiveMembersCompleted += new YahooGroup.ReceiveMembersCompletedEventHandler(OnReceiveMembersCompleted);

                _currentYahooGroup.ReceiveMembersAsync();
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error. " + ex.Message);

                return;
            }
        }

        private void OnLoadMembersForAllGroupsRequest()
        {
            if (_discussionGroups.Count == 0)
            {
                return;
            }

            _requestedLoadingMembersForAllGroups = true;

            _refreshingDiscussionGroup = _discussionGroups.First();

            if (_refreshingDiscussionGroup.GroupSettings.GetAsBoolean("skip_during_get_members_for_all_groups") == false)
            {
                OnLoadMembersRequest(_refreshingDiscussionGroup);
            }
            else
            {
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Group {0} skipped", _refreshingDiscussionGroup.Name));

                OnReceiveMembersCompleted();
            }            
        }

        private void OnReceiveMembersCompleted()
        {
            _refreshingDiscussionGroup.LastMembersUpdate = DateTime.Now;

            _refreshingDiscussionGroup.SaveMembers();

            if (_requestedLoadingMembersForAllGroups)
            {
                DiscussionGroup nextDiscussionGroup = GetNextDiscussionGroup(_refreshingDiscussionGroup);

                if (nextDiscussionGroup != null)
                {
                    _refreshingDiscussionGroup = nextDiscussionGroup;

                    OnLoadMembersRequest(_refreshingDiscussionGroup);
                }
                else
                {
                    _requestedLoadingMembersForAllGroups = false;

                    _refreshingDiscussionGroup = null;
                }
            }
            else
            {
                _refreshingDiscussionGroup = null;
            }
        }
        #endregion

        private void OnDiscussionGroupSelected(DiscussionGroup discussionGroup)
        {
            if (!discussionGroup.IsHandlerSet)
            {
                discussionGroup.PropertyChanged += new PropertyChangedEventHandler((sender, e) => _formMainView.UpdateDiscussionGroupsList());
            }

            _currentDiscussionGroup = discussionGroup;

            _formMainView.EnableMenuItemFullscreen(_currentDiscussionGroup != null);

            _messagesListPresenter.SetDiscussionGroup(_currentDiscussionGroup);

            _currentMessage = null;

            var totalMessageCount = _currentDiscussionGroup.GetMessageCount();

            _formMainView.StatusBar1.Items.Clear();

            _formMainView.StatusBar1.Items.Add(new TextBlock { Text = _currentDiscussionGroup.Name });

            _formMainView.StatusBar1.Items.Add(new Separator());

            _formMainView.StatusBar1.Items.Add(new TextBlock { Text = String.Format("{0:n0}", totalMessageCount) + " total messages" });
        }
    }
}
