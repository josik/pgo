﻿using System.Windows;
using System.Windows.Controls;
using PGOffline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;

namespace PGOffline
{
    public abstract class ImportManager<T> : IImportManager
    {
        #region Public Properties

        public event UpdateProgressEventHandler UpdateProgressRequest;

        public event ImportCompletedEventHandler ImportComplete;

        public string DbConnectionString { get; set; }
        
        public IDbConnection ImportConnection { get; set; }

        //public IDbCommand importCommand;

        public int ProgressPercentage { get; set; }

        #endregion Public Properties

        #region Protected Properties
        
        protected long UnitsToImportCount = 0;

        protected long UnitsProceed = 0;

        protected string DbFilePath { get; set; }

        protected List<ImportUnit> UnitsToImport;

        protected ImportSettings CurrentImportSettings;

        protected Dictionary<DiscussionGroup, ImportUnit> GroupsUnits;

        public BackgroundWorker ImportDataWorker { get; set; }

        public MediaElement StillWorkingSpinner { get; set; }

        //private Forms.FormDeterminateProgress _formIndeterminateProgress;

        #endregion Protected Properties

        #region Protected Abstract Members

        protected abstract IDbConnection GetNewDbConnection();
        protected abstract void ImportGroups(IEnumerable<ImportUnit> unitsToImport);
        public abstract List<ImportUnit> GetImportUnits(string mdbLocation);
        protected abstract long GetImportUnitsCount(IEnumerable<ImportUnit> unitsToImport);
        protected abstract void SynchronizeFavoritesStatus();
        
        
        
        #endregion Protected Abstract Members

        #region Protected Members

        protected void ReportProgressPercentage(int progressPercentage)
        {
            ImportDataWorker.ReportProgress(progressPercentage < 1 ? 1 : progressPercentage);
        }

        protected void ReportProgressMessage(string groupName)
        {

        }

        protected static void InsertPerson(string senderName, string senderEmail, DateTime senderJoined, long groupId)
        {
            if (senderEmail != "")
            {
                if (Person.GetByEmail(senderEmail) != null) return;

                Person person = new Person(senderName, senderJoined, senderEmail, false, groupId);

                person.Insert();
            }
            else
            {
                if (Person.GetByName(senderName) != null) return;

                Person person = new Person(senderName, senderJoined, senderEmail, false, groupId);

                person.Insert();
            }
        }

        #endregion Protected Members

        #region Constrcutors
            // Constructors
        #endregion Constructors

        #region Public Interface Members

        //public void ImportDataAsync()
        //{
            //if (ImportDataWorker != null)
            //{
            //    ImportDataCancelAsync();
            //}

            //ImportDataWorker = new BackgroundWorker { WorkerSupportsCancellation = true, WorkerReportsProgress = true };

            //ImportDataWorker.DoWork += new DoWorkEventHandler((sender, e) => ImportData());

            //ImportDataWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((sender, e) => ImportDataComplete());

            //ImportDataWorker.ProgressChanged += new ProgressChangedEventHandler((sender, e) => ImportDataProgress(e.ProgressPercentage));

            ////_formIndeterminateProgress = new Forms.FormDeterminateProgress();

            ////_formIndeterminateProgress.Show();

            //ImportDataWorker.RunWorkerAsync();
        //}


        private void ImportDataProgress(double progressPercentage)
        {
            //_formIndeterminateProgress.SetProgress(progressPercentage);
            OnUpdateProgress(progressPercentage);
        }
        private void OnUpdateProgress(double progressPercentage)
        {
            // Note the copy to a local variable, so that we don't risk a
            // NullReferenceException if another thread unsubscribes between the test and
            // the invocation.

            UpdateProgressEventHandler handler = UpdateProgressRequest;

            if (handler != null)
            {
                handler();
            }
        }


        public void ImportDataComplete()
        {
            // Update group settings.
            if (CurrentImportSettings.RememberLastMessageNumber)
            {
                foreach (ImportUnit importUnit in UnitsToImport)
                {
                    if (importUnit.LastImportedMessage <= 0) continue;

                    DiscussionGroup discussionGroup = DiscussionGroup.GetByName(importUnit.GroupName);

                    discussionGroup.GroupSettings.SetValue("start_download_from_message", (importUnit.LastImportedMessage + 1).ToString());

                    discussionGroup.GroupSettings.SaveSettings();
                }
            }

            OnImportComplete();

            //_formIndeterminateProgress.Close();
        }
        
        //public void ImportDataCancelAsync()
        //{
        //    ImportDataWorker.CancelAsync();
        //    //_formIndeterminateProgress.Close();
        //}

        #endregion Public Interface Members
        
        


        #region Private Members

        public void ImportData()
        {
            // cycle connection, otherwise we seem to encounter table contraint issues when inserts occur right after deletes within the same opne connection
            //DatabaseManager.Instance.OpenConnection(DbFilePath);
            DatabaseManager.Instance.ResetConnection();

            if (UnitsToImport.Count == 0)
            {
                return;
            }

            ReportProgressPercentage(1); // show process as having begun

            //StillWorkingSpinner.Visibility = Visibility.Visible;

            if (CurrentImportSettings.ImportType == ImportType.OnlyFavoritesNumbers)
            {
                SynchronizeFavoritesStatus();
            }
            else
            {
                ImportGroups(UnitsToImport);
            }

            //StillWorkingSpinner.Visibility = Visibility.Hidden;

            DatabaseManager.Instance.ResetConnection();
        }

        private void OnImportComplete()
        {
            // Note the copy to a local variable, so that we don't risk a
            // NullReferenceException if another thread unsubscribes between the test and
            // the invocation.

            ImportCompletedEventHandler handler = ImportComplete;

            if (handler != null)
            {
                handler();
            }
        }



        //protected void FillMembers(DiscussionGroup group)
        //{
        //    IEnumerable<IDataRecord> members = GetMembersToImport(group);

        //    if (members == null)
        //    {
        //        Trace.WriteLine("Added no members.");

        //        return;
        //    }

        //    SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin();

        //    foreach (var member in members)
        //    {
        //        //if (rawPersonName.Contains("@..."))
        //        //    rawPersonName = rawPersonName.Replace("@...", "");

        //        string personName = member.GetString(1);

        //        string email = member.GetString(2);

        //        DateTime joined = DateTime.MinValue;

        //        if (member.IsDBNull(3) != true)
        //        {
        //            joined = member.GetDateTime(3);
        //        }

        //        Person person = new Person(personName, joined, email, true, @group.Id);

        //        person.Insert();

        //        if (UnitsProceed % 10000 == 0 && UnitsProceed > 0)
        //        {
        //            transaction.Commit();

        //            transaction = DatabaseManager.Instance.TransactionBegin();
        //        }

        //        Trace.WriteLine("Adding member: " + person.Name);

        //        UnitsProceed++;

        //        ImportDataWorker.ReportProgress((int)(((double)UnitsProceed / (double)UnitsToImportCount) * 100.0f));

        //        if (ImportDataWorker.CancellationPending) break;
        //    }

        //    transaction.Commit();
        //}





        protected void FillMessage(DiscussionGroup @group, IDataReader reader, int messageNumberIndex, int senderEmailIndex,
            int senderNameIndex, int messageSubjectIndex, int messageDateIndex, int messageContentIndex,
            int messageFavoriteIndex, int messageUnreadIndex)
        {
            long number;
            DateTime? date = null;
            string subject;
            byte[] content;
            bool favorite;
            bool unread;
            long personId;
            long discussionGroupId;

            number = GetInt(reader, messageNumberIndex);

            string senderEmail = GetString(reader, senderEmailIndex);

            if (senderEmail.EndsWith(">"))
            {
                senderEmail = senderEmail.Remove(senderEmail.Length - 1);
            }

            string senderName = GetString(reader, senderNameIndex);

            personId = senderEmail != "" ? Person.GetByEmail(senderEmail).Id : Person.GetByName(senderName).Id;

            subject = GetString(reader, messageSubjectIndex);

            date = GetNullableDateTime(reader, messageDateIndex);

            string messageText = GetString(reader, messageContentIndex);

            content = Encoding.UTF8.GetBytes(messageText);

            favorite = CurrentImportSettings.ImportFavoriteStatus == true && GetBool(reader, messageFavoriteIndex);

            unread = GetBool(reader, messageUnreadIndex);

            discussionGroupId = @group.Id;

            Message messageToWrite = new Message(number, date, subject, favorite, unread, personId, discussionGroupId, 0, 0, 0, 0, 0, 0, 0);

            messageToWrite.Insert(content);

            GroupsUnits[@group].LastImportedMessage = messageToWrite.Number;

            UnitsProceed++;
        }
        
        protected bool IsTableExists(string tableName)
        {
            try
            {
                using (IDbConnection con = GetNewDbConnection())
                {
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        con.Open();

                        cmd.CommandText = string.Format("SELECT * FROM [{0}] WHERE 1 = 0;", tableName);

                        cmd.ExecuteNonQuery();

                        return true; // no error, table exists
                    }
                }
            }
            catch (Exception ex)
            {
                return false; // error occured, table doesn't exist
            }
        }

        protected static string GetString(IDataReader reader, int columnIndex)
        {
            return columnIndex == -1 ? "" : reader.GetString(columnIndex);
        }

        protected static DateTime GetDateTime(IDataReader reader, int columnIndex)
        {
            DateTime dateTime = DateTime.MinValue;

            if (columnIndex == -1) return dateTime;

            try
            {
                if (reader.IsDBNull(columnIndex) != true)
                {
                    string type = reader.GetDataTypeName(columnIndex);

                    switch (type)
                    {
                        case "DBTYPE_WVARCHAR":
                            {
                                string dateString = reader.GetString(columnIndex);

                                return DateTime.Parse(dateString);
                            }

                        case "DBTYPE_DATE":
                            return reader.GetDateTime(columnIndex);

                        default:
                            throw new Exception(string.Format("Unknown DateType {0}", type));
                    }
                }
            }
            catch (Exception)
            {
                dateTime = DateTime.MinValue;
            }

            return dateTime;
        }

        private static DateTime? GetNullableDateTime(IDataReader reader, int columnIndex)
        {
            if (columnIndex == -1) return null;

            if (reader.IsDBNull(columnIndex)) return null;

            try
            {
                string type = reader.GetDataTypeName(columnIndex);

                switch (type)
                {
                    case "DBTYPE_WVARCHAR":
                        {
                            string dateString = reader.GetString(columnIndex);

                            return DateTime.Parse(dateString);
                        }
                    case "DBTYPE_DATE":
                        return reader.GetDateTime(columnIndex);
                }
            }
            catch (Exception)
            {
                return null;
            }

            return null;
        }

        private static int GetInt(IDataReader reader, int columnIndex)
        {
            return columnIndex == -1 ? 0 : reader.GetInt32(columnIndex);
        }

        private static bool GetBool(IDataReader reader, int columnIndex)
        {
            return columnIndex != -1 && reader.GetBoolean(columnIndex);
        }



        #endregion Private Members


    }

    public class ImportUnit
    {
        public long GroupId { get; set; }
        public string GroupName { get; set; }
        public string CharSet { get; set; }
        public int FirstMessageNumber { get; set; }
        public int LastMessageNumber { get; set; }
        public int StartImportFromNumber { get; set; }
        public int EndImportAtNumber { get; set; }
        public bool SetToImport { get; set; }
        public long LastImportedMessage { get; set; }
        public DateTime? LastMembersUpdate { get; set; }
    }
    
    public struct ImportSettings
    {
        public bool CopyAttachments { get; set; }
        public bool RememberLastMessageNumber { get; set; }
        public bool ImportFavoriteStatus { get; set; }
        public ImportType ImportType { get; set; }
    }
    
    public enum ImportType
    {
        AllMessages = 0,
        OnlyFavorites,
        OnlyFavoritesNumbers
    }



}
