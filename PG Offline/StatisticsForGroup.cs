﻿using System.Collections.Generic;

using PGOffline.Model;

namespace PGOffline
{
    struct StatisticsForGroup
    {
        public string groupName;

        public int repliesCount;
        public double repliesPerMessage;

        public Dictionary<string, int> repliesPerSubject;

        public List<Person> lurkers;
        public double lurkersPercentage;

        public int newMembersCount;
        public double mewMembersPercentageChange;
    }
}