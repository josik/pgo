﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.Windows.Threading;
using PGOffline.Model;

namespace PGOffline
{
    public class PGOTraceListener : TraceListener
    {
        delegate void TextChanger(string message);

        private TextBox output;

        private System.IO.TextWriter _fileWriter;

        public PGOTraceListener(TextBox output)
        {
            this.Name = "PGOTraceListener";
            this.output = output;

            if (Options.Instance.GetAsBoolean("miscellaneous_options_log_events_to_file"))
            {
                SetOutputFile(Options.Instance.GetAsString("miscellaneous_options_log_events_file_path"));
            }
        }

        public void SetOutputFile(string filePath)
        {
            if (filePath == null && _fileWriter != null)
            {
                _fileWriter.Close();
                _fileWriter = null;

                return;
            }

            try
            {
                _fileWriter = new System.IO.StreamWriter(filePath, true);
            }
            catch (Exception exception)
            {
                _fileWriter = null;
            }
        }

        public override void Write(string message)
        {
            if (output.Dispatcher.CheckAccess())
            {
                output.AppendText(message);
                output.ScrollToEnd();

                if (_fileWriter != null)
                {
                    _fileWriter.WriteLine(message);
                    _fileWriter.Flush();
                }
            }
            else
            {
                output.Dispatcher.BeginInvoke(new TextChanger(this.Write), DispatcherPriority.Normal, message);
            }
        }

        public override void WriteLine(string message)
        {
            Write(message + Environment.NewLine);
        }
    }
}
