﻿using PGOffline.Forms;
using PGOffline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SQLite;
using System.IO;
using System.Windows.Forms;

namespace PGOffline
{
    class ExportDb3Manager : IExportManager
    {
        public event ExportCompletedEventHandler ExportCompleted;

        private BackgroundWorker _exportWorker;

        private FormIndeterminateProgress _formProgress;

        public ExportDb3Manager(string databasePath, IEnumerable<ExportUnit> units, ExportSettings settings)
        {
            _exportWorker = new BackgroundWorker
            {
                WorkerSupportsCancellation = true, 
                WorkerReportsProgress = false
            };

            _exportWorker.DoWork += new DoWorkEventHandler((sender, e) => BackgroundExport(e, databasePath, units, settings));

            _exportWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BackgroundExportCompleted);
        }

        public ExportDb3Manager(string databasePath, DiscussionGroup discussionGroup, IEnumerable<long> messagesNumbers, ExportSettings settings)
        {
            _exportWorker = new BackgroundWorker
            {
                WorkerSupportsCancellation = true, 
                WorkerReportsProgress = false
            };

            _exportWorker.DoWork += new DoWorkEventHandler((sender, e) => BackgroundExport(e, databasePath, discussionGroup, messagesNumbers, settings));

            _exportWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BackgroundExportCompleted);
        }

        public void ExportDataAsync()
        {
            _exportWorker.RunWorkerAsync();

            _formProgress = new FormIndeterminateProgress();

            _formProgress.SetTitle("Exporting...");

            _formProgress.Closed += new EventHandler((sender, e) => _exportWorker.CancelAsync());

            _formProgress.ShowDialog();
        }

        public void ExportDataCancelAsync()
        {
            throw new NotImplementedException();
        }

        void BackgroundExport(DoWorkEventArgs args, string databasePath, IEnumerable<ExportUnit> units, ExportSettings settings)
        {
            CreateEmptyDatabase(databasePath);

            ExportData(databasePath, units, settings);

            args.Result = true;
        }

        void BackgroundExport(DoWorkEventArgs args, string databasePath, DiscussionGroup discussionGroup, IEnumerable<long> messagesNumbers, ExportSettings settings)
        {
            CreateEmptyDatabase(databasePath);

            ExportData(databasePath, discussionGroup, messagesNumbers, settings);

            args.Result = true;
        }

        void BackgroundExportCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                _formProgress.Close();

                if ((bool)e.Result)
                {
                    System.Windows.MessageBox.Show("Export finished", "Export", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CreateEmptyDatabase(string databasePath)
        {
            SQLiteConnection.CreateFile(databasePath);

            SQLiteConnection sqliteExportConnection = new SQLiteConnection("Data source=" + databasePath);

            if (sqliteExportConnection.State != System.Data.ConnectionState.Open)
            {
                sqliteExportConnection.Open();
            }

            StreamReader reader = File.OpenText(string.Format("{0}\\Data\\ygroups_script_production.sql", AppDomain.CurrentDomain.BaseDirectory));

            SQLiteCommand cmd = sqliteExportConnection.CreateCommand();

            cmd.CommandText = reader.ReadToEnd();

            cmd.ExecuteNonQuery();

            sqliteExportConnection.Close();                
        }

        private void ExportData(string databasePath, IEnumerable<ExportUnit> units, ExportSettings settings)
        {
            DatabaseManager.Instance.ExecuteQuery(string.Format("ATTACH DATABASE '{0}' AS db_export", databasePath));

            using (SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin())
            {
                DatabaseManager.Instance.ExecuteQuery(
                    "INSERT INTO db_export.user_account SELECT * FROM main.user_account");

                //DatabaseManager.Instance.ExecuteQuery("INSERT INTO db_export.email_account SELECT * FROM main.email_account");
                //DatabaseManager.Instance.ExecuteQuery("INSERT INTO db_export.email_account (id, email) VALUES(1, ' ')");

                DatabaseManager.Instance.ExecuteQuery(
                    "UPDATE db_export.user_account SET name = ' ', password = ' '");

                DatabaseManager.Instance.ExecuteQuery(
                    "UPDATE db_export.email_account SET email = ' ', smtp_address = ' ', smtp_port = 0, smtp_login = ' ', smtp_password = ' '");

                transaction.Commit();
            }

            foreach (ExportUnit unit in units)
            {
                if (_exportWorker.CancellationPending)
                {
                    DatabaseManager.Instance.ExecuteQuery("DETACH db_export");

                    return;
                }

                using (SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin())
                {
                    DatabaseManager.Instance.ExecuteQuery(String.Format("INSERT INTO db_export.discussion_group SELECT id, name, charset, null AS actual_email, null AS user_account, null AS username, null AS password, null AS last_members_update, 0 AS unread_count FROM main.discussion_group WHERE main.discussion_group.id = {0}", unit.Group.Id));

                    DatabaseManager.Instance.ExecuteQuery(String.Format("INSERT INTO db_export.group_settings SELECT * FROM main.group_settings WHERE main.group_settings.discussion_group = {0}", unit.Group.Id));

                    DatabaseManager.Instance.ExecuteQuery(String.Format("INSERT INTO db_export.person SELECT DISTINCT t1.* FROM main.person t1 INNER JOIN main.group_message t2 ON t1.id = t2.person WHERE t1.discussion_group = {0} AND t1.discussion_group = t2.discussion_group AND t2.number BETWEEN {1} AND {2}", unit.Group.Id, unit.StartExportFromNumber, unit.EndExportAtNumber));
                
                    transaction.Commit();
                }
                
                using (SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin())
                {
                    switch (settings.ExportType)
                    {
                        case ExportType.AllMessages:
                            DatabaseManager.Instance.ExecuteQuery(String.Format("INSERT INTO db_export.group_message SELECT t1.* FROM main.group_message t1 LEFT JOIN main.person t2 ON t1.person = t2.id WHERE t1.discussion_group = {0} AND t1.discussion_group = t2.discussion_group {1} AND t1.number BETWEEN {2} AND {3}", unit.Group.Id, "", unit.StartExportFromNumber, unit.EndExportAtNumber));
                            break;
                        case ExportType.OnlyFavorites:
                            DatabaseManager.Instance.ExecuteQuery(String.Format("INSERT INTO db_export.group_message SELECT t1.* FROM main.group_message t1 LEFT JOIN main.person t2 ON t1.person = t2.id WHERE t1.discussion_group = {0} AND t1.discussion_group = t2.discussion_group {1} AND t1.number BETWEEN {2} AND {3}", unit.Group.Id, "AND t1.favorite = 1", unit.StartExportFromNumber, unit.EndExportAtNumber));
                            break;
                        //case ExportType.OnlyFavoritesNumbers:
                        //    DatabaseManager.Instance.ExecuteQuery(String.Format("INSERT INTO db_export.group_message (id, number, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time) SELECT id, number, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time FROM main.group_message WHERE main.group_message.discussion_group = {0} AND main.group_message.favorite = 1 AND main.group_message.number BETWEEN {1} AND {2}", unit.Group.Id, unit.StartExportFromNumber, unit.EndExportAtNumber));
                        //    break;
                    }


                    // scrub personal account info

                    DatabaseManager.Instance.ExecuteQuery("UPDATE db_export.group_settings SET value = ' '");

                    DatabaseManager.Instance.ExecuteQuery("UPDATE db_export.group_message SET unread = 1 WHERE db_export.group_message.favorite = 0");

                    if (settings.ExportFavoriteStatus == false)
                    {
                        DatabaseManager.Instance.ExecuteQuery("UPDATE db_export.group_message SET unread = 1 WHERE db_export.group_message.favorite = 1");
                    }

                    transaction.Commit();
                }
            }
            
            DatabaseManager.Instance.ExecuteQuery("DETACH db_export");
        }

        private void ExportData(string databasePath, DiscussionGroup discussionGroup, IEnumerable<long> messagesNumbers, ExportSettings settings)
        {
            DatabaseManager.Instance.ExecuteQuery(string.Format("ATTACH DATABASE '{0}' AS db_export", databasePath));

            using (SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin())
            {
                DatabaseManager.Instance.ExecuteQuery("INSERT INTO db_export.user_account SELECT * FROM main.user_account");

                //DatabaseManager.Instance.ExecuteQuery("INSERT INTO db_export.email_account SELECT * FROM main.email_account");
                
                transaction.Commit();
            }

            using (SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin())
            {

                var r1 = DatabaseManager.Instance.ExecuteQuery(
                    String.Format(
                        "INSERT INTO db_export.discussion_group SELECT id, name, charset, null AS actual_email, null AS user_account, null AS username, null AS password, null AS last_members_update, 0 AS unread_count FROM main.discussion_group WHERE main.discussion_group.id = {0}",
                        discussionGroup.Id));

                var r2 = DatabaseManager.Instance.ExecuteQuery(
                    String.Format(
                        "INSERT INTO db_export.group_settings SELECT * FROM main.group_settings WHERE main.group_settings.discussion_group = {0}",
                        discussionGroup.Id));

                //DatabaseManager.Instance.ExecuteQuery(
                //    String.Format(
                //        "INSERT OR IGNORE INTO db_export.person SELECT * FROM main.person WHERE main.person.discussion_group = {0}",
                //        discussionGroup.Id));

                foreach (long messageNumber in messagesNumbers)
                {
                    DatabaseManager.Instance.ExecuteQuery(
                        String.Format(
                            "INSERT OR IGNORE INTO db_export.person SELECT DISTINCT t1.* FROM main.person t1 INNER JOIN main.group_message t2 ON t1.id = t2.person WHERE t1.discussion_group = {0} AND t1.discussion_group = t2.discussion_group AND t2.number = {1}",
                            discussionGroup.Id, messageNumber));
                    
                    switch (settings.ExportType)
                    {
                        case ExportType.AllMessages:
                            DatabaseManager.Instance.ExecuteQuery(
                                String.Format(
                                    "INSERT INTO db_export.group_message SELECT t1.* FROM main.group_message t1 LEFT JOIN main.person t2 ON t1.person = t2.id WHERE t1.discussion_group = {0} AND t1.discussion_group = t2.discussion_group {1} AND t1.number = {2}", discussionGroup.Id, "", messageNumber));
                            break;
                        case ExportType.OnlyFavorites:
                            DatabaseManager.Instance.ExecuteQuery(
                                String.Format(
                                    "INSERT INTO db_export.group_message SELECT t1.* FROM main.group_message t1 LEFT JOIN main.person t2 ON t1.person = t2.id WHERE t1.discussion_group = {0} AND t1.discussion_group = t2.discussion_group {1} AND t1.number = {2}", discussionGroup.Id, "AND t1.favorite = 1", messageNumber));
                            break;
                        //case ExportType.OnlyFavoritesNumbers:
                        //    DatabaseManager.Instance.ExecuteQuery(
                        //        String.Format(
                        //            "INSERT INTO db_export.group_message (id, number, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time) SELECT id, number, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time FROM main.group_message WHERE main.group_message.discussion_group = {0} AND main.group_message.favorite = 1 AND main.group_message.number = {1}",
                        //            discussionGroup.Id, messageNumber));
                        //    break;
                    }

                    if (!_exportWorker.CancellationPending) continue;

                    transaction.Commit();

                    DatabaseManager.Instance.ExecuteQuery("DETACH db_export");

                    return;
                }

                transaction.Commit();
            }

            using (SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin())
            {
                DatabaseManager.Instance.ExecuteQuery(
                    "UPDATE db_export.group_message SET unread = 1 WHERE db_export.group_message.favorite = 0");

                if (settings.ExportFavoriteStatus == false)
                {
                    DatabaseManager.Instance.ExecuteQuery(
                        "UPDATE db_export.group_message SET unread = 1 WHERE db_export.group_message.favorite = 1");
                }

                DatabaseManager.Instance.ExecuteQuery(
                    "UPDATE db_export.user_account SET name = ' ', password = ' '");

                DatabaseManager.Instance.ExecuteQuery(
                    "UPDATE db_export.email_account SET email = ' ', smtp_address = ' ', smtp_port = 0, smtp_login = ' ', smtp_password = ' '");

                DatabaseManager.Instance.ExecuteQuery(
                    "UPDATE db_export.group_settings SET value = ' '");

                transaction.Commit();
            }

            DatabaseManager.Instance.ExecuteQuery("DETACH db_export");
        }

    }
}
