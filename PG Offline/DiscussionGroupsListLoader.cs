﻿using Asfdfdfd.Api.Yahoo.Groups;
using PGOffline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;

namespace PGOffline
{
    class DiscussionGroupsListLoader
    {
        public delegate void DiscussionGroupsListLoadedEventHandler(List<YahooGroup> groups);

        public event DiscussionGroupsListLoadedEventHandler DiscussionGroupsListLoaded;

        public delegate void DiscussionGroupsListLoadErrorEventHandler(string message);

        public event DiscussionGroupsListLoadErrorEventHandler DiscussionGroupsListLoadError;

        BackgroundWorker _worker = new BackgroundWorker();

        YahooGroupsServer _yahooServer;

        bool _finished = false;

        List<YahooGroup> _groups = null;

        public DiscussionGroupsListLoader(YahooGroupsServer yahooServer)
        {
            _yahooServer = yahooServer;

            _worker.WorkerReportsProgress = false;

            _worker.WorkerSupportsCancellation = false;

            _worker.DoWork += new DoWorkEventHandler(DoWork);

            _worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(RunWorkerCompleted);
        }

        public void LoadAsync()
        {
            _worker.RunWorkerAsync();
        }

        private void DoWork(object sender, DoWorkEventArgs e)
        {
            UserAccount userAccount = UserAccount.GetFirst();

            Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("User - {0}", userAccount.Name));

            _yahooServer.ReceiveGroupsCompleted += OnReceiveYahooGroupsListCompleted;

            if (!_yahooServer.Login(userAccount.Name, userAccount.Password)) return;

            _yahooServer.ReceiveGroupsAsync();

            while (!_finished)
            {
                Thread.Sleep(1000);
            }

            e.Result = _groups;
        }

        void OnReceiveYahooGroupsListCompleted(List<YahooGroup> yahooGroups)
        {
            _yahooServer.ReceiveGroupsCompleted -= OnReceiveYahooGroupsListCompleted;

            _finished = true;

            _groups = yahooGroups;
        }

        void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                DiscussionGroupsListLoaded((List<YahooGroup>)e.Result);
            }
            catch (Exception err)
            {
                DiscussionGroupsListLoadError(err.InnerException != null ? err.InnerException.Message : err.Message);
            }
        }
    }

}
