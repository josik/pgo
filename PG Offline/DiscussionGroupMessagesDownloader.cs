﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Asfdfdfd.Api.Yahoo.Groups;
using PGOffline.Model;
using PGOffline.Properties;

namespace PGOffline
{
    public class DiscussionGroupMessagesDownloader
    {
        private int _maxMessageHeadersPerBundle = 25;

        private DiscussionGroup _group;

        private WorkflowOptions _workflowOptions;

        private AppDownloadOptions _appDownloadOptions;

        private Stopwatch _speedLimitStopwatch = new Stopwatch();

        private SpeedLimitOptions _speedLimitOptions;

        private YahooGroup _yahooGroup;

        private YahooGroupsServer _yahooServer;

        private CancellationToken _token;

        private TaskFactory _uiFactory;

        private DiscussionGroupsMessagesDownloader.MessageDownloadedEventHandler _MessageDownloaded;

        private long _startMessageNumber;
        
        private long _stopMessageNumber;
        
        private long _prevMessageNumber;
        
        private long _downloadMessagesLimit;

        private bool _willDownloadAllMessages = false;

        private bool _isLimitBreak = false;

        private int _messageDownloadedInLastAction = 0;

        public DiscussionGroupMessagesDownloader(YahooGroupsServer yahooServer, DiscussionGroup group, long downloadMessagesLimit, CancellationToken token, TaskFactory uiFactory, DiscussionGroupsMessagesDownloader.MessageDownloadedEventHandler MessageDownloaded)
        {
            _token = token;

            _uiFactory = uiFactory;

            _yahooServer = yahooServer;

            _group = group;

            _downloadMessagesLimit = downloadMessagesLimit;

            _appDownloadOptions = Utils.GetAppDownloadOptions();

            _workflowOptions = GetWorkflowOptions();

            var authGood = TryAuthorization(uiFactory, token);

            if (!authGood)
            {
                if (_uiFactory.CancellationToken.CanBeCanceled && _uiFactory.CancellationToken.IsCancellationRequested)
                {
                    _token.ThrowIfCancellationRequested();

                    return;
                }
#if DEBUG
                Trace.Write(string.Format("/DEBUG/[{0}]", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));
#endif
                Trace.WriteLine("Login failed.");
                
                return;
            }

            //if (!TryAuthorization())
            //{
            //    _willDownloadAllMessages = true;
            //}

            _yahooGroup = _yahooServer.CreateGroup(group.Name);

            if (_yahooGroup != null)
            {
                SetDownloadAttachmentsOptions();
            }

            GetMessageParsingSettings();

            //TryAuthorization();
            //UserAccount userAccount = UserAccount.GetFirst();
            //_yahooServer.Login(userAccount.Name, userAccount.Password);
            
            _willDownloadAllMessages = DownloadNewDiscussionGroupMessages();
        }

        public bool GetWillDownloadAllMessages()
        {
            return _willDownloadAllMessages;
        }

        private bool DownloadNewDiscussionGroupMessages()
        {
            bool willDownloadAllMessages = _willDownloadAllMessages;

            SetWillDownloadAllMessages();

            try
            {
                int groupMessagesCount = GetGroupMessagesCount();

                long newMessagesCount = GetNewMessagesCount(groupMessagesCount);

                if (newMessagesCount == 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);

                return true;
            }

            //HashSet<long> existingMessagesNumbers = DatabaseManager.Instance.GetMessagesNumbers(_group);

            // get AppDownloadOptions in case they have been updated by the user since the last time we attempted to download
            _appDownloadOptions = Utils.GetAppDownloadOptions();

            _yahooServer.SetAppDownloadOptions(_appDownloadOptions);

            _yahooGroup.SetAppDownloadOptions(_appDownloadOptions);

            _speedLimitOptions = GetSpeedLimitOptions();

            IsCancellationRequested();

            if (_speedLimitOptions.IsEnabled)
            {
                StartSpeedLimitStopwatch();
            }

            int allowedInvalidPagesCount = _workflowOptions.InvalidPagesMaxCount;

            uint firstMessageNumberInBundle = 0;

            while (_prevMessageNumber <= _stopMessageNumber)
            {
                // switch servers
                SourceCountryUrlPrefixOption.Instance.DisableCurrentCodeInUse();
                SourceCountryUrlPrefixOption.Instance.SetNextEnabledCode();

                // download the message bundle from Yahoo
                SortedList<uint, YahooGroupMessage> messages = DownloadMessagesBundle();

                if (messages == null)
                {
                    // try another server before giving up
                    SourceCountryUrlPrefixOption.Instance.DisableCurrentCodeInUse();
                    SourceCountryUrlPrefixOption.Instance.SetNextEnabledCode();
                    //_yahooServer.Login();
                    messages = DownloadMessagesBundle();
                    if (messages == null)
                    {
                        throw new YahooGroupsException("Failed to retreive messages. [err: 74363]");
                    }
                }
                
                IsCancellationRequested();

                if (messages.Count == 0 || (firstMessageNumberInBundle > 0 && firstMessageNumberInBundle == messages.Values[0].Number))
                {
#if DEBUG
                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("/DEBUG/ _prevMessageNumber={0} allowedInvalidPagesCount={1}", _prevMessageNumber, allowedInvalidPagesCount));
#endif
                    allowedInvalidPagesCount--;

                    if (allowedInvalidPagesCount > 0)
                    {
                        IsCancellationRequested();

#if DEBUG
                        Trace.Write(string.Format("/DEBUG/[{0}]", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));
#endif
                        Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Message {1} does not exist.", _group, _prevMessageNumber));

                        _prevMessageNumber++;

                        continue;
                    }

                    IsCancellationRequested();
#if DEBUG
                    Trace.Write(string.Format("/DEBUG/[{0}]", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));
#endif
                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Downloaded invalid page.", _group));

                    break;
                }

                firstMessageNumberInBundle = messages.Values[0].Number;

                _prevMessageNumber = firstMessageNumberInBundle;

                MessagesFiltration(messages);

                SetGroupSettings();

                if (_isLimitBreak)
                {
                    return willDownloadAllMessages;
                }

                IsCancellationRequested();

                if (_speedLimitOptions.IsEnabled)
                {
                    SpeedLimitStopwatchWork();
                }

            }

            return willDownloadAllMessages;
        }

        private long GetNewMessagesCount(long allMessagesCount)
        {
            long newMessagesCount = (allMessagesCount - _startMessageNumber) + 1;

            newMessagesCount = newMessagesCount >= 0 ? newMessagesCount : 0;

            return newMessagesCount;
        }

        private void IsCancellationRequested()
        {
            if (_token.IsCancellationRequested)
            {
                _token.ThrowIfCancellationRequested();
            }
        }

        private void GetMessageParsingSettings()
        {
            _startMessageNumber = _group.GroupSettings.GetAsLong("start_download_from_message");

            _stopMessageNumber = _group.GroupSettings.GetAsLong("end_download_at_message");

            _prevMessageNumber = _startMessageNumber;
        }
        
        private WorkflowOptions GetWorkflowOptions()
        {
            var workflowOptions = new WorkflowOptions(
               Options.Instance.GetAsInt("workflow_options_stop_after_receiving_invalid_pages"),
               Options.Instance.GetAsInt("workflow_options_on_failure_rerequest_message_times"),
               Options.Instance.GetAsBoolean("workflow_options_stop_after_receiving_invalid_pages_enabled"),
               Options.Instance.GetAsBoolean("workflow_options_on_lockout_resume_after_minutes_enabled"),
               Options.Instance.GetAsInt("workflow_options_on_lockout_resume_after_minutes")
               );

            return workflowOptions;
        }

        private bool TryAuthorization(TaskFactory uiFactory, CancellationToken token)
        {
            if (Options.Instance.GetAsBoolean("workflow_options_skip_authorization")) return true;

            bool isLoggedIn;
            
            try
            {
                UserAccount currentUser = _group.GetUserAccount();

                isLoggedIn = _yahooServer.Login(currentUser.Name, currentUser.Password);
            }
            catch (Exception ex)
            {
#if DEBUG
                Trace.Write(string.Format("/DEBUG/[{0}]", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));
#endif
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Error. {1}", _group.Name, ex.Message));

                return false;
            }

            return isLoggedIn;
        }

        private SpeedLimitOptions GetSpeedLimitOptions()
        {
            SpeedLimitOptions speedLimitOptions;

            if (Options.Instance.GetAsBoolean("download_options_enable_download_limit_speed"))
            {
                speedLimitOptions = new SpeedLimitOptions(
                    Options.Instance.GetAsInt("download_options_download_limit_speed_type"),
                    Options.Instance.GetAsInt("download_options_messages_per_minute"),
                    Options.Instance.GetAsInt("download_options_initial_delay"),
                    Options.Instance.GetAsInt("download_options_download"),
                    Options.Instance.GetAsInt("download_options_then_wait"));
            }
            else
            {
                speedLimitOptions = new SpeedLimitOptions();
            }

            return speedLimitOptions;
        }

        private void SetWillDownloadAllMessages()
        {
            //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("_startMessageNumber = {0}", _startMessageNumber));
            if (_startMessageNumber + _downloadMessagesLimit - 1 < _stopMessageNumber)
            {
                _stopMessageNumber = _startMessageNumber + _downloadMessagesLimit - 1;

                _willDownloadAllMessages = false;
            }
            else if (_startMessageNumber + _downloadMessagesLimit - 1 > _stopMessageNumber)
            {
                _downloadMessagesLimit = _stopMessageNumber - _startMessageNumber + 1;
            }
        }

        private int GetGroupMessagesCount()
        {
            int groupMessagesCount = 0;

            try
            {
                groupMessagesCount = _yahooGroup.GetMessagesCount();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                //throw ex;
            }

            if (_stopMessageNumber > groupMessagesCount)
            {
                _stopMessageNumber = groupMessagesCount;
            }

            IsCancellationRequested();

            //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Total messages: {1}. New messages: {2}.", _group, groupMessagesCount, GetNewMessagesCount(groupMessagesCount)));

            return groupMessagesCount;
        }

        private SortedList<uint, YahooGroupMessage> DownloadMessagesBundle()
        {
            var messages = new SortedList<uint, YahooGroupMessage>();

            try
            {
                IsCancellationRequested();

                var maxMessageCountThisRequest = (_stopMessageNumber >= (_prevMessageNumber + _maxMessageHeadersPerBundle)) ? _maxMessageHeadersPerBundle : ((_stopMessageNumber - _prevMessageNumber) + 1);

                messages = _yahooGroup.DownloadMessagesBundle(_prevMessageNumber, maxMessageCountThisRequest);

                IsCancellationRequested();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            return messages;
        }

        private void SetGroupSettings()
        {
            _group.GroupSettings.SetValue("start_download_from_message", _prevMessageNumber.ToString());

            Int32 endMessageNumber = _group.GroupSettings.GetAsInt("end_download_at_message");

            if (_prevMessageNumber > endMessageNumber)
            {
                _group.GroupSettings.SetValue("end_download_at_message", (Int32.MaxValue).ToString());
            }

            _group.GroupSettings.SaveSettings();
        }

        private void MessagesFiltration(SortedList<uint, YahooGroupMessage> messages)
        {
            HashSet<long> existingMessages = DatabaseManager.Instance.GetMessagesNumbers(_group);
#if DEBUG
            Trace.Write(string.Format("/DEBUG/[{0}]", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));
#endif
            Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Downloaded and adding messages '{1}' through '{2}'.", 
                _group.Name, 
                messages.First().Key, 
                messages.Last().Key));

            foreach (YahooGroupMessage message in messages.Values)
            {
                if (_token.IsCancellationRequested) break;

                if (message.Number > _stopMessageNumber)
                {
                    _isLimitBreak = true;

                    break;
                }

                if (message.Number < _startMessageNumber)
                {
                    if (_token.IsCancellationRequested) break;

                    //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Wrong message number '{1}'. Skipping.", _group, message.Number));

                    continue;
                }
                
                // handle gaps of missing messages
                while (_prevMessageNumber + 1 < message.Number)
                {
                    if (_token.IsCancellationRequested) break;
#if DEBUG
                    Trace.Write(string.Format("/DEBUG/[{0}]", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));
#endif
                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Message {1} does not exist.", _group, _prevMessageNumber));

                    //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Could not find message with number '{1}'.", _group, _prevMessageNumber));

                    _prevMessageNumber++;
                }

                // handle messages which are empty
                if (string.IsNullOrEmpty(message.Content) && string.IsNullOrEmpty(message.Email) && string.IsNullOrEmpty(message.Author))
                {
#if DEBUG
                    Trace.Write(string.Format("/DEBUG/[{0}]", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));
#endif
                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Message {1} does not exist or is empty.", _group, _prevMessageNumber));

                    _prevMessageNumber++;

                    continue;
                }

                if (!existingMessages.Contains(message.Number))
                {
                    if (_token.IsCancellationRequested) break;

                    //if (_group.GroupSettings.AttachmentLimitSize)

                    if (!_group.GroupSettings.GetAsBoolean("disable_attachements_downloading"))
                    {
                        _yahooGroup.DownloadAttachments(message);
                    }
                    
                    Message createdMessage = _group.AddMessage(message);

                    _uiFactory.StartNew(() =>
                    {
                        _group.MessageAddedNotify(createdMessage);

                        if (_MessageDownloaded != null)
                        {
                            _MessageDownloaded(_group, createdMessage);
                        }
                    }, _token).Wait(_token);

                    _messageDownloadedInLastAction++;

                    existingMessages.Add(message.Number);

                    //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Downloaded message with number '{1}'.", _group, message.Number));
                }
                else
                {
#if DEBUG
                    Trace.Write(string.Format("/DEBUG/[{0}]", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));
#endif
                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Skipping existing message with number '{1}'.", _group, message.Number));
                }

                _prevMessageNumber = message.Number + 1;

                if (_token.IsCancellationRequested) break;
            }
        }

        private void SpeedLimitStopwatchWork()
        {
            switch (_speedLimitOptions.SpeedLimitType)
            {
                case DownloadSpeedLimitType.Speed:
                    RestartSpeedLimitStopwatch();
                    break;

                case DownloadSpeedLimitType.Repeat:
                    if (_messageDownloadedInLastAction >= _speedLimitOptions.Download)
                    {
                        _messageDownloadedInLastAction = 0;
                        
                        IsCancellationRequested();
                        
                        Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Waiting '{0}' min.", _speedLimitOptions.ThenWait));
                        
                        Thread.Sleep(_speedLimitOptions.ThenWait * 60 * 1000);
                        
                        IsCancellationRequested();
                    }

                    break;
            }
        }

        private void StartSpeedLimitStopwatch()
        {
            switch (_speedLimitOptions.SpeedLimitType)
            {
                case (DownloadSpeedLimitType.Speed):
                    _speedLimitStopwatch.Start();
                    break;
                
                case (DownloadSpeedLimitType.Repeat):
                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Initial delay '{0}' min.", _speedLimitOptions.InitialDelay));
                    IsCancellationRequested();
                    Thread.Sleep(_speedLimitOptions.InitialDelay * 60 * 1000);
                    IsCancellationRequested();
                    break;
            }
        }

        private void RestartSpeedLimitStopwatch()
        {
            if (_speedLimitStopwatch.ElapsedMilliseconds < 1000 && (_speedLimitOptions.MessagesPerMinute <= _messageDownloadedInLastAction))
            {
                int overdownloadedMessagesCount = _messageDownloadedInLastAction - _speedLimitOptions.MessagesPerMinute;

                int pauseSec = (overdownloadedMessagesCount / _speedLimitOptions.MessagesPerMinute) * 60;

                IsCancellationRequested();

                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Waiting '{0}' sec.", pauseSec));

                Thread.Sleep(pauseSec * 1000);

                IsCancellationRequested();

                _messageDownloadedInLastAction = 0;

                IsCancellationRequested();

                _speedLimitStopwatch.Restart();
            }
            else if (_speedLimitStopwatch.ElapsedMilliseconds >= 1000 && (_speedLimitOptions.MessagesPerMinute > _messageDownloadedInLastAction))
            {
                _messageDownloadedInLastAction = 0;

                IsCancellationRequested();

                _speedLimitStopwatch.Restart();
            }
        }

        private void SetDownloadAttachmentsOptions()
        {
            _yahooGroup.DownloadOptions.FolderForAttachments = _group.GroupSettings.GetValue("attachments_folder");

            _yahooGroup.DownloadOptions.IncludeAttachmentsPattern = _group.GroupSettings.GetValue("include_attachments_pattern");

            _yahooGroup.DownloadOptions.ExcludeAttachmentsPattern = _group.GroupSettings.GetValue("exclude_attachments_pattern");

            _yahooGroup.DownloadOptions.MinimumAttachmentSize = _group.GroupSettings.MinimumAttachmentSize;

            _yahooGroup.DownloadOptions.MaximumAttachmentSize = _group.GroupSettings.MaximumAttachmentSize;
        }

    }
}
