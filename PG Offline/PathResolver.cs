﻿using System;
using System.IO;

namespace PGOffline
{
    public class PathResolver
    {
        public static string GetAppDir()
        {
#if DEBUG
            //File.AppendAllText(@"c:\temp\PgOffline_debug.log", " GetAppdir=" + Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\..\..\..\");
            //return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\..\..\..\";
#endif
            //return Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), @"..\..\..\");
            return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\";
        }

        public static string GetDefaultDatabasePath()
        {
            String databasePath = Path.Combine(GetAppDir(), @"Data\ygroups.db3");

            if (!File.Exists(databasePath))
            {
                databasePath = Path.Combine(GetApplicationDataPath(), "ygroups.db3");
            }

            return databasePath;
        }

        public static string GetOptionsPath()
        {
            String databasePath = Path.Combine(GetAppDir(), @"Data\options.db3");

            if (!File.Exists(databasePath))
            {
                databasePath = Path.Combine(GetApplicationDataPath(), "options.db3");
            }

            return databasePath;
        }

        public static string GetSchemaScriptPath()
        {
            return Path.Combine(GetAppDir(), @"Data\ygroups_script_production.sql");
        }

        public static string GetOptionsSchemaScriptPath()
        {
            return Path.Combine(GetAppDir(), @"Data\options.sql");
        }

        public static string GetApplicationDataPath()
        {
            string appDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "PG Offline 4");

            Directory.CreateDirectory(appDataPath);

            return appDataPath;
        }

        public static string GetTrialDataPath()
        {
            string trialDataPath = Path.Combine(PathResolver.GetApplicationDataPath(), "Trials");

            Directory.CreateDirectory(trialDataPath);

            return trialDataPath;
        }
    }
}
