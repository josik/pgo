﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using PGOffline.Model;
using System.ComponentModel;

namespace PGOffline
{
    public class ImportDb3Manager
    {
        private string sqlite_db_v18_group_message = "id, number, date, subject, content, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic,next_in_topic, prev_in_time, next_in_time";

        public event ImportCompletedEventHandler ImportComplete;

        string _dbFilePath;
        
        static SQLiteConnection _connection;
        
        List<ImportUnit> _unitsToImport;
        
        ImportSettings _importSettings;
        
        Dictionary<DiscussionGroup, ImportUnit> _groupsUnits;
        
        BackgroundWorker _importDataWorker;
        
        Forms.FormDeterminateProgress _formIndeterminateProgress;

        public ImportDb3Manager(string databasePath, List<ImportUnit> unitsToImport, ImportSettings importSettings)
        {
            _dbFilePath = databasePath;

            _connection = new SQLiteConnection("Data source=" + _dbFilePath);
            
            _unitsToImport = unitsToImport;
            
            _importSettings = importSettings;
            
            _groupsUnits = new Dictionary<DiscussionGroup, ImportUnit>();
        }

        public void ImportDataAsync()
        {
            if (_importDataWorker != null)
            {
                ImportDataCancelAsync();
            }

            _importDataWorker = new BackgroundWorker
            {
                WorkerSupportsCancellation = true, 
                WorkerReportsProgress = true
            };

            _importDataWorker.DoWork += new DoWorkEventHandler((sender, e) => ImportData());

            _importDataWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((sender, e) => ImportDataComplete());

            _importDataWorker.ProgressChanged += new ProgressChangedEventHandler((sender, e) => ImportDataProgress(e.ProgressPercentage));

            _formIndeterminateProgress = new Forms.FormDeterminateProgress();

            _formIndeterminateProgress.Show();

            _importDataWorker.RunWorkerAsync();  
        }

        private void ImportData()
        {
            if (_connection.State != System.Data.ConnectionState.Open)
            {
                _connection.Open();
            }

            int unitsCompleted = 0;

            foreach (ImportUnit importUnit in _unitsToImport)
            {
                SQLiteCommand cmd1 = _connection.CreateCommand();

                cmd1.CommandText = "SELECT id, name, charset, last_members_update FROM discussion_group WHERE id = :groupId";

                cmd1.Parameters.AddWithValue(":groupId", importUnit.GroupId);
                
                object[] groupRawData = new object[8];

                SQLiteDataReader reader1 = cmd1.ExecuteReader();

                reader1.Read();

                reader1.GetValues(groupRawData);

                // user_account
                groupRawData[4] = string.Empty;

                // username
                groupRawData[5] = string.Empty;

                // password
                groupRawData[6] = string.Empty;

                DiscussionGroup group = DiscussionGroup.GetByName((string)groupRawData[1]);
                
                if (group == null)
                {
                    group = new DiscussionGroup(groupRawData);

                    group.Insert();
                }

                SQLiteCommand cmd2 = _connection.CreateCommand();
                
                switch (_importSettings.ImportType)
                {
                    case ImportType.AllMessages:
                        cmd2.CommandText = string.Format("SELECT {0} FROM group_message WHERE discussion_group = :groupId AND number BETWEEN :startNumber AND :stopNumber", sqlite_db_v18_group_message);
                        break;
                    case ImportType.OnlyFavorites:
                        cmd2.CommandText = string.Format("SELECT {0} FROM group_message WHERE discussion_group = :groupId AND number BETWEEN :startNumber AND :stopNumber AND favorite = 1", sqlite_db_v18_group_message);
                        break;
                    case ImportType.OnlyFavoritesNumbers:
                        cmd2.CommandText = "SELECT number FROM group_message WHERE discussion_group = :groupId AND number BETWEEN :startNumber AND :stopNumber AND favorite = 1";
                        break;
                }

                cmd2.Parameters.AddWithValue(":groupId", importUnit.GroupId);

                cmd2.Parameters.AddWithValue(":startNumber", importUnit.StartImportFromNumber);

                cmd2.Parameters.AddWithValue(":stopNumber", importUnit.EndImportAtNumber);

                SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin();

                SQLiteDataReader reader2 = cmd2.ExecuteReader();

                while (reader2.Read())
                {
                    if (_importSettings.ImportType != ImportType.OnlyFavoritesNumbers)
                    {
                        object[] messageRawData = new object[17];

                        reader2.GetValues(messageRawData);
                        
                        long oldPersonId = (long)messageRawData[7];

                        object[] personRawData;

                        using (SQLiteCommand cmd3 = _connection.CreateCommand())
                        {
                            cmd3.CommandText = "SELECT name, joined, email, is_member FROM person WHERE id = :personId";

                            cmd3.Parameters.AddWithValue(":personId", oldPersonId);

                            personRawData = new object[6];

                            using (SQLiteDataReader reader3 = cmd3.ExecuteReader())
                            {
                                if (!reader3.Read())
                                {
                                    continue;
                                }

                                reader3.GetValues(personRawData);
                            }
                        }

                        Person person = null;
                        
                        try
                        {
                            person = new Person((string)personRawData[1], (DateTime?)personRawData[2], (string)personRawData[3], (bool)personRawData[4], group.Id);
                        }
                        catch(InvalidCastException)
                        {
                            person = new Person((string)personRawData[1], null, (string)personRawData[3], (bool)personRawData[4], group.Id);
                        }

                        person.Insert();

                        try
                        {
                            var m_author = person.Name;
                            var m_email = person.Email;
                            var m_number = (long) messageRawData[1];
                            var m_date = (DateTime?) messageRawData[2];
                            var m_subject = (string) messageRawData[3];
                            var m_content = (byte[]) messageRawData[4];
                            var m_favorite = _importSettings.ImportFavoriteStatus != false && (bool) messageRawData[5];
                            var m_unread = (bool) messageRawData[6];
                            var m_person = (long) messageRawData[7];
                            var m_charset = (messageRawData[8] == DBNull.Value
                                ? string.Empty
                                : (string) messageRawData[8]);
                            var m_discussion_group = (long) messageRawData[9];
                            var m_topic_id = (long) messageRawData[10];
                            var m_parent_number = (long) messageRawData[11];
                            var m_thread_level = (long) messageRawData[12];
                            var m_prev_in_topic = (long) messageRawData[13];
                            var m_next_in_topic = (long) messageRawData[14];
                            var m_prev_in_time = (long) messageRawData[15];
                            var m_next_in_time = (long) messageRawData[16];

                            // AddMessage(string author, string email, long number, DateTime? date, string subject, byte[] content, bool favorite, bool unread, long topicId, long parentId, long threadLevel, long prevInTopic, long nextInTopic, long prevInTime, long nextInTime, bool callMessageEdit = true, List<YahooGroupAttachment> attachments = null, string charset = null
                            group.AddMessage(m_author, m_email, m_number, m_date, m_subject, m_content, m_favorite,
                                m_unread, m_topic_id, m_parent_number, m_thread_level, m_prev_in_topic, m_next_in_topic,
                                m_prev_in_time, m_next_in_time, callMessageEdit: false, attachments: null,
                                charset: m_charset);

                        }
                        catch (InvalidCastException ex)
                        {
                            throw;
                            //group.AddMessage(person.Name, person.Email, (long)messageRawData[1], null, (string)messageRawData[3], (byte[])messageRawData[4], (bool)messageRawData[5], (bool)messageRawData[6], (long)messageRawData[7], (long)messageRawData[8], (long)messageRawData[9], (long)messageRawData[10], (long)messageRawData[11], (long)messageRawData[12], (long)messageRawData[13], false);
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                    }
                    else if (_importSettings.ImportFavoriteStatus == true)
                    {
                        try
                        {
                            object[] messageRawData = new object[1];

                            reader2.GetValues(messageRawData);

                            using (SQLiteCommand cmd6 = DatabaseManager.Instance.CreateCommand("UPDATE group_message SET favorite = 1 WHERE number = :number AND discussion_group = :group"))
                            {
                                cmd6.Parameters.AddWithValue(":number", messageRawData[0]);

                                cmd6.Parameters.AddWithValue(":group", @group.Id);

                                cmd6.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                        
                    }
                }

                transaction.Commit();

                _importDataWorker.ReportProgress((int)(((double)++unitsCompleted / (double)_unitsToImport.Count) * 100.0f));
            }
        }

        public void ImportDataProgress(int progressPercentage)
        {
            _formIndeterminateProgress.SetProgress(progressPercentage);
        }

        public void ImportDataComplete()
        {
            // Update group settings.
            if (_importSettings.RememberLastMessageNumber)
            {
                foreach (ImportUnit importUnit in _unitsToImport)
                {
                    if (importUnit.LastImportedMessage <= 0) continue;

                    DiscussionGroup discussionGroup = DiscussionGroup.GetByName(importUnit.GroupName);

                    discussionGroup.GroupSettings.SetValue("start_download_from_message", (importUnit.LastImportedMessage + 1).ToString());

                    discussionGroup.GroupSettings.SaveSettings();
                }
            }

            ImportComplete();

            _formIndeterminateProgress.Close();
        }

        public void ImportDataCancelAsync()
        {
            if (_importDataWorker != null)
            {
                ImportDataCancelAsync();
            }

            _importDataWorker = new BackgroundWorker();

            _importDataWorker.WorkerSupportsCancellation = true;

            _importDataWorker.WorkerReportsProgress = true;

            _importDataWorker.DoWork += new DoWorkEventHandler((sender, e) => ImportData());

            _importDataWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((sender, e) => ImportDataComplete());

            _importDataWorker.ProgressChanged += new ProgressChangedEventHandler((sender, e) => ImportDataProgress(e.ProgressPercentage));

            _formIndeterminateProgress = new Forms.FormDeterminateProgress();

            _formIndeterminateProgress.Show();

            _importDataWorker.RunWorkerAsync();
        }

        public static List<ImportUnit> GetImportUnits(string databasePath)
        {
            SQLiteConnection importConnection = new SQLiteConnection("Data source=" + databasePath);

            if (importConnection.State != System.Data.ConnectionState.Open)
            {
                importConnection.Open();
            }

            SQLiteCommand cmd = importConnection.CreateCommand();

            cmd.CommandText = "SELECT id, name FROM discussion_group";

            SQLiteDataReader reader = cmd.ExecuteReader();

            List<ImportUnit> retval = new List<ImportUnit>();

            while (reader.Read())
            {
                ImportUnit unit = new ImportUnit
                {
                    GroupId = reader.GetInt64(0), 
                    GroupName = reader.GetString(1)
                };

                using (SQLiteCommand tmpCmd1 = importConnection.CreateCommand())
                {
                    tmpCmd1.CommandText = "SELECT number FROM group_message WHERE discussion_group = :groupId ORDER BY number ASC LIMIT 1";

                    tmpCmd1.Parameters.AddWithValue(":groupId", reader.GetInt64(0));

                    unit.FirstMessageNumber = (int)((long)tmpCmd1.ExecuteScalar());
                }

                using (SQLiteCommand tmpCmd2 = importConnection.CreateCommand())
                {
                    tmpCmd2.CommandText = "SELECT number FROM group_message WHERE discussion_group = :groupId ORDER BY number DESC LIMIT 1";

                    tmpCmd2.Parameters.AddWithValue(":groupId", reader.GetInt64(0));

                    unit.LastMessageNumber = (int)((long)tmpCmd2.ExecuteScalar());
                }

                unit.StartImportFromNumber = unit.FirstMessageNumber;

                unit.EndImportAtNumber = unit.LastMessageNumber;

                retval.Add(unit);
            }

            return retval;
        }
    }
}