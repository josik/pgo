﻿using PGOffline.Model;
using PGOffline.Presenter;
using System;
using System.IO;
using System.Net;
using System.Windows;

namespace PGOffline
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void ApplicationStartup(object sender, StartupEventArgs e)
        {
            try
            {
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

                Application.Current.DispatcherUnhandledException += new System.Windows.Threading.DispatcherUnhandledExceptionEventHandler(Current_DispatcherUnhandledException);

                MoveDataFromRoamingToLocal();

                Options.Instance.Initialize();

                FormMain formMain = new FormMain();

                FormMainPresenter formMainPresenter = new FormMainPresenter(formMain);

                formMain.Show();
            }
            catch (Exception ex)
            {
#if DEBUG
                File.AppendAllText(@"c:\temp\PgOffline_exceptions.log", ex.Message);
#endif
                throw;
            }
        }

        void ApplicationExit(object sender, ExitEventArgs e)
        {
        }

        // This functions moves data from Roaming to Local Settings.
        void MoveDataFromRoamingToLocal()
        {
            string oldAppDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "PG Offline 4");

            string newAppDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "PG Offline 4");

            try
            {
                Directory.Move(oldAppDataPath, newAppDataPath);
            }
            catch (Exception)
            {
            }
        }

        void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            UploadException(e.Exception);
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            UploadException((Exception)e.ExceptionObject);
        }

        private string GetErrorMessage(Exception exception)
        {
            string message = string.Format("{0}\r\n{1}\r\n{2}", exception.GetType(), exception.Message, exception.StackTrace);

            if (exception.InnerException != null)
            {
                message += string.Format("\r\n{0}\r\n{1}\r\n{2}", exception.InnerException.GetType(), exception.InnerException.Message, exception.InnerException.StackTrace);
            }

            try
            {
                message += string.Format("\r\nExtended result code: {0}", DatabaseManager.Instance.GetConnection().ExtendedResultCode());
            }
            catch (Exception ex)
            {
            }

            return message;
        }

        private void UploadException(Exception exception)
        {
            ServicePointManager.Expect100Continue = false;

            try
            {
                //using (WebClient webClient = new WebClient())
                //{
                //    Assembly assembly = Assembly.GetExecutingAssembly();
                //    FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                //    string version = fvi.ProductVersion;

                //    webClient.UploadValues("http://pgoffline.freelance.asfdfdfd.com/bugreport.php",
                //        new System.Collections.Specialized.NameValueCollection()
                //        {
                //            {"username", Environment.UserName},
                //            {"version", version},
                //            {"message", GetErrorMessage(exception)}
                //        });
                //}
            }
            catch (Exception ex)
            {
                // could not send exception
                //throw;
            }
        }
    }

}
