﻿using System.Windows;
using System.Windows.Media;

namespace PGOffline
{
    public static class Helper
    {

        #region Helper methods

        public static T GetAncestor<T>(DependencyObject reference) where T : DependencyObject
        {
            DependencyObject parent = VisualTreeHelper.GetParent(reference);

            while (!(parent is T))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }
            return (T)parent;
        }

        #endregion Helper methods
    }
}
