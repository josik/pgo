﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using PGOffline.Exceptions;

namespace PGOffline
{
    public class TrialVerifier
    {
        public static int DaysLeft
        {
            get
            {
                return TrialData.Load().DaysLeft;
            } 
        }
    }
}
