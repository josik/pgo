﻿namespace PGOffline
{
    public enum StatisticsType
    {
        StatisticsBasedOnDate = 0,
        StatisticsBasedOnMessagesNumbers,
        ComparisonByWeek,
        ComparisonByMonth,
        ComparisonByYear,
        ComparisonByCustomTime,
        ComparisonByCustomNumbers,
        GroupVsGroup
    }
}