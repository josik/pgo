﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Fizzler;
using PGOffline.Exceptions;
using PGOffline.Model;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace PGOffline
{
 
    public class ImportMdbManager : ImportManager<OleDbCommand>
    {
        public ImportMdbManager(string dbLocation)
            : this(dbLocation, new List<ImportUnit>(), new ImportSettings())
        {
        }

        public ImportMdbManager(string dbLocation, List<ImportUnit> unitsToImport, ImportSettings importSettings)
        {
            DbFilePath = dbLocation;

            DbConnectionString = string.Format(@"Provider=Microsoft.JET.OlEDB.4.0; Data Source={0}", dbLocation);

            UnitsToImport = unitsToImport;

            CurrentImportSettings = importSettings;

            GroupsUnits = new Dictionary<DiscussionGroup, ImportUnit>();
        }


        #region Protected Methods

        protected override IDbConnection GetNewDbConnection()
        {
            return new OleDbConnection { ConnectionString = DbConnectionString };
        }

        protected override void ImportGroups(IEnumerable<ImportUnit> unitsToImport)
        {
            List<DiscussionGroup> groupsToWrite = new List<DiscussionGroup>();

            // Prepare groups.
            foreach (ImportUnit unitToImport in unitsToImport)
            {
                try
                {
                    string groupNameInImportBase = null;

                    Nullable<DateTime> lastMembersUpdate;

                    using (OleDbConnection con = new OleDbConnection { ConnectionString = DbConnectionString })
                    {
                        con.Open();

                        using (OleDbCommand cmd = new OleDbCommand())
                        {
                            cmd.Connection = con;

                            cmd.CommandText = string.Format("SELECT * FROM [tblGroups] WHERE [GroupName] = '{0}';", unitToImport.GroupName);

                            using (OleDbDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader != null && reader.HasRows)
                                {
                                    reader.Read();

                                    groupNameInImportBase = reader.GetString(1);

                                    lastMembersUpdate = null;

                                    try
                                    {
                                        lastMembersUpdate = reader.GetDateTime(13);
                                    }
                                    catch (Exception ex)
                                    {
#if DEBUG
                                        //File.AppendAllText(string.Format("{0}\\exception.log", AppDomain.CurrentDomain.BaseDirectory), string.Format("lastMembersUpdate exception :: {0}{1}", ex.Message, Environment.NewLine));
#endif
                                        lastMembersUpdate = DateTime.MinValue;
                                    }
                                }
                                else
                                {
                                    // TODO: throw exception ImportGroupTableNameNotFound()
                                    throw new Exception("throw exception ImportGroupTableNameNotFound()");
                                }
                            }
                        }
                    }

                    DiscussionGroup existingGroup = DiscussionGroup.GetByName(unitToImport.GroupName);

                    if (existingGroup != null)
                    {
                        groupsToWrite.Add(existingGroup);

                        GroupsUnits.Add(existingGroup, unitToImport);
                    }
                    else
                    {
                        if (groupNameInImportBase == null) continue;

                        //DiscussionGroup rawGroup = new DiscussionGroup(groupNameInImportBase, "UTF-8", 0, 0, UserAccount.GetFirst().Id, "", "", lastMembersUpdate);

                        DiscussionGroup rawGroup = new DiscussionGroup(groupNameInImportBase);

                        Trace.WriteLine("Adding group: " + rawGroup.Name);

                        rawGroup.Insert();

                        groupsToWrite.Add(rawGroup);

                        GroupsUnits.Add(rawGroup, unitToImport);
                    }
                }
                catch (Exception ex)
                {
                    // import has failed

#if DEBUG
                    File.AppendAllText(string.Format("{0}\\exception.log", AppDomain.CurrentDomain.BaseDirectory), string.Format("ImportGroups() exception :: {0}{1}", ex.Message, Environment.NewLine));
#endif

                    throw new ImportDatabaseBadData();
                }
            }

            UnitsToImportCount = GetImportUnitsCount(UnitsToImport);

            foreach (DiscussionGroup groupToWrite in groupsToWrite)
            {
                long ticks;

                long ticks2;
                
                // for Access DB handle member table, if it exists
                ticks = DateTime.Now.Ticks;
                FillMembers(groupToWrite);
                ticks2 = DateTime.Now.Ticks;
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("FillMembers completed in {0} ms.", ((ticks2 - ticks) / 10000)));

                ticks = DateTime.Now.Ticks;
                FillSenders(groupToWrite);
                ticks2 = DateTime.Now.Ticks;
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("FillSenders completed in {0} ms.", ((ticks2 - ticks) / 10000)));

                ticks = DateTime.Now.Ticks;
                FillMessages(groupToWrite);
                ticks2 = DateTime.Now.Ticks;
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("FillMessages completed in {0} ms.", ((ticks2 - ticks) / 10000)));
            }
        }
        
        public override List<ImportUnit> GetImportUnits(string mdbLocation)
        {
            List<ImportUnit> units;

            using (OleDbConnection con = new OleDbConnection { ConnectionString = DbConnectionString })
            {
                try
                {
                    con.Open();
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Error opening file. " + ex.Message);

                    return null;
                }

                using (OleDbCommand cmd = new OleDbCommand())
                {
                    cmd.Connection = con;

                    cmd.CommandText = "SELECT * from [tblGroups];";

                    OleDbDataReader rdr;

                    try
                    {
                        rdr = cmd.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        System.Windows.MessageBox.Show("Error reading database. " + ex.Message);

                        return null;
                    }

                    units = new List<ImportUnit>();

                    while (rdr != null && rdr.Read())
                    {
                        ImportUnit iu = new ImportUnit
                        {
                            GroupName = rdr.GetString(1),
                            SetToImport = false
                        };

                        units.Add(iu);
                    }

                    if (rdr != null)
                    {
                        rdr.Close();

                        rdr.Dispose();
                    }
                }
            }

            List<ImportUnit>.Enumerator enumerator = units.GetEnumerator();

            while (enumerator.MoveNext())
            {
                using (OleDbConnection con = new OleDbConnection { ConnectionString = DbConnectionString })
                {
                    con.Open();

                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        cmd.Connection = con;

                        if (enumerator.Current == null) continue;

                        cmd.CommandText = string.Format("SELECT MIN(YahooMessageID), MAX(YahooMessageID) FROM [tblYgr_{0}];", enumerator.Current.GroupName);

                        OleDbDataReader rdr = cmd.ExecuteReader();

                        if (rdr == null || !rdr.Read()) continue;

                        if (rdr.IsDBNull(0) != false) continue;

                        if (enumerator.Current == null) continue;

                        enumerator.Current.FirstMessageNumber = rdr.GetInt32(0);

                        enumerator.Current.LastMessageNumber = rdr.GetInt32(1);

                        enumerator.Current.StartImportFromNumber = enumerator.Current.FirstMessageNumber;

                        enumerator.Current.EndImportAtNumber = enumerator.Current.LastMessageNumber;
                    }
                }
            }

            return units;
        }
        
        protected override long GetImportUnitsCount(IEnumerable<ImportUnit> unitsToImport)
        {
            long importUnits = 0;

            // TODO: catch exception and send message to log window

            foreach (ImportUnit importUnit in unitsToImport)
            {
                if (CurrentImportSettings.ImportType == ImportType.OnlyFavoritesNumbers)
                {
                    using (OleDbConnection con = new OleDbConnection { ConnectionString = DbConnectionString })
                    {
                        con.Open();

                        using (OleDbCommand cmd = new OleDbCommand())
                        {
                            cmd.Connection = con;

                            string tableName = string.Format("tblYgr_{0}", importUnit.GroupName);

                            cmd.CommandText = string.Format("SELECT COUNT(*) FROM [{0}] WHERE [YahooMessageID] >= {1} AND [YahooMessageID] <= {2} AND [FavMsgFlag] = TRUE;", tableName, importUnit.StartImportFromNumber, importUnit.EndImportAtNumber);

                            importUnits += (int)cmd.ExecuteScalar();
                        }
                    }
                }
                else
                {
                    using (OleDbConnection con = new OleDbConnection { ConnectionString = DbConnectionString })
                    {
                        con.Open();

                        using (OleDbCommand cmd = new OleDbCommand())
                        {
                            cmd.Connection = con;

                            string tableName = string.Format("tblYgr_{0}", importUnit.GroupName);

                            cmd.CommandText = String.Format("SELECT COUNT(*) FROM [{0}] WHERE [YahooMessageID] >= {1} AND [YahooMessageID] <= {2};", tableName, importUnit.StartImportFromNumber, importUnit.EndImportAtNumber);

                            importUnits += (int)cmd.ExecuteScalar() * 2;
                        }
                    }

                    using (OleDbConnection con = new OleDbConnection { ConnectionString = DbConnectionString })
                    {
                        con.Open();

                        using (OleDbCommand cmd = new OleDbCommand())
                        {
                            cmd.Connection = con;

                            string tableName = string.Format("tblYgr_{0}_Mmb", importUnit.GroupName);

                            if (IsTableExists(tableName) == false) continue;

                            cmd.CommandText = String.Format("SELECT COUNT(*) FROM [{0}];", tableName);

                            importUnits += (int)cmd.ExecuteScalar();
                        }
                    }
                }
            }

            return importUnits;
        }

        protected void FillSenders(DiscussionGroup @destinationGroup)
        {
            ReportProgressMessage(destinationGroup.Name);

            string tableName = string.Format("tblYgr_{0}", destinationGroup.Name);

            ImportUnit importUnit = GroupsUnits[destinationGroup];

            using (OleDbConnection con = new OleDbConnection { ConnectionString = DbConnectionString })
            {
                con.Open();

                using (OleDbCommand cmd = new OleDbCommand())
                {
                    cmd.Connection = con;

                    string commandText =
                        String.Format("SELECT * FROM [{0}] WHERE [YahooMessageID] >= {1} AND [YahooMessageID] <= {2};",
                            tableName, importUnit.StartImportFromNumber, importUnit.EndImportAtNumber);

                    if (CurrentImportSettings.ImportType == ImportType.OnlyFavorites)
                    {
                        commandText = commandText.Replace(";", " AND [FavMsgFlag] = TRUE;");
                    }

                    commandText = commandText.Replace(";", " ORDER BY [YahooMessageID] ASC;");

                    cmd.CommandText = commandText;

                    using (OleDbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader != null && !reader.HasRows)
                        {
                            Trace.WriteLine("Added no senders.");

                            return;
                        }

                        int senderNameIndex = -1;

                        int senderEmailIndex = -1;

                        int senderJoinedIndex = -1;

                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string columnName = reader.GetName(i);

                            switch (columnName)
                            {
                                case "From":
                                    senderNameIndex = i;
                                    break;
                                case "FromEmail":
                                    senderEmailIndex = i;
                                    break;
                                case "Received":
                                case "RecDate":
                                    senderJoinedIndex = i;
                                    break;
                            }
                        }

                        bool lastRow = false;

                        while (!lastRow)
                        {
                            using (SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin())
                            {
                                try
                                {
                                    do
                                    {
                                        if (!reader.Read())
                                        {
                                            lastRow = true;

                                            break;
                                        }

                                        string senderName = GetString(reader, senderNameIndex);

                                        string senderEmail = GetString(reader, senderEmailIndex);

                                        if (senderEmail.EndsWith(">"))
                                        {
                                            senderEmail = senderEmail.Remove(senderEmail.Length - 1);
                                        }

                                        DateTime senderJoined = GetDateTime(reader, senderJoinedIndex);

                                        InsertPerson(senderName, senderEmail, senderJoined, destinationGroup.Id);

                                        UnitsProceed++;

                                        //ImportDataWorker.ReportProgress(
                                        //    (int) (((double) UnitsProceed/(double) UnitsToImportCount)*100.0f));

                                        //if (ImportDataWorker.CancellationPending) break;

                                    } while (UnitsProceed % 100 != 0 && UnitsProceed > 0);
                                    
                                    ProgressPercentage = ((int)(((double)UnitsProceed / (double)UnitsToImportCount) * 100.0f));

                                    ReportProgressPercentage(ProgressPercentage);

                                    long ticks = DateTime.Now.Ticks;

                                    transaction.Commit();

                                    long ticks2 = DateTime.Now.Ticks;

                                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Writing messages completed in {0} ms.", ((ticks2 - ticks)/10000)));

                                    GC.Collect();
                                }
                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void FillMessages(DiscussionGroup @destinationGroup)
        {
            string tableName = string.Format("tblYgr_{0}", destinationGroup.Name);

            ImportUnit importUnit = GroupsUnits[destinationGroup];

            using (OleDbConnection con = new OleDbConnection { ConnectionString = DbConnectionString })
            {
                con.Open();

                using (OleDbCommand cmd = new OleDbCommand())
                {
                    cmd.Connection = con;

                    string commandText =
                        string.Format("SELECT * FROM [{0}] WHERE [YahooMessageID] >= {1} AND [YahooMessageID] <= {2};",
                            tableName, importUnit.StartImportFromNumber, importUnit.EndImportAtNumber);

                    if (CurrentImportSettings.ImportType == ImportType.OnlyFavorites)
                    {
                        commandText = commandText.Replace(";", " AND [FavMsgFlag] = TRUE;");
                    }

                    commandText = commandText.Replace(";", " ORDER BY [YahooMessageID] ASC;");

                    cmd.CommandText = commandText;

                    using (OleDbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader != null && !reader.HasRows)
                        {
                            Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Added no messages for group {0}.", destinationGroup.Name));

                            return;
                        }

                        int messageNumberIndex = -1;
                        int messageDateIndex = -1;
                        int messageSubjectIndex = -1;
                        int messageContentIndex = -1;
                        int messageFavoriteIndex = -1;
                        int messageUnreadIndex = -1;
                        int senderEmailIndex = -1;
                        int senderNameIndex = -1;

                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string columnName = reader.GetName(i);

                            switch (columnName)
                            {
                                case "From":
                                    senderNameIndex = i;
                                    break;
                                case "FromEmail":
                                    senderEmailIndex = i;
                                    break;
                                case "Received":
                                case "RecDate":
                                    messageDateIndex = i;
                                    break;
                                case "Subject":
                                    messageSubjectIndex = i;
                                    break;
                                case "Message":
                                    messageContentIndex = i;
                                    break;
                                case "YahooMessageID":
                                    messageNumberIndex = i;
                                    break;
                                case "NewMsgFlag":
                                    messageUnreadIndex = i;
                                    break;
                                case "FavMsgFlag":
                                    messageFavoriteIndex = i;
                                    break;
                            }
                        }

                        if (!reader.HasRows) return;

                        bool lastRow = false;

                        while (!lastRow)
                        {
                            using (SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin())
                            {
                                try
                                {
                                    do
                                    {
                                        if (!reader.Read())
                                        {
                                            lastRow = true;

                                            break;
                                        }

                                        FillMessage(
                                            destinationGroup, reader, messageNumberIndex, senderEmailIndex, senderNameIndex,
                                            messageSubjectIndex, messageDateIndex, messageContentIndex,
                                            messageFavoriteIndex,
                                            messageUnreadIndex);

                                        //ImportDataWorker.ReportProgress(
                                        //    (int) (((double) UnitsProceed/(double) UnitsToImportCount)*100.0f));

                                        //if (ImportDataWorker.CancellationPending) break;

                                    } while (UnitsProceed % 100 != 0 && UnitsProceed > 0);

                                    ProgressPercentage = ((int)(((double)UnitsProceed / (double)UnitsToImportCount) * 100.0f));

                                    ReportProgressPercentage(ProgressPercentage);

                                    long ticks = DateTime.Now.Ticks;

                                    transaction.Commit();

                                    long ticks2 = DateTime.Now.Ticks;

                                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Writing messages completed in {0} ms.",
                                        ((ticks2 - ticks)/10000)));

                                    GC.Collect();
                                }
                                catch (Exception)
                                {
                                    transaction.Rollback();
                                }
                            }
                        }
                    }
                }
            }
        }

        protected override void SynchronizeFavoritesStatus()
        {
            UnitsToImportCount = GetImportUnitsCount(UnitsToImport);

            if (UnitsToImportCount < 0) return;

            foreach (ImportUnit unit in UnitsToImport)
            {
                using (SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin())
                {
                    string tableName = string.Format("tblYgr_{0}", unit.GroupName);

                    using (OleDbConnection con = new OleDbConnection { ConnectionString = DbConnectionString })
                    {
                        con.Open();

                        using (OleDbCommand cmd = new OleDbCommand())
                        {
                            cmd.Connection = con;

                            cmd.CommandText = string.Format("SELECT YahooMessageID, FavMsgFlag FROM [{0}] WHERE [YahooMessageID] >= {1} AND [YahooMessageID] <= {2} AND [FavMsgFlag] = TRUE;", tableName, unit.StartImportFromNumber, unit.EndImportAtNumber);

                            using (OleDbDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader != null && !reader.HasRows) continue;

                                while (reader.Read())
                                {
                                    Message message = Message.GetMessageByNumber(reader.GetInt32(0));

                                    if (message != null)
                                    {
                                        message.Favorite = reader.GetBoolean(1);
                                    }
                                    
                                    // Save not needed sine changing the property saves the change
                                    //if (message != null) message.Save();

                                    UnitsProceed++;
                                    
                                    ProgressPercentage = ((int)(((double)UnitsProceed / (double)UnitsToImportCount) * 100.0f));

                                    ReportProgressPercentage(ProgressPercentage);

                                    if (ImportDataWorker.CancellationPending)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    transaction.Commit();
                }
            }
        }

        #endregion Protected Methods


        #region Private Methods

        private void FillMembers(DiscussionGroup group)
        {
            using (OleDbConnection con = new OleDbConnection { ConnectionString = DbConnectionString })
            {
                con.Open();

                using (OleDbCommand cmd = new OleDbCommand())
                {
                    cmd.Connection = con;

                    string tableName = string.Format("tblYgr_{0}_Mmb", @group.Name);

                    if (IsTableExists(tableName) == false) return;

                    cmd.CommandText = string.Format("SELECT * FROM [{0}];", tableName);

                    using (OleDbDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader != null && !reader.HasRows)
                        {
                            Trace.WriteLine("Added no members.");

                            return;
                        }

                        SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin();

                        if (reader != null && !reader.HasRows) return;

                        while (reader != null && reader.Read())
                        {
                            //if (rawPersonName.Contains("@..."))
                            //    rawPersonName = rawPersonName.Replace("@...", "");

                            string personName = reader.GetString(1);

                            string email = reader.GetString(2);

                            DateTime joined = DateTime.MinValue;

                            if (reader.IsDBNull(3) != true)
                            {
                                joined = reader.GetDateTime(3);
                            }

                            Person person = new Person(personName, joined, email, true, @group.Id);

                            person.Insert();

                            if (UnitsProceed % 10000 == 0 && UnitsProceed > 0)
                            {
                                transaction.Commit();

                                transaction = DatabaseManager.Instance.TransactionBegin();
                            }

                            Trace.WriteLine("Adding member: " + person.Name);

                            UnitsProceed++;
                            
                            ProgressPercentage = ((int)(((double)UnitsProceed / (double)UnitsToImportCount) * 100.0f));

                            ReportProgressPercentage(ProgressPercentage);

                            if (ImportDataWorker.CancellationPending) break;
                        }

                        transaction.Commit();
                    }
                }
            }
        }

        //protected IEnumerable<IDataRecord> GetMembersToImport(DiscussionGroup group)
        //{
        //    using (IDbConnection con = GetNewDbConnection())
        //    {
        //        using (IDbCommand cmd = con.CreateCommand())
        //        {
        //            con.Open();

        //            string tableName = string.Format("tblYgr_{0}_Mmb", @group.Name);

        //            if (IsTableExists(tableName) == false) yield return null;

        //            cmd.CommandText = string.Format("SELECT * FROM [{0}];", tableName);

        //            using (IDataReader rdr = cmd.ExecuteReader())
        //            {
        //                //if (rdr == null) yield return null;

        //                while (rdr.Read())
        //                {
        //                    yield return rdr;
        //                }

        //                yield return null;
        //            }
        //        }
        //    }
        //}

        #endregion Private Methods

    }
}
