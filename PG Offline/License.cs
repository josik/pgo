﻿using LogicNP.CryptoLicensing;
using Microsoft.Win32;
using System;
using System.Management;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Windows;

namespace PGOffline
{

    // currently pg4 is saying localhost:3030 is not running even though it seems to be
    // , cadded the my PRD trial key as disabled, I think I should work on this piece using the localhost service

    // my subscrption test license:
    // tgCiA/kzhIN+vc8BSXgLnk/c0AE7AEVtYWlsPSNOYW1lPSNFbmNyeXB0aW9uS2V5PSNEYXRlSXNzdWVkPTgvMjEvMjAxNCA0OjI1OjU3IFBNAQGJ8/HtIp1D68rZxH7/QWxBy1CNOjxaEO1EuSQnkjVJRfFWbWPAWBuBk8FMoPJIPDPl5gJpAEd0oEsjdLE/62XY/7vIpURE7QyE+h2WNqdn7G+WFSb3xJwuc9wUvVqj+ejNTW/++TSVN1laur07eoYhUXMkyOJ8dfhxP8Qol9UXAxnIdpegsATP1DXSGSS60+lMJtkRUKEveXxXSVk3kc8IY4/8BPIR36PGeGkbfQ3uap6/pZuSIGcLmpXqmWNIXJ6cZSwHwH6IlhT3Z5phZOxd9GBOP9yn5V+eriwRfPGGqS4Pi9goFKXX1bOObNtyxXBipg2EDJrbFXEViafhNa9e

    public class License : Singleton<License>
    {
#if DEBUG
        private static string LicenseServiceURL = "http://localhost:3030/LicenseService-2856734632843.asmx";
#else
        private static string LicenseServiceURL = "https://pgofflinelicenseserver.file2.me/LicenseService-2856734632843.asmx";
#endif

        public LicenseTypeEnum LicenseType
        {
            get
            {
                if (_license == null) return LicenseTypeEnum.Trial;

                if (_license.HasDateExpires) return LicenseTypeEnum.Subscription;

                if (_license.IsEvaluationLicense()) return LicenseTypeEnum.Trial;

                return LicenseTypeEnum.Perpetual;
            }
        }

        private string LicenseCode
        {
            get
            {
                return _license == null ? string.Empty : _license.LicenseCode;
            }
        }

        private CryptoLicense _license;

        //private const string ValidationKey = "lgCAAOci+YZDrc8BdgBOYW1lPURhbiBCdWxsb2NrI0NvbXBhbnk9UGVyc29uYWwgR3JvdXB3YXJlI0VtYWlsPWRhbmJAYXNwaXRlbC5jb20jU3Vic2NyaXB0aW9uPVRydWUjU3Vic2NyaXB0aW9uVmFsaWRUaWxsPTE0LUp1bC0yMDE1AQM+ChDxxW5Vt8/PLeMAfjMOoICwWIjQK+jZi+y3+vJC7LUngyxn96aANh+lop1w/Yw=";
        //TRIAL
        private const string ValidationKey = "AAAEAAGTZRlRueCBPsmFc9/sQ6OeAPHWQbtKYIyM3EHsNNEjBbukbeS55A5psdsirV4CWimuO08mKBFyqh1dbxeke6GLRRyNFovAk/KIbCkSwRt78AwVIbImKZoo3cej3nLSEu7+a5lIGY1uX6DJQw09QdsheiGXx6whY4EsWCaZZCjl37Uo2tMS2hN3jCH7yCnSPfV3QCya4dh9MV1cYyBkVE2sqmUtSg57ed+g24Lh4fmNYK4K5qFharoMYrMuNLnAuiixrpVTWQj+e3fR28BTOOcoFUDhlFXChwZosYQLZ1JUG01VLBN4r0VuFkkw78vjICp1JF7QUbbrqn03eC4acwrVAwABAAE=";

/*
        public CryptoLicense GetCryptoLicense()
        {
            return new CryptoLicense(ValidationKey);
        }
*/

/*
        public LicenseStatus GetLicenseStatus()
        {
            return _license.Status;
        }
*/

        public string GetMaskedLicenseCode()
        {
            if (_license == null) return string.Empty;

            string code = _license.LicenseCode;
            
            if (string.IsNullOrEmpty(code)) return string.Empty;

            int length = code.Length;

            if (length <= 8) return string.Empty;

            return string.Format("****{0}", code.Substring(length-8, 8));
        }

        public bool LoadLicenseFromRegistry()
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\PG4 Offline\\4.x");

                if (key == null) return false;

                byte[] encCodeBytes = (byte[])key.GetValue("LicenseCode", null);

                if (encCodeBytes == null) return false;

                byte[] decCodeBytes = ProtectedData.Unprotect(encCodeBytes, Encoding.Unicode.GetBytes("ere4534rf#$^%^$@@#%"), DataProtectionScope.CurrentUser);

                string decCode = Encoding.Unicode.GetString(decCodeBytes);

                SetLicense(decCode, saveToRegistry: false);
            }
            catch (Exception ex)
            {
                //throw;
            }

            return true;
        }

        public void SetTrialLicense()
        {
            //var uuid = UUID;

            CryptoLicense lic = new CryptoLicense();

            lic.LicenseServiceURL = LicenseServiceURL;

            //var request = LicenseServerHttpWebRequest();
            //lic.InitWebRequest(request);

            var uid = lic.GetLocalMachineCodeAsString();

            string newLicense = lic.QueryWebService("GetNewTrialLicense", paramNames: new string[] { "uniqueMachineId" }, paramValues: new string[] { uid });

            SetLicense(newLicense, true);
        }

        private static HttpWebRequest LicenseServerHttpWebRequest()
        {
            Uri uri = new Uri(LicenseServiceURL);

            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(uri);

            NetworkCredential credentials = new NetworkCredential("atest", "atest");

            request.Credentials = credentials;

            return request;
        }

        public bool SetLicense(string licenseCode, bool saveToRegistry)
        {
            if (string.IsNullOrEmpty(licenseCode)) return false;

            CryptoLicense newLicense = new CryptoLicense(licenseCode, ValidationKey)
            {
                LicenseServiceURL = LicenseServiceURL
            };
            
            if (newLicense.Status != LicenseStatus.Valid) return false;

            _license = newLicense;

            if (!saveToRegistry) return true;

            // store encrypted license code in the registry
            RegistryKey key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\PG4 Offline\\4.x");
            byte[] decCodeBytes = Encoding.Unicode.GetBytes(_license.LicenseCode);
            byte[] encCode = ProtectedData.Protect(decCodeBytes, Encoding.Unicode.GetBytes("ere4534rf#$^%^$@@#%"), DataProtectionScope.CurrentUser);
            key.SetValue("LicenseCode", encCode, RegistryValueKind.Binary);

            return true;
        }
        
        public bool IsLicenseValid()
        {
            if (_license == null) return false;

            // Ping license server
            if (_license.PingLicenseService() != null)
            {
                //ShowLicenseServerUnavailableMessage();
                return false;
                //throw new Exception("License server is not accessible.");
            }

            // TRIAL
            if (_license.IsEvaluationLicense())
            {
                return _license.Status == LicenseStatus.Valid;
                //return _license.IsEvaluationLicense() && !_license.IsEvaluationExpired() && _license.IsMachineCodeValid();
            }

            // SUBSCRIPTION
            if (_license.HasMaxUsageDays)
            {
                bool isLicenseDisabledInService = _license.IsLicenseDisabledInService();
                return _license.Status == LicenseStatus.Valid;
            }

            return true;
        }

        public void ShowLicenseInvalidMessage()
        {
            MessageBox.Show("Invalid license. \n\n" + _license.GetAllStatusExceptionsAsString(), this.LicenseCode);
        }
        
        public void ShowSerialInvalidMessage()
        {
            MessageBox.Show("Serial Validation Failed.\n\n" + _license.GetAllStatusExceptionsAsString(), this.LicenseCode);
        }

        public void ShowLicenseServerUnavailableMessage()
        {
            MessageBox.Show("License server is not accessible.");
        }

        public DateTime GetExpirationDate()
        {
            if (LicenseType == LicenseTypeEnum.Subscription && _license.HasDateExpires)
            {
                return _license.DateExpires;
            }

            return DateTime.MaxValue;

        }

        public static string UUID
        {
            get
            {
                string uuid = string.Empty;

                ManagementClass mc = new ManagementClass("Win32_ComputerSystemProduct");

                ManagementObjectCollection moc = mc.GetInstances();

                foreach (var o in moc)
                {
                    var mo = (ManagementObject) o;

                    uuid = mo.Properties["UUID"].Value.ToString();

                    break;
                }

                return uuid;
            }
        }

    }

    public enum LicenseTypeEnum
    {
        Perpetual = 0,
        Subscription,
        Trial,
        Expired_Trial,
        Expired_Subscription
    }

}
