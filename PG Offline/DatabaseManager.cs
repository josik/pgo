﻿using System.Data;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using PGOffline.Exceptions;
using PGOffline.Model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Text;

namespace PGOffline
{
    public class DatabaseManager : Singleton<DatabaseManager>
    {
        SQLiteConnection _connection;

        private string _connectionString;

        private string _databasePath;

        private string _lastDatabasePath;

        private bool _shouldCreateDatabase = false;

        public const int IntFailedReturnValue = -1;

        public string CurrentDatabasePath
        {
            get
            {
                return _databasePath;
            }
        }

        public int ExecuteQueryAndGetInt(string query)
        {
            if (!IsConnectionIsReady()) return IntFailedReturnValue;

            using (SQLiteCommand command = new SQLiteCommand(query, GetConnection()))
            {
                using (SQLiteDataReader dataReader = command.ExecuteReader())
                {
                    dataReader.Read();

                    int value = dataReader.GetInt32(0);

                    dataReader.Close();

                    return value;
                }
            }
        }

        public object ExecuteQueryAndGetValue(string query, int fieldIndex)
        {
            if (!IsConnectionIsReady()) return null;

            using (SQLiteCommand command = new SQLiteCommand(query, GetConnection()))
            {
                using (SQLiteDataReader dataReader = command.ExecuteReader())
                {
                    dataReader.Read();

                    object value = dataReader.GetValue(fieldIndex);

                    dataReader.Close();

                    return value;
                }
            }
        }

        public object[] ExecuteQueryAndGetValues(string query)
        {
            if (!IsConnectionIsReady()) return null;

            using (SQLiteCommand command = new SQLiteCommand(query, GetConnection()))
            {
                using (SQLiteDataReader dataReader = command.ExecuteReader())
                {
                    dataReader.Read();

                    if (!dataReader.HasRows) return null;

                    object[] values = new object[dataReader.FieldCount];

                    dataReader.GetValues(values);

                    dataReader.Close();

                    return values;
                }
            }
        }

        public List<object> ExecuteQueryAndGetValueList(string query, int fieldIndex)
        {
            //if (!IsConnectionIsReady()) return null;

            return ExecuteCommandAndGetValueList(CreateCommand(query), fieldIndex);
        }

        public List<object> ExecuteCommandAndGetValueList(SQLiteCommand command, int fieldIndex)
        {
            if (!IsConnectionIsReady()) return null;

            using (SQLiteDataReader dataReader = command.ExecuteReader())
            {
                List<object> values = new List<object>();

                while (dataReader.Read())
                {
                    object value = dataReader.GetValue(fieldIndex);

                    values.Add(value);
                }

                dataReader.Close();

                command.Dispose();

                return values;
            }
        }

        public List<object[]> ExecuteQueryAndGetValuesList(string query)
        {
            if (!IsConnectionIsReady()) return null;

            return ExecuteCommandAndGetValuesList(CreateCommand(query));
        }

        public List<object[]> ExecuteCommandAndGetValuesList(SQLiteCommand command)
        {
            if (!IsConnectionIsReady()) return null;

            using (SQLiteDataReader dataReader = command.ExecuteReader())
            {
                List<object[]> values = new List<object[]>();

                while (dataReader.Read())
                {
                    object[] value = new object[dataReader.FieldCount];

                    dataReader.GetValues(value);

                    values.Add(value);
                }

                dataReader.Close();
                
                command.Dispose();

                return values;
            }
        }

        public object ExecuteQuery(string query)
        {
            if (!IsConnectionIsReady()) return null;

            object returnValue;

            using (SQLiteCommand command = new SQLiteCommand(query, _connection))
            {
                returnValue = command.ExecuteNonQuery();
            }

            return returnValue;
        }
        
        public SQLiteCommand CreateCommand(string query)
        {
            if (!IsConnectionIsReady()) return null;

            //SQLiteCommand cmd = GetConnection().CreateCommand();

            SQLiteCommand cmd = _connection.CreateCommand();

            cmd.CommandText = query;

            return cmd;
        }

        public bool IsConnectionIsReady()
        {
            try
            {
                switch (_connection.State)
                {
                    case ConnectionState.Open:
                        break;

                    case ConnectionState.Broken:
                    case ConnectionState.Closed:
                        {
                            ResetConnection();

                            break;
                        }

                    case ConnectionState.Connecting:
                    case ConnectionState.Executing:
                    case ConnectionState.Fetching:
                        {
                            // wait for 10 seconds, checking every second
                            for (int i = 0; i < 10; i++)
                            {
                                Thread.Sleep(1000);

                                if (_connection.State == ConnectionState.Open) break;
                            }

                            if (_connection.State == ConnectionState.Closed) ResetConnection();

                            break;
                        }
                }

                return _connection.State == ConnectionState.Open;
            }
            catch (Exception ex)
            {
                //return false;
                if (_connection == null) OpenConnection();
                return _connection != null && _connection.State == ConnectionState.Open;
                //throw;
            }
        }

        public bool BackupDatabase(string sourceName, string destName)
        {
            try
            {
                File.Copy(sourceName, destName, overwrite: true);

                return File.Exists(destName);
            }
            catch (Exception ex)
            {
                // TODO: new exception
                throw;
            }
        }
        
        public HashSet<long> GetMessagesNumbers(DiscussionGroup discussionGroup)
        {
            if (!IsConnectionIsReady()) return null;

            try
            {
                using (SQLiteCommand cmd = CreateCommand("SELECT number FROM group_message WHERE discussion_group = @id"))
                {
                    cmd.Parameters.AddWithValue("@id", discussionGroup.Id);

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        var retval = new HashSet<long>();

                        while (reader.Read())
                        {
                            retval.Add(reader.GetInt64(0));
                        }

                        reader.Close();

                        return retval;
                    }
                }
            }
            catch (Exception ex)
            {
                // TODO: new exception
                throw;
            }
        }

        public void UpdateAttachmentsLinks(string newPath, DiscussionGroup discussionGroup)
        {
            if (!IsConnectionIsReady()) return;
            
            try
            {
                // Transaction temporarily turned off because of possible nesting.
                // SQLiteTransaction transaction = TransactionBegin();

                List<object[]> rawAttachments = ExecuteQueryAndGetValuesList(String.Format("SELECT attachment.* FROM group_message_attachment INNER JOIN group_message ON group_message.id = group_message_attachment.group_message INNER JOIN attachment ON attachment.id = group_message_attachment.attachment WHERE group_message.discussion_group = {0}", discussionGroup.Id));

                foreach (object[] rawAttachment in rawAttachments)
                {
                    ExecuteQuery(String.Format("UPDATE attachment SET filename = `{0}` WHERE id = {1}", newPath + Path.GetFileName((String)rawAttachment[1]), (long)rawAttachment[0]));
                }

                // transaction.Commit();
            }
            catch (Exception ex)
            {
                // TODO: new exception
                throw;
            }
            
        }

        private long IndexOfByteSubstring(byte[] input, byte[] pattern)
        {
            int startIndex = 0;

            while ((startIndex = Array.IndexOf(input, pattern[0], startIndex)) >= 0)
            {
                for (int i = 0; i < pattern.Length; i++)
                {
                    if (startIndex + i >= input.Length || pattern[i] != input[startIndex + i]) break;

                    if (i == pattern.Length - 1) return startIndex;
                }

                startIndex += pattern.Length;

                if (startIndex >= input.Length) return -1;
            }

            return -1;
        }
        
        public bool OpenConnection(string databasePath = null)
        {
            try
            {
                _lastDatabasePath = databasePath;

                CloseConnection();

                CreateConnection(databasePath);

                using (var pragma = new SQLiteCommand("PRAGMA foreign_keys = true;", _connection))
                {
                    pragma.ExecuteNonQuery();
                }

                return _connection.State == ConnectionState.Open;  
            }
            catch (Exception ex)
            {
                // TODO: new exception
                throw;
            }
        }

        public void ResetConnection()
        {
            OpenConnection(_lastDatabasePath);
        }

        private SQLiteConnection CreateConnection(string databasePath = null)
        {
            _databasePath = databasePath ?? PathResolver.GetDefaultDatabasePath();

            _shouldCreateDatabase = !File.Exists(_databasePath);

            _connectionString = string.Format(@"Data source={0}", _databasePath);

            try
            {
                _connection = new SQLiteConnection(_connectionString);

                _connection.Open();
            }
            catch (IOException e)
            {
                throw new DatabaseNoAccessException(_databasePath, e.Message);
            }
            catch (SQLiteException e)
            {
                throw new DatabaseWrongFormatException(_databasePath, e.Message);
            }

            if (_shouldCreateDatabase)
            {
                CreateDatabase();
            }
            else
            {
                try
                {
                    new SchemaUpdater(_connection, _databasePath).TryDatabaseUpgrade();

                    _connection = new SQLiteConnection(_connectionString);

                    _connection.Open();

                    //if (_connection == null)
                       // OpenConnection();
                }
                catch (DatabaseWrongFormatException e)
                {
                    CloseConnection();

                    throw;
                }
            }

            // FixUnreadCount();

            return _connection;
        }
        
        private bool CreateDatabase()
        {
            if (!IsConnectionIsReady()) return false;

            using (StreamReader reader = File.OpenText(PathResolver.GetSchemaScriptPath()))
            {
                using (SQLiteCommand cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = reader.ReadToEnd();

                    cmd.ExecuteNonQuery();
                }
            }

            return true;
        }

        public SQLiteConnection GetConnection()
        {
            return _connection;
        }

        public void CloseConnection()
        {
            if (_connection == null) return;

            _connection.Dispose();

            _connection = null;

            GC.Collect();
        }

        public SQLiteTransaction TransactionBegin()
        {
            if (_connection == null) OpenConnection();

            return _connection.BeginTransaction();
        }

        public long GetLastInsertRowId(SQLiteConnection connection)
        {
            if (!IsConnectionIsReady()) return IntFailedReturnValue;

            //var id = (long) (new SQLiteCommand("SELECT last_insert_rowid();", connection).ExecuteScalar());

            long id = IntFailedReturnValue;

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT last_insert_rowid();";

                object idObject = cmd.ExecuteScalar();

                //if (idObject == null)
                //{
                //    var t = "";
                //}

                if (idObject != null) id = (long) idObject;
            }
#if DEBUG
            //File.AppendAllText("c:/temp/debug_import.log", string.Format("last message insert id: {0} {1}", id, Environment.NewLine));
#endif
            //Thread.Sleep(1000);

            return id;
        }

        public void FixUnreadCount()
        {
            if (!IsConnectionIsReady()) return;

            StringBuilder queryBuilder = new StringBuilder();

            using (SQLiteCommand cmd1 = _connection.CreateCommand())
            {
                cmd1.CommandText = "SELECT id FROM discussion_group";

                using (SQLiteDataReader rdr1 = cmd1.ExecuteReader())
                {
                    while (rdr1.Read())
                    {
                        var groupId = rdr1.GetValue(0);

                        using (SQLiteCommand cmd2 = _connection.CreateCommand())
                        {
                            cmd2.CommandText = string.Format("SELECT COUNT(id) FROM group_message WHERE unread=1 AND discussion_group = {0}", groupId);

                            var unreadCount = cmd2.ExecuteScalar();

                            queryBuilder.AppendLine(String.Format("UPDATE discussion_group SET unread_count = {0} WHERE id = {1};", unreadCount, groupId));
                        }
                    }

                    rdr1.Close();
                }
            }

            using (SQLiteTransaction transaction = _connection.BeginTransaction())
            {
                using (var cmd = new SQLiteCommand(queryBuilder.ToString(), _connection))
                {
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }
    }
}
