﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using Asfdfdfd.Api.Yahoo.Groups;
using PGOffline.Model;
using PGOffline.Properties;

namespace PGOffline
{
    public static class Utils
    {
        public static AppDownloadOptions GetAppDownloadOptions()
        {
            string cacheServerName =
                string.IsNullOrEmpty(Options.Instance.GetAsString("download_options_cache_server_name"))
                    ? Settings.Default.DefaultCacheServerName
                    : Options.Instance.GetAsString("download_options_cache_server_name");
            
            AppDownloadOptions appDownloadOptions = new AppDownloadOptions(
                Options.Instance.GetAsBoolean("download_options_enable_download_limit_speed"),
                Options.Instance.GetAsInt("download_options_download_limit_speed_type"),
                Options.Instance.GetAsInt("download_options_messages_per_minute"),
                Options.Instance.GetAsInt("download_options_initial_delay"),
                Options.Instance.GetAsInt("download_options_download"),
                Options.Instance.GetAsInt("download_options_then_wait"),
                Options.Instance.GetAsBoolean("download_options_enable_cache_server"),
                cacheServerName,
                Settings.Default.CacheServerRestApiVersion,
                Options.Instance.GetAsBoolean("download_options_enable_messages_caching"),
                Options.Instance.GetAsInt("download_options_receive_messages_per_request")
                );

            return appDownloadOptions;
        }

        public static void OpenLinkInExternalBrowser(string sUrl)
        {
            try
            {
                Process.Start(sUrl);
            }
            catch (Win32Exception)
            {
                try
                {
                    var startInfo = new ProcessStartInfo("IExplore.exe", sUrl);
                    Process.Start(startInfo);
                    startInfo = null;
                }
                catch (Exception)
                {
                }
            }
        }

        public static class Encryption
        {
            //static byte[] entropy = Encoding.Unicode.GetBytes("Add some entropy");
            static byte[] entropy = CreateRandomEntropy();

            public static byte[] CreateRandomEntropy()
            {
                // Create a byte array to hold the random value.
                var entropy = new byte[16];

                // Create a new instance of the RNGCryptoServiceProvider.
                // Fill the array with a random value.
                new RNGCryptoServiceProvider().GetBytes(entropy);

                // Return the array.
                return entropy;
            }

            public static string EncryptString(SecureString input)
            {
                byte[] encryptedData = ProtectedData.Protect(Encoding.Unicode.GetBytes(ToInsecureString(input)), entropy, DataProtectionScope.CurrentUser);
                return Convert.ToBase64String(encryptedData);
            }

            public static SecureString DecryptString(string encryptedData)
            {
                try
                {
                    byte[] decryptedData = ProtectedData.Unprotect(Convert.FromBase64String(encryptedData), entropy, DataProtectionScope.CurrentUser);
                    return ToSecureString(Encoding.Unicode.GetString(decryptedData));
                }
                catch
                {
                    return new SecureString();
                }
            }

            public static SecureString ToSecureString(string input)
            {
                var secure = new SecureString();
                foreach (char c in input)
                {
                    secure.AppendChar(c);
                }
                secure.MakeReadOnly();
                return secure;
            }

            public static string ToInsecureString(SecureString input)
            {
                string returnValue = String.Empty;
                IntPtr ptr = Marshal.SecureStringToBSTR(input);
                try
                {
                    returnValue = Marshal.PtrToStringBSTR(ptr);
                }
                finally
                {
                    Marshal.ZeroFreeBSTR(ptr);
                }
                return returnValue;
            }
        }

    }
}
