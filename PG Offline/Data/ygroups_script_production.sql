PRAGMA auto_vacuum=FULL;
PRAGMA foreign_keys=ON;

BEGIN TRANSACTION;

CREATE TABLE options (
key TEXT PRIMARY KEY,
value TEXT);

CREATE TABLE email_account (
id INTEGER PRIMARY KEY AUTOINCREMENT,
email TEXT NOT NULL,
smtp_address TEXT,
smtp_port INTEGER,
smtp_login TEXT,
smtp_password TEXT);

CREATE TABLE group_settings (
key TEXT,
value TEXT,
discussion_group INTEGER,
PRIMARY KEY (key, discussion_group),
FOREIGN KEY (discussion_group) REFERENCES discussion_group(id) ON DELETE CASCADE);

CREATE INDEX group_settings_key ON group_settings (key);
CREATE INDEX group_settings_value ON group_settings (value);

CREATE TABLE user_account (
id INTEGER PRIMARY KEY AUTOINCREMENT,
name TEXT NOT NULL,
password TEXT NOT NULL,
sortOrder INTEGER);

CREATE TABLE discussion_group (
id INTEGER PRIMARY KEY AUTOINCREMENT,
name TEXT NOT NULL,
charset TEXT NOT NULL DEFAULT 'UTF-8',
actual_email INTEGER,
user_account INTEGER,
username TEXT,
password TEXT,
last_members_update DATETIME,
unread_count INTEGER NOT NULL DEFAULT 0,
FOREIGN KEY (actual_email) REFERENCES email_account(id),
FOREIGN KEY (user_account) REFERENCES user_account(id));

CREATE INDEX discussion_group_name ON discussion_group (name);
CREATE INDEX discussion_group_charset ON discussion_group (charset);

CREATE TABLE person (
id INTEGER PRIMARY KEY AUTOINCREMENT,
name TEXT NOT NULL,
joined DATETIME,
email TEXT,
is_member BOOLEAN,
discussion_group INTEGER NOT NULL,
FOREIGN KEY (discussion_group) REFERENCES discussion_group(id) ON DELETE CASCADE);

CREATE INDEX person_email ON person (email);
CREATE INDEX person_name ON person (name);
CREATE INDEX person_id ON person (id);
CREATE INDEX person_discussion_group ON person (discussion_group);

CREATE TABLE photo (
id INTEGER PRIMARY KEY AUTOINCREMENT,
content BLOB,
discussion_group INTEGER NOT NULL,
FOREIGN KEY (discussion_group) REFERENCES discussion_group(id) ON DELETE CASCADE);

CREATE TABLE file 
(id INTEGER PRIMARY KEY AUTOINCREMENT,
content BLOB,
discussion_group INTEGER NOT NULL,
FOREIGN KEY (discussion_group) REFERENCES discussion_group(id) ON DELETE CASCADE);

CREATE TABLE group_message (
id INTEGER PRIMARY KEY AUTOINCREMENT,
number INTEGER NOT NULL,
date DATETIME,
subject TEXT NOT NULL DEFAULT '',
content BLOB,
favorite BOOLEAN NOT NULL,
unread BOOLEAN NOT NULL,
person INTEGER NOT NULL,
charset TEXT,
discussion_group INTEGER NOT NULL,
topic_id INTEGER,
parent_id INTEGER,
thread_level INTEGER,
prev_in_topic INTEGER,
next_in_topic INTEGER,
prev_in_time INTEGER,
next_in_time INTEGER,
FOREIGN KEY (discussion_group) REFERENCES discussion_group(id) ON DELETE CASCADE,
FOREIGN KEY (person) REFERENCES person(id),
UNIQUE (number, discussion_group) ON CONFLICT REPLACE);

CREATE INDEX group_message_id ON group_message (id);
CREATE INDEX group_message_number ON group_message (number);
CREATE UNIQUE INDEX group_message_number_and_discussion_group ON group_message (discussion_group, number);
CREATE INDEX group_message_discussion_group ON group_message (discussion_group);
CREATE INDEX group_message_unread ON group_message (unread);
CREATE INDEX group_message_person ON group_message (person);
CREATE INDEX group_message_charset ON group_message (charset);
CREATE INDEX group_message_unread_group ON group_message (unread, discussion_group);
CREATE INDEX group_message_charset_group ON group_message (discussion_group, charset);

CREATE TRIGGER after_unread_group_message_insert AFTER INSERT ON group_message WHEN new.unread = 1 BEGIN
UPDATE discussion_group SET unread_count=unread_count+1 WHERE discussion_group.id=new.discussion_group;
END;

CREATE TRIGGER after_unread_group_message_delete AFTER DELETE ON group_message WHEN old.unread = 1 BEGIN
UPDATE discussion_group SET unread_count=unread_count-1 WHERE discussion_group.id=old.discussion_group;
END;

CREATE TRIGGER after_group_message_update_set_unread AFTER UPDATE OF unread ON group_message WHEN new.unread = 1 AND old.unread = 0 BEGIN
UPDATE discussion_group SET unread_count=unread_count+1 WHERE discussion_group.id=new.discussion_group;
END;

CREATE TRIGGER after_group_message_update_set_read AFTER UPDATE OF unread ON group_message WHEN new.unread = 0 AND old.unread = 1 BEGIN
UPDATE discussion_group SET unread_count=unread_count-1 WHERE discussion_group.id=new.discussion_group;
END;

CREATE TABLE attachment (
id INTEGER PRIMARY KEY AUTOINCREMENT,
filename TEXT);

CREATE TABLE group_message_attachment (
group_message INTEGER,
attachment INTEGER,
PRIMARY KEY(group_message, attachment),
FOREIGN KEY (group_message) REFERENCES group_message(id) ON DELETE CASCADE,
FOREIGN KEY (attachment) REFERENCES attachment(id));

CREATE TABLE email_message_attachment (
email_message INTEGER,
attachment INTEGER,
PRIMARY KEY(email_message, attachment),
FOREIGN KEY (email_message) REFERENCES email_message(id),
FOREIGN KEY (attachment) REFERENCES attachment(id));

CREATE TABLE email_message (
id INTEGER PRIMARY KEY AUTOINCREMENT,
subject TEXT,
content TEXT,
sent BOOLEAN,
date DATETIME);

CREATE TABLE email (
address TEXT PRIMARY KEY);

CREATE TABLE message_to_email (
message INTEGER,
email TEXT,
PRIMARY KEY(message, email),
FOREIGN KEY (message) REFERENCES email_message(id),
FOREIGN KEY (email) REFERENCES email(address));

CREATE TABLE message_copy_email (
message INTEGER,
email TEXT,
PRIMARY KEY(message, email),
FOREIGN KEY (message) REFERENCES email_message(id),
FOREIGN KEY (email) REFERENCES email(address));

CREATE TABLE address_book (
id INTEGER PRIMARY KEY AUTOINCREMENT,
name TEXT,
comment TEXT,
email TEXT,
FOREIGN KEY (email) REFERENCES email(address));

CREATE TABLE search_params (
version INTEGER,
name TEXT,
value TEXT,
UNIQUE (name) ON CONFLICT REPLACE);

INSERT INTO options (key, value) VALUES ('database_version', '18');

COMMIT;
