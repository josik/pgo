﻿using System.Threading;
using PGOffline.Exceptions;
using PGOffline.Model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Text;

namespace PGOffline
{
    public class DatabaseManager2 : Singleton<DatabaseManager2>
    {
        private SQLiteConnection _connection;

        private string _connectionString;

        private string _databasePath;

        private string _lastDatabasePath;

        private bool _shouldCreateDatabase = false;

        public string CurrentDatabasePath
        {
            get { return _databasePath; }
        }

        public SQLiteConnection GetNewConnection(string databasePath = null)
        {
            _lastDatabasePath = databasePath;

            var connection = CreateNewConnection(databasePath);

            using (var pragma = new SQLiteCommand("PRAGMA foreign_keys = true;", connection))
            {
                pragma.ExecuteNonQuery();
            }

            return connection;
        }

        private SQLiteConnection CreateNewConnection(string databasePath = null)
        {
            SQLiteConnection connection;

            if (databasePath == null) PathResolver.GetDefaultDatabasePath();

            var shouldCreateDatabase = !File.Exists(databasePath);

            var connectionString = string.Format(@"Data source={0}", databasePath);

            try
            {
                connection = new SQLiteConnection(connectionString);

                connection.Open();
            }
            catch (IOException e)
            {
                throw new DatabaseNoAccessException(databasePath, e.Message);
            }
            catch (SQLiteException e)
            {
                throw new DatabaseWrongFormatException(databasePath, e.Message);
            }

            if (_shouldCreateDatabase)
            {
                //CreateDatabase();
            }
            else
            {
                try
                {
                    new SchemaUpdater(connection, databasePath).TryDatabaseUpgrade();

                    connection = new SQLiteConnection(connectionString);

                    connection.Open();
                }
                catch (DatabaseWrongFormatException e)
                {
                    connection.Close();

                    connection.Dispose();

                    throw;
                }
            }

            return connection;
        }

        public int ExecuteQueryAndGetInt(string query)
        {
            using (SQLiteCommand command = new SQLiteCommand(query, CreateNewConnection()))
            {
                using (SQLiteDataReader dataReader = command.ExecuteReader())
                {
                    dataReader.Read();

                    int value = dataReader.GetInt32(0);

                    dataReader.Close();

                    return value;
                }
            }
        }

        public object ExecuteQueryAndGetValue(string query, int fieldIndex)
        {
            using (SQLiteCommand command = new SQLiteCommand(query, CreateNewConnection()))
            {
                using (SQLiteDataReader dataReader = command.ExecuteReader())
                {
                    dataReader.Read();

                    object value = dataReader.GetValue(fieldIndex);

                    dataReader.Close();

                    return value;
                }
            }
        }

        public object[] ExecuteQueryAndGetValues(string query)
        {
            using (SQLiteCommand command = new SQLiteCommand(query, CreateNewConnection()))
            {
                using (SQLiteDataReader dataReader = command.ExecuteReader())
                {
                    dataReader.Read();

                    if (!dataReader.HasRows) return null;

                    object[] values = new object[dataReader.FieldCount];

                    dataReader.GetValues(values);

                    dataReader.Close();

                    return values;
                }
            }
        }

        public List<object> ExecuteQueryAndGetValueList(string query, int fieldIndex)
        {
            return ExecuteCommandAndGetValueList(query, fieldIndex);
        }

        public List<object> ExecuteCommandAndGetValueList(string query, int fieldIndex)
        {
            using (SQLiteConnection connection = CreateNewConnection())
            {
                using (SQLiteCommand command = connection.CreateCommand())
                {
                    command.CommandText = query;

                    using (SQLiteDataReader dataReader = command.ExecuteReader())
                    {
                        List<object> values = new List<object>();

                        while (dataReader.Read())
                        {
                            object value = dataReader.GetValue(fieldIndex);

                            values.Add(value);
                        }

                        dataReader.Close();

                        command.Dispose();

                        return values;
                    }
                }
            }
        }

        public List<object[]> ExecuteQueryAndGetValuesList(string query)
        {
            return ExecuteCommandAndGetValuesList(query);
        }

        public List<object[]> ExecuteCommandAndGetValuesList(string query)
        {
            using (SQLiteConnection connection = CreateNewConnection())
            {
                using (SQLiteCommand command = connection.CreateCommand())
                {
                    command.CommandText = query;

                    using (SQLiteDataReader dataReader = command.ExecuteReader())
                    {
                        List<object[]> values = new List<object[]>();

                        while (dataReader.Read())
                        {
                            object[] value = new object[dataReader.FieldCount];

                            dataReader.GetValues(value);

                            values.Add(value);
                        }

                        dataReader.Close();

                        command.Dispose();

                        return values;
                    }
                }
            }
        }

        public void ExecuteQuery(string query)
        {
            using (SQLiteConnection connection = CreateNewConnection())
            {
                using (SQLiteCommand command = connection.CreateCommand())
                {
                    command.CommandText = query;

                    command.ExecuteNonQuery();
                }
            }
        }

        public bool BackupDatabase(string sourceName, string destName)
        {
            File.Copy(sourceName, destName, overwrite: true);

            return File.Exists(destName);
        }

        public HashSet<long> GetMessagesNumbers(DiscussionGroup discussionGroup)
        {
            using (SQLiteConnection connection = CreateNewConnection())
            {
                using (SQLiteCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT number FROM group_message WHERE discussion_group = @id";

                    cmd.Parameters.AddWithValue("@id", discussionGroup.Id);

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        var retval = new HashSet<long>();

                        while (reader.Read())
                        {
                            retval.Add(reader.GetInt64(0));
                        }

                        reader.Close();

                        return retval;
                    }
                }
            }
        }

        public void UpdateAttachmentsLinks(string newPath, DiscussionGroup discussionGroup)
        {
            // Transaction temporarily turned off because of possible nesting.
            // SQLiteTransaction transaction = TransactionBegin();

            List<object[]> rawAttachments =
                ExecuteQueryAndGetValuesList(
                    String.Format(
                        "SELECT attachment.* FROM group_message_attachment INNER JOIN group_message ON group_message.id = group_message_attachment.group_message INNER JOIN attachment ON attachment.id = group_message_attachment.attachment WHERE group_message.discussion_group = {0}",
                        discussionGroup.Id));

            foreach (object[] rawAttachment in rawAttachments)
            {
                ExecuteQuery(String.Format("UPDATE attachment SET filename = `{0}` WHERE id = {1}",
                    newPath + Path.GetFileName((String) rawAttachment[1]), (long) rawAttachment[0]));
            }

            // transaction.Commit();
        }

        private long IndexOfByteSubstring(byte[] input, byte[] pattern)
        {
            int startIndex = 0;

            while ((startIndex = Array.IndexOf(input, pattern[0], startIndex)) >= 0)
            {
                for (int i = 0; i < pattern.Length; i++)
                {
                    if (startIndex + i >= input.Length || pattern[i] != input[startIndex + i]) break;

                    if (i == pattern.Length - 1) return startIndex;
                }

                startIndex += pattern.Length;

                if (startIndex >= input.Length) return -1;
            }

            return -1;
        }

        private void CreateDatabase()
        {
            using (StreamReader reader = File.OpenText(PathResolver.GetSchemaScriptPath()))
            {
                using (SQLiteConnection connection = CreateNewConnection())
                {
                    using (SQLiteCommand cmd = connection.CreateCommand())
                    {
                        cmd.CommandText = reader.ReadToEnd();

                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public long GetLastInsertRowId(SQLiteConnection connection)
        {
            //var id = (long) (new SQLiteCommand("SELECT last_insert_rowid();", connection).ExecuteScalar());

            long id = -1;

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT last_insert_rowid();";

                object idObject = cmd.ExecuteScalar();

                if (idObject == null)
                {
                    var t = "";
                }
                if (idObject != null) id = (long) idObject;
            }
#if DEBUG
            //File.AppendAllText("c:/temp/debug_import.log", string.Format("last message insert id: {0} {1}", id, Environment.NewLine));
#endif
            //Thread.Sleep(1000);
            return id;
        }

        public void FixUnreadCount()
        {
            StringBuilder queryBuilder = new StringBuilder();

            using (SQLiteConnection connection = CreateNewConnection())
            {
                using (SQLiteCommand cmd1 = connection.CreateCommand())
                {
                    cmd1.CommandText = "SELECT id FROM discussion_group";

                    using (SQLiteDataReader rdr1 = cmd1.ExecuteReader())
                    {
                        while (rdr1.Read())
                        {
                            var groupId = rdr1.GetValue(0);

                            using (SQLiteCommand cmd2 = _connection.CreateCommand())
                            {
                                cmd2.CommandText =
                                    string.Format(
                                        "SELECT COUNT(id) FROM group_message WHERE unread=1 AND discussion_group = {0}",
                                        groupId);

                                var unreadCount = cmd2.ExecuteScalar();

                                queryBuilder.AppendLine(
                                    String.Format("UPDATE discussion_group SET unread_count = {0} WHERE id = {1};",
                                        unreadCount, groupId));
                            }
                        }

                        rdr1.Close();
                    }
                }
            }

            using (SQLiteTransaction transaction = _connection.BeginTransaction())
            {
                using (var cmd = new SQLiteCommand(queryBuilder.ToString(), _connection))
                {
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }
    }
}

