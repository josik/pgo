﻿using System.Collections.Generic;
using System.Linq;
using PGOffline.Model;

namespace PGOffline
{
    class LocalCache : Singleton<LocalCache>
    {
        private Dictionary<long, Person> _personsById = new Dictionary<long, Person>();

        private Dictionary<string, Person> _personsByName = new Dictionary<string, Person>();

        private Dictionary<string, Person> _personsByEmail = new Dictionary<string, Person>();

        private readonly object _personsLock = new object();

        public void Add(Person person)
        {
            lock (_personsLock)
            {
                if (!_personsById.ContainsKey(person.Id))
                {
                    _personsById.Add(person.Id, person);
                }

                if (!_personsByName.ContainsKey(person.Name))
                {
                    _personsByName.Add(person.Name, person);
                }

                if (!_personsByEmail.ContainsKey(person.Email))
                {
                    _personsByEmail.Add(person.Email, person);
                }
            }
        }

        public Person GetPersonById(long id)
        {
            return !_personsById.ContainsKey(id) ? null : _personsById[id];
        }

        public Person GetPersonByName(string name)
        {
            return !_personsByName.ContainsKey(name) ? null : _personsByName[name];
        }

        public Person GetPersonByEmail(string email)
        {
            return !_personsByEmail.ContainsKey(email) ? null : _personsByEmail[email];
        }

        public void DropPersonCache()
        {
            lock (_personsLock)
            {
                _personsByEmail.Clear();
                _personsById.Clear();
                _personsByName.Clear();
            }
        }

        private Dictionary<long, DiscussionGroup> _discussionGroups = new Dictionary<long, DiscussionGroup>();

        private readonly object _discussionGroupsLock = new object();

        public DiscussionGroup Add(DiscussionGroup group)
        {
            lock (_discussionGroupsLock)
            {
                if (!_discussionGroups.ContainsKey(group.Id)) _discussionGroups.Add(group.Id, group);
            }
            return group;
        }

        public bool Remove(long id)
        {
            lock (_discussionGroupsLock)
            {
                var test1  = _discussionGroups.FirstOrDefault(z => z.Value.Name == "7x10minilathe").Key;

                var test2 = _discussionGroups.FirstOrDefault(z => z.Value.Name == "7x10minilath").Key;

                if (!_discussionGroups.ContainsKey(id)) return false;

                _discussionGroups.Remove(id);

                return true;
            }
        }

        public bool Remove(string name)
        {
            lock (_discussionGroupsLock)
            {
                long id = _discussionGroups.FirstOrDefault(z => z.Value.Name == name).Key;

                if (!_discussionGroups.ContainsKey(id)) return false;

                _discussionGroups.Remove(id);

                return true;
            }
        }

        public DiscussionGroup GetDiscussionGroupById(long id)
        {
            return !_discussionGroups.ContainsKey(id) ? null : _discussionGroups[id];
        }

        public DiscussionGroup GetDiscussionGroupByName(string name)
        {
            long id = _discussionGroups.FirstOrDefault(z => z.Value.Name == name).Key;

            return !_discussionGroups.ContainsKey(id) ? null : _discussionGroups[id];
        }

        public void DropAllCaches()
        {
            lock (_personsLock)
            {
                _personsByEmail.Clear();
                _personsById.Clear();
                _personsByName.Clear();
            }

            lock (_discussionGroupsLock)
            {
                _discussionGroups.Clear();
            }
        }
    }

}
