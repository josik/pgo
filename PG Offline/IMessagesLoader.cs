﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Asfdfdfd.Api.Yahoo.Groups;
using PGOffline.Model;

namespace PGOffline
{
    public delegate void LoadingCompletedEventHandler();
    public delegate void LoadingProgressEventHandler(DiscussionGroup group, List<YahooGroupMessage> messages);

    interface IMessagesLoader
    {
        event LoadingCompletedEventHandler LoadingCompleted;
        event LoadingProgressEventHandler LoadingProgress;

        bool IsBusy { get; }

        void LoadAsync();
        void LoadCancelAsync();
    }
}
