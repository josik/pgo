﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;
using PGOffline.Presenter;

namespace PGOffline
{
    public delegate void ImportCompletedEventHandler();

    public delegate void UpdateProgressEventHandler();

    interface IImportManager
    {
        event ImportCompletedEventHandler ImportComplete;

        event UpdateProgressEventHandler UpdateProgressRequest;

        BackgroundWorker ImportDataWorker { get; set; }

        MediaElement StillWorkingSpinner { get; set; }

        int ProgressPercentage { get; set; }

        //void ImportDataAsync();

        void ImportData();
        
        //void ImportDataProgress(int progressPercentage);

        void ImportDataComplete();

        //void ImportDataCancelAsync();

        //List<ImportUnit> GetImportUnits(string databasePath);
    }
}
