﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    class TrialExpiredException : TrialException
    {
        public TrialExpiredException()
            : base("Trial has expired.")
        {
        }
    }
}
