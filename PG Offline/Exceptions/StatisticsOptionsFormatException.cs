﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    public class StatisticsOptionsFormatException : Exception
    {
        public StatisticsOptionsFormatException(string message)
            : base(message)
        {
        }
    }
}
