﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    class TrialUnableToRefreshException : TrialException
    {
        public TrialUnableToRefreshException(string message)
            : base("Unable to refresh trial period. '" + message + "'")
        {
        }
    }
}
