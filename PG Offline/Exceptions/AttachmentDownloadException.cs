﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    class AttachmentDownloadException : Exception
    {
        public AttachmentDownloadException(string message)
            : base(message)
        {
        }
    }
}
