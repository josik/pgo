﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    class DatabaseNoAccessException : DatabaseException
    {
        public DatabaseNoAccessException(string path, string message)
            : base("Could not open or create database on path '" + path + "'. Please check that you have enough space on system disk and program has access to this path.\r\n" + message)
        {
        }
    }
}
