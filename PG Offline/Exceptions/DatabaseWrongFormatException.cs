﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    public class DatabaseWrongFormatException : DatabaseException
    {
        public DatabaseWrongFormatException(string path, string message)
            : base("Wrong database format on path '" + path + "'. Backup this file and try to delete it or send to developers.\r\n" + message)
        {
        }
    }
}
