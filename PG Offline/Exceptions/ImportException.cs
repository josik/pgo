﻿using System;

namespace PGOffline.Exceptions
{
    public class ImportException : Exception
    {
        public ImportException(string message)
            : base(message)
        {
        }
    }
}