﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    public class DatabaseException : Exception
    {
        public DatabaseException(string message)
            : base(message)
        {
        }
    }
}
