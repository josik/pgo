﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    class TrialDataCorruptedException : TrialException
    {
        public TrialDataCorruptedException()
            : base("Unable to read trial data.")
        {
        }
    }
}
