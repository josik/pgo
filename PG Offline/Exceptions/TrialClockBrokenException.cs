﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    class TrialClockBrokenException : TrialException
    {
        public TrialClockBrokenException()
            : base("System clock broken.")
        {
        }
    }
}
