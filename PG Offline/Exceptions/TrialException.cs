﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    class TrialException : Exception
    {
        public TrialException(string message)
            : base(message)
        {
        }
    }
}
