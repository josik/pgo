﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    public class SchemaMigrationException : DatabaseException
    {
        public SchemaMigrationException(string path, string message)
            : base("Can't update database.\r\n" + message)
        {
        }
    }
}
