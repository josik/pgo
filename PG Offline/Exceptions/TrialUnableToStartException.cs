﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    class TrialUnableToStartException : TrialException
    {
        public TrialUnableToStartException(String message)
            : base("Unable to start trial period. '" + message + "'")
        {
        }
    }
}
