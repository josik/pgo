﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGOffline.Exceptions
{
    class DatabaseWrongVersionException : DatabaseException
    {
        public DatabaseWrongVersionException(string version)
            : base(String.Format("Wrong database version. Expected number but received '{0}'. Please contact developers.", version).ToString())
        {
        }
    }
}
