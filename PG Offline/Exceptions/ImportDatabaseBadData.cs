﻿namespace PGOffline.Exceptions
{
    public class ImportDatabaseBadData : ImportException
    {
        public ImportDatabaseBadData()
            : base("Import has failed. Import encountered invalid data.")
        {
        }
    }
}
