﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;

namespace PGOffline
{
    public sealed class ErrorLog
    {
        #region Properties

        public string LogPath { get; private set; }

        public string LogFile { get; private set; }

        #endregion

        #region Constructors

        public ErrorLog()
        {
            LogPath = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), System.Windows.Forms.Application.ProductName), "Errors");
            if (!Directory.Exists(LogPath))
                Directory.CreateDirectory(LogPath);
            LogFile = string.Format(@"{0}\{1}.txt", LogPath, DateTime.Now.ToString("yyyy-MM-dd_HH.mm.ss"));
        }

        public ErrorLog(string logPath)
        {
            LogPath = logPath;
            if (!Directory.Exists(LogPath))
                Directory.CreateDirectory(LogPath);
            LogFile = string.Format(@"{0}\{1}.txt", LogPath, DateTime.Now.ToString("yyyy-MM-dd_HH.mm.ss"));
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Logs exception information to the assigned log file.
        /// </summary>
        /// <param name="exception">Exception to log.</param>
        public string LogError(Exception exception)
        {
            Assembly caller = Assembly.GetEntryAssembly();
            Process thisProcess = Process.GetCurrentProcess();

            //string logFile = string.Format("{0}.txt", DateTime.Now.ToString("yyyy-MM-dd_HH.mm.ss"));

            using (var sw = new StreamWriter(LogFile))
            {
                sw.WriteLine("==============================================================================");
                sw.WriteLine(caller.FullName);
                sw.WriteLine("------------------------------------------------------------------------------");
                sw.WriteLine("Application Information");
                sw.WriteLine("------------------------------------------------------------------------------");
                sw.WriteLine("Program      : " + caller.Location);
                sw.WriteLine("Time         : " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                sw.WriteLine("User         : " + Environment.UserName);
                sw.WriteLine("Computer     : " + Environment.MachineName);
                sw.WriteLine("OS           : " + Environment.OSVersion);
                sw.WriteLine("Culture      : " + CultureInfo.CurrentCulture.Name);
                sw.WriteLine("Processors   : " + Environment.ProcessorCount);
                sw.WriteLine("Working Set  : " + Environment.WorkingSet);
                sw.WriteLine("Framework    : " + Environment.Version);
                sw.WriteLine("Run Time     : " + (DateTime.Now - Process.GetCurrentProcess().StartTime));
                sw.WriteLine("------------------------------------------------------------------------------");
                sw.WriteLine("Exception Information");
                sw.WriteLine("------------------------------------------------------------------------------");
                sw.WriteLine("Source       : " + exception.Source.Trim());
                sw.WriteLine("Method       : " + exception.TargetSite.Name);
                sw.WriteLine("Type         : " + exception.GetType());
                sw.WriteLine("Error        : " + GetExceptionStack(exception));
                sw.WriteLine("Stack Trace  : " + exception.StackTrace.Trim());
                sw.WriteLine("------------------------------------------------------------------------------");
                sw.WriteLine("Loaded Modules");
                sw.WriteLine("------------------------------------------------------------------------------");
                foreach (ProcessModule module in thisProcess.Modules)
                {
                    try
                    {
                        sw.WriteLine("{0} | {1} | {2}", module.FileName, module.FileVersionInfo.FileVersion, module.ModuleMemorySize);
                    }
                    catch (FileNotFoundException)
                    {
                        sw.WriteLine("File Not Found: {0}", module);
                    }
                    catch (Exception)
                    {

                    }
                }
                sw.WriteLine("------------------------------------------------------------------------------");
                sw.WriteLine(LogFile);
                sw.WriteLine("==============================================================================");
            }

            return LogFile;
        }

        #endregion

        #region Private Methods

        private string GetExceptionStack(Exception ex)
        {
            StringBuilder message = new StringBuilder();
            message.Append(ex.Message);
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                message.Append(Environment.NewLine);
                message.Append(ex.Message);
            }

            return message.ToString();
        }

        #endregion
    }
}
