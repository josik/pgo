﻿
namespace PGOffline
{
    class LoopMode
    {
        public const int LOOP_NONE = -1;

        public const int LOOP_TIMES = 0;

        public const int LOOP_ALL = 1;
    }
}
