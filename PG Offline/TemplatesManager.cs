﻿using System.Collections.Generic;

namespace PGOffline
{
    public class TemplatesManager
    {
        private static Dictionary<int, string> _templates = new Dictionary<int, string>();

        public static string GetTemplate(int templateNumber)
        {
            if (_templates.ContainsKey(templateNumber)) return _templates[templateNumber];

            var htmlTemplateFile = string.Format("{0}\\HtmlTemplates\\{1}.html", System.AppDomain.CurrentDomain.BaseDirectory, templateNumber);

            _templates[templateNumber] = System.IO.File.OpenText(htmlTemplateFile).ReadToEnd();

            return _templates[templateNumber];
        }
    }
}
