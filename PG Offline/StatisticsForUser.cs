﻿using System;

namespace PGOffline
{
    struct StatisticsForUser
    {
        public string userName;

        public int postCount;
        public double postPercentage;

        public int threadsStarted;
        public double threadsPercentage;

        public DateTime first;
        public DateTime last;
    }
}