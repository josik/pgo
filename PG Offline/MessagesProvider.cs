﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

using PGOffline.Model;
using System.Data.SQLite;

namespace PGOffline
{
    class MessagesProvider : IItemsProvider<Message>
    {
        private DiscussionGroup _group;

        public MessagesProvider(DiscussionGroup group)
        {
            _group = group;

            _group.MessageAdded += new DiscussionGroup.MessageAddedEventHandler(OnMessageAdded);
        }

        public event ItemsProviderItemAddedEventHandler<Message> ItemsProviderItemAdded;

        private void OnMessageAdded(Message message)
        {
            if (ItemsProviderItemAdded == null) return;

            ItemsProviderItemAdded(message);
        }

        public bool Contains(Message message)
        {
            return DatabaseManager.Instance.ExecuteQueryAndGetInt(String.Format("select count(*) from group_message where discussion_group = {0} and id = {1}", _group.Id, message.Id)) != 0;
        }

        public int IndexOf(Message message)
        {
            int messageIndex = DatabaseManager.Instance.ExecuteQueryAndGetInt(String.Format("select count(*) from group_message where discussion_group = {0} and number < {1}", _group.Id, message.Number));

            System.Diagnostics.Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Message(id={0}, number{1}). messageIndex={2})", message.Id, message.Number, messageIndex), "DEBUG");

            return messageIndex;
        }

        public int FetchCount()
        {
            SQLiteCommand command = new SQLiteCommand(String.Format("select count(*) from group_message where discussion_group = {0}", _group.Id), DatabaseManager.Instance.GetConnection());
            SQLiteDataReader dataReader = command.ExecuteReader();
            dataReader.Read();
            int itemsCount = dataReader.GetInt32(0);
            dataReader.Close();
            return itemsCount;
        }

        public IList<Message> FetchRange(int startIndex, int count)
        {
            SQLiteCommand command = new SQLiteCommand(String.Format("select * from group_message where discussion_group = {0} LIMIT {1} OFFSET {2}", _group.Id, count, startIndex), DatabaseManager.Instance.GetConnection());
            SQLiteDataReader dataReader = command.ExecuteReader();

            List<Message> messages = new List<Message>();

            while (dataReader.Read())
            {
                object[] values = new object[11];
                dataReader.GetValues(values);
                Message message = new Message(values);
                messages.Add(message);
            }

            dataReader.Close();

            return messages;
        }
    }
}
