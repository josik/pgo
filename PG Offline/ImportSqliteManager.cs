﻿using PGOffline.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;

namespace PGOffline
{
    public class ImportSqliteManager : ImportManager<OleDbConnection>
    {
        private string sqlite_db_v18_group_message = "id, number, date, subject, content, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic,next_in_topic, prev_in_time, next_in_time";
        
        public ImportSqliteManager(string dbLocation)
            : this(dbLocation, new List<ImportUnit>(), new ImportSettings())
        {
        }

        public ImportSqliteManager(string dbLocation, List<ImportUnit> unitsToImport, ImportSettings importSettings)
        {
            DbFilePath = dbLocation;

            DbConnectionString = string.Format(@"Data source={0}", dbLocation);

            ImportConnection = DatabaseManager.Instance.GetConnection();

            UnitsToImport = unitsToImport;

            CurrentImportSettings = importSettings;

            GroupsUnits = new Dictionary<DiscussionGroup, ImportUnit>();
        }

        protected override IDbConnection GetNewDbConnection()
        {
            return new SQLiteConnection { ConnectionString = DbConnectionString };
        }

        protected override void ImportGroups(IEnumerable<ImportUnit> unitsToImport)
        {
            UnitsToImportCount = GetImportUnitsCount(unitsToImport);

            if (UnitsToImportCount < 0) return;

            int unitsCompleted = 0;

            //using (SQLiteConnection importConnection = (SQLiteConnection)GetNewDbConnection())
            using (SQLiteConnection importConnection = new SQLiteConnection { ConnectionString = DbConnectionString })
            {
                if (importConnection.State != ConnectionState.Open)
                {
                    importConnection.Open();
                }

                var importUnits = unitsToImport as ImportUnit[] ?? unitsToImport.ToArray();

                foreach (ImportUnit importUnit in importUnits)
                {
                    ReportProgressMessage(importUnit.GroupName);

                    DiscussionGroup @group;

                    using (SQLiteCommand cmd = importConnection.CreateCommand())
                    {
                        cmd.CommandText = "SELECT id, name, charset, last_members_update FROM discussion_group WHERE id = :groupId";

                        cmd.Parameters.AddWithValue(":groupId", importUnit.GroupId);
                    
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            rdr.Read();

                            if (!rdr.HasRows) continue;
                        
                            @group = DiscussionGroup.GetByName(Convert.ToString(rdr["name"]));

                            if (@group == null)
                            {
                                //group = new DiscussionGroup(groupRawData);

                                @group = new DiscussionGroup(Convert.ToString(rdr["name"]))
                                {
                                    Charset = Convert.ToString(rdr["charset"]),
                                    LastMembersUpdate = rdr["last_members_update"] == DBNull.Value
                                        ? default(DateTime)
                                        : Convert.ToDateTime(rdr["last_members_update"])
                                };

                                @group.Insert();
                            }
                        }
                    }


                    using (SQLiteCommand cmd1 = importConnection.CreateCommand())
                    {
                        switch (CurrentImportSettings.ImportType)
                        {
                            case ImportType.AllMessages:
                                cmd1.CommandText =
                                    string.Format(
                                        "SELECT {0} FROM group_message WHERE discussion_group = :groupId AND number BETWEEN :startNumber AND :stopNumber",
                                        sqlite_db_v18_group_message);
                                break;

                            case ImportType.OnlyFavorites:
                                cmd1.CommandText =
                                    string.Format(
                                        "SELECT {0} FROM group_message WHERE discussion_group = :groupId AND number BETWEEN :startNumber AND :stopNumber AND favorite = 1",
                                        sqlite_db_v18_group_message);
                                break;

                            case ImportType.OnlyFavoritesNumbers:
                                cmd1.CommandText =
                                    "SELECT number FROM group_message WHERE discussion_group = :groupId AND number BETWEEN :startNumber AND :stopNumber AND favorite = 1";
                                break;
                        }

                        cmd1.Parameters.AddWithValue(":groupId", importUnit.GroupId);

                        cmd1.Parameters.AddWithValue(":startNumber", importUnit.StartImportFromNumber);

                        cmd1.Parameters.AddWithValue(":stopNumber", importUnit.EndImportAtNumber);


                        using (SQLiteDataReader rdr1 = cmd1.ExecuteReader())
                        {
                            bool lastRow = false;

                            while (!lastRow)
                            {

                                using (SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin())
                                {
                                    do
                                    {
                                        if (!rdr1.Read())
                                        {
                                            lastRow = true;

                                            break;
                                        }

                                        if (CurrentImportSettings.ImportType != ImportType.OnlyFavoritesNumbers)
                                        {
                                            object[] messageRawData = new object[17];

                                            rdr1.GetValues(messageRawData);

                                            long oldPersonId = (long) messageRawData[7];

                                            object[] personRawData;

                                            using (SQLiteCommand cmd2 = importConnection.CreateCommand())
                                            {
                                                cmd2.CommandText =
                                                    "SELECT name, joined, email, is_member FROM person WHERE id = :personId";

                                                cmd2.Parameters.AddWithValue(":personId", oldPersonId);

                                                personRawData = new object[6];

                                                using (SQLiteDataReader rdr2 = cmd2.ExecuteReader())
                                                {
                                                    if (!rdr2.Read())
                                                    {
                                                        continue;
                                                    }

                                                    rdr2.GetValues(personRawData);
                                                }
                                            }

                                            Person person = null;

                                            try
                                            {
                                                var personName = Convert.ToString(personRawData[0]);

                                                var personJoinedDate = personRawData[1] == DBNull.Value
                                                    ? default(DateTime)
                                                    : Convert.ToDateTime(personRawData[1]);

                                                var personEmail = Convert.ToString(personRawData[2]);

                                                var personIsMember = Convert.ToBoolean(personRawData[3]);

                                                if (personName == string.Empty && personEmail == string.Empty)
                                                {
                                                    personName = personEmail = "(unknown)";
                                                }

                                                person = new Person(personName, personJoinedDate, personEmail,
                                                    personIsMember, @group.Id);
                                            }
                                            catch (InvalidCastException ex)
                                            {
                                                throw;
                                            }

                                            try
                                            {
                                                //File.AppendAllText("c:/temp/debug_rawmessage.log", string.Format("{0} name:{1} email:{2} {3}", ((long) messageRawData[1]), person.Name, person.Email, Environment.NewLine));

                                                var m_author = person.Name;
                                                var m_email = person.Email;
                                                var m_number = (long) messageRawData[1];
                                                var m_date = messageRawData[2] == DBNull.Value
                                                    ? default (DateTime)
                                                    : Convert.ToDateTime(messageRawData[2]);
                                                var m_subject = (string) messageRawData[3];
                                                var m_content = (byte[]) messageRawData[4];
                                                var m_favorite = CurrentImportSettings.ImportFavoriteStatus != false &&
                                                                 (bool) messageRawData[5];
                                                var m_unread = (bool) messageRawData[6];
                                                var m_person = (long) messageRawData[7];
                                                var m_charset = (messageRawData[8] == DBNull.Value
                                                    ? string.Empty
                                                    : (string) messageRawData[8]);
                                                var m_discussion_group = (long) messageRawData[9];
                                                var m_topic_id = (long) messageRawData[10];
                                                var m_parent_number = (long) messageRawData[11];
                                                var m_thread_level = (long) messageRawData[12];
                                                var m_prev_in_topic = (long) messageRawData[13];
                                                var m_next_in_topic = (long) messageRawData[14];
                                                var m_prev_in_time = (long) messageRawData[15];
                                                var m_next_in_time = (long) messageRawData[16];

                                                //File.AppendAllText("c:/temp/debug_rawmessage.log", string.Format("Adding message{0}", Environment.NewLine));

                                                // AddMessage(string author, string email, long number, DateTime? date, string subject, byte[] content, bool favorite, bool unread, long topicId, long parentId, long threadLevel, long prevInTopic, long nextInTopic, long prevInTime, long nextInTime, bool callMessageEdit = true, List<YahooGroupAttachment> attachments = null, string charset = null
                                                @group.AddMessage(m_author, m_email, m_number, m_date, m_subject,
                                                    m_content,
                                                    m_favorite, m_unread, m_topic_id, m_parent_number, m_thread_level,
                                                    m_prev_in_topic,
                                                    m_next_in_topic, m_prev_in_time, m_next_in_time,
                                                    callMessageEdit: false,
                                                    attachments: null,
                                                    charset: m_charset);

                                                //File.AppendAllText("c:/temp/debug_rawmessage.log", string.Format("Message added.{0}", Environment.NewLine));
                                            }
                                            catch (InvalidCastException ex)
                                            {
                                                throw;
                                                //group.AddMessage(person.Name, person.Email, (long)messageRawData[1], null, (string)messageRawData[3], (byte[])messageRawData[4], (bool)messageRawData[5], (bool)messageRawData[6], (long)messageRawData[7], (long)messageRawData[8], (long)messageRawData[9], (long)messageRawData[10], (long)messageRawData[11], (long)messageRawData[12], (long)messageRawData[13], false);
                                            }
                                            catch (Exception ex)
                                            {
                                                throw;
                                            }
                                        }
                                        else if (CurrentImportSettings.ImportFavoriteStatus)
                                        {
                                            object[] messageRawData = new object[1];

                                            rdr1.GetValues(messageRawData);

                                            using (
                                                SQLiteCommand cmd6 =
                                                    DatabaseManager.Instance.CreateCommand(
                                                        "UPDATE group_message SET favorite = 1 WHERE number = :number AND discussion_group = :group")
                                                )
                                            {
                                                cmd6.Parameters.AddWithValue(":number", messageRawData[0]);

                                                cmd6.Parameters.AddWithValue(":group", @group.Id);

                                                cmd6.ExecuteNonQuery();
                                            }
                                        }

                                        if (ImportDataWorker.CancellationPending)
                                        {
                                            transaction.Commit();

                                            return;
                                        }

                                    } while (++unitsCompleted%100 != 0);

                                    // } while (UnitsProceed % 5000 != 0 && UnitsProceed > 0);

#if DEBUG
                                    //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("unitsCompleted {0}", unitsCompleted));
#endif

                                    ProgressPercentage =
                                        ((int) (((double) unitsCompleted/(double) UnitsToImportCount)*100.0f));

                                    ReportProgressPercentage(ProgressPercentage);

                                    transaction.Commit();

                                    GC.Collect();
                                }
                            }
                        }
                    }

                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Group {0} imported.", importUnit.GroupName));
                }
            }
        }

        public override List<ImportUnit> GetImportUnits(string dbLocation)
        {
            //SQLiteConnection importConnection = (SQLiteConnection) GetNewDbConnection();
            SQLiteConnection importConnection = new SQLiteConnection {ConnectionString = DbConnectionString};

            if (importConnection.State != ConnectionState.Open)
            {
                importConnection.Open();
            }

            SQLiteCommand cmd = importConnection.CreateCommand();

            cmd.CommandText = "SELECT id, name, charset, last_members_update FROM discussion_group;";

            SQLiteDataReader reader = cmd.ExecuteReader();

            List<ImportUnit> retval = new List<ImportUnit>();

            while (reader.Read())
            {
                ImportUnit unit = new ImportUnit
                {
                    GroupId = Convert.ToInt64(reader["id"]),
                    GroupName = Convert.ToString(reader["name"]),
                    CharSet = Convert.ToString(reader["charset"]),
                    LastMembersUpdate = reader["last_members_update"] == DBNull.Value ? default (DateTime) : Convert.ToDateTime(reader["last_members_update"]),
                    FirstMessageNumber = 0,
                    LastMessageNumber = 0,
                    StartImportFromNumber = 0,
                    EndImportAtNumber = 0
                };
                
                using (SQLiteCommand tmpcmd = importConnection.CreateCommand())
                {
                    tmpcmd.CommandText = "SELECT number FROM group_message WHERE discussion_group = :groupId ORDER BY number ASC LIMIT 1";

                    tmpcmd.Parameters.AddWithValue(":groupId", unit.GroupId);

                    var rdr = tmpcmd.ExecuteReader();

                    if (!rdr.HasRows) continue;

                    rdr.Read();
                    
                    unit.StartImportFromNumber = unit.FirstMessageNumber = Convert.ToInt32(rdr["number"]);
                }

                using (SQLiteCommand tmpcmd = importConnection.CreateCommand())
                {
                    tmpcmd.CommandText = "SELECT number FROM group_message WHERE discussion_group = :groupId ORDER BY number DESC LIMIT 1";

                    tmpcmd.Parameters.AddWithValue(":groupId", unit.GroupId);

                    var rdr = tmpcmd.ExecuteReader();

                    if (!rdr.HasRows) continue;

                    rdr.Read();

                    unit.EndImportAtNumber = unit.LastMessageNumber = Convert.ToInt32(rdr["number"]);
                }

                retval.Add(unit);
            }

            return retval;
        }

        protected override long GetImportUnitsCount(IEnumerable<ImportUnit> unitsToImport)
        {
            long importUnits = 0;

            // TODO: catch exception and send message to log window

            foreach (ImportUnit importUnit in unitsToImport)
            {
                if (CurrentImportSettings.ImportType == ImportType.OnlyFavoritesNumbers)
                {
                    // OnlyFavoritesNumbers
                    using (SQLiteConnection con = new SQLiteConnection { ConnectionString = DbConnectionString })
                    {
                        con.Open();

                        using (SQLiteCommand cmd = con.CreateCommand())
                        {
                            cmd.CommandText = string.Format("SELECT COUNT(*) FROM group_message WHERE discussion_group = {0} AND number >= {1} AND number <= {2} AND favorite = 1;", importUnit.GroupId, importUnit.StartImportFromNumber, importUnit.EndImportAtNumber);

                            importUnits += (long)cmd.ExecuteScalar();
                        }
                    }
                }
                else
                {
                    // messages
                    using (SQLiteConnection con = new SQLiteConnection { ConnectionString = DbConnectionString })
                    {
                        con.Open();

                        using (SQLiteCommand cmd = con.CreateCommand())
                        {
                            cmd.CommandText = String.Format("SELECT COUNT(*) FROM group_message WHERE discussion_group = {0} AND number >= {1} AND number <= {2};", importUnit.GroupId, importUnit.StartImportFromNumber, importUnit.EndImportAtNumber);

                            importUnits += (long)cmd.ExecuteScalar();
                        }
                    }
                }
            }

            return importUnits;
        }

        protected override void SynchronizeFavoritesStatus()
        {
            int unitsCompleted = 0;

            UnitsToImportCount = GetImportUnitsCount(UnitsToImport);

            if (UnitsToImportCount < 0) return;

            foreach (ImportUnit unit in UnitsToImport)
            {
                using (SQLiteConnection con = new SQLiteConnection {ConnectionString = DbConnectionString})
                {
                    con.Open();

                    using (SQLiteCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText =
                            string.Format(
                                "SELECT number, favorite FROM group_message WHERE discussion_group = {0} AND number >= {1} AND number <= {2} AND favorite = 1;",
                                unit.GroupId, unit.StartImportFromNumber, unit.EndImportAtNumber);

                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader != null && !reader.HasRows) continue;

                            while (reader.Read())
                            {
                                Message message = Message.GetMessageByNumber(reader.GetInt32(0));

                                if (message != null)
                                {
                                    message.Favorite = reader.GetBoolean(1);
                                }

                                unitsCompleted++;

                                ProgressPercentage =
                                    ((int) (((double) unitsCompleted/(double) UnitsToImportCount)*100.0f));

                                ReportProgressPercentage(ProgressPercentage);

                                if (ImportDataWorker.CancellationPending)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }

                GC.Collect();
            }
        }

    }
}
