﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using PGOffline.Exceptions;
using System.Reflection;
using System.Diagnostics;

namespace PGOffline
{
    class TrialData
    {
        private long _firstStartDate;
        private long _lastStartDate;

        public TrialData(long firstStartDate, long lastStartDate)
        {
            _firstStartDate = firstStartDate;
            _lastStartDate = lastStartDate;
        }

        #region Static
        public static TrialData Load()
        {
            if (!File.Exists(DataFilePath))
            {
                try
                {
                    CreateDataFile();
                }
                catch (Exception ex)
                {
                    throw new TrialUnableToStartException(ex.Message);
                }
                
                // On some system precision of the ticks is not enough to make test on
                // TrialClockBrokenException. We have to move time back.
                return new TrialData(DateTime.Now.Ticks - 16000, DateTime.Now.Ticks - 15000);
            }

            StreamReader sr = null;

            try
            {
                sr = new StreamReader(DataFilePath);

                return new TrialData (long.Parse(sr.ReadLine()), long.Parse(sr.ReadLine()));
            }
            catch (FormatException ex)
            {
                throw new TrialDataCorruptedException();
            }
            catch (Exception ex)
            {
                throw new TrialException("Unknown trial checking error. '" + ex.Message + "'");
            }
            finally
            {
                sr.Close();
            }
        }

        private static void CreateDataFile()
        {
            using (StreamWriter sw = new StreamWriter(DataFilePath, false))
            {
                long ticks = DateTime.Now.Ticks;

                sw.WriteLine(ticks);

                sw.WriteLine(ticks);
            }
        }

        public void Save()
        {
            using (StreamWriter sw = new StreamWriter(DataFilePath, false))
            {
                sw.WriteLine(FirstStartDate);

                sw.WriteLine(CurrentDate);
            }
        }

        private static long TrialDuration
        {
            get
            {
                DateTime start = new DateTime();

                DateTime end = start.AddDays(60);

                return end.Ticks - start.Ticks;
            }
        }

        private static string DataFilePath
        {
            get
            {
                return Path.Combine(PathResolver.GetTrialDataPath(), Assembly.GetExecutingAssembly().GetName().Version.ToString());
            }
        }
        #endregion

        public long FirstStartDate
        {
            get
            {
                return _firstStartDate;
            }
        }

        public long LastStartDate
        {
            get
            {
                return _lastStartDate;
            }
        }

        public long CurrentDate
        {
            get
            {
                return DateTime.Now.Ticks;
            }
        }

        public int DaysLeft
        {
            get
            {
                long currentTicks = CurrentDate;

                if (currentTicks <= LastStartDate || currentTicks <= FirstStartDate)
                {
                    throw new TrialClockBrokenException();
                }

                if (currentTicks - FirstStartDate >= TrialDuration)
                {
                    throw new TrialExpiredException();
                }

                try
                {
                    Save();
                }
                catch (Exception ex)
                {
                    throw new TrialUnableToRefreshException(ex.Message);
                }
                
                return new TimeSpan(TrialDuration - currentTicks + FirstStartDate).Days;
            }
        }
    }
}
