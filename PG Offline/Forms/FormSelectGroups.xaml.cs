﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

using PGOffline.View;


namespace PGOffline
{
    public partial class FormSelectGroups : Window, IFormSelectGroupsView
    {
        public event AddLocalDiscussionGroupRequestEventHandler AddLocalDiscussionGroupRequest;

        public event RemoveLocalDiscussionGroupRequestEventHandler RemoveLocalDiscussionGroupRequest;

        public event SaveGroupsListChangesRequestEventHandler SaveGroupsListChangesRequest;
        
        public FormSelectGroups()
        {
            InitializeComponent();
        }

        public void SetLocalGroups(SortedList<string, string> localGroups)
        {
            listViewLocalGroups.ItemsSource = localGroups.Values;
        }

        public void SetRemoteGroups(SortedList<string, string> remoteGroups)
        {
            listViewRemoteGroups.ItemsSource = remoteGroups.Values;
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            AddLocalDiscussionGroupRequest((string)listViewRemoteGroups.SelectedItem);

            listViewLocalGroups.Items.Refresh();

            listViewRemoteGroups.Items.Refresh();
        }

        private void buttonRemove_Click(object sender, RoutedEventArgs e)
        {
            RemoveLocalDiscussionGroupRequest((string)listViewLocalGroups.SelectedItem);

            listViewLocalGroups.Items.Refresh();

            listViewRemoteGroups.Items.Refresh();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            SaveGroupsListChangesRequest();

            Close();
        }
    }
}
