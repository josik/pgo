﻿using System;
using System.Windows;
using System.Windows.Input;
using PGOffline.View;

namespace PGOffline.Forms
{
   
    /// <summary>
    /// Interaction logic for FormRegistration.xaml
    /// </summary>
    public partial class FormRegistration : Window, IFormRegistrationView
    {
        
        public FormRegistration()
        {
            InitializeComponent();
        }


        public void CommandBinding_PurchaseProduct(object sender, ExecutedRoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void CommandBinding_CloseWindow(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }



        // Steps to create keys that can used on both Linux and Windows
        // http://stackoverflow.com/questions/3260319/interoperability-between-rsacryptoserviceprovider-and-openssl
        //openssl genrsa -out private.pem 1024
        //openssl rsa -in private.pem -out public.pem -pubout
        //openssl req -nodes -x509 -days 3650 -subj '/CN=www.example.com/emailAddress=info@example.com' -new -key private.pem -out certificate.crt
        //openssl pkcs12 -export -out certificate.pfx -inkey private.pem -in certificate.crt

        //private string publicKey = "AAAAB3NzaC1yc2EAAAABJQAAAgEAoYH23BZZI5Q1rgihzR4S4BjlSc0dKwUjgTcbcZi1LfU1W1lxtSJk0nD73xNPK0NSvmkddU0p+OtHRELmv6uvJkBDD12+Azeg78cHQsAu7IqY0egTWovoEQHAh/Tgmku8Pr5+P/dySUUmRVZuAi0ZRlbs/egNG2A4Z6dSWGMiqoW32YVOdo7CSu3IlLkS9ge/gFvRfkcXYM1EwisOKrXx5qcjPTfppm9nhPVuSbO51Htnw5Y7R2EK5g9841Zc9ELGzrUBu1z0K48CHAtUfsYNiXF0xpD0kT+wHMGknxXiIlxc+z+wD9+2enE6oz60cX2FZplWYDxTBchr5jURaAuBkuY7VZEstydPNg0cQbMQKCa13zWKzdcTxRJOPeC+6wNrg95V32+jE8UhDB46G7b32rh8J6+kiUFuRnHmuGW5Lhd58sDMphnHPS7SzivB3QHPgD70B027eFdgkhJ0u7wTh2dqQXhCxBDbVn9bop6pvP7arkFVfA9zh0eKm+wwHXhdKCYVNVEn4dPre2sPRObp54Am2YOiEpiCJVFVmpZlVOKpisz0hCvu1adMwafCvgi+6jtAAZg5gSWdBmzwXgLFycfC4B6n0VXfHLeHlP5tPFQnCtLD75QP7rogngJsUbEx1TjP79nIzcp77P0lFOoKEW9LyRp6JRkktg7lDdeh0NM=";
        //private static string _privateKey;
        //private static string _publicKey;
        //private static UnicodeEncoding _encoder = new UnicodeEncoding();

        //public void CommandBinding_RegisterProduct2(object sender, ExecutedRoutedEventArgs e)
        //{
        //    // to load the PFX key and get the keys formatted for use in RSACryptoServiceProvider
        //    //var cert = new X509Certificate2("certificate.pfx", "", X509KeyStorageFlags.Exportable);
        //    //RSACryptoServiceProvider rsaProvider = cert.PrivateKey as RSACryptoServiceProvider;
        //    //var privKey = rsaProvider.ToXmlString(true);
        //    //var pubKey = rsaProvider.ToXmlString(false);



        //    string secret = "{Expiration:5/9/2016}";

        //    RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

        //    // load public key
        //    rsa.FromXmlString("<RSAKeyValue><Modulus>oeC8mvKHILHM03BvEwLGJp53SL3Afs87UEN1TgJvXHzdLirwe4IFgsvqHVGewQbXbUr+7+ySvViM2TjFsjO2/aNZ9hs97TwHtPcr5YnfFZvBuUCM6lMbUmsv4c8sLKwg5bfyQTRViGDjCjF3wW+a4Iz7QX1epbvlzuGD1L16WsTe6YddC/vIyA3/dSGnvix81rrp3SimsOfIWC1ZGZjlEQVcR5W/UlYCw0Lp6iNmknPvl5J6u3HVNqlfaqxY8G0Nl1hWtb8xSVdARUcLEz3LzZka5C/khUFdr604p1YnAnlTCe6NZXed8BTh3YfcqGgv4dxVmd1uMWyrri5NMF/yJE8XoxJJVPFZkoGEZVNpTMd0G5VrkRp0WwpDb8sYZgek3MH/PxoVQllp1CXeJSVysj/LQ//++8oUZ+emhXwSDg1GYe0TvA37HkuQDgqxjfXIO95chtMv2pykhsdGjwLUBpEA3m7spZtkyglyg3ccNjBCrtuU7UHav4yb/RtCDUxXIwkNq/Ro6Qy5kmRpZqKFtsARiGF+w4N1IdkDyzWBRiZ8U4s3EMAFc3tnrm4j/Ck3Qyr2jwOld7vB7L/Ig7x2n/gZy6VXOd/PmgKuLBEq24+mgEp5dujzVLPuV5k4QGsVwaj9LTxC7syo0XwXXmtFJY7WAwrUvksKVEL1r36Q5qU=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");

        //    byte[] secretData = Encoding.UTF8.GetBytes(secret);

        //    // Encrypt it using the private key:
        //    byte[] encrypted = rsa.PrivareEncryption(secretData);

        //    // Decrypt it using the public key
        //    byte[] decrypted = rsa.PublicDecryption(encrypted);

        //    string decString = Encoding.UTF8.GetString(decrypted);  // And back to string




        //    var registrationKey = textBoxRegistrationKey.Text;

        //    var serverResponse = GetServerResponseKey(registrationKey);
            
        //    var isKeyValid = ValidateKey(registrationKey, serverResponse);

        //    //if (!isKeyValid) return;

        //    Options.Instance.Set("registration_key", registrationKey);
        //    Options.Instance.Set("registration_key_result", serverResponse);
        //    Options.Instance.SaveOptions();

        //    this.Close();
        //}
        
        //private bool ValidateKey(string registrationKey, string serverResponse)
        //{


        //    // decrypt serverResponse and if it is signed ok then we're ok
        //    var rsa = new RSACryptoServiceProvider();
        //    _privateKey = rsa.ToXmlString(true);
        //    _publicKey = rsa.ToXmlString(false);
            
        //    var dataArray = serverResponse.Split(new char[] { ',' });
        //    byte[] dataByte = new byte[dataArray.Length];
        //    for (int i = 0; i < dataArray.Length; i++)
        //    {
        //        dataByte[i] = Convert.ToByte(dataArray[i]);
        //    }

        //    rsa.FromXmlString(_privateKey);
        //    var decryptedByte = rsa.Decrypt(dataByte, false);
        //    var decodeValue = _encoder.GetString(decryptedByte);
            
        //    return true;
        //}

        //private string GetServerResponseKey(string registrationKey)
        //{
        //    // send users key to server and get the response
        //    var serverResponse = "dsfjhdskfhskjewfef";
        //    return serverResponse;
        //}

        //static public byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        //{
        //    try
        //    {
        //        byte[] encryptedData;
        //        //Create a new instance of RSACryptoServiceProvider.
        //        using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
        //        {

        //            //Import the RSA Key information. This only needs
        //            //toinclude the public key information.
        //            RSA.ImportParameters(RSAKeyInfo);

        //            //Encrypt the passed byte array and specify OAEP padding.  
        //            //OAEP padding is only available on Microsoft Windows XP or
        //            //later.  
        //            encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
        //        }
        //        return encryptedData;
        //    }
        //    //Catch and display a CryptographicException  
        //    //to the console.
        //    catch (CryptographicException e)
        //    {
        //        Console.WriteLine(e.Message);

        //        return null;
        //    }

        //}

        //static public byte[] RSADecrypt(byte[] DataToDecrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        //{
        //    try
        //    {
        //        byte[] decryptedData;
        //        //Create a new instance of RSACryptoServiceProvider.
        //        using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
        //        {
        //            //Import the RSA Key information. This needs
        //            //to include the private key information.
        //            RSA.ImportParameters(RSAKeyInfo);

        //            //Decrypt the passed byte array and specify OAEP padding.  
        //            //OAEP padding is only available on Microsoft Windows XP or
        //            //later.  
        //            decryptedData = RSA.Decrypt(DataToDecrypt, DoOAEPPadding);
        //        }
        //        return decryptedData;
        //    }
        //    //Catch and display a CryptographicException  
        //    //to the console.
        //    catch (CryptographicException e)
        //    {
        //        Console.WriteLine(e.ToString());

        //        return null;
        //    }

        //}

        
    }
    
}
