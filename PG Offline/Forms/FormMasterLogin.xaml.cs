﻿using PGOffline.View;
using System.Windows;
using System.Windows.Input;

namespace PGOffline
{
    public partial class FormMasterLogin : Window, IFormMasterLoginView
    {
        public event MasterCredentialsChangedRequestHandler MasterCredentialsChanged;
        public event CloseRequestEventHandler CloseRequest;

        public FormMasterLogin()
        {
            InitializeComponent();
        }

        public string Username
        {
            get
            {
                return textBoxUserName.Text;
            }

            set
            {
                textBoxUserName.Text = value;
            }
        }

        public string Password
        {
            get
            {
                return textBoxPassword.Password;
            }

            set
            {
                textBoxPassword.Password = value;
            }
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            if (CloseRequest != null)
                CloseRequest();

            Close();
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            MasterCredentialsChanged();
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (CloseRequest != null)
                CloseRequest();
        }

        private void textBoxUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter)
                return;

            MasterCredentialsChanged();
            Close();
        }

        private void textBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter)
                return;

            MasterCredentialsChanged();
            Close();
        }
    }
}
