﻿using PGOffline.View;
using System;
using System.Runtime.InteropServices;
using System.Windows;

namespace PGOffline
{
    public partial class FormDigest : Window, IFormDigestView
    {
        public event ApplyDisplayOptionsRequestEventHandler ApplyDisplayOptionsRequest;
        public event ToggleQuotesRequestEventHandler ToggleQuotesRequest;
        public event PrintDigestRequestEventHandler PrintDigestRequest;
        public event ShowNextMessagesRequestEventHandler ShowNextMessagesRequest;
        public event ShowPrevMessagesRequestEventHandler ShowPrevMessagesRequest;
        public event ForwardRequestEventHandler ForwardRequest;
        public event ChangeMarkAsReadStatusEventHandler ChangeMarkAsReadStatus;

        public FormDigest()
        {
            InitializeComponent();
        }

        public int MessagesPerPage
        {
            get
            {
                return int.Parse(integerUpDownMsgPerPage.Text);
            }
        }

        public bool? MarkMessagesAsRead
        {
            get
            {
                return checkBoxMarkMsgAsRead.IsChecked;
            }
        }

        public void DisplayDigest(string digest)
        {
            webBrowser.NavigateToString(digest);
        }

        private void buttonApply_Click(object sender, RoutedEventArgs e)
        {
            ApplyDisplayOptionsRequest();
        }

        private void buttonNext_Click(object sender, RoutedEventArgs e)
        {
            ShowNextMessagesRequest();
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void buttonForward_Click(object sender, RoutedEventArgs e)
        {
        }

        private void buttonPrevious_Click(object sender, RoutedEventArgs e)
        {
            ShowPrevMessagesRequest();
        }

        public void SetGroupName(string groupName)
        {
            Title = "Digest for " + groupName;
        }

        private void checkBoxMarkMsgAsRead_Checked(object sender, RoutedEventArgs e)
        {
            ChangeMarkAsReadStatus(checkBoxMarkMsgAsRead.IsChecked);
        }

        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
        internal interface IServiceProvider
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            object QueryService(ref Guid guidService, ref Guid riid);
        }

        static readonly Guid SID_SWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");

        private void buttonPrint_Click(object sender, RoutedEventArgs e)
        {
            IServiceProvider serviceProvider = null;

            if (webBrowser.Document != null)
            {
                serviceProvider = (IServiceProvider)webBrowser.Document;
            }

            Guid serviceGuid = SID_SWebBrowserApp;

            Guid iid = typeof(SHDocVw.IWebBrowser2).GUID;

            object NullValue = null;

            SHDocVw.IWebBrowser2 target = (SHDocVw.IWebBrowser2)serviceProvider.QueryService(ref serviceGuid, ref iid);

            target.ExecWB(SHDocVw.OLECMDID.OLECMDID_PRINTPREVIEW, SHDocVw.OLECMDEXECOPT.OLECMDEXECOPT_DODEFAULT, ref NullValue, ref NullValue);
        }
    }
}
