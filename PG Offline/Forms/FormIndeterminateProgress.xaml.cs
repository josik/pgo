﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PGOffline.Forms
{
    public partial class FormIndeterminateProgress : Window
    {
        public FormIndeterminateProgress()
        {
            InitializeComponent();
        }

        public void SetProgress(double progress)
        {
            progressBar.Value = progress;
        }

        public void SetTitle(string title)
        {
            this.Title = title;
        }
    }
}
