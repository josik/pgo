﻿using System.Diagnostics;
using System.Reflection;
using System.Windows;

namespace PGOffline
{
    public partial class FormAbout : Window
    {
        public FormAbout()
        {
            InitializeComponent();

            var assembly = Assembly.GetExecutingAssembly();
            var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            var version = fvi.ProductVersion;
            textBoxVersion.Text = string.Format("Version {0}", version);
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
