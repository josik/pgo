﻿using PGOffline.Model;
using System.Windows;

namespace PGOffline
{
    public partial class FormQuickStatistics : Window
    {
        public FormQuickStatistics(DiscussionGroup group)
        {
            InitializeComponent();

            listViewStats.ItemsSource = group.GetStatUnits();
        }
    }
}
