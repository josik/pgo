﻿using System.Linq;
using PGOffline.Model;
using PGOffline.View;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;

namespace PGOffline
{
    public partial class FormFindPerson : Window, IFormFindPerson
    {
        public event FindingUsersCompleteEventHandler FindingUsersComplete;

        public event FormFindPersonLoadCompleteEventHandler FormFindPersonLoadComplete;

        public FormFindPerson()
        {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            List<Person> selectedPersons = listViewUsers.SelectedItems.Cast<Person>().ToList();

            FindingUsersComplete(selectedPersons);

            this.Close();
        }

        public void SetPesons(List<Person> persons)
        {
            listViewUsers.ItemsSource = persons;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FormFindPersonLoadComplete();
        }
    }
}
