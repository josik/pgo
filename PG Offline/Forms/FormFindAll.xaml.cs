﻿using System.Linq;
using System.Windows.Input;
using PGOffline.Model;
using PGOffline.View;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;

namespace PGOffline
{
    public partial class FormFindAll : Window, IFormFindAll
    {
        public event FormFindAllLoadCompleteEventHandler FormFindAllLoadComplete;
        public event FindPersonRequestEventHandler FindPersonRequest;
        public event SaveSearchParamsRequestEventHandler SaveSearchParamsRequest;
        public event LoadSearchParamsRequestEventHandler LoadSearchParamsRequest;
        public event StartSearchRequestEventHandler StartSearchRequest;
        public event StopSearchRequestEventHandler StopSearchRequest;

        public void CommandBinding_Findall(object sender, ExecutedRoutedEventArgs executedRoutedEventArgs)
        {
            
        }

        public FormFindAll()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FormFindAllLoadComplete();
        }

        public string Persons
        {
            get
            {
                return textBoxFrom.Text;
            }

            set
            {
                textBoxFrom.Text = value;
            }
        }

        public string Subject
        {
            get
            {
                return textBoxSubject.Text;
            }

            set
            {
                textBoxSubject.Text = value;
            }
        }

        public string Message
        {
            get
            {
                return textBoxMessage.Text;
            }

            set
            {
                textBoxMessage.Text = value;
            }
        }

        public long? FromMessage
        {
            get
            { 
                long value;

                if (long.TryParse(textBoxFromID.Text, out value))
                {
                    return value;
                }

                return null;
            }

            set
            {
                textBoxFromID.Text = value == null ? "" : value.ToString();
            }
        }

        public long? ToMessage
        {
            get
            {
                long value;

                if (long.TryParse(textBoxToID.Text, out value))
                {
                    return value;
                }

                return null;
            }

            set
            {
                textBoxToID.Text = value == null ? "" : value.ToString();
            }
        }

        public void AddUsers(string usersNames)
        {
            if (textBoxFrom.Text == "")
            {
                textBoxFrom.Text += usersNames;
            }
            else
            {
                textBoxFrom.Text += ", " + usersNames;
            }
        }

        public List<DiscussionGroup> GetDiscussionGroups()
        {
            List<DiscussionGroup> selectedGroups = new List<DiscussionGroup>(listViewGroups.SelectedItems.Count);
            
            selectedGroups.AddRange(listViewGroups.SelectedItems.Cast<DiscussionGroup>());

            return selectedGroups;            
        }

        public void SetControlsReadyToSearch()
        {
            buttonStop.IsEnabled = false;

            buttonClose.IsEnabled = true;
            buttonFindAll.IsEnabled = true;
            buttonFindPerson.IsEnabled = true;
            buttonLoadParams.IsEnabled = true;
            buttonSaveParams.IsEnabled = true;
        }

        public void SetControlsSearchInProgress()
        {
            buttonStop.IsEnabled = true;

            buttonClose.IsEnabled = false;
            buttonFindAll.IsEnabled = false;
            buttonFindPerson.IsEnabled = false;
            buttonLoadParams.IsEnabled = false;
            buttonSaveParams.IsEnabled = false;
        }

        public void SetSelectedGroups(List<DiscussionGroup> groups)
        {
            groups.ForEach((group) => listViewGroups.SelectedItems.Add(group));
        }

        public void PopulateGroupsList(List<DiscussionGroup> groups)
        {
            listViewGroups.Items.Clear();

            listViewGroups.ItemsSource = groups;
        }

        private void buttonFindPerson_Click(object sender, RoutedEventArgs e)
        {
            FindPersonRequest();
        }

        private void buttonLoadParams_Click(object sender, RoutedEventArgs e)
        {
            LoadSearchParamsRequest();
        }

        private void buttonSaveParams_Click(object sender, RoutedEventArgs e)
        {
            SaveSearchParamsRequest();
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonFindAll_Click(object sender, RoutedEventArgs e)
        {
            StartSearchRequest();
        }

        private void buttonStop_Click(object sender, RoutedEventArgs e)
        {
            StopSearchRequest();
        }
    }
}
