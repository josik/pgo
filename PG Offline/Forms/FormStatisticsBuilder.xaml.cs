﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using PGOffline.View;
using PGOffline.Model;

namespace PGOffline
{
    public partial class FormStatisticsBuilder : Window, IFormStatisticsBuilderView
    {
        public event BuildStatisticsRequestHandler BuildStatisticsRequest;
        public event CancelStatisticsCalculationRequestEventHandler CancelStatisticsCalculationRequest;
        public event BaseStatisticsOnDateSelectedEventHandler BaseStatisticsOnDateSelected;
        public event BaseStatisticsOnNumberSelectedEventHandler BaseStatisticsOnNumberSelected;
        public event CompareByWeekSelectedEventHandler CompareByWeekSelected;
        public event CompareByMonthSelectedEventHandler CompareByMonthSelected;
        public event CompareByYearSelectedEventHandler CompareByYearSelected;
        public event CompareByCustomDateSelectedEventHandler CompareByCustomDateSelected;
        public event CompareByMessageNumberSelectedEventHandler CompareByMessageNumberSelected;
        public event GroupVsGroupSelectedEventHandler GroupVsGroupSelected;
        public event DisplayGroupsOnStatsRequestEventHandler DisplayGroupsRequest;
        public event SaveStatisticsOptionsRequestEventHandler SaveStatisticsOptionsRequest;
        public event SetStatisticsOptionsRequestEventHandler SetStatisticsOptionsRequest;

        public FormStatisticsBuilder()
        {
            InitializeComponent();
        }

        public StatisticsOptions StatisticsOptions
        {
            get
            {
                StatisticsType statisticsType = new StatisticsType();

                if ((bool)radioButtonDate.IsChecked) statisticsType = StatisticsType.StatisticsBasedOnDate;
                if ((bool)radioButtonMessageNumber.IsChecked) statisticsType = StatisticsType.StatisticsBasedOnMessagesNumbers;
                if ((bool)radioButtonMessageNumberCompare.IsChecked) statisticsType = StatisticsType.ComparisonByCustomNumbers;
                if ((bool)radioButtonMonth.IsChecked) statisticsType = StatisticsType.ComparisonByMonth;
                if ((bool)radioButtonWeek.IsChecked) statisticsType = StatisticsType.ComparisonByWeek;
                if ((bool)radioButtonYear.IsChecked) statisticsType = StatisticsType.ComparisonByYear;
                if ((bool)radioButtonCustom.IsChecked) statisticsType = StatisticsType.ComparisonByCustomTime;
                if ((bool)radioButtonGroupVsGroup.IsChecked) statisticsType = StatisticsType.GroupVsGroup;

                StatisticsOptions statisticsOptions = new StatisticsOptions();

                statisticsOptions.statisticsType = statisticsType;

                statisticsOptions.postings = (bool)checkBoxPostings.IsChecked;
                statisticsOptions.repliers = (bool)checkBoxRepliers.IsChecked;
                statisticsOptions.threadInitiators = (bool)checkBoxThreadInitiators.IsChecked;
                statisticsOptions.topTenSubjects = (bool)checkBoxTopTenSubjects.IsChecked;
                statisticsOptions.newMembers = (bool)checkBoxNewMembers.IsChecked;
                statisticsOptions.lurkers = (bool)checkBoxLurkers.IsChecked;

                bool parseFrom = long.TryParse(textBoxRangeFrom.Text, out statisticsOptions.messageNumberFirst);
                bool parseTo = long.TryParse(textBoxRangeTo.Text, out statisticsOptions.messageNumberLast);

                AdjustNumberInterval(parseFrom, parseTo, textBoxRangeFrom.Text, textBoxRangeTo.Text, out statisticsOptions.messageNumberFirst, out statisticsOptions.messageNumberLast);

                parseFrom = long.TryParse(textBoxCompareRange1From.Text, out statisticsOptions.compare1StartNumber);
                parseTo = long.TryParse(textBoxCompareRange1To.Text, out statisticsOptions.compare1EndNumber);

                AdjustNumberInterval(parseFrom, parseTo, textBoxCompareRange1From.Text, textBoxCompareRange1To.Text, out statisticsOptions.compare1StartNumber, out statisticsOptions.compare1EndNumber);

                parseFrom = long.TryParse(textBoxCompareRange2From.Text, out statisticsOptions.compare2StartNumber);
                parseTo = long.TryParse(textBoxCompareRange2To.Text, out statisticsOptions.compare2EndNumber);

                AdjustNumberInterval(parseFrom, parseTo, textBoxCompareRange2From.Text, textBoxCompareRange2To.Text, out statisticsOptions.compare2StartNumber, out statisticsOptions.compare2EndNumber);

                if (datePickerDateFrom.SelectedDate != null)
                    statisticsOptions.dateTimeStart = (DateTime)datePickerDateFrom.SelectedDate;
                if (datePickerDateTo.SelectedDate != null)
                    statisticsOptions.dateTimeEnd = (DateTime)datePickerDateTo.SelectedDate;

                if (datePickerComparePeriod1From.SelectedDate != null)
                    statisticsOptions.compare1StartDate = (DateTime)datePickerComparePeriod1From.SelectedDate;
                if (datePickerComparePeriod1To.SelectedDate != null)
                    statisticsOptions.compare1EndDate = (DateTime)datePickerComparePeriod1To.SelectedDate;

                if (datePickerComparePeriod2From.SelectedDate != null)
                    statisticsOptions.compare2StartDate = (DateTime)datePickerComparePeriod2From.SelectedDate;
                if (datePickerComparePeriod2To.SelectedDate != null)
                    statisticsOptions.compare2EndDate = (DateTime)datePickerComparePeriod2To.SelectedDate;

                if (comboBoxCompareGroup1.SelectedValue != null)
                    statisticsOptions.compareGroup1Name = comboBoxCompareGroup1.SelectedValue.ToString();
                else
                    statisticsOptions.compareGroup1Name = String.Empty;

                if (comboBoxCompareGroup2.SelectedValue != null)
                    statisticsOptions.compareGroup2Name = comboBoxCompareGroup2.SelectedValue.ToString();
                else
                    statisticsOptions.compareGroup2Name = String.Empty;

                return statisticsOptions;
            }
            set
            {
                if (value.statisticsType == StatisticsType.StatisticsBasedOnDate) radioButtonDate.IsChecked = true;
                if (value.statisticsType == StatisticsType.StatisticsBasedOnMessagesNumbers) radioButtonMessageNumber.IsChecked = true;
                if (value.statisticsType == StatisticsType.ComparisonByCustomNumbers) radioButtonMessageNumberCompare.IsChecked = true;
                if (value.statisticsType == StatisticsType.ComparisonByMonth) radioButtonMonth.IsChecked = true;
                if (value.statisticsType == StatisticsType.ComparisonByWeek) radioButtonWeek.IsChecked = true;
                if (value.statisticsType == StatisticsType.ComparisonByYear) radioButtonYear.IsChecked = true;
                if (value.statisticsType == StatisticsType.ComparisonByCustomTime) radioButtonCustom.IsChecked = true;
                if (value.statisticsType == StatisticsType.GroupVsGroup) radioButtonGroupVsGroup.IsChecked = true;

                checkBoxPostings.IsChecked = value.postings;
                checkBoxRepliers.IsChecked = value.repliers;
                checkBoxThreadInitiators.IsChecked = value.threadInitiators;
                checkBoxTopTenSubjects.IsChecked = value.topTenSubjects;
                checkBoxNewMembers.IsChecked = value.newMembers;
                checkBoxLurkers.IsChecked = value.lurkers;

                textBoxRangeFrom.Text = value.messageNumberFirst.ToString();
                if (value.messageNumberLast == long.MaxValue)
                    textBoxRangeTo.Text = "Last";
                else
                    textBoxRangeTo.Text = value.messageNumberLast.ToString();

                textBoxCompareRange1From.Text = value.compare1StartNumber.ToString();
                if (value.compare1EndNumber == long.MaxValue)
                    textBoxCompareRange1To.Text = "Last";
                else
                    textBoxCompareRange1To.Text = value.compare1EndNumber.ToString();

                textBoxCompareRange2From.Text = value.compare2StartNumber.ToString();
                if (value.compare2EndNumber == long.MaxValue)
                    textBoxCompareRange2To.Text = "Last";
                else
                    textBoxCompareRange2To.Text = value.compare2EndNumber.ToString();

                datePickerDateFrom.SelectedDate = value.dateTimeStart;
                datePickerDateTo.SelectedDate = value.dateTimeEnd;
                datePickerComparePeriod1From.SelectedDate = value.compare1StartDate;
                datePickerComparePeriod1To.SelectedDate = value.compare1EndDate;
                datePickerComparePeriod2From.SelectedDate = value.compare2StartDate;
                datePickerComparePeriod2To.SelectedDate = value.compare2EndDate;

                foreach (DiscussionGroup item in comboBoxCompareGroup1.Items.SourceCollection)
                {
                    if (item.Name == value.compareGroup1Name)
                        comboBoxCompareGroup1.SelectedValue = item;

                    if (item.Name == value.compareGroup2Name)
                        comboBoxCompareGroup2.SelectedValue = item;
                }
            }
        }

        private void buttonBuild_Click(object sender, RoutedEventArgs e)
        {
            StatisticsOptions statisticsOptions = StatisticsOptions;

            if (statisticsOptions.messageNumberFirst < 1 || statisticsOptions.messageNumberLast < 1 ||
                statisticsOptions.compare1StartNumber < 1 || statisticsOptions.compare1EndNumber < 1 ||
                statisticsOptions.compare2StartNumber < 1 || statisticsOptions.compare2EndNumber < 1)
            {
                System.Windows.MessageBox.Show("Invalid range value.", "PG Offline");
                return;
            }

            List<DiscussionGroup> groups = new List<DiscussionGroup>();

            if ((bool)radioButtonGroupVsGroup.IsChecked)
            {
                groups.Add((DiscussionGroup)comboBoxCompareGroup1.SelectedItem);
                groups.Add((DiscussionGroup)comboBoxCompareGroup2.SelectedItem);
            }
            else
            {
                foreach (var item in listViewGroups.SelectedItems)
                {
                    groups.Add((DiscussionGroup)item);
                }
            }

            BuildStatisticsRequest(groups, statisticsOptions);
        }

        private void AdjustNumberInterval(bool parseFrom, bool parseTo, string beginNumber, string endNumber, out long begin, out long end)
        {
            if (parseFrom == false && parseTo == false)
            {
                begin = long.MaxValue;
                end = long.MaxValue;
                return;
            }
            
            if (parseFrom == true && parseTo == false)
            {
                begin = long.Parse(beginNumber);
                end = long.MaxValue;
                return;
            }
            
            if (parseFrom == false && parseTo == true)
            {
                begin = long.Parse(endNumber);
                end = long.MaxValue;
                return;
            }
            
            begin = long.Parse(beginNumber);
            end = long.Parse(endNumber);
        }

        private void buttonStop_Click(object sender, RoutedEventArgs e)
        {
            CancelStatisticsCalculationRequest();
        }

        public void DisplayGroups(List<DiscussionGroup> groups)
        {
            listViewGroups.ItemsSource = groups;
            comboBoxCompareGroup1.ItemsSource = groups;
            comboBoxCompareGroup2.ItemsSource = groups;
        }

        public void ShowReport(string report)
        {
            FormStatisticsReport frmReport = new FormStatisticsReport(report);
            frmReport.ShowDialog();
        }

        private void buttonExit_Click(object sender, RoutedEventArgs e)
        {
            CancelStatisticsCalculationRequest();
            Close();
        }

        private void radioButtonDate_Checked(object sender, RoutedEventArgs e)
        {
            BaseStatisticsOnDateSelected();
        }

        private void radioButtonMessageNumberDate_Checked(object sender, RoutedEventArgs e)
        {
            BaseStatisticsOnNumberSelected();
        }

        public void SelectDateInputForms()
        {
            datePickerDateFrom.IsEnabled = true;
            datePickerDateTo.IsEnabled = true;
        }

        public void SelectMessageNumberInputForms()
        {
            textBoxRangeFrom.IsEnabled = true;
            textBoxRangeTo.IsEnabled = true;
        }

        public void SelectComparingDateInputForms()
        {
            datePickerComparePeriod1From.IsEnabled = true;
            datePickerComparePeriod1To.IsEnabled = true;
            datePickerComparePeriod2From.IsEnabled = true;
            datePickerComparePeriod2To.IsEnabled = true;
        }

        public void SelectComparingMessageNumbersInputForms()
        {
            textBoxCompareRange1From.IsEnabled = true;
            textBoxCompareRange1To.IsEnabled = true;
            textBoxCompareRange2From.IsEnabled = true;
            textBoxCompareRange2To.IsEnabled = true;
        }

        public void SelectGroupsComboboxes()
        {
            comboBoxCompareGroup1.IsEnabled = true;
            comboBoxCompareGroup2.IsEnabled = true;
        }
        
        private void radioButtonWeek_Checked(object sender, RoutedEventArgs e)
        {
            CompareByWeekSelected();
        }

        private void radioButtonMonth_Checked(object sender, RoutedEventArgs e)
        {
            CompareByMonthSelected();
        }

        private void radioButtonYear_Checked(object sender, RoutedEventArgs e)
        {
            CompareByYearSelected();
        }

        private void radioButtonCustom_Checked(object sender, RoutedEventArgs e)
        {
            CompareByCustomDateSelected();
        }

        private void radioButtonMessageNumberCompare_Checked(object sender, RoutedEventArgs e)
        {
            CompareByMessageNumberSelected();
        }

        private void radioButtonGroupVsGroup_Checked(object sender, RoutedEventArgs e)
        {
            GroupVsGroupSelected();
        }

        public void DisableAllInputForms()
        {
            textBoxCompareRange1From.IsEnabled = false;
            textBoxCompareRange1To.IsEnabled = false;
            textBoxCompareRange2From.IsEnabled = false;
            textBoxCompareRange2To.IsEnabled = false;
            textBoxRangeFrom.IsEnabled = false;
            textBoxRangeTo.IsEnabled = false;
            datePickerComparePeriod1From.IsEnabled = false;
            datePickerComparePeriod1To.IsEnabled = false;
            datePickerComparePeriod2From.IsEnabled = false;
            datePickerComparePeriod2To.IsEnabled = false;
            datePickerDateFrom.IsEnabled = false;
            datePickerDateTo.IsEnabled = false;
            comboBoxCompareGroup1.IsEnabled = false;
            comboBoxCompareGroup2.IsEnabled = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DisplayGroupsRequest();
            SetStatisticsOptionsRequest();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveStatisticsOptionsRequest(StatisticsOptions);
        }
    }
}
