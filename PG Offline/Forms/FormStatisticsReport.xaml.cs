﻿using System.Windows;

namespace PGOffline
{
    /// <summary>
    /// Interaction logic for frmStatisticsReport.xaml
    /// </summary>
    public partial class FormStatisticsReport : Window
    {
        public FormStatisticsReport(string report)
        {
            InitializeComponent();

            webBrowserReport.NavigateToString(report);
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
