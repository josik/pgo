﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using PGOffline.View;
using PGOffline.Model;

namespace PGOffline
{
    public partial class FormFullScreen : Window, IFormFullScreenView
    {
        private Message _message;

        public event AddMessagesToFavoritesRequestEventHandler AddMessagesToFavoritesRequest;
        public event RemoveMessagesFromFavoritesRequestEventHandler RemoveMessagesFromFavoritesRequest;
        public event DeleteMessagesRequestEventHandler DeleteMessagesRequest;

        public event FullScreenPreviousMessageRequestEventHandler FullScreenPreviousMessageRequest;
        public event FullScreenNextMessageRequestEventHandler FullScreenNextMessageRequest;

        public event FullScreenUpGroupRequestEventHandler FullScreenUpGroupRequest;
        public event FullScreenDownGroupRequestEventHandler FullScreenDownGroupRequest;
       
        public FormFullScreen()
        {
            InitializeComponent();
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonPreviousMessage_Click(object sender, RoutedEventArgs e)
        {
            FullScreenPreviousMessageRequest();
        }

        private void buttonNextMessage_Click(object sender, RoutedEventArgs e)
        {
            FullScreenNextMessageRequest();
        }

        private void buttonUpGroup_Click(object sender, RoutedEventArgs e)
        {
            FullScreenUpGroupRequest(this);
        }

        private void buttondownGroup_Click(object sender, RoutedEventArgs e)
        {
            FullScreenDownGroupRequest(this);
        }

        private void buttonReply_Click(object sender, RoutedEventArgs e)
        {
            
        }

        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
        internal interface IServiceProvider
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            object QueryService(ref Guid guidService, ref Guid riid);
        }
        static readonly Guid SID_SWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");

        private void buttonPrint_Click(object sender, RoutedEventArgs e)
        {
            IServiceProvider serviceProvider = null;
            if (webBrowser.Document != null)
            {
                serviceProvider = (IServiceProvider)webBrowser.Document;
            }

            Guid serviceGuid = SID_SWebBrowserApp;
            Guid iid = typeof(SHDocVw.IWebBrowser2).GUID;

            object NullValue = null;

            SHDocVw.IWebBrowser2 target = (SHDocVw.IWebBrowser2)serviceProvider.QueryService(ref serviceGuid, ref iid);
            target.ExecWB(SHDocVw.OLECMDID.OLECMDID_PRINTPREVIEW, SHDocVw.OLECMDEXECOPT.OLECMDEXECOPT_DODEFAULT, ref NullValue, ref NullValue);
        }

        private void buttonDelete_Click(object sender, RoutedEventArgs e)
        {
            IList messages = new ArrayList();
            messages.Add(_message);
            DeleteMessagesRequest(messages);
        }

        private void buttonAddToFavorites_Click(object sender, RoutedEventArgs e)
        {
            IList messages = new ArrayList();
            messages.Add(_message);
            AddMessagesToFavoritesRequest(messages);
        }

        private void buttonRemoveFromFavorites_Click(object sender, RoutedEventArgs e)
        {
            IList messages = new ArrayList();
            messages.Add(_message);
            RemoveMessagesFromFavoritesRequest(messages);
        }

        public void SetDiscussionGroup(DiscussionGroup group)
        {
            labelGroup.Content = "Group: " + group.Name;
        }

        public void SetMessage(PGOffline.Model.Message message)
        {
            _message = message;

            if (message != null)
            {
                labelMessageNumber.Content = "Message :" + message.Number.ToString();

                if (message.Content != null)
                    webBrowser.NavigateToString(message.Content);

                if (message.Author != null)
                    labelMessageAuthor.Content = "From: " + message.Author;

                if (message.Date != null)
                    labelMessageDate.Content = "Received :" + message.Date.ToString("F");

                if (message.Subject != null)
                    labelMessageSubject.Content = "Subject: " + message.Subject;

                imageFavoriteStatus.DataContext = message;
                imageReadedStatus.DataContext = message;
                buttonAddToFavorites.DataContext = message;
                buttonRemoveFromFavorites.DataContext = message;
            }
        }

        public void SetEmptyContent()
        {
            labelMessageAuthor.Content = "From: ";
            labelMessageDate.Content = "Received: ";
            labelMessageNumber.Content = "Message: ";
            labelMessageSubject.Content = "Subject: ";
            webBrowser.NavigateToString(" ");

            imageFavoriteStatus.DataContext = null;
            imageReadedStatus.DataContext = null;
            buttonAddToFavorites.DataContext = null;
            buttonRemoveFromFavorites.DataContext = null;
        }
    }
}
