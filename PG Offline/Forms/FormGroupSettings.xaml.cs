﻿using PGOffline.View;
using System;
using System.Windows;

namespace PGOffline
{
    public partial class FormGroupSettings : Window, IFormGroupSettingsView
    {
        public event GroupSettingsSaveRequestEventHandler GroupSettingsSaveRequest;
        public event GroupSettingsLoadRequestEventHandler GroupSettingsLoadRequest;
        public event SaveActiveTabRequestEventHandler SaveActiveTabRequest;
        public event BrowseAttachmentsFolderRequestEventHandler BrowseAttachmentsFolderRequest;
        public event BrowseOutlookFolderRequestEventHandler BrowseOutlookFolderRequest;

        public FormGroupSettings()
        {
            InitializeComponent();
        }

        public int StartMessageNumber 
        {
            get
            { 
                int startMessageNumber = 1;
                Int32.TryParse(textBoxStartFromMsg.Text, out startMessageNumber);
                return startMessageNumber;
            }
            set
            {
                textBoxStartFromMsg.Text = value.ToString();        
            }
        }

        public int EndMessageNumber
        {
            get
            {
                int endMessageNumber;
                bool canParse = Int32.TryParse(textBoxEndAtMsg.Text, out endMessageNumber);

                if (canParse)
                    return endMessageNumber;
                return Int32.MaxValue;
            }
            set
            {
                if (value == Int32.MaxValue)
                    textBoxEndAtMsg.Text = "Last";
                else
                    textBoxEndAtMsg.Text = value.ToString();
            }
        }

        public string IncludeAttachmentsPattern
        {
            get
            {
                return textBoxAttachmentsFilter.Text;
            }
            set
            {
                textBoxAttachmentsFilter.Text = value;
            }
        }

        public string ExcludeAttachmentsPattern
        {
            get
            {
                return textBoxAttachmentsExclude.Text;
            }
            set
            {
                textBoxAttachmentsExclude.Text = value;
            }            
        }

        public string AttachmentsFolder
        {
            get
            {
                return textBoxAttachmentsFolder.Text;
            }
            set
            {
                if (value.Contains("APPDATA"))
                {
                    string userProfilePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    textBoxAttachmentsFolder.Text = value.Replace("APPDATA", userProfilePath);
                }
                else
                {
                    textBoxAttachmentsFolder.Text = value;
                }
            }
        }

        public string OutlookFolder 
        { 
            get
            {
                return textBoxOutlookFolder.Text;
            }
            set
            {
                textBoxOutlookFolder.Text = value;
            }
        }

        public bool DisableAttachementsDownloading
        {
            get
            {
                return (bool) checkDisableAttachementsDownloading.IsChecked;
            }
            set
            {
                checkDisableAttachementsDownloading.IsChecked = value;
            }
        }

        public bool UpdateAttachmentsLinks 
        {
            get
            {
                return (bool) checkBoxUpdateLinks.IsChecked;
            }
            set
            {
                checkBoxUpdateLinks.IsChecked = value;
            }
        }

        public int AttachmentsLimitationSize
        {
            get
            {
                int size = 0;
                Int32.TryParse(textBoxAttachmetSizeLimit.Text, out size);
                return size;
            }
            set
            {
                textBoxAttachmetSizeLimit.Text = value.ToString();
            }
        }
        
        public string AttachmentsLimitationType
        {
            get
            {
                if (radioButtonAtLeast.IsChecked != null && (bool) radioButtonAtLeast.IsChecked)
                {
                    return "at_least";
                }
                return "at_most";
            }
            set
            {
                if (value == "at_least")
                {
                    radioButtonAtLeast.IsChecked = true;
                    radioButtonAtMost.IsChecked = false;
                }
                else 
                {
                    radioButtonAtLeast.IsChecked = false;
                    radioButtonAtMost.IsChecked = true;                
                }
            }
        }

        public bool SkipDuringRefreshAllGroups
        {
            get
            {
                return checkBoxSkipMessagesRefresh.IsChecked != null && (bool) checkBoxSkipMessagesRefresh.IsChecked;
            }
            set
            {
                checkBoxSkipMessagesRefresh.IsChecked = value;
            }
        }

        public bool SkipDuringGetMembersForAllGroups
        {
            get
            {
                return checkBoxSkipMembersRefresh.IsChecked != null && (bool) checkBoxSkipMembersRefresh.IsChecked;
            }
            set
            {
                checkBoxSkipMembersRefresh.IsChecked = value;
            }
        }
        
        public bool UseOldDesignFormat
        {
            get
            {
                return checkBoxUseOldFormat.IsChecked != null && (bool) checkBoxUseOldFormat.IsChecked;
            }
            set
            {
                checkBoxUseOldFormat.IsChecked = value;
            }
        }

        //public string GroupUserAccount
        //{
        //    get
        //    {
        //        return dropdownUserAccount.Text;
        //    }
        //    set
        //    {
        //        dropdownUserAccount.Text = value;
        //    }
        //}

        public string GroupLogin
        {
            get
            {
                return textBoxYahooUserName.Text;
            }
            set
            {
                textBoxYahooUserName.Text = value;
            }
        }

        public string GroupPassword
        {
            get
            {
                return passwordBoxYahooUserPassword.Password;
            }
            set
            {
                passwordBoxYahooUserPassword.Password = value;
            }
        }

        public string GroupName
        {
            get
            {
                return textBoxGroupName.Text;
            }
        }

        public int ActiveTab
        {
            get
            {
                return tabControlSettings.SelectedIndex;
            }
            set
            {
                tabControlSettings.SelectedIndex = value;
            }
        }

        public void SetGroupName(string name)
        {
            windowGroupSettings.Title = string.Format("{0} Group Settings", name);

            textBoxGroupName.Text = name;
        }

        public void SetHeader(string header)
        {
            windowGroupSettings.Title = header;
        }

        public void SetGroupNameEditability(bool isEditable)
        {
            textBoxGroupName.IsReadOnly = !isEditable;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GroupSettingsLoadRequest();
        }

        private void textBoxYahooUserName_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxYahooUserName.Text == "(default)")
            {
                textBoxYahooUserName.Text = string.Empty;
            }
        }

        private void passwordBoxYahooUserPassword_Click(object sender, RoutedEventArgs e)
        {
            if (passwordBoxYahooUserPassword.Password == "(default)")
            {
                passwordBoxYahooUserPassword.Clear();
            }
        }

        public void CloseWindow()
        {
            this.Close();
        }
        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            GroupSettingsSaveRequest();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
        }

        private void windowGroupSettings_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (SaveActiveTabRequest != null)
            {
                SaveActiveTabRequest(tabControlSettings.SelectedIndex);
            }
        }

        private void buttonBrowse_Click(object sender, RoutedEventArgs e)
        {
            BrowseAttachmentsFolderRequest();
        }

        private void buttonSelect_Click(object sender, RoutedEventArgs e)
        {
            BrowseOutlookFolderRequest();
        }
    }
}
