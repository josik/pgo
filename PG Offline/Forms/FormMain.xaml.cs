﻿using Asfdfdfd.Api.Yahoo.Groups;
using Fizzler.Systems.HtmlAgilityPack;
using HtmlAgilityPack;
using NetFwTypeLib;
using PGOffline.Forms;
using PGOffline.Model;
using PGOffline.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Control = System.Windows.Controls.Control;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using ListViewItem = System.Windows.Controls.ListViewItem;
using Message = PGOffline.Model.Message;
using StatusBar = System.Windows.Controls.Primitives.StatusBar;
using TextBox = System.Windows.Controls.TextBox;


namespace PGOffline
{
    public partial class FormMain : Window, IFormMainView, INotifyPropertyChanged, View.IMessagesListView
    {
        public bool IsCtrlKeyDown = false;

        #region Menu events

        public event OpenDatabaseRequestEventHandler OpenDatabaseRequest;
        public event ShowMailClientRequestEventHandler ShowMailClientRequest;
        public event ShowAddressBookRequestEventHandler ShowAddressBookRequest;
        public event ShowStatisticsBuilderRequestEventHandler ShowStatisticsBuilderRequest;
        public event ShowStatsRequestEventHandler ShowStatsRequest;
        public event ShowMasterLoginSettingsRequestEventHandler ShowMasterLoginSettingsRequest;
        public event ShowDigestRequestEventHandler ShowDigestRequest;
        public event ShowSelectionAsDigestRequestEventHandler ShowSelectionAsDigestRequest;
        public event OpenMessageInYahooRequestEventHandler OpenMessageInYahooRequest;
        public event FullScreenViewRequestEventHandler FullScreenViewRequest;
        public event ImportFromFolderRequestEventHandler ImportFromFolderRequest;
        public event OpenDataFolderRequestEventHandler OpenDataFolderRequest;
        public event ExportToFolderRequestEventHandler ExportToFolderRequest;
        public event ShowOptionsRequestEventHandler ShowOptionsRequest;
        public event ShowGroupSettingsRequestEventHander ShowGroupSettingsRequest;
        public event NewGroupRequestEventHandler NewGroupRequest;
        public event ShowDownloadGroupFilesRequestEventHandler ShowDownloadGroupFilesRequest;
        public event ShowDownloadGroupPhotosRequestEventHandler ShowDownloadGroupPhotosRequest;
        public event ExportSelectedMessagesRequestEventHandler ExportSelectedMessagesRequest;
        public event DeleteDiscussionGroupRequestEventHandler DeleteDiscussionGroupRequest;
        public event ReplyMessageRequestEventHandler ReplyMessageRequest;
        public event HideQuoteStringRequestEventHandler HideQuoteStringRequest;
        public event RecentDatabasesRequestEventHandler RecentDatabasesRequest;

        #endregion
        
        public event WindowActivatedEventHandler WindowActivated;
        public event ExitRequestEventHandler ExitRequest;
        public event PropertyChangedEventHandler PropertyChanged;
        public event LoadGroupsListRequestEventHandler LoadGroupsListRequest;
        public event LoadMessagesRequestEventHandler LoadMessagesRequest;
        public event RefreshAllGroupsRequestEventHander RefreshAllGroupsRequest;
        public event LoadMembersRequestEventHandler LoadMembersRequest;
        public event LoadMembersForAllGroupsRequestEventHandler LoadMembersForAllGroupsRequest;
        public event DisplayGroupsRequestEventHandler DisplayGroupsRequest;
        public event DiscussionGroupSelectedEventHandler DiscussionGroupSelected;
        public event CancelProcedureRequestEventHandler CancelProcedureRequest;
        public event SearchRequestEventHandler SearchRequest;

        public event AddMessagesToFavoritesRequestEventHandler AddMessagesToFavoritesRequest;
        public event RemoveMessagesFromFavoritesRequestEventHandler RemoveMessagesFromFavoritesRequest;
        public event MarkMessagesAsReadRequestEventHandler MarkMessagesAsReadRequest;
        public event MarkMessagesAsUnreadRequestEventHandler MarkMessagesAsUnreadRequest;
        public event MarkAllMessagesAsReadRequestEventHandler MarkAllMessagesAsReadRequest;
        public event MarkAllMessagesAsUnreadRequestEventHandler MarkAllMessagesAsUnreadRequest;
        public event DeleteMessagesRequestEventHandler DeleteMessagesRequest;
        public event MarkAllGroupsAsReadRequestEventHandler MarkAllGroupsAsReadRequest;

        public event ShowAllMessagesRequestEventHandler ShowAllMessagesRequest;
        public event ShowOnlyNewMessagesRequestEventHandler ShowOnlyNewMessagesRequest;
        public event ShowOnlyFavoritesMessagesRequestEventHandler ShowOnlyFavoritesMessagesRequest;
        
        public event ToolbarCaptionsVisibilityChangedHandler ToolbarCaptionsVisibilityChanged;
        public event ToolbarButtonsSizeChangedHandler ToolbarButtonsSizeChanged;

        public event SetEncodingHandler SetEncoding;

        public string WindowTitle { get; set; }
        public string DatabaseTitle { get; set; }

        public FormMain()
        {
            try
            {
                // DEBUG: code below can be used to have the app pause until the debugger attaches
                //File.AppendAllText(@"c:\temp\PgOffline_exceptions.log", "Waiting for debugger to attach.");
                //while (!Debugger.IsAttached)
                //{
                //    Thread.Sleep(1000);
                //}
                //File.AppendAllText(@"c:\temp\PgOffline_exceptions.log", "Debugger attached.");


                InitializeComponent();

                // Used for self-signed certs
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                
                this.Closing += new System.ComponentModel.CancelEventHandler(OnMainWindowClosing);

                SetStartupWindowProperties();

                listViewMessages.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);

                string globalEncoding = Options.Instance.GetAsString("global_encoding");

                SetMenuEncoding(globalEncoding);

                this.KeyDown += OnKeyDown;

                this.KeyUp += OnKeyUp;



                //try
                //{

                //    //License.Instance.SetTrialLicense();
                //    // load license
                //    if (!License.Instance.LoadLicenseFromRegistry())
                //    {
                //        // set trial license
                //        License.Instance.SetTrialLicense();
                //    }

                //    //bool isValid = License.Instance.IsLicenseValid();
                //}
                //catch (Exception ex)
                //{
                //   //throw;
                //}

                //var isLicenseValid = License.Instance.GetLicenseStatus() == LicenseStatus.Valid;


                try
                {
                    INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(
                    Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
                    firewallPolicy.Rules.Item("PhantomJs");
                }
                catch
                {
                    const int ERROR_CANCELLED = 1223; //The operation was canceled by the user.

                    string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                    UriBuilder uri = new UriBuilder(codeBase);
                    string path = Path.Combine(Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path)), "RegisterExtensionInFirewall.exe");

                    ProcessStartInfo info = new ProcessStartInfo(@path);
                    info.UseShellExecute = true;
                    info.Verb = "runas";
                    try
                    {
                        Process.Start(info);
                    }
                    catch (Win32Exception ex)
                    {
                        if (ex.NativeErrorCode == ERROR_CANCELLED)
                            System.Windows.Forms.Application.Exit();
                        else
                            throw;
                    }
                }

            }
            catch (Exception ex)
            {
#if DEBUG
                File.AppendAllText(@"c:\temp\PgOffline_exceptions.log", ex.Message);
#endif
                throw;
            }
            
        }
        
        private void listViewGroupsContextMenu_Opened(Object sender, RoutedEventArgs e)
        {
            bool shouldBeEnabled = listViewGroups.SelectedItem != null;

            listViewGroupsContextMenuItemGroupDelete.IsEnabled = shouldBeEnabled;
            listViewGroupsContextMenuItemGroupSettings.IsEnabled = shouldBeEnabled;
            listViewGroupsContextMenuItemGroupStats.IsEnabled = shouldBeEnabled;
            listViewGroupsContextMenuItemGroupGetMembers.IsEnabled = shouldBeEnabled;
            listViewGroupsContextMenuItemGroupDownloadFiles.IsEnabled = shouldBeEnabled;
            listViewGroupsContextMenuItemGroupDownloadPhotos.IsEnabled = shouldBeEnabled;
        }


        private string _messageHtml;

        public string MessageHtml
        {
            get
            {
                return _messageHtml;
            }
            set
            {
                _messageHtml = value;

                OnPropertyChanged("MessageHtml");
            }
        }



        public void SetMenuItemGroupsListIsEnabled(bool enableButton)
        {
            menuItemGetGroupsList.IsEnabled = enableButton;

            buttonLoadGroupsList.IsEnabled = enableButton;
        }

        public void SetMenuItemStopIsEnabled(bool enableButton)
        {
            buttonStop.IsEnabled = enableButton;
        }
        
        private void OnKeyUp(object sender, KeyEventArgs keyEventArgs)
        {
            var myKey = keyEventArgs.Key.ToString();

            if (myKey == "LeftCtrl" || myKey == "RightCtrl")
            {
                IsCtrlKeyDown = false;
                //MessageBox.Show("Yerp, it done happened :: " + keyEventArgs.Key.ToString());
            }

            return;
        }

        private void OnKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            var myKey = keyEventArgs.Key.ToString();

            if (myKey == "LeftCtrl" || myKey == "RightCtrl")
            {
                IsCtrlKeyDown = true;
                //MessageBox.Show("Yerp, it done happened :: " + keyEventArgs.Key.ToString());
            }
            
            return;
            //if (keyEventArgs.IsDown && keyEventArgs.Key.HasFlag(Keys.ControlKey))
            //{
            //    // == Keys.V && ModifierKeys.HasFlag(Keys.Control)
            //    statusBar1.Items.Clear();
            //    statusBar1.Items.Add(new TextBlock { Text = "key" });
            //    MessageBox.Show("Yerp, it done happened");
            //}
            //        MessageBox.Show("Yerp, it done happened");
        }

        private void SetStartupWindowProperties()
        {
            // main window
            double windowHeight = Options.Instance.GetAsDouble("main_window_height");
            double windowWidth = Options.Instance.GetAsDouble("main_window_width");

            if (double.IsNaN(windowHeight) || double.IsNaN(windowWidth) || windowHeight < Properties.Settings.Default.AppMinHeight || windowWidth < Properties.Settings.Default.AppMinWidth)
            {
                // default and minimum
                windowHeight = Properties.Settings.Default.AppMinHeight;
                windowWidth = Properties.Settings.Default.AppMinWidth;
                Options.Instance.Set("main_window_height", windowHeight);
                Options.Instance.Set("main_window_width", windowWidth);
            }

            this.Height = windowHeight;
            this.Width = windowWidth;
            this.MinHeight = Properties.Settings.Default.AppMinHeight;
            this.MinWidth = Properties.Settings.Default.AppMinWidth;

            // content layout
            var contentLayoutGroupsWidth = Options.Instance.GetAsDouble("main_window_contentlayout_groups_width") < 0.2 ? 2 : Options.Instance.GetAsDouble("main_window_contentlayout_groups_width");
            var contentLayoutMessagesHeight = Options.Instance.GetAsDouble("main_window_contentlayout_messages_height") < 0.2 ? 1 : Options.Instance.GetAsDouble("main_window_contentlayout_messages_height");
            var contentLayoutLogHeight = Options.Instance.GetAsDouble("main_window_contentlayout_log_height") < 0.2 ? 1 : Options.Instance.GetAsDouble("main_window_contentlayout_log_height");
            ContentLayout.ColumnDefinitions[0].Width = new GridLength(contentLayoutGroupsWidth, GridUnitType.Star);
            ContentLayout.RowDefinitions[0].Height = new GridLength(contentLayoutMessagesHeight, GridUnitType.Star);
            ContentLayout.RowDefinitions[2].Height = new GridLength(contentLayoutLogHeight, GridUnitType.Star);
            
            // message list view
            GvMessageList.Columns[0].Width = Options.Instance.GetAsDouble("main_window_messagelistview_column_status_width") < 5 ? 80 : Options.Instance.GetAsDouble("main_window_messagelistview_column_status_width");
            GvMessageList.Columns[1].Width = Options.Instance.GetAsDouble("main_window_messagelistview_column_favorite_width") < 5 ? 50 : Options.Instance.GetAsDouble("main_window_messagelistview_column_favorite_width");
            GvMessageList.Columns[2].Width = Options.Instance.GetAsDouble("main_window_messagelistview_column_messageid_width") < 5 ? 40 : Options.Instance.GetAsDouble("main_window_messagelistview_column_messageid_width");
            GvMessageList.Columns[3].Width = Options.Instance.GetAsDouble("main_window_messagelistview_column_from_width") < 5 ? 150 : Options.Instance.GetAsDouble("main_window_messagelistview_column_from_width");
            GvMessageList.Columns[4].Width = Options.Instance.GetAsDouble("main_window_messagelistview_column_subject_width") < 5 ? 200 : Options.Instance.GetAsDouble("main_window_messagelistview_column_subject_width");
            GvMessageList.Columns[5].Width = Options.Instance.GetAsDouble("main_window_messagelistview_column_received_width") < 5 ? 200 : Options.Instance.GetAsDouble("main_window_messagelistview_column_received_width");
            GvMessageList.Columns[6].Width = Options.Instance.GetAsDouble("main_window_messagelistview_column_attachments_width") < 5 ? 80 : Options.Instance.GetAsDouble("main_window_messagelistview_column_attachments_width");
            GvMessageList.Columns[7].Width = Options.Instance.GetAsDouble("main_window_messagelistview_column_topicid_width") < 5 ? 50 : Options.Instance.GetAsDouble("main_window_messagelistview_column_topicid_width");
            // re-apply messages view sort order
            ApplySort();

            // menu/button enabled states
            buttonStart.IsEnabled = false;
            menuItemReceiveMessages.IsEnabled = false;
            
            buttonSettings.IsEnabled = false;
            menuItemGroupSettings.IsEnabled = false;

            buttonStats.IsEnabled = false;
            menuItemGroupStats.IsEnabled = false;

            buttonReply.IsEnabled = false;
            menuItemMessageReply.IsEnabled = false;

            buttonStop.IsEnabled = false;
        }

        protected virtual void OnMainWindowClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            SaveAppWindowProperties();
        }

        private void SaveAppWindowProperties()
        {
            // main window
            if ((this.Height >= Properties.Settings.Default.AppMinHeight) && (this.Width >= Properties.Settings.Default.AppMinWidth))
            {
                Options.Instance.Set("main_window_height", this.Height);

                Options.Instance.Set("main_window_width", this.Width);
            }
            
            // content layout
            Options.Instance.Set("main_window_contentlayout_groups_width", ContentLayout.ColumnDefinitions[0].Width.Value.ToString());
            Options.Instance.Set("main_window_contentlayout_messages_height", ContentLayout.RowDefinitions[0].Height.Value.ToString());
            Options.Instance.Set("main_window_contentlayout_log_height", ContentLayout.RowDefinitions[2].Height.Value.ToString());

            // message list view
            Options.Instance.Set("main_window_messagelistview_column_status_width", GvMessageList.Columns[0].Width);
            Options.Instance.Set("main_window_messagelistview_column_favorite_width", GvMessageList.Columns[1].Width);
            Options.Instance.Set("main_window_messagelistview_column_messageid_width", GvMessageList.Columns[2].Width);
            Options.Instance.Set("main_window_messagelistview_column_from_width", GvMessageList.Columns[3].Width);
            Options.Instance.Set("main_window_messagelistview_column_subject_width", GvMessageList.Columns[4].Width);
            Options.Instance.Set("main_window_messagelistview_column_received_width", GvMessageList.Columns[5].Width);
            Options.Instance.Set("main_window_messagelistview_column_attachments_width", GvMessageList.Columns[6].Width);
            Options.Instance.Set("main_window_messagelistview_column_topicid_width", GvMessageList.Columns[7].Width);
            
            Options.Instance.SaveOptions();
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            ExitRequest();
        }

        private void SetMenuEncoding(string globalEncoding)
        {
            if (globalEncoding == null)
            {
                menuItemEncodingDefault.IsChecked = true;
            }
            else if (Encoding.GetEncoding("windows-1251").Equals(Encoding.GetEncoding(globalEncoding)))
            {
                menuItemEncodingWindows1251.IsChecked = true;
            }
            else if (Encoding.GetEncoding("windows-1252").Equals(Encoding.GetEncoding(globalEncoding)))
            {
                menuItemEncodingWindows1252.IsChecked = true;
            }
            else if (Encoding.GetEncoding("koi8-r").Equals(Encoding.GetEncoding(globalEncoding)))
            {
                menuItemEncodingKoi8R.IsChecked = true;
            }
            else if (Encoding.GetEncoding("greek").Equals(Encoding.GetEncoding(globalEncoding)))
            {
                menuItemEncodingGreek.IsChecked = true;
            }
            else if (Encoding.GetEncoding("hebrew").Equals(Encoding.GetEncoding(globalEncoding)))
            {
                menuItemEncodingHebrew.IsChecked = true;
            }
            else if (Encoding.UTF8.Equals(Encoding.GetEncoding(globalEncoding)))
            {
                menuItemEncodingGreek.IsChecked = true;
            }
        }

        private void OnPropertyChanged(String property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public void EnableControls()
        {
            foreach (Control control in toolbarMain.Items)
            {
                control.IsEnabled = true;
            }

            menuItemReceiveMessages.IsEnabled = true;

            menuItemRefreshAllGroups.IsEnabled = true;
        }

        public void DisableControls()
        {
            foreach (Control control in toolbarMain.Items)
            {
                control.IsEnabled = false;
            }

            buttonStop.IsEnabled = true;

            buttonHelp.IsEnabled = true;

            menuItemReceiveMessages.IsEnabled = false;

            menuItemRefreshAllGroups.IsEnabled = false;
        }
        
        private void ItemContainerGenerator_StatusChanged(object sender, EventArgs e)
        {
            if (listViewMessages.SelectedIndex == -1)
            {
                return;
            }

            ListViewItem item = listViewMessages.ItemContainerGenerator.ContainerFromIndex(listViewMessages.SelectedIndex) as ListViewItem;

            if (item != null)
            {
                item.Focus();
            }
        }

        public TextBox LogArea
        {
            get
            {
                return textBoxLog;
            }
        }

        public StatusBar StatusBar1
        {
            get
            {
                return statusBar1;
            }
        }

        private void menuItemGetMembers_Click(object sender, RoutedEventArgs e)
        {
            DiscussionGroup selectedGroup = (DiscussionGroup)listViewGroups.SelectedItem;

            if(selectedGroup != null)
            {
                LoadMembersRequest(selectedGroup);
            }
        }

        private void buttonLoadGroupsList_Click(object sender, RoutedEventArgs e)
        {
            LoadGroupsListRequest();
        }

        private void window_Loaded(object sender, RoutedEventArgs e)
        {
            DisplayGroupsRequest();

            TrayMinimize(Options.Instance.GetAsBoolean("miscellaneous_options_minimize_to_system_tray"));
        }

        public void TrayMinimize(bool status)
        {
            if (status)
                MinimizeToTray.Enable(this);
            else
                MinimizeToTray.Disable(this);
        }
        
        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            if (listViewGroups.SelectedItem != null)
            {
                LoadMessagesRequest((DiscussionGroup) listViewGroups.SelectedItem);

            }
        }

        private void buttonRefresh_Click(object sender, RoutedEventArgs e)
        {
            RefreshAllGroupsRequest();  
        }

        private void buttonSettings_Click(object sender, RoutedEventArgs e)
        {
            ShowGroupSettingsRequest();            
        }

        private void buttonStats_Click(object sender, RoutedEventArgs e)
        {
            ShowStatsRequest((DiscussionGroup)listViewGroups.SelectedItem);
        }

        private void buttonStop_Click(object sender, RoutedEventArgs e)
        {
            if (listViewGroups.SelectedItem != null)
            {
                CancelProcedureRequest((DiscussionGroup) listViewGroups.SelectedItem);
            }
        }

        private void buttonSearch_Click(object sender, RoutedEventArgs e)
        {
            SearchRequest();
        }

        private void listViewGroups_SelectedItemChanged(object sender, RoutedEventArgs e)
        {
            if (listViewGroups.SelectedItem != null)
            {
                DiscussionGroupSelected((DiscussionGroup) listViewGroups.SelectedItem);

                buttonStart.IsEnabled = true;
                buttonSettings.IsEnabled = true;
                buttonStats.IsEnabled = true;
            }
            else
            {
                buttonStart.IsEnabled = false;
                buttonSettings.IsEnabled = false;
                buttonStats.IsEnabled = false;
            }
        }

        public void DisplayMessage(Message message)
        {
            if (message == null)
            {
                webBrowserMessage.NavigateToString(" ");

                return;
            }

            //webBrowserMessage.NavigateToString(message.Content != "" ? message.Content + message.AttachmentLinks : " ");

            webBrowserMessage.NavigateToString(message.Content != "" ? message.Content : " ");
        }

        public void DisplayStats(DiscussionGroup group)
        {
            var formQuickStatistics = new FormQuickStatistics(group);

            formQuickStatistics.ShowDialog();
        }

        private void menuItemGroupStats_Click(object sender, RoutedEventArgs e)
        {
            ShowStatsRequest((DiscussionGroup)listViewGroups.SelectedItem);
        }

        private void menuItemStatisticsBuilder_Click(object sender, RoutedEventArgs e)
        {
            ShowStatisticsBuilderRequest();
        }

        private void menuItemImportFromFolder_Click(object sender, RoutedEventArgs e)
        {
            ImportFromFolderRequest();
        }
        private void menuItemOpenDataFolder_Click(object sender, RoutedEventArgs e)
        {
            OpenDataFolderRequest();
        }

        private void menuItemNewGroup_Click(object sender, RoutedEventArgs e)
        {
            NewGroupRequest();
        }

        public void AddMessage(Message message)
        {
            if (message != null)
            {
                listViewMessages.Items.Refresh();
            }
        }

        public void SetDiscussionGroupsList(List<DiscussionGroup> discussionGroups)
        {
            listViewGroups.ItemsSource = discussionGroups;
        }

        public void UpdateDiscussionGroupsList()
        {
            listViewGroups.Items.Refresh();
        }

        /*public void UpdateMessagesList()
        {
            // TODO. Implement using ObservableCollection.
            // listViewMessages.InvalidateProperty(ListView.ItemsSourceProperty);
            listViewMessages.Items.Refresh();
        }*/

        public void UpdateMessagesList()
        {
            listViewMessages.ItemsSource = null;

            listViewMessages.Items.Refresh();

            if (listViewGroups.SelectedItem != null)
            {
                DiscussionGroupSelected((DiscussionGroup)listViewGroups.SelectedItem);
            }

            //listViewMessages.Items.Refresh();
        }

        public void RefreshMessagesList()
        {
            listViewMessages.Items.Refresh();
        }

        private void menuItemMasterLogin_Click(object sender, RoutedEventArgs e)
        {
            ShowMasterLoginSettingsRequest();
        }

        private void menuItemShowDigest_Click(object sender, RoutedEventArgs e)
        {
            ShowDigestRequest();
        }

        private void menuItemOpenInYahoo_Click(object sender, RoutedEventArgs e)
        {
            Message selectedMessage = (Message)listViewMessages.SelectedItem;

            if (selectedMessage != null)
            {
                OpenMessageInYahooRequest(selectedMessage);
            }
        }

        private void menuItemFullScreen_Click(object sender, RoutedEventArgs e)
        {
            FullScreenViewRequest();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            WindowActivated();
        }

        private void menuItemRefreshAllGroups_Click(object sender, RoutedEventArgs e)
        {
            RefreshAllGroupsRequest();
        }

        private void menuItemShowAllMessages_Click(object sender, RoutedEventArgs e)
        {
            menuItemShowOnlyFavoritesMessages.IsChecked = false;

            menuItemShowOnlyNewMessages.IsChecked = false;

            ShowAllMessagesRequest();
        }

        private void menuItemShowOnlyNewMessages_Click(object sender, RoutedEventArgs e)
        {
            menuItemShowOnlyFavoritesMessages.IsChecked = false;

            menuItemShowAllMessages.IsChecked = false;

            ShowOnlyNewMessagesRequest();
        }

        private void menuItemShowOnlyFavoritesMessages_Click(object sender, RoutedEventArgs e)
        {
            menuItemShowAllMessages.IsChecked = false;

            menuItemShowOnlyNewMessages.IsChecked = false;

            ShowOnlyFavoritesMessagesRequest();
        }

        private void menuItemGetMembersForAllGroups_Click(object sender, RoutedEventArgs e)
        {
            LoadMembersForAllGroupsRequest();
        }

        private void menuItemGroupSettings_Click(object sender, RoutedEventArgs e)
        {
            ShowGroupSettingsRequest();
        }

        private void menuItemDownloadFiles_Click(object sender, RoutedEventArgs e)
        {
            ShowDownloadGroupFilesRequest();
        }

        private void menuItemDownloadPhotos_Click(object sender, RoutedEventArgs e)
        {
            ShowDownloadGroupPhotosRequest();
        }

        public void EnableMenuItemFullscreen(bool value)
        {
            menuItemFullScreen.IsEnabled = value;
        }

        private void menuItemOpenDatabase_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.OpenFileDialog
            {
                FileName = "",
                DefaultExt = ".db3",
                Filter = "PGOffline 4 database file (.db3)|*.db3"
            };

            if (dlg.ShowDialog() == true)
            {
                OpenDatabaseRequest(dlg.FileName);
            }
        }

        private void menuItemOptions_Click(object sender, RoutedEventArgs e)
        {
            ShowOptionsRequest();
        }

        private void webBrowsesMessage_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (e.Uri == null || e.Uri.ToString() == "about:blank") return;

            e.Cancel = true;

            Utils.OpenLinkInExternalBrowser(e.Uri.ToString());
        }

        private void menuItemSupportPage_Click(object sender, RoutedEventArgs e)
        {
            Utils.OpenLinkInExternalBrowser("http://www.personalgroupware.com/support.htm");
        }
        
        private void menuItemCheckForUpdates_Click(object sender, RoutedEventArgs e)
        {
            CheckForUpdates updater = new CheckForUpdates();

            updater.HelpCheckForUpdates_Click(sender, e);
        }

        private void menuItemConfigureCheckForUpdates_Click(object sender, RoutedEventArgs e)
        {
            CheckForUpdates updater = new CheckForUpdates();

            updater.OptionsAutoUpdaters_Click(sender, e);
        }

        private void menuItemAbout_Click(object sender, RoutedEventArgs e)
        {
            FormAbout formAbout = new FormAbout();

            formAbout.ShowDialog();
        }

        private void menuItemMarkAllGroupsAsRead_Click(object sender, RoutedEventArgs e)
        {
            MarkAllGroupsAsReadRequest();
        }

        private void menuItemGetGroupsList_Click(object sender, RoutedEventArgs e)
        {
            LoadGroupsListRequest();
        }

        private void menuItemReceiveMessages_Click(object sender, RoutedEventArgs e)
        {
            LoadMessagesRequest((DiscussionGroup)listViewGroups.SelectedItem);
        }

        private void menuItemSearch_Click(object sender, RoutedEventArgs e)
        {
            SearchRequest();
        }

        public IMessagesListView MessagesListView
        {
            get { return this; }
        }

        #region IMessagesListView

        public event MessageSelectedEventHandler MessageSelected;

        private void menuItemMarkMessageAsRead_Click(object sender, RoutedEventArgs e)
        {
            MarkMessagesAsReadRequest(listViewMessages.SelectedItems);
        }

        private void menuItemMarkMessageAsUnread_Click(object sender, RoutedEventArgs e)
        {
            MarkMessagesAsUnreadRequest(listViewMessages.SelectedItems);
        }

        private void menuItemMarkAllMessagesAsRead_Click(object sender, RoutedEventArgs e)
        {
            MarkAllMessagesAsReadRequest();
        }

        private void menuItemMarkAllMessagesAsUnread_Click(object sender, RoutedEventArgs e)
        {
            MarkAllMessagesAsUnreadRequest();
        }

        private void menuItemAddMessageToFavorites_Click(object sender, RoutedEventArgs e)
        {
            AddMessagesToFavoritesRequest(listViewMessages.SelectedItems);
        }

        private void menuItemRemoveMessageFromFavorites_Click(object sender, RoutedEventArgs e)
        {
            RemoveMessagesFromFavoritesRequest(listViewMessages.SelectedItems);
        }

        private async void menuItemDeleteMessages_Click(object sender, RoutedEventArgs e)
        {
            DeleteMessagesRequest(listViewMessages.SelectedItems);
         
            //UpdateMessagesList();
        }
        private void menuItemAttachmentAction_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void SetFilter(Predicate<object> filter)
        {
            listViewMessages.Items.Filter = filter;
        }

        public void SetMessages(IList<Message> messages)
        {
            webBrowserMessage.Navigate("about:blank");

            listViewMessages.ItemsSource = null;
            listViewMessages.Items.Refresh();
            listViewMessages.ItemsSource = messages;
            listViewMessages.Items.Refresh();
            
            ApplySort();
        }

        public void SelectNextMessage()
        {
            int selectedIndex = listViewMessages.SelectedIndex;

            if (listViewMessages.Items.Count <= 0) return;

            if (selectedIndex == -1)
            {
                listViewMessages.SelectedIndex = 0;
            }
            else
            {
                selectedIndex++;

                if (selectedIndex >= listViewMessages.Items.Count)
                    selectedIndex = 0;

                listViewMessages.SelectedIndex = selectedIndex;
            }
        }

        public void SelectPreviousMessage()
        {
            int selectedIndex = listViewMessages.SelectedIndex;

            if (listViewMessages.Items.Count <= 0) return;

            if (selectedIndex == -1)
            {
                listViewMessages.SelectedIndex = listViewMessages.Items.Count - 1;
            }
            else
            {
                selectedIndex--;

                if (selectedIndex == -1)
                    selectedIndex = listViewMessages.Items.Count - 1;

                listViewMessages.SelectedIndex = selectedIndex;
            }
        }

        public void SelectNextDiscussionGroup()
        {
            int selectedIndex = listViewGroups.SelectedIndex;

            if (listViewGroups.Items.Count <= 0) return;

            if (selectedIndex == -1)
            {
                listViewGroups.SelectedIndex = 0;
            }
            else
            {
                selectedIndex++;

                if (selectedIndex >= listViewGroups.Items.Count)
                {
                    selectedIndex = 0;
                }

                listViewGroups.SelectedIndex = selectedIndex;
            }
        }

        public void SelectPreviousDiscussionGroup()
        {
            int selectedIndex = listViewGroups.SelectedIndex;

            if (listViewGroups.Items.Count <= 0) return;

            if (selectedIndex == -1)
            {
                listViewGroups.SelectedIndex = listViewGroups.Items.Count - 1;
            }
            else
            {
                selectedIndex--;

                if (selectedIndex == -1)
                {
                    selectedIndex = listViewMessages.Items.Count - 1;
                }

                listViewGroups.SelectedIndex = selectedIndex;
            }
        }

        public IEnumerable<Message> GetMessages()
        {
            return listViewMessages.ItemsSource.Cast<Message>();
        }

        public List<long> GetSelectedMessagesNumbers()
        {
            List<long> retval = new List<long>();

            foreach (Message message in listViewMessages.SelectedItems)
            {
                retval.Add(message.Number);
            }

            return retval;
        }
        #endregion

        private void menuItemSelectAll_Click(object sender, RoutedEventArgs e)
        {
            listViewMessages.SelectAll();
        }

        private void menuItemShowSelectionAsDigest_Click(object sender, RoutedEventArgs e)
        {
            ShowSelectionAsDigestRequest(listViewMessages.SelectedItems);
        }

        private void listViewMessages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            buttonReply.IsEnabled = listViewMessages.SelectedItem != null;

            Message message = (Message) listViewMessages.SelectedItem;

            if (message == null) return;

            //UpdateMessageAttachmentsPath(message);

            AddMessageAttachments(message);

            MessageSelected(message);

            menuItemShowSelectionAsDigest.IsEnabled = listViewMessages.SelectedItems.Count > 0;

            contextMenuItemShowSelectionAsDigest.IsEnabled = listViewMessages.SelectedItems.Count > 0;
        }

        private void AddMessageAttachments(Message message)
        {
            //throw new NotImplementedException();
        }

        private void UpdateMessageAttachmentsPath(Message message)
        {
            var doc = new HtmlDocument();

            doc.LoadHtml(message.Content);

            List<HtmlNode> filesNodes = doc.DocumentNode.QuerySelectorAll(YahooGroupsServer.GetParsingOptionsValue("fizzler_attachment_node")).ToList();

            List<HtmlNode> photosNode = doc.DocumentNode.QuerySelectorAll(YahooGroupsServer.GetParsingOptionsValue("fizzler_photo_attachment_node")).ToList();

            filesNodes.AddRange(photosNode);

            foreach (Attachment attachment in message.Attachments)
            {
                String attachmentName = attachment.Filename.Substring(attachment.Filename.LastIndexOf("\\") + 1);

                attachmentName = attachmentName.Substring(0, attachmentName.LastIndexOf("."));

                foreach (HtmlNode node in filesNodes)
                {
                    string fileName = null;

                    HtmlNode aNode = node.QuerySelector("a");

                    HtmlNode imgNode = node.QuerySelector("img");

                    // Attachment is photo.
                    if (aNode != null && imgNode != node)
                    {
                        fileName = node.QuerySelector("img").Attributes["alt"].Value;
                    }
                    // Attachment is file.
                    else
                    {
                        fileName = WebUtility.HtmlDecode(node.InnerText);
                    }

                    if (!fileName.Equals(attachmentName)) continue;

                    UpdateAttachmentPath(node, attachment.Filename);

                    break;
                }
            }

            message.Content = doc.DocumentNode.InnerHtml;
        }

        private void UpdateAttachmentPath(HtmlNode node, string fileName)
        {
            HtmlNode aNode = node.QuerySelector("a");

            HtmlNode imgNode = node.QuerySelector("img");

            if (aNode != null && imgNode != node)
            {
                node.QuerySelector("img").Attributes["src"].Value = fileName;
            }
            else
            {
                node.Attributes["href"].Value = fileName;
            }
        }

        private void menuItemExportToFolder_Click(object sender, RoutedEventArgs e)
        {
            ExportToFolderRequest();
        }

        public void SetFont(string fontName)
        {
            FontFamily = new FontFamily(fontName);
        }

        private void listViewGridViewColumnHeader_Click(object sender, RoutedEventArgs e)
        {
        }

        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
        internal interface IServiceProvider
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            object QueryService(ref Guid guidService, ref Guid riid);
        }
        static readonly Guid SID_SWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");

        private void menuItemPrintMessage_Click(object sender, RoutedEventArgs e)
        {
            IServiceProvider serviceProvider = null;

            if (webBrowserMessage.Document != null)
            {
                serviceProvider = (IServiceProvider)webBrowserMessage.Document;
            }

            Guid serviceGuid = SID_SWebBrowserApp;

            Guid iid = typeof(SHDocVw.IWebBrowser2).GUID;

            object NullValue = null;

            var target = (SHDocVw.IWebBrowser2) serviceProvider.QueryService(ref serviceGuid, ref iid);

            target.ExecWB(SHDocVw.OLECMDID.OLECMDID_PRINTPREVIEW, SHDocVw.OLECMDEXECOPT.OLECMDEXECOPT_DODEFAULT, ref NullValue, ref NullValue);
        }

        private void menuItemExportSelectedToTheDatabase_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.SaveFileDialog
            {
                FileName = "",
                DefaultExt = ".db3",
                Filter = "PGOffline 4 database file (.db3)|*.db3"
            };

            if (dlg.ShowDialog() == true)
            {
                ExportSelectedMessagesRequest(dlg.FileName);
            }
        }

        private void menuItemDeleteGroup_Click(object sender, RoutedEventArgs e)
        {
            DeleteDiscussionGroupRequest(listViewGroups.SelectedItem as DiscussionGroup);

            listViewMessages.ItemsSource = null;

            listViewMessages.Items.Refresh();

            buttonStart.IsEnabled = false;
            buttonSettings.IsEnabled = false;
            buttonStats.IsEnabled = false;
            buttonSearch.IsEnabled = (listViewMessages.Items.Count > 0);
        }

        public int DisplayedMessagesCount()
        {
            return listViewMessages.Items.Count;
        }

        private void menuItemCreateDatabase_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.SaveFileDialog
            {
                FileName = "",
                DefaultExt = ".db3",
                Filter = "PGOffline 4 database file (.db3)|*.db3"
            };

            if (dlg.ShowDialog() == true)
                OpenDatabaseRequest(dlg.FileName);
        }

        Visibility _toolbarCaptionsVisibility = Visibility.Visible;

        public Visibility ToolbarCaptionsVisibility
        {
            get
            {
                return _toolbarCaptionsVisibility;
            }

            set
            {
                _toolbarCaptionsVisibility = value;

                OnPropertyChanged("ToolbarCaptionsVisibility");
            }
        }

        int _toolbarButtonSize = 32;

        public int ToolbarButtonSize
        {
            get
            {
                return _toolbarButtonSize;
            }

            set
            {
                _toolbarButtonSize = value;

                OnPropertyChanged("ToolbarButtonSize");
            }
        }

        private void menuItemHideToolbarCaptions_Click(object sender, RoutedEventArgs e)
        {
            SetToolbarCaptionsVisibility(!menuItemHideToolbarCaptions.IsChecked);

            ToolbarCaptionsVisibilityChanged(!menuItemHideToolbarCaptions.IsChecked);
        }

        private void menuItemSmallToolbarButtons_Click(object sender, RoutedEventArgs e)
        {
            SetToolbarSmallButtons(menuItemSmallToolbarButtons.IsChecked);

            ToolbarButtonsSizeChanged(menuItemSmallToolbarButtons.IsChecked);
        }

        public void SetToolbarCaptionsVisibility(bool visible)
        {
            ToolbarCaptionsVisibility = visible ? Visibility.Visible : Visibility.Collapsed;

            menuItemHideToolbarCaptions.IsChecked = !visible;
        }

        public void SetToolbarSmallButtons(bool small)
        {
            ToolbarButtonSize = small ? 20 : 32;

            menuItemSmallToolbarButtons.IsChecked = small;
        }

        private void ApplySort()
        {
            // get saved sort order fro mthe saved options
            if (!Options.Instance.GetAsBoolean("miscellaneous_options_save_sort_order")) return;

            string sortedPropertyName = Options.Instance.GetAsString("messages_list_view_sort_property");

            ListSortDirection direction = Options.Instance.GetAsBoolean("messages_list_view_sort_direction_asc") ? ListSortDirection.Ascending : ListSortDirection.Descending;

            //GridViewSort.ApplySort(listViewMessages.Items, sortedPropertyName, listViewMessages, Options.Instance.GetAsBoolean("messages_list_view_sort_direction_asc"));

            if (sortedPropertyName == null || typeof (Message).GetProperty(sortedPropertyName) == null) return;

            if (listViewMessages.ItemsSource == null) return;

            // restore the saved sort order to the view
            ICollectionView dataView = CollectionViewSource.GetDefaultView(listViewMessages.ItemsSource);

            if (dataView == null) return;
            
            dataView.SortDescriptions.Clear();

            dataView.SortDescriptions.Add(new SortDescription(sortedPropertyName, direction));

            dataView.Refresh();
        }

        private void ReplyClicked(object sender, RoutedEventArgs e)
        {
            var msg = (Message) listViewMessages.SelectedItem;

            if (msg != null)
            {
                ReplyMessageRequest(msg);
            }
        }

        private void hideQuoteString_Click(object sender, RoutedEventArgs e)
        {
            HideQuoteStringRequest(HideQuoteStringToggleButton.IsChecked == true);
        }

        public void SetQuoteFiltering(bool value)
        {
            HideQuoteStringToggleButton.IsChecked = value;
        }

        public void UpdateWindowTitle()
        {
            try
            {
                string licenseText = (License.Instance.LicenseType == LicenseTypeEnum.Trial) ? "(TRIAL)" : string.Empty;

                this.Title = string.Format("{0} {1} {2}", WindowTitle, licenseText, (string.IsNullOrEmpty(DatabaseTitle) ? string.Empty : DatabaseTitle));
            }
            catch (Exception ex)
            {
                //throw
            }
        }
        public void SetTitle(string title)
        {
            string licenseText = (License.Instance.LicenseType == LicenseTypeEnum.Trial) ? "(TRIAL)" : string.Empty;

            WindowTitle = title;

            this.Title = string.Format("{0} {1} {2}", title, licenseText, (string.IsNullOrEmpty(DatabaseTitle) ? string.Empty : DatabaseTitle));
        }
        public void SetDatabaseTitle(string title)
        {
            //DatabaseTitle = (string.IsNullOrEmpty(title) ? string.Empty : title);
            
            //this.Title = string.Format("{0} {1}", "", title);
        }

        private void menuItemEncodingDefault_Click(object sender, RoutedEventArgs e)
        {
            SetEncoding(Encoding.Default);
        }

        private void menuItemEncodingWindows1251_Click(object sender, RoutedEventArgs e)
        {
            SetEncoding(Encoding.GetEncoding("windows-1251"));
        }

        private void menuItemEncodingWindows1252_Click(object sender, RoutedEventArgs e)
        {
            SetEncoding(Encoding.GetEncoding("windows-1252"));
        }

        private void menuItemEncodingKoi8R_Click(object sender, RoutedEventArgs e)
        {
            SetEncoding(Encoding.GetEncoding("koi8-r"));
        }

        private void menuItemEncodingGreek_Click(object sender, RoutedEventArgs e)
        {
            SetEncoding(Encoding.GetEncoding("greek"));
        }

        private void menuItemEncodingHebrew_Click(object sender, RoutedEventArgs e)
        {
            SetEncoding(Encoding.GetEncoding("hebrew"));
        }

        private void menuItemEncodingUtf8_Click(object sender, RoutedEventArgs e)
        {
            SetEncoding(Encoding.UTF8);
        }
        
        private void CommandBinding_MenuItemRegisterProduct(object sender, ExecutedRoutedEventArgs e)
        {
            var registrationForm = new FormRegistration();

            registrationForm.ShowDialog();
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
        }
    }
    
}