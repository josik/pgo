﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using PGOffline.Model;
using PGOffline.Presenter;
using PGOffline.View;

namespace PGOffline
{
    public partial class FormExportToFolder : Window, IFormExportToFolder
    {
        private int _groupsChecked { get; set; }

        public event ExportRequestEventHandler ExportRequest;

        public FormExportToFolder()
        {
            InitializeComponent();

            buttonExport.IsEnabled = false;
        }

        void buttonExport_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxDestinationPath.Text.Length == 0)
            {
                MessageBox.Show("Please input destination path.", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return;
            }

            buttonClose.IsEnabled = false;

            List<ExportUnit> units = listViewGroups.Items.Cast<ExportUnit>().Where(unit => unit.SetToExport).ToList();

            ExportSettings settings = new ExportSettings
            {
                CopyAttachments = checkBoxCopyAttachments.IsChecked != null && (bool) checkBoxCopyAttachments.IsChecked,
                ExportFavoriteStatus = checkBoxExportFavStatus.IsChecked != null && (bool) checkBoxExportFavStatus.IsChecked
            };

            if (radioButtonExportAll.IsChecked != null && (bool)radioButtonExportAll.IsChecked) settings.ExportType = ExportType.AllMessages;

            if (radioButtonExportFavorites.IsChecked != null && (bool)radioButtonExportFavorites.IsChecked) settings.ExportType = ExportType.OnlyFavorites;

            if (radioButtonFavsIDs.IsChecked != null && (bool)radioButtonFavsIDs.IsChecked) settings.ExportType = ExportType.OnlyFavoritesNumbers;

            ExportRequest(textBoxDestinationPath.Text, units, settings);

            buttonExport.IsEnabled = true;
            buttonClose.IsEnabled = true;
        }

        void buttonBrowse_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog
            {
                FileName = "",
                DefaultExt = ".db3",
                Filter = "PGOffline 4 database file (.db3)|*.db3"
            };

            bool? result = dlg.ShowDialog();

            if (result == true)
            {
                textBoxDestinationPath.Text = dlg.FileName;
            }
        }

        void listViewGroupsItem_MouseDoubleClick(object sender, RoutedEventArgs e)
        {
            ExportUnit unit = (ExportUnit) listViewGroups.SelectedItem;

            if (unit == null)
            {
                return;
            }

            FormRanges formRanges = new FormRanges(unit.StartExportFromNumber, unit.EndExportAtNumber);

            FormExportRangesPresenter formRangesPresenter = new FormExportRangesPresenter(formRanges, unit);

            formRangesPresenter.SetRangeComplete += new FormExportRangesPresenter.SetRangeCompleteEventHandler(OnSetRangeComplete);

            formRanges.ShowDialog();            
        }

        private void OnSetRangeComplete()
        {
            listViewGroups.Items.Refresh();
        }

        public void SetDiscussionGroups(List<DiscussionGroup> groups)
        {
            List<ExportUnit> units = (from @group in groups where @group.MessagesCount > 0 select new ExportUnit(@group)).ToList();

            listViewGroups.ItemsSource = units;

            listViewGroups.Items.Refresh();
        }

        private void GroupCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            _groupsChecked++;

            if (textBoxDestinationPath.Text.Length > 0)
            {
                buttonExport.IsEnabled = true;
            }
        }

        private void GroupCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            _groupsChecked--;

            if (_groupsChecked == 0 || textBoxDestinationPath.Text.Length == 0)
            {
                buttonExport.IsEnabled = false;
            }
        }

        private void buttonExport_Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
