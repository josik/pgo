﻿using PGOffline.View;
using System;
using System.Windows;
using System.Windows.Forms;
using MessageBox = System.Windows.MessageBox;

namespace PGOffline
{
    public partial class FormOptions : Window, IFormOptionsView
    {
        public event SaveImportOptionsRequestEventHandler SaveOptionsRequest;

        public FormOptions()
        {
            InitializeComponent();
        }

        #region Download options

        public bool EnableDownloadLimitSpeed
        {
            get
            {
                return (bool)checkBoxEnableSpeedLimit.IsChecked;
            }

            set
            {
                checkBoxEnableSpeedLimit.IsChecked = value;
            }
        }

        public int DownloadLimitSpeedType
        {
            get
            {
                return radioButtonSpeed.IsChecked == true ? 0 : 1;
            }

            set
            {
                if (value == 0)
                    radioButtonSpeed.IsChecked = true;
                else
                    radioButtonRepeat.IsChecked = true;
            }
        }

        public int MessagesPerMinute
        {
            get
            {
                int speed;

                return int.TryParse(textBoxSpeed.Text, out speed) ? speed : 1;
            }

            set
            {
                textBoxSpeed.Text = value.ToString();
            }
        }

        public int InitialDelay
        {
            get
            {
                int delay;

                return int.TryParse(textBoxDelay.Text, out delay) ? delay : 1;
            }

            set
            {
                textBoxDelay.Text = value.ToString();
            }
        }

        public int Download
        {
            get
            {
                int download;

                return int.TryParse(textBoxDownloadMessagesAmout.Text, out download) ? download : 1;
            }

            set
            {
                textBoxDownloadMessagesAmout.Text = value.ToString();
            }
        }

        public int ThenWait
        {
            get
            {
                int minutes;

                return int.TryParse(textBoxWaitMinutes.Text, out minutes) ? minutes : 1;
            }

            set
            {
                textBoxWaitMinutes.Text = value.ToString();
            }
        }
        
        public bool EnableCacheServer
        {
            get
            {
                return (bool)checkBoxEnableRemoteCaching.IsChecked;
            }

            set
            {
                checkBoxEnableRemoteCaching.IsChecked = value;
            }
        }

        public string CacheServerName
        {
            get
            {
                return textBoxCacheServerName.Text;
            }

            set
            {
                textBoxCacheServerName.Text = value;
            }
        }

        public bool EnableMessagesCaching
        {
            get
            {
                return (bool)checkBoxEnableMessageCaching.IsChecked;
            }

            set
            {
                checkBoxEnableMessageCaching.IsChecked = value;
            }
        }

        public int ReceiveMessagesPerRequest
        {
            get
            {
                int messages;

                return int.TryParse(textBoxMessagesPerRequestAmount.Text, out messages) ? messages : 30;
            }

            set
            {
                textBoxMessagesPerRequestAmount.Text = value.ToString();
            }
        }

        #endregion

        #region WorkflowOptions

        public int OnFailureReRequestMessageTimes
        {
            get
            {
                int value;

                if (int.TryParse(textBoxReRequestTimes.Text, out value))
                    return value;

                return 0;
            }

            set
            {
                textBoxReRequestTimes.Text = value.ToString();
            }
        }

        public bool OnLockoutResumeAfterMinutesEnabled
        {
            get
            {
                return (bool)checkBoxResumeAfter.IsChecked;
            }

            set
            {
                checkBoxResumeAfter.IsChecked = value;
            }
        }

        public int OnLockoutResumeAfterMinutes
        {
            get
            {
                int value;

                if (int.TryParse(textBoxMinutes.Text, out value))
                    return value;

                return 0;
            }

            set
            {
                textBoxMinutes.Text = value.ToString();
            }
        }

        public bool StopAfterReceivingInvalidPagesEnabled
        {
            get
            {
                return (bool)checkBoxStopAfterReceiving.IsChecked;
            }

            set
            {
                checkBoxStopAfterReceiving.IsChecked = value;
            }
        }

        public int StopAfterReceivingInvalidPages
        {
            get
            {
                int value;

                if (int.TryParse(textBoxInvalidPages.Text, out value))
                    return value;

                return 0;
            }

            set
            {
                textBoxInvalidPages.Text = value.ToString();
            }
        }

        public bool SkipAuthorization
        {
            get
            {
                return (bool)checkBoxSkipAuthorization.IsChecked;
            }

            set
            {
                checkBoxSkipAuthorization.IsChecked = value;
            }
        }

        #endregion

        #region MailOptions
        public string SmtpServer
        {
            get { return textBoxSMTPServer.Text; }
            set { textBoxSMTPServer.Text = value; }
        }

        public int SmtpPort
        {
            get
            {
                int smtpPort = 25;
                int.TryParse(textBoxSMTPPort.Text, out smtpPort);
                return smtpPort;
            }

            set
            {
                textBoxSMTPPort.Text = value.ToString();
            }
        }

        public string SmtpLogin
        {
            get { return textBoxSMTPLogin.Text; }
            set { textBoxSMTPLogin.Text = value; }
        }

        public string SmtpPassword
        {
            get { return textBoxSMTPPassword.Password; }
            set { textBoxSMTPPassword.Password = value; }
        }

        public string From
        {
            get { return textBoxFrom.Text; }
            set { textBoxFrom.Text = value; }
        }

        public bool EnableSsl
        {
            get { return (bool)checkBoxEnableSsl.IsChecked; }
            set { checkBoxEnableSsl.IsChecked = value; }
        }

        public bool UseExternalMailClient { get; set; }
        public string ExternalMailClientPath { get; set; }
        #endregion

        #region MiscellaneousOptions

        public int MarkMessagesAsReadAfterSeconds
        {
            get
            {
                int seconds;

                if (int.TryParse(textBoxSeconds.Text, out seconds))
                    return seconds;

                return 0;
            }

            set
            {
                textBoxSeconds.Text = value.ToString();
            }
        }

        public bool MinimizeToSystemTray
        {
            get
            {
                return (bool)checkBoxMinimizeToTray.IsChecked;
            }

            set
            {
                checkBoxMinimizeToTray.IsChecked = value;
            }
        }

        public int ShowEventsOnMainFormMessagesPaneCount
        {
            get
            {
                int eventsCount;

                if (int.TryParse(textBoxEventsCount.Text, out eventsCount))
                    return eventsCount;

                return 0;
            }

            set
            {
                textBoxEventsCount.Text = value.ToString();
            }
        }

        public bool ShowMembersListOutOfDateWarning
        {
            get
            {
                return checkBoxShowOutOfDateWarning.IsChecked == true;
            }

            set
            {
                checkBoxShowOutOfDateWarning.IsChecked = value;
            }
        }

        public bool SaveSortOrderInMessagesPane
        {
            get
            {
                return checkBoxSaveSortOrder.IsChecked == true;
            }

            set
            {
                checkBoxSaveSortOrder.IsChecked = value;
            }
        }

        //public MessagesPaneSortOrder MessagesPaneSortOrder { get; set; }
        public int SortedMessagesPaneIndex { get; set; }

        public bool UseSelectedSortOrderForNewMessagesShow { get; set; }

        public bool LogEventsToFile
        {
            get
            {
                return (bool)checkBoxLogEventsToFile.IsChecked;
            }

            set
            {
                checkBoxLogEventsToFile.IsChecked = value;
            }
        }

        public string LogEventsFilePath
        {
            get
            {
                return textBoxLogFileName.Text;
            }

            set
            {
                textBoxLogFileName.Text = value;
            }
        }

        public string MainWindowPanesFontName
        {
            get
            {
                return textBoxPanesFontName.Text;
            }

            set
            {
                textBoxPanesFontName.Text = value;
            }
        }

        #endregion

        #region RefreshGroupOptions

        public int RefreshGroupType
        {
            get
            {
                if (radioButtonRefreshGroupsOnStartup.IsChecked == true)
                    return 1;

                if (radioButtonRefreshGroupsPeriodically.IsChecked == true)
                    return 2;

                return 0;
            }

            set
            {
                switch (value)
                {
                    case 0:
                        radioButtonRefreshGroupsManually.IsChecked = true;
                        break;

                    case 1:
                        radioButtonRefreshGroupsOnStartup.IsChecked = true;
                        break;

                    case 2:
                        radioButtonRefreshGroupsPeriodically.IsChecked = true;
                        break;
                }
            }
        }

        public int HoursPeriod
        {
            get
            {
                int hours = 0;

                Int32.TryParse(textBoxHoursPeriod.Text, out hours);

                return hours;
            }

            set
            {
                textBoxHoursPeriod.Text = value.ToString();
            }
        }

        public int MinutesPeriod
        {
            get
            {
                int minutes = 0;

                Int32.TryParse(textBoxMinutesPeriod.Text, out minutes);

                return minutes;
            }

            set
            {
                textBoxMinutesPeriod.Text = value.ToString();
            }
        }

        public bool DownloadMessagesPerGroupLimit
        {
            get
            {
                return checkBoxRefreshGroupsDownloadMessages.IsChecked == true;
            }

            set
            {
                checkBoxRefreshGroupsDownloadMessages.IsChecked = value;
            }
        }

        public int DownloadMessagesPerGroupLimitCount
        {
            get
            {
                int messages = 0;

                int.TryParse(textBoxMessagesPerGroupCount.Text, out messages);

                return messages;
            }

            set
            {
                textBoxMessagesPerGroupCount.Text = value.ToString();
            }
        }

        public int RefreshGroupLoopType
        {
            get
            {
                return radioButtonRefreshGroupsLoopAll.IsChecked == true ? 1 : 0;
            }

            set
            {
                switch (value)
                {
                    case 0:
                        radioButtonRefreshGroupsLoopTimes.IsChecked = true;
                        break;
                    case 1:
                        radioButtonRefreshGroupsLoopAll.IsChecked = true;
                        break;
                }
            }
        }

        public int LoopCount
        {
            get
            {
                int count = 0;

                int.TryParse(textBoxLoopCountTimes.Text, out count);

                return count;
            }

            set
            {
                textBoxLoopCountTimes.Text = value.ToString();
            }
        }

        #endregion

        #region RefreshMembersOptions

        public int RefreshMembersType
        {
            get
            {
                if (checkBoxRefreshManually.IsChecked == true)
                    return 0;

                if (checkBoxRefreshOnStartup.IsChecked == true)
                    return 1;

                if (checkBoxPeriodically.IsChecked == true)
                    return 2;

                return 0;
            }

            set
            {
                switch (value)
                {
                    case 0:
                        checkBoxRefreshManually.IsChecked = true;
                        break;
                    case 1:
                        checkBoxRefreshOnStartup.IsChecked = true;
                        break;
                    case 2:
                        checkBoxPeriodically.IsChecked = true;
                        break;
                }
            }
        }
        
        public int RefreshPeriod
        {
            get
            {
                int period;

                return int.TryParse(textBoxHours.Text, out period) ? period : 1;
            }

            set
            {
                textBoxHours.Text = value.ToString();
            }
        }
        
        #endregion

        #region ParsingOptions

            public string FizzlerMessageTitle
            {
                get
                {
                    return textBoxFizzlerMessageTitle.Text;
                }
                set
                {
                    textBoxFizzlerMessageTitle.Text = value;
                }
            }

            public string FizzlerMessageEmail
            {
                get
                {
                    return textBoxFizzlerMessageEmail.Text;
                }
                set
                {
                    textBoxFizzlerMessageEmail.Text = value;
                }
            }

            public string FizzlerMessageSenderName
            {
                get
                {
                    return textBoxFizzlerMessageSenderName.Text;
                }
                set
                {
                    textBoxFizzlerMessageSenderName.Text = value;
                }
            }

            public string FizzlerMessageSenderNameAlternative
            {
                get
                {
                    return textBoxFizzlerMessageSenderNameAlternative.Text;
                }
                set
                {
                    textBoxFizzlerMessageSenderNameAlternative.Text = value;
                }
            }

            public string FizzlerMessageDate
            {
                get
                {
                    return textBoxFizzlerMessageDate.Text;
                }
                set
                {
                    textBoxFizzlerMessageDate.Text = value;
                }
            }

            public string FizzlerMessageContents
            {
                get
                {
                    return textFizzlerMessageContents.Text;
                }
                set
                {
                    textFizzlerMessageContents.Text = value;
                }
            }

            public string RegexMessagesCount
            {
                get
                {
                    return textBoxRegexMessagesCount.Text;
                }
                set
                {
                    textBoxRegexMessagesCount.Text = value;
                }
            }

            public string FizzlerAttachmentNode
            {
                get
                {
                    return textBoxFizzlerAttachmentNode.Text;
                }
                set
                {
                    textBoxFizzlerAttachmentNode.Text = value;
                }
            } 
            public string FizzlerAttachmentOriginalMessage
            {
                get
                {
                    return textBoxFizzlerAttachmentOriginalMessage.Text;
                }
                set
                {
                    textBoxFizzlerAttachmentOriginalMessage.Text = value;
                }
            } 
            public string RegexAttachmentsCount
            {
                get
                {
                    return  textBoxRegexAttachmentsCount.Text;
                }
                set
                {
                    textBoxRegexAttachmentsCount.Text = value;
                }
            } 

            public string FizzlerMembersTableRow
            {
                get
                {
                    return textBoxFizzlerMembersTableRow.Text;
                }
                set
                {
                    textBoxFizzlerMembersTableRow.Text = value;
                }
            } 
            public string FizzlerMemberName
            {
                get
                {
                    return textBoxFizzlerMemberName.Text;
                }
                set
                {
                    textBoxFizzlerMemberName.Text = value;
                }
            } 
            public string FizzlerMemberNameAlternative
            {
                get
                {
                    return textBoxFizzlerMemberNameAlternative.Text;
                }
                set
                {
                    textBoxFizzlerMemberNameAlternative.Text = value;
                }
            } 
            public string FizzlerMemberEmail
            {
                get
                {
                    return textBoxFizzlerMemberEmail.Text;
                }
                set
                {
                    textBoxFizzlerMemberEmail.Text = value;
                }
            } 
            public string FizzlerMemberJoinDate
            {
                get
                {
                    return textBoxFizzlerMemberJoinDate.Text;
                }
                set
                {
                    textBoxFizzlerMemberJoinDate.Text = value;
                }
            } 
            public string RegexMembersCount
            {
                get
                {
                    return textBoxRegexMembersCount.Text;
                }
                set
                {
                    textBoxRegexMembersCount.Text = value;
                }
            } 

            public string FizzlerFileUri
            {
                get
                {
                    return textBoxFizzlerFileUri.Text;
                }
                set
                {
                    textBoxFizzlerFileUri.Text = value;
                }
            } 

            public string FizzlerPhotoDiv
            {
                get
                {
                    return textBoxFizzlerPhotoDiv.Text;
                }
                set
                {
                    textBoxFizzlerPhotoDiv.Text = value;
                }
            } 
            public string FizzlerPhotoUri
            {
                get
                {
                    return textBoxFizzlerPhotoUri.Text;
                }
                set
                {
                    textBoxFizzlerPhotoUri.Text = value;
                }
            } 
            public string FizzlerPhotoThumbnail
            {
                get
                {
                    return textBoxFizzlerPhotoThumbnail.Text;
                }
                set
                {
                    textBoxFizzlerPhotoThumbnail.Text = value;
                }
            }
            public string FizzlerAlbumLink
            {
                get
                {
                    return textBoxFizzlerAlbumLink.Text;
                }
                set
                {
                    textBoxFizzlerAlbumLink.Text = value;
                }
            }
            public string FizzlerAlbumLinkSecondary
            {
                get
                {
                    return textBoxFizzlerAlbumLinkSecond.Text;
                }
                set
                {
                    textBoxFizzlerAlbumLinkSecond.Text = value;
                }
            }
            public string FizzlerUnfoundGroupName
            {
                get
                {
                    return textBoxFizzlerUnfoundGroupName.Text;
                }
                set
                {
                    textBoxFizzlerUnfoundGroupName.Text = value;
                }
            }
            public string FizzlerUnfoundGroupCriterion
            {
                get
                {
                    return textBoxFizzlerUnfoundGroupCriterion.Text;
                }
                set
                {
                    textBoxFizzlerUnfoundGroupCriterion.Text = value;
                }
            }
        #endregion

        #region ClickEvents

        private void buttonSaveOptions_Click(object sender, RoutedEventArgs e)
        {
            if (RefreshGroupType == 2 && (HoursPeriod < 0 || MinutesPeriod < 0 || (HoursPeriod <= 0 && MinutesPeriod <= 0)))
            {
                MessageBox.Show("Please input correct hours and minutes values.");
            }
            else
            {
                SaveOptionsRequest();

                Close();
            }
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void buttonChooseFont_Click(object sender, RoutedEventArgs e)
        {
            FontDialog fontDialog = new FontDialog();

            DialogResult result = fontDialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK) return;

            textBoxPanesFontName.Text = fontDialog.Font.Name;
        }

        private void buttonBrowse_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();

            DialogResult result = saveFileDialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK) return;

            textBoxLogFileName.Text = saveFileDialog.FileName;
        }

        #endregion ClickEvents

    }
}
