﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using PGOffline.View;

namespace PGOffline
{
    public partial class FormSaveSearchParams : Window, IFormSaveSearchParams
    {
        public event SaveSearchParamsHandler SaveSearchParams;

        public FormSaveSearchParams()
        {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            SaveSearchParams(textBoxName.Text);
        }
    }
}
