﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using PGOffline.Model;
using PGOffline.View;

namespace PGOffline
{
    public partial class FormContactEditor : Window, IFormContactEditorView
    {
        public event ContactSaveRequestEventHandler ContactSaveRequest;

        public FormContactEditor()
        {
            InitializeComponent();
        }

        public string Name
        {
            get
            {
                return textBoxName.Text;
            }

            set
            {
                textBoxName.Text = value;
            }
        }

        public string Email
        {
            get
            {
                return textBoxEmail.Text;
            }

            set
            {
                textBoxEmail.Text = value;
            }
        }

        public string Comment
        {
            get
            {
                return textBoxComments.Text;
            }

            set
            {
                textBoxComments.Text = value;
            }
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            ContactSaveRequest();
            Close();
        }
    }
}
