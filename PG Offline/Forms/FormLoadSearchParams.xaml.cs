﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using PGOffline.View;

namespace PGOffline
{
    public partial class FormLoadSearchParams : Window, IFormLoadSearchParams
    {
        public event LoadSearhParamHandler LoadSearchParam;
        public event RemoveSearchParamHandler RemoveSearchParam;

        public FormLoadSearchParams()
        {
            InitializeComponent();
        }

        public IEnumerable<string> SearchParamsNames
        {
            set
            {
                listViewSaves.ItemsSource = value;
            }
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonLoad_Click(object sender, RoutedEventArgs e)
        {
            LoadSearchParam((string)listViewSaves.SelectedItem);
        }

        private void buttonRemove_Click(object sender, RoutedEventArgs e)
        {
            RemoveSearchParam((string)listViewSaves.SelectedItem);
        }
    }
}
