﻿using PGOffline.View;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;

namespace PGOffline
{
    public partial class FormImportFromFolder : Window, IFormImportFromFolder
    {
        public event BrowseDatabaseRequestEventHandler BrowseDatabaseRequest;
        public event GetGroupsListRequestEventHandler GetGroupsListRequest;
        public event ImportDataRequestEventHandler ImportDataRequest;
        public event ChangeRangeRequestEventHandler ChangeRangeRequest;
        public event ImportCancelRequestEventHander ImportCancelRequest;
        public event SaveImportFromMdbOptionsRequestEventHandler SaveImportFromMdbOptionsRequest;
        public event LoadImportFromMdbOptionsRequestEventHandler LoadImportFromMdbOptionsRequest;

        public FormImportFromFolder()
        {
            InitializeComponent();

            EnableImportButton();

            DisableProgressBar();
        }

        double progress = 0;
        public double ProgressValue
        {
            get { return progress; }
            set { progress = value / 100; }
        }
        private void progressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ProgressValue = ProgressBar.Value;
        }
        public void UpdateProgress(double progressValue)
        {
            ProgressBar.Value = progressValue;
        }

        private void buttonBrowse_Click(object sender, RoutedEventArgs e)
        {
            BrowseDatabaseRequest();
        }

        public void DisplayGroups(IEnumerable<ImportUnit> groups)
        {
            listViewGroups.ItemsSource = groups;

            listViewGroups.Items.Refresh();
        }

        public void SetDatabasePath(string path)
        {
            textBoxSourcePath.Text = path;
        }

        private void buttonGetList_Click(object sender, RoutedEventArgs e)
        {
            GetGroupsListRequest(textBoxSourcePath.Text);
        }

        private void buttonImport_Click(object sender, RoutedEventArgs e)
        {


            List<ImportUnit> units = new List<ImportUnit>();

            foreach (ImportUnit unit in listViewGroups.Items)
            {
                if (unit.SetToImport)
                {
                    units.Add(unit);
                }
            }

            ImportSettings settings = new ImportSettings
            {
                CopyAttachments = checkBoxCopyAttachments.IsChecked != null && (bool) checkBoxCopyAttachments.IsChecked,
                ImportFavoriteStatus = checkBoxImportFavStatus.IsChecked != null && (bool) checkBoxImportFavStatus.IsChecked,
                RememberLastMessageNumber = checkBoxStartFromLast.IsChecked != null && (bool) checkBoxStartFromLast.IsChecked
            };

            if (radioButtonImportAll.IsChecked != null && (bool)radioButtonImportAll.IsChecked) settings.ImportType = ImportType.AllMessages;

            if (radioButtonImportFavorites.IsChecked != null && (bool)radioButtonImportFavorites.IsChecked) settings.ImportType = ImportType.OnlyFavorites;

            if (radioButtonFavsIDs.IsChecked != null && (bool)radioButtonFavsIDs.IsChecked) settings.ImportType = ImportType.OnlyFavoritesNumbers;

            if (units.Count == 0 || !File.Exists(textBoxSourcePath.Text)) return;

            ImportDataRequest(units, textBoxSourcePath.Text, settings);
        }

        private void listViewGroupsItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ChangeRangeRequest((ImportUnit)listViewGroups.SelectedItem);
        }

        public void RefreshImportUnits()
        {
            listViewGroups.Items.Refresh();
        }

        private void buttonStop_Click(object sender, RoutedEventArgs e)
        {
            ImportCancelRequest();
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void EnableImportButton()
        {
            buttonImport.IsEnabled = true;

            buttonStop.IsEnabled = false;

            buttonClose.IsEnabled = true;
        }
        
        public void DisableImportButton()
        {
            buttonImport.IsEnabled = false;

            buttonStop.IsEnabled = true;

            buttonClose.IsEnabled = false;
        }

        public void EnableProgressBar()
        {
            //ProgressBar.Visibility = Visibility.Visible;

            //ProgressBarPercentage.Visibility = Visibility.Visible;
        }
        
        public void DisableProgressBar()
        {
            //ProgressBar.Visibility = Visibility.Hidden;

            //ProgressBarPercentage.Visibility = Visibility.Hidden;
        }
        
        public bool CopyAttachments
        {
            get
            {
                return checkBoxCopyAttachments.IsChecked != null && (bool)checkBoxCopyAttachments.IsChecked;
            }
            set
            {
                checkBoxCopyAttachments.IsChecked = value;
            }
        }

        public bool RememberLastMessageNumber
        {
            get
            {
                return checkBoxStartFromLast.IsChecked != null && (bool)checkBoxStartFromLast.IsChecked;
            }
            set
            {
                checkBoxStartFromLast.IsChecked = value;
            }
        }
        
        public bool ImportFavoriteStatus
        {
            get
            {
                return checkBoxImportFavStatus.IsChecked != null && (bool)checkBoxImportFavStatus.IsChecked;
            }
            set
            {
                checkBoxImportFavStatus.IsChecked = value;
            }
        }

        public ImportType ImportType
        {
            get
            {
                if (radioButtonFavsIDs.IsChecked != null && (bool)radioButtonFavsIDs.IsChecked) return ImportType.OnlyFavoritesNumbers;

                if (radioButtonImportAll.IsChecked != null && (bool)radioButtonImportAll.IsChecked) return ImportType.AllMessages;

                if (radioButtonImportFavorites.IsChecked != null && (bool)radioButtonImportFavorites.IsChecked) return ImportType.OnlyFavorites;

                return ImportType.AllMessages;
            }
            set
            {
                radioButtonFavsIDs.IsChecked = false;

                radioButtonImportAll.IsChecked = false;

                radioButtonImportFavorites.IsChecked = false;

                if (value == ImportType.OnlyFavoritesNumbers) radioButtonFavsIDs.IsChecked = true;

                if (value == ImportType.AllMessages) radioButtonImportAll.IsChecked = true;

                if (value == ImportType.OnlyFavorites) radioButtonImportFavorites.IsChecked = true;
            }
        }

        public string DatabasePath
        {
            get 
            {
                return textBoxSourcePath.Text;
            }
            set
            {
                textBoxSourcePath.Text = value;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadImportFromMdbOptionsRequest();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            textBoxSourcePath.Text = string.Empty;

            SaveImportFromMdbOptionsRequest();
        }

    }
}
