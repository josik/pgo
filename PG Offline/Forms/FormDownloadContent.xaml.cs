﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using PGOffline.View;
using Asfdfdfd.Api.Yahoo.Groups;

namespace PGOffline
{
    public partial class FormDownloadContent : Window, IFormDownloadContentView
    {
        public event GetContentListRequestEventHandler GetContentListRequest;
        public event DownloadContentRequestEventHandler DownloadContentRequest;
        public event CloseDownloadContentFormRequestEventHandler CloseDownloadContentFormRequest;

        public FormDownloadContent()
        {
            InitializeComponent();
        }

        public void SetWindowTitle(string title)
        {
            this.Title = title;
        }

        private void buttonDownload_Click(object sender, RoutedEventArgs e)
        {
            DownloadContentRequest();
        }

        private void buttonListContent_Click(object sender, RoutedEventArgs e)
        {
            GetContentListRequest();
            buttonListContent.IsEnabled = false;
        }

        public void PopulateTreeView(List<object> filesDirectories)
        {
            treeView1.ItemsSource = filesDirectories;
        }

        public List<object> GetSelectedItems()
        {
            if (treeView1.Items.Count <= 0) return null;
            if (treeView1.Items[0].GetType() == typeof(YahooGroupAlbumOfPhotos))
                return GetPhotos();
            return GetFiles();
        }

        private List<object> GetFiles()
        {
            List<YahooGroupFile> files = new List<YahooGroupFile>();
            List<YahooGroupDirectory> directoriesToReview = new List<YahooGroupDirectory>();
            foreach (var item in treeView1.Items)
            {
                if (item.GetType() == typeof(YahooGroupDirectory))
                    directoriesToReview.Add((YahooGroupDirectory)item);

                if (item.GetType() != typeof (YahooGroupFile)) continue;
                if (((YahooGroupFile)item).IsChecked)
                    files.Add((YahooGroupFile)item);
            }

            while (directoriesToReview.Count > 0)
            {
                List<YahooGroupDirectory> childDirectories = new List<YahooGroupDirectory>();

                foreach (YahooGroupDirectory directory in directoriesToReview)
                {
                    childDirectories.AddRange(directory.ChildDirectories);
                    foreach (YahooGroupFile file in directory.Files)
                        if (file.IsChecked)
                            files.Add(file);
                }

                directoriesToReview.Clear();
                directoriesToReview.AddRange(childDirectories);
            }

            return files.ToList<object>();            
        }

        private List<object> GetPhotos()
        {
            var albums = new List<YahooGroupAlbumOfPhotos>(treeView1.Items.Cast<YahooGroupAlbumOfPhotos>());
            return albums.ToList<object>();
        }
        
        public void DisableDownloadControls()
        {
            buttonDownload.IsEnabled = false;
            buttonSelectAll.IsEnabled = false;
            buttonSelectNone.IsEnabled = false;
        }

        public void EnableDownloadControls()
        {
            buttonDownload.IsEnabled = true;
            buttonSelectAll.IsEnabled = true;
            buttonSelectNone.IsEnabled = true;
        }

        public void CloseForm()
        {
            this.Close();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            CloseDownloadContentFormRequest();
        }

        private void buttonSelectAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in treeView1.Items)
            {
                if (item.GetType() == typeof(YahooGroupDirectory))
                    ((YahooGroupDirectory)item).IsChecked = true;

                if (item.GetType() == typeof(YahooGroupFile))
                    ((YahooGroupFile)item).IsChecked = true;

                if (item.GetType() == typeof(YahooGroupAlbumOfPhotos))
                    ((YahooGroupAlbumOfPhotos)item).IsChecked = true;
            }
        }

        private void buttonSelectNone_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in treeView1.Items)
            {
                if (item.GetType() == typeof(YahooGroupDirectory))
                    ((YahooGroupDirectory)item).IsChecked = false;

                if (item.GetType() == typeof(YahooGroupFile))
                    ((YahooGroupFile)item).IsChecked = false;

                if (item.GetType() == typeof(YahooGroupAlbumOfPhotos))
                    ((YahooGroupAlbumOfPhotos)item).IsChecked = false;
            }
        }
    }
}
