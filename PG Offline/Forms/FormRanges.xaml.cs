﻿using PGOffline.View;
using System;
using System.Windows;

namespace PGOffline
{
    public partial class FormRanges : Window, IFormRanges
    {
        public event SetRangeRequestEventHandler SetRangeRequest;

        public FormRanges(long leftEdge, long rightEdge)
        {
            InitializeComponent();

            textBoxStart.Text = leftEdge.ToString();
            textBoxEnd.Text = rightEdge.ToString();
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            SetRangeRequest(Int32.Parse(textBoxStart.Text), Int32.Parse(textBoxEnd.Text));
            this.Close();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
