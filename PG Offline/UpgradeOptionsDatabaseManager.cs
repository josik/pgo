﻿using System.Diagnostics;
using PGOffline.Model;
using System;
using System.Data.SQLite;
using System.IO;
using System.Text.RegularExpressions;

namespace PGOffline
{
    public class UpgradeOptionsDatabaseManager : Singleton<UpgradeOptionsDatabaseManager>
    {
        public bool UpgradeDatabase()
        {
            int targetDatabase = GetExpectedDatabaseVersion();

            int currentDatabase = GetDatabaseVersion();

            int previousVersionAttempt = currentDatabase;

            while (currentDatabase < targetDatabase)
            {
                // backup database before upgrading this version
                if (
                    !Options.Instance.BackupDatabase(Options.Instance.DatabasePath,
                        string.Format("{0}.UpgradeAutoBackup.v{1}.db3", Options.Instance.DatabasePath,
                            currentDatabase))) break;

                SQLiteTransaction transaction = Options.Instance.TransactionBegin();

                switch (currentDatabase)
                {
                    case 2:
                        Upgrade2To3();
                        break;
                    case 1:
                        Upgrade1To2();
                        break;
                    case 0:
                        Upgrade0To1();
                        break;
                }

                transaction.Commit();

                // re-check for current database version, following the trans commit
                currentDatabase = GetDatabaseVersion();

                // detect a failed upgrade and prevent infinite looping
                if (currentDatabase == previousVersionAttempt)
                {
                    // TODO: throw exception?
                    break;
                }
            }

            return GetDatabaseVersion() == targetDatabase;
        }

        private void Upgrade2To3()
        {
            // update existing record values
            Options.Instance.Set("database_version", 3);

            // add new records

            // migrate existing records

            // remove existing records

            // save changes
            Options.Instance.SaveOptions();
        }

        private void Upgrade1To2()
        {
            // update existing record values
            Options.Instance.Set("database_version", 2);

            // add new records

            // migrate existing records

            // remove existing records

            // save changes
            Options.Instance.SaveOptions();
        }

        private void Upgrade0To1()
        {
            // update existing record values

            // add new records
            Options.Instance.Set("database_version", 1);
            
            // migrate existing records

            // remove existing records

            // save changes
            Options.Instance.SaveOptions();
        }

        private int GetDatabaseVersion()
        {
            int databaseVersion;

            try
            {
                var oDatabaseVersion = Options.Instance.ExecuteQueryAndGetValue("SELECT value FROM options WHERE key = 'database_version'", 0);
                databaseVersion = Convert.ToInt16(oDatabaseVersion.ToString());
            }
            catch (Exception)
            {
                // database may be old and not contain database_version, so assume version 0
                databaseVersion = 0;
            }

            return databaseVersion;
        }

        private int GetExpectedDatabaseVersion()
        {
            int existingdatabaseVersion;
            
            try
            {
                //string optionsSchemaPath = PathResolver.GetOptionsSchemaScriptPath();
                string schemaPath = PathResolver.GetOptionsSchemaScriptPath();
                string fileContents = File.ReadAllText(schemaPath);

                // find version within ('database_version', 'XX') found within the .sql file
                Regex versionRegex = new Regex("\\('database_version', '(.*)'\\)");
                Match match = versionRegex.Match(fileContents);

                existingdatabaseVersion = Convert.ToInt16(match.Groups[1].Value);
            }
            catch (Exception ex)
            {
                // cannot retrieve the value, so return 0 so no work will be done.
                existingdatabaseVersion = 0;
            }

            return existingdatabaseVersion;
        }

        public bool IsDatabaseVersionCurrent()
        {
            var existingdatabaseVersion = GetDatabaseVersion();
            
            var expectedDatabaseVersion = GetExpectedDatabaseVersion();
            
            return existingdatabaseVersion >= expectedDatabaseVersion;
        }
    }

}
