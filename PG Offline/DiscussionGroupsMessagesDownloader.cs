﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Asfdfdfd.Api.Yahoo.Groups;
using PGOffline.Model;

namespace PGOffline
{
    public class DiscussionGroupsMessagesDownloader
    {
        public delegate void MessageDownloadedEventHandler(DiscussionGroup group, Message message);
        public event MessageDownloadedEventHandler MessageDownloaded;

        public delegate void FinishedEventHandler();
        public event FinishedEventHandler Finished;

        public delegate void CanceledEventHandler();
        public event CanceledEventHandler Canceled;

        private CancellationTokenSource _tokenSource;
        private CancellationToken _token;

        private TaskFactory _uiFactory = new TaskFactory(TaskScheduler.FromCurrentSynchronizationContext());

        private YahooGroupsServer _yahooServer;

        public DiscussionGroupsMessagesDownloader(YahooGroupsServer server)
        {
            _yahooServer = server;

            _tokenSource = new CancellationTokenSource();

            _token = _tokenSource.Token; 
        }

        private void DownloadNewMessages(List<DiscussionGroup> groups)
        {
            DownloadNewDiscussionGroupsMessages(groups);
        }

        public void DownloadNewMessagesAsync(List<DiscussionGroup> groups)
        {
            Task t = null;

            t = Task.Factory.StartNew(() =>
            {
                try
                {
                    DownloadNewMessages(groups);

                    if (Finished != null)
                    {
                        _uiFactory.StartNew(() =>
                        {
                            Finished();
                        }).Wait();
                    }
                }
                catch (OperationCanceledException)
                {
                    if (Canceled != null)
                    {
                        _uiFactory.StartNew(() =>
                        {
                            Canceled();
                        }).Wait();
                    }
                }
            }, _token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        public void DownloadNewMessages(DiscussionGroup group)
        {
            DownloadNewDiscussionGroupMessages(group);
        }

        public void DownloadNewMessagesAsync(DiscussionGroup group)
        {
            Task t = null;

            t = Task.Factory.StartNew(() =>
            {
                try
                {
                    DownloadNewMessages(group);

                    if (Finished != null)
                    {
                        _uiFactory.StartNew(() =>
                        {
                            Finished();
                        }).Wait();
                    }
                }
                catch (OperationCanceledException)
                {
                    if (Canceled != null)
                    {
                        _uiFactory.StartNew(() =>
                        {
                            Canceled();
                        }).Wait();
                    }
                }
            }, _token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        public void CancelAsync()
        {
            _tokenSource.Cancel();
        }

        public void DownloadNewDiscussionGroupsMessages(List<DiscussionGroup> groups)
        {
            if (_token.IsCancellationRequested) _token.ThrowIfCancellationRequested();

            groups = new List<DiscussionGroup>(groups);

            bool hasGroupMessagesDownloadLimit = Options.Instance.GetAsBoolean("refresh_groups_download_messages_per_group_limit");

            int messagesPerGroupLimit = 0;

            int loopMode = LoopMode.LOOP_NONE;

            int loopCount = 0;

            if (hasGroupMessagesDownloadLimit)
            {
                messagesPerGroupLimit = Options.Instance.GetAsInt("refresh_groups_download_messages_per_group_limit_count");

                loopMode = Options.Instance.GetAsInt("refresh_groups_loop_type");

                if (loopMode == LoopMode.LOOP_TIMES)
                {
                    loopCount = Options.Instance.GetAsInt("refresh_groups_loop_count");
                }
            }

            int currentGroupIndex = 0;

            while (groups.Count > 0)
            {
                if (_token.IsCancellationRequested) 
                    _token.ThrowIfCancellationRequested();

                DiscussionGroup group = groups[currentGroupIndex];

                bool allMessagesDownloadedFromGroup = false;

                if (!group.GroupSettings.SkipDuringRefreshAllGroups)
                {
                    int limit = hasGroupMessagesDownloadLimit ? messagesPerGroupLimit : int.MaxValue;

                    if (_token.IsCancellationRequested) _token.ThrowIfCancellationRequested();

                    allMessagesDownloadedFromGroup = DownloadNewDiscussionGroupMessages(group, limit);

                    if (_token.IsCancellationRequested) _token.ThrowIfCancellationRequested();
                }
                else
                {
                    allMessagesDownloadedFromGroup = true;

                    if (_token.IsCancellationRequested) _token.ThrowIfCancellationRequested();

                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Group '{0}' skipped", group.Name));
                }

                if (_token.IsCancellationRequested) _token.ThrowIfCancellationRequested();

                if (allMessagesDownloadedFromGroup)
                {
                    groups.RemoveAt(currentGroupIndex);
                }
                else
                {
                    if (currentGroupIndex == groups.Count - 1)
                        currentGroupIndex = 0;
                    else
                        currentGroupIndex++;
                }

                if (_token.IsCancellationRequested) _token.ThrowIfCancellationRequested();

                if (!hasGroupMessagesDownloadLimit) continue;

                if (groups.Count <= 0 || currentGroupIndex < groups.Count) continue;

                currentGroupIndex = 0;

                if (loopMode != LoopMode.LOOP_TIMES) continue;

                loopCount--;

                if (loopCount == -1)
                {
                    groups.Clear();
                }
            }
        }

        public bool DownloadNewDiscussionGroupMessages(DiscussionGroup group, long downloadMessagesLimit = int.MaxValue)
        {
            long _downloadMessagesLimit = downloadMessagesLimit;

            if (_downloadMessagesLimit == 0)
            {
                _downloadMessagesLimit = int.MaxValue;
            }

            var messagesDownloader = new DiscussionGroupMessagesDownloader(_yahooServer, group, _downloadMessagesLimit, _token, _uiFactory, MessageDownloaded);

            return messagesDownloader.GetWillDownloadAllMessages();
        }
    }
}
