﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Specialized;
using PGOffline.Model;
using System.Data.SQLite;

namespace PGOffline
{
    class DiscussionGroupMessagesList : IList<Message>, IList, INotifyCollectionChanged, IDisposable
    {
        private DiscussionGroup _group;

        private int _count;
        private long id;

        private List<Message> _messagesByIndex = new List<Message>();

        private Dictionary<long, Message> _messagesByNumber = new Dictionary<long, Message>();

        public DiscussionGroupMessagesList(DiscussionGroup group)
        {
            _group = group;

            LoadMessages();

            _group.MessageAdded += OnMessageAddedToDiscussionGroup;
        }

        private void LoadMessages()
        {
            _messagesByNumber.Clear();
            
            _messagesByIndex.Clear();
            
            try
            {
                using (SQLiteCommand cmd = DatabaseManager.Instance.CreateCommand("select id, number, date, subject, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time from group_message where discussion_group = @group"))
                {
                    cmd.Parameters.AddWithValue("@group", _group.Id);

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            _count++;

#if DEBUG
                            //File.AppendAllText(string.Format("{0}\\DEBUG_LoadMessages.log", AppDomain.CurrentDomain.BaseDirectory), string.Format("group:{0} count:{1}{2}", _group.Id, _count, Environment.NewLine));
#endif

                            object[] values = new object[reader.FieldCount];

                            reader.GetValues(values);

                            Message message = new Message(values);

#if DEBUG
                            //File.AppendAllText(string.Format("{0}\\DEBUG_LoadMessages.log", AppDomain.CurrentDomain.BaseDirectory), string.Format("message.Number:{0} message.Id:{0}{1}", message.Number, message.Id, Environment.NewLine));
#endif
                            id = message.Id;
                            _messagesByNumber.Add(message.Number, message);

                            _messagesByIndex.Add(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var t = ex.Message;
            }
            
        }

        private void OnMessageAddedToDiscussionGroup(Message message)
        {
            // lock (_messagesByNumber)
            // {
                // This check is mandatory because of multithreaded messages downloading.
                // Sometimes messages saved to the database and viewed by user before
                // event fired and received.
                if (_messagesByNumber.Keys.Contains(message.Number)) return;

                _messagesByNumber.Add(message.Number, message);

                _messagesByIndex.Add(message);

                _count++;

            if (CollectionChanged != null)
            {
                CollectionChanged(this,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, message, _count - 1));
            }
            // }
        }

        public void Dispose()
        {
            _group.MessageAdded -= OnMessageAddedToDiscussionGroup;
        }

        #region IList<Message> Members

        public int IndexOf(Message item)
        {
            return DatabaseManager.Instance.ExecuteQueryAndGetInt(string.Format("SELECT COUNT(*) FROM group_message WHERE discussion_group = {0} AND number < {1}", _group.Id, item.Number));
        }

        public void Insert(int index, Message item)
        {
 	        throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
 	        throw new NotImplementedException();
        }

        public Message this[int index]
        {
	        get 
	        {
                return _messagesByIndex[index];
            }

	        set 
	        { 
		        throw new NotImplementedException(); 
	        }
        }

        #endregion

        #region ICollection<Message> Members

        public void Add(Message item)
        {
 	        throw new NotImplementedException();
        }

        public void Clear()
        {
 	        throw new NotImplementedException();
        }

        public bool Contains(Message item)
        {
            return DatabaseManager.Instance.ExecuteQueryAndGetInt(String.Format("select count(*) from group_message where discussion_group = {0} and id = {1}", _group.Id, item.Id)) != 0;
        }

        public void CopyTo(Message[] array, int arrayIndex)
        {
 	        throw new NotImplementedException();
        }

        public int Count
        {
	        get
            {
                return _count;
            }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Message item)
        {
            bool result = true;

            try
            {
                _messagesByNumber.Remove(item.Number);
                _messagesByIndex.Remove(item);

                _count--;
            }
            catch
            {
                result = false;
            }

            return result;
        }

        #endregion

        #region IEnumerable<Message> Members

        public IEnumerator<Message> GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
            {
                yield return this[i];
            }
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region IList Members

        public int Add(object value)
        {
 	        throw new NotImplementedException();
        }

        public bool Contains(object value)
        {
            return Contains(value as Message);
        }

        public int IndexOf(object value)
        {
            return IndexOf(value as Message);
        }

        public void Insert(int index, object value)
        {
 	        throw new NotImplementedException();
        }

        public bool IsFixedSize
        {
            get { return false; }
        }

        public void Remove(object value)
        {
 	        throw new NotImplementedException();
        }

        object IList.this[int index]
        {
	        get 
	        {
                return this[index];
	        }

	        set 
	        { 
		        throw new NotImplementedException(); 
	        }
        }

        #endregion

        #region ICollection Members

        public void CopyTo(Array array, int index)
        {
 	        throw new NotImplementedException();
        }

        public bool IsSynchronized
        {
            get { return false; }
        }

        public object SyncRoot
        {
            get { return this; }
        }

        #endregion

        #region INotifyCollectionChanged Members

        public event NotifyCollectionChangedEventHandler  CollectionChanged;

        #endregion
    }
}
