﻿using System;

namespace PGOffline
{
    public struct StatisticsOptions
    {
        public StatisticsType statisticsType;

        public bool postings;
        public bool repliers;
        public bool topTenSubjects;
        public bool threadInitiators;
        public bool newMembers;
        public bool lurkers;

        public DateTime dateTimeStart;
        public DateTime dateTimeEnd;

        public long messageNumberFirst;
        public long messageNumberLast;

        public DateTime compare1StartDate;
        public DateTime compare1EndDate;

        public DateTime compare2StartDate;
        public DateTime compare2EndDate;

        public long compare1StartNumber;
        public long compare1EndNumber;

        public long compare2StartNumber;
        public long compare2EndNumber;

        public string compareGroup1Name;
        public string compareGroup2Name;
    }
}