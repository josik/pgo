﻿using System;
using System.Data.SQLite;
using System.IO;
using System.Text.RegularExpressions;

namespace PGOffline
{
    public class UpgradeDatabaseManager : Singleton<UpgradeDatabaseManager>
    {
        public bool UpgradeDatabase()
        {
            int targetDatabase = GetExpectedDatabaseVersion();

            int currentDatabase = GetDatabaseVersion();

            int previousVersionAttempt = currentDatabase;

            while (currentDatabase < targetDatabase)
            {
                // backup database before upgrading this version
                if (!DatabaseManager.Instance.BackupDatabase(DatabaseManager.Instance.CurrentDatabasePath,
                        string.Format("{0}.UpgradeAutoBackup.v{1}.db3", DatabaseManager.Instance.CurrentDatabasePath,
                            currentDatabase))) break;

                SQLiteTransaction transaction = DatabaseManager.Instance.TransactionBegin();

                switch (currentDatabase)
                {
                    case 17:
                        // remove deplucate records from group_message
                        DatabaseManager.Instance.ExecuteQuery("DELETE FROM group_message WHERE id NOT IN (SELECT MAX(id) id FROM group_message GROUP BY discussion_group, number);");
                        // remove redundant index's from group_message
                        DatabaseManager.Instance.ExecuteQuery("DROP INDEX IF EXISTS group_message_idx_discussion_group;");
                        DatabaseManager.Instance.ExecuteQuery("DROP INDEX IF EXISTS group_message_idx_number;");
                        // remove index; we want to add it back with the "unique" attribute
                        DatabaseManager.Instance.ExecuteQuery("DROP INDEX IF EXISTS group_message_number_and_discussion_group;");
                        // add indexes that were missing from my test db, they might be missing from users db's as well; re-add for good measure
                        DatabaseManager.Instance.ExecuteQuery("CREATE INDEX IF NOT EXISTS group_message_id ON group_message (id);");
                        DatabaseManager.Instance.ExecuteQuery("CREATE INDEX IF NOT EXISTS group_message_number ON group_message (number);");
                        DatabaseManager.Instance.ExecuteQuery("CREATE UNIQUE INDEX group_message_number_and_discussion_group ON group_message (discussion_group, number);");
                        DatabaseManager.Instance.ExecuteQuery("CREATE INDEX IF NOT EXISTS group_message_discussion_group ON group_message (discussion_group);");
                        DatabaseManager.Instance.ExecuteQuery("CREATE INDEX IF NOT EXISTS group_message_unread ON group_message (unread);");
                        DatabaseManager.Instance.ExecuteQuery("CREATE INDEX IF NOT EXISTS group_message_person ON group_message (person);");
                        DatabaseManager.Instance.ExecuteQuery("CREATE INDEX IF NOT EXISTS group_message_charset ON group_message (charset);");
                        DatabaseManager.Instance.ExecuteQuery("CREATE INDEX IF NOT EXISTS group_message_unread_group ON group_message (unread, discussion_group);");
                        DatabaseManager.Instance.ExecuteQuery("CREATE INDEX IF NOT EXISTS group_message_charset_group ON group_message (discussion_group, charset);");
                        // re-add triggers
                        DatabaseManager.Instance.ExecuteQuery("DROP TRIGGER IF EXISTS after_unread_group_message_insert;");
                        DatabaseManager.Instance.ExecuteQuery("DROP TRIGGER IF EXISTS after_unread_group_message_delete;");
                        DatabaseManager.Instance.ExecuteQuery("DROP TRIGGER IF EXISTS after_group_message_update_set_unread;");
                        DatabaseManager.Instance.ExecuteQuery("DROP TRIGGER IF EXISTS after_group_message_update_set_read;");
                        DatabaseManager.Instance.ExecuteQuery("CREATE TRIGGER after_unread_group_message_insert AFTER INSERT ON group_message WHEN new.unread = 1 BEGIN UPDATE discussion_group SET unread_count=unread_count+1 WHERE discussion_group.id=new.discussion_group; END;");
                        DatabaseManager.Instance.ExecuteQuery("CREATE TRIGGER after_unread_group_message_delete AFTER DELETE ON group_message WHEN old.unread = 1 BEGIN UPDATE discussion_group SET unread_count=unread_count-1 WHERE discussion_group.id=old.discussion_group; END;");
                        DatabaseManager.Instance.ExecuteQuery("CREATE TRIGGER after_group_message_update_set_unread AFTER UPDATE OF unread ON group_message WHEN new.unread = 1 AND old.unread = 0 BEGIN UPDATE discussion_group SET unread_count=unread_count+1 WHERE discussion_group.id=new.discussion_group; END;");
                        DatabaseManager.Instance.ExecuteQuery("CREATE TRIGGER after_group_message_update_set_read AFTER UPDATE OF unread ON group_message WHEN new.unread = 0 AND old.unread = 1 BEGIN UPDATE discussion_group SET unread_count=unread_count-1 WHERE discussion_group.id=new.discussion_group; END;");
                        // update database version
                        DatabaseManager.Instance.ExecuteQuery("UPDATE options SET value = '18' WHERE key = 'database_version'");
                        // update the unread count for each discussion_group
                        DatabaseManager.Instance.FixUnreadCount();
                        break;
                    case 16:
                        DatabaseManager.Instance.ExecuteQuery("ALTER TABLE group_message ADD COLUMN prev_in_topic INTEGER;");
                        DatabaseManager.Instance.ExecuteQuery("ALTER TABLE group_message ADD COLUMN next_in_topic INTEGER;");
                        DatabaseManager.Instance.ExecuteQuery("ALTER TABLE group_message ADD COLUMN prev_in_time INTEGER;");
                        DatabaseManager.Instance.ExecuteQuery("ALTER TABLE group_message ADD COLUMN next_in_time INTEGER;");
                        DatabaseManager.Instance.ExecuteQuery("CREATE INDEX group_message_idx_number ON group_message(number DESC);");
                        DatabaseManager.Instance.ExecuteQuery("CREATE INDEX group_message_idx_discussion_group ON group_message(discussion_group ASC);");
                        DatabaseManager.Instance.ExecuteQuery("UPDATE options SET value = '17' WHERE key = 'database_version'");
                        break;
                    case 15:
                        DatabaseManager.Instance.ExecuteQuery("ALTER TABLE group_message ADD COLUMN topic_id INTEGER;");
                        DatabaseManager.Instance.ExecuteQuery("ALTER TABLE group_message ADD COLUMN parent_id INTEGER;");
                        DatabaseManager.Instance.ExecuteQuery("ALTER TABLE group_message ADD COLUMN thread_level INTEGER;");
                        DatabaseManager.Instance.ExecuteQuery("UPDATE options SET value = '16' WHERE key = 'database_version'");
                        break;
                    case 14:
                        DatabaseManager.Instance.ExecuteQuery("UPDATE options SET key = 'refresh_members_options_refresh_members_type' WHERE key = 'resfresh_members_options_refresh_members_type'");
                        DatabaseManager.Instance.ExecuteQuery("UPDATE options SET key = 'refresh_members_options_refresh_period' WHERE key = 'resfresh_members_options_refresh_period'");
                        DatabaseManager.Instance.ExecuteQuery("UPDATE options SET value = 'div.yg-list-title a' WHERE key = 'fizzler_file_uri'");
                        DatabaseManager.Instance.ExecuteQuery("UPDATE options SET value = '15' WHERE key = 'database_version'");
                        break;
                    case 13:
                        //Upgrade13To14();
                        DatabaseManager.Instance.ExecuteQuery("UPDATE options SET value = '14' WHERE key = 'database_version'");
                        break;
                    case 12:
                        //Upgrade12To13();
                        DatabaseManager.Instance.ExecuteQuery("ALTER TABLE user_account ADD COLUMN sortOrder INTEGER;");
                        DatabaseManager.Instance.ExecuteQuery("UPDATE options SET value = '13' WHERE key = 'database_version'");
                        break;
                }

                transaction.Commit();

                // re-check for current database version, following the trans commit
                currentDatabase = GetDatabaseVersion();

                // detect a failed upgrade and prevent infinite looping
                if (currentDatabase == previousVersionAttempt)
                {
                    // TODO: throw exception?
                    break;
                }
            }

            return GetDatabaseVersion() == targetDatabase;
        }

        private void Upgrade13To14()
        {
            // update existing record values
            
            // add new records

            // migrate existing records

            // remove existing records
        }

        private void Upgrade12To13()
        {
            // update existing record values
            
            // add new records

            // migrate existing records

            // remove existing records
        }
        
        private int GetDatabaseVersion()
        {
            int databaseVersion;

            try
            {
                var oDatabaseVersion = DatabaseManager.Instance.ExecuteQueryAndGetValue("SELECT value FROM options WHERE key = 'database_version'", 0);
                databaseVersion = Convert.ToInt16(oDatabaseVersion.ToString());
            }
            catch (Exception)
            {
                // database may be old and not contain database_version, so assume version 12
                databaseVersion = 12;
            }

            return databaseVersion;
        }

        public int GetExpectedDatabaseVersion()
        {
            int existingdatabaseVersion;
            
            try
            {
                //string optionsSchemaPath = PathResolver.GetOptionsSchemaScriptPath();
                string schemaPath = PathResolver.GetSchemaScriptPath();
                string fileContents = File.ReadAllText(schemaPath);

                // find version within ('database_version', 'XX') found within the .sql file
                Regex versionRegex = new Regex("\\('database_version', '(.*)'\\)");
                Match match = versionRegex.Match(fileContents);

                existingdatabaseVersion = Convert.ToInt16(match.Groups[1].Value);
            }
            catch (Exception ex)
            {
                // cannot retrieve the value, so return 0 so no work will be done.
                existingdatabaseVersion = 0;
            }

            return existingdatabaseVersion;
        }

        public bool IsDatabaseVersionCurrent()
        {
            var existingdatabaseVersion = GetDatabaseVersion();
            
            var expectedDatabaseVersion = GetExpectedDatabaseVersion();
            
            return existingdatabaseVersion >= expectedDatabaseVersion;
        }
    }

}
