﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

using PGOffline.Model;

namespace PGOffline.Converters
{
    class MessageStatusToIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            MessageStatus messageStatus = (MessageStatus)value;

            if (messageStatus == MessageStatus.Read)
            {
                return "/Assets/Images/mail_read_32.png";
            }
            else if (messageStatus == MessageStatus.Unread)
            {
                return "/Assets/Images/mail_unread_32.png";
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
