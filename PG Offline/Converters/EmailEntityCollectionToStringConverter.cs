﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Windows.Data;
using PGOffline.Model;

namespace PGOffline.Converters
{
    class EmailEntityCollectionToStringConverter : IValueConverter
    {

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            EntityCollection<Email> emails = (EntityCollection<Email>)value;

            StringBuilder emailsBuilder = new StringBuilder();
            int currentEmail = 0;
            foreach (Email email in emails)
            {
                if (currentEmail == emails.Count - 1)
                {
                    emailsBuilder.Append(email.Address);
                }
                else
                {
                    emailsBuilder.AppendLine(email.Address);
                }

                currentEmail++;
            }

            return emailsBuilder.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
