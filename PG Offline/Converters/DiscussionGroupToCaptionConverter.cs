﻿using PGOffline.Model;
using System;
using System.Windows.Data;

namespace PGOffline.Converters
{
    [ValueConversion(typeof(DiscussionGroup), typeof(string))]
    class DiscussionGroupToCaptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var group = (DiscussionGroup)value;

            return @group.UnreadNonDeletedMessagesCount > 0 ? string.Format("{0} ({1})", @group.Name, @group.UnreadNonDeletedMessagesCount) : @group.Name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
