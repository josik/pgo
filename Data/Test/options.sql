PRAGMA auto_vacuum=FULL;
PRAGMA foreign_keys=ON;

BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS options (
key TEXT PRIMARY KEY,
value TEXT);

INSERT OR IGNORE INTO options (key, value) VALUES ('download_options_enable_download_limit_speed', 'false');
INSERT OR IGNORE INTO options (key, value) VALUES ('download_options_download_limit_speed_type', '0');
INSERT OR IGNORE INTO options (key, value) VALUES ('download_options_messages_per_minute', '75');
INSERT OR IGNORE INTO options (key, value) VALUES ('download_options_initial_delay', '0');
INSERT OR IGNORE INTO options (key, value) VALUES ('download_options_download', '150');
INSERT OR IGNORE INTO options (key, value) VALUES ('download_options_then_wait', '15');
INSERT OR IGNORE INTO options (key, value) VALUES ('download_options_enable_messages_caching', 'true');
INSERT OR IGNORE INTO options (key, value) VALUES ('download_options_receive_messages_per_request', '100');

INSERT OR IGNORE INTO options (key, value) VALUES ('workflow_options_on_failure_rerequest_message_times', '0');
INSERT OR IGNORE INTO options (key, value) VALUES ('workflow_options_on_lockout_resume_after_minutes_enabled', 'false');
INSERT OR IGNORE INTO options (key, value) VALUES ('workflow_options_on_lockout_resume_after_minutes', '120');
INSERT OR IGNORE INTO options (key, value) VALUES ('workflow_options_stop_after_receiving_invalid_pages_enabled', 'false');
INSERT OR IGNORE INTO options (key, value) VALUES ('workflow_options_stop_after_receiving_invalid_pages', '50');
INSERT OR IGNORE INTO options (key, value) VALUES ('workflow_options_skip_authorization', 'false');

INSERT OR IGNORE INTO options (key, value) VALUES ('mail_options_smtp_server', '');
INSERT OR IGNORE INTO options (key, value) VALUES ('mail_options_smtp_port', '25');
INSERT OR IGNORE INTO options (key, value) VALUES ('mail_options_smtp_login', '');
INSERT OR IGNORE INTO options (key, value) VALUES ('mail_options_smtp_password', '');
INSERT OR IGNORE INTO options (key, value) VALUES ('mail_options_smtp_ssl', '');
INSERT OR IGNORE INTO options (key, value) VALUES ('mail_options_from', '');
INSERT OR IGNORE INTO options (key, value) VALUES ('mail_options_use_external_mail_client', 'false');
INSERT OR IGNORE INTO options (key, value) VALUES ('mail_options_external_mail_client_path', '');

INSERT OR IGNORE INTO options (key, value) VALUES ('miscellaneous_options_mark_messages_as_read_after_seconds', '5');
INSERT OR IGNORE INTO options (key, value) VALUES ('miscellaneous_options_show_members_list_out_of_date_warning', 'false');
INSERT OR IGNORE INTO options (key, value) VALUES ('miscellaneous_options_minimize_to_system_tray', 'false');
INSERT OR IGNORE INTO options (key, value) VALUES ('miscellaneous_options_save_sort_order_in_message_pane', 'true');
INSERT OR IGNORE INTO options (key, value) VALUES ('miscellaneous_options_message_pane_sort_order', '0');
INSERT OR IGNORE INTO options (key, value) VALUES ('miscellaneous_options_sorted_messages_pane_index', '2');
INSERT OR IGNORE INTO options (key, value) VALUES ('miscellaneous_options_use_selected_sort_order_for_new_messages_show', 'false');
INSERT OR IGNORE INTO options (key, value) VALUES ('miscellaneous_options_show_events_on_main_form_messages_pane_count', '200');
INSERT OR IGNORE INTO options (key, value) VALUES ('miscellaneous_options_log_events_to_file', 'false');
INSERT OR IGNORE INTO options (key, value) VALUES ('miscellaneous_options_log_events_file_path', '');
INSERT OR IGNORE INTO options (key, value) VALUES ('miscellaneous_options_main_window_panes_font_name', 'Tahoma');

INSERT OR IGNORE INTO options (key, value) VALUES ('refresh_group_options_refresh_group_type', '0');
INSERT OR IGNORE INTO options (key, value) VALUES ('refresh_group_options_hours_period', '1');
INSERT OR IGNORE INTO options (key, value) VALUES ('refresh_group_options_minutes_period', '0');
INSERT OR IGNORE INTO options (key, value) VALUES ('refresh_group_options_download_messages_per_group_limit', 'true');
INSERT OR IGNORE INTO options (key, value) VALUES ('refresh_group_options_download_messages_per_group_limit_count', '1000');
INSERT OR IGNORE INTO options (key, value) VALUES ('refresh_group_options_refresh_group_loop_type', '0');
INSERT OR IGNORE INTO options (key, value) VALUES ('refresh_group_options_refresh_group_loop_count', '1');

INSERT OR IGNORE INTO options (key, value) VALUES ('refresh_members_options_refresh_members_type', '0');
INSERT OR IGNORE INTO options (key, value) VALUES ('refresh_members_options_refresh_period', '24');

INSERT OR IGNORE INTO options (key, value) VALUES ('group_settings_default_attachments_folder', 'APPDATA\PG Offline 4\Attachments');
INSERT OR IGNORE INTO options (key, value) VALUES ('group_settings_default_files_folder', 'APPDATA\PG Offline 4\Files');
INSERT OR IGNORE INTO options (key, value) VALUES ('group_settings_default_photos_folder', 'APPDATA\PG Offline 4\Photos');

INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_compare_1_end_date' , strftime('%d.%m.%Y', 'string'));
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_compare_1_end_number', 'Last');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_compare_1_start_date' , 'compare');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_compare_1_start_number', '1');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_compare_2_end_date', strftime('%d.%m.%Y', 'now'));
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_compare_2_end_number', 'Last');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_compare_2_start_date', 'value');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_compare_2_start_number', '1');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_compare_group_1_name', '');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_compare_group_2_name', '');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_date_time_end' , strftime('%d.%m.%Y', 'now'));
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_date_time_start', '01.01.2012');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_lurkers', 'True');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_message_number_first', '1');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_message_number_last', 'Last');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_new_members', 'True');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_postings', 'True');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_repliers', 'True');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_statistics_type', '0');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_thread_initiators', 'True');
INSERT OR IGNORE INTO options (key, value) VALUES ('statistics_options_top_ten_subjects', 'True');

INSERT OR IGNORE INTO options (key, value) VALUES ('import_from_mdb_options_copy_attachments', 'True');
INSERT OR IGNORE INTO options (key, value) VALUES ('import_from_mdb_options_import_favorite_status', 'True');
INSERT OR IGNORE INTO options (key, value) VALUES ('import_from_mdb_options_remember_last_message_number', 'True');
INSERT OR IGNORE INTO options (key, value) VALUES ('import_from_mdb_options_import_type', '0');
INSERT OR IGNORE INTO options (key, value) VALUES ('import_from_mdb_options_database_path', '');

INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_message_title', '.ygrp-topic-title');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_message_email', '.email');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_message_sender_name', '.name > a');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_message_sender_name_alternative', '.name');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_message_date', '.updated');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_message_contents', '.msgarea');
INSERT OR IGNORE INTO options (key, value) VALUES ('regex_messages_count', 'data-total-count="([0-9]+)" data-prev-page-start=');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_attachment_node', '.ygrp-file-title > a');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_attachment_original_message', '.ygrp-photos-view-original > a');
INSERT OR IGNORE INTO options (key, value) VALUES ('regex_attachments_count', 'of ([0-9]+)&nbsp;');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_members_table_row', 'table.datatable > tr');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_member_name', 'td.info > span.name > a[title]');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_member_name_alternative', 'td.info > span.name');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_member_email', 'td.info > span.email');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_member_join_date', 'td.info > span.date');
INSERT OR IGNORE INTO options (key, value) VALUES ('regex_members_count', '<ul class="ygrp-ul ygrp-info"><li>Members: ([0-9]*) </li>');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_file_uri', 'div.yg-list-title a');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_photo_div', 'div.ygrp-photos-thumbnail');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_photo_uri', 'div.photo > a > img');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_photo_thumbnail', 'div.photo > a');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_album_link', 'a.thumbnail-title');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_album_link_second', 'a.thumbnail-title-indent');
INSERT OR IGNORE INTO options (key, value) VALUES ('fizzler_photo_attachment_node', 'div.ygrp-photo');

COMMIT;