﻿using System.Web;
using CsQuery;
using CsQuery.ExtensionMethods;
using CsQuery.Utility;
using Fizzler.Systems.HtmlAgilityPack;
using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class DownloadOptions
    {
        public long StartMessageNumber { get; set; }
        public long EndMessageNumber { get; set; }
        public string IncludeAttachmentsPattern { get; set; }
        public string ExcludeAttachmentsPattern { get; set; }
        public string FolderForAttachments { get; set; }
        public int MinimumAttachmentSize { get; set; }
        public int MaximumAttachmentSize { get; set; }
    }

    public class YahooGroup
    {
        private SpeedLimitOptions _speedLimitOptions;

        private WorkflowOptions _workflowOptions;

        private CookieContainer _cookieContainer;
        private string _name;
        private List<YahooGroupMessage> _messages = new List<YahooGroupMessage>();
        private List<YahooGroupAttachment> _attachments = new List<YahooGroupAttachment>();
        private List<YahooGroupMember> _members = new List<YahooGroupMember>();
        private List<YahooGroupFile> _files = new List<YahooGroupFile>();
        private List<YahooGroupPhoto> _photos = new List<YahooGroupPhoto>();

        private MessagesDownloader _messagesDownloader = null;

        private DownloadOptions _downloadOptions = new DownloadOptions();

        private AppDownloadOptions _appDownloadOptions;

        public DownloadOptions DownloadOptions
        {
            get
            {
                return _downloadOptions;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public YahooGroup(CookieContainer cookieContainer, string name, SpeedLimitOptions speedLimitOptions, WorkflowOptions workflowOptions, AppDownloadOptions appDownloadOptions)
        {
            _cookieContainer = cookieContainer;

            _name = name;

            _speedLimitOptions = speedLimitOptions;

            _appDownloadOptions = appDownloadOptions;

            SetWorkflow(workflowOptions);
        }

        public override string ToString()
        {
            return _name;
        }

        public void SetSpeedLimit(SpeedLimitOptions speedLimitOptions)
        {
            _speedLimitOptions = speedLimitOptions;
        }
        public void SetAppDownloadOptions(AppDownloadOptions appDownloadOptions)
        {
            _appDownloadOptions = appDownloadOptions;
        }

        public void SetWorkflow(WorkflowOptions workflowOptions)
        {
            _workflowOptions = workflowOptions;
        }

        public int GetMessagesCount()
        {
            var url = String.Format("http://groups.yahoo.com/api/v1/groups/{0}/messages?start=0&count=1&sortOrder=desc&direction=-1&chrome=raw", _name);
#if DEBUG
            //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[dbg] GetGroupCountPageContent() :: '{0}'", url));
#endif
            string pageContents = Utils.DownloadYahooWebPage(url, _cookieContainer, allowAutoRedirect: false);

            if (string.IsNullOrEmpty(pageContents))
            {
                // try another server before giving up
                SourceCountryUrlPrefixOption.Instance.DisableCurrentCodeInUse();
                SourceCountryUrlPrefixOption.Instance.SetNextEnabledCode();
                url = String.Format("http://groups.yahoo.com/api/v1/groups/{0}/messages?start=0&count=1&sortOrder=desc&direction=-1&chrome=raw", _name);
                pageContents = Utils.DownloadYahooWebPage(url, _cookieContainer, allowAutoRedirect: false);

                if (string.IsNullOrEmpty(pageContents))
                {
                    throw new YahooGroupsException("Failed to retreive groups messages count.");
                }
            }
            
            var oRoot = JObject.Parse(pageContents);
            
            // wrap error check below in a try-catch
            //var errorMessage = (int)oRoot["ygError"]["errorMessage"];

            var messageCount = (int)oRoot["ygData"]["totalRecords"];

            //if (!IsGroupFound(_name))
            //    throw new YahooGroupsException("Group not found.");

            if (messageCount > 1)
            {
                return messageCount;
            }

            throw new YahooGroupsException("Failed to retreive groups messages count.");
        }
        
        public void DownloadAttachments(YahooGroupMessage message)
        {
            if (message.HasAttachments == 0) return;

            var attachmentsDownloader = new AttachmentsDownloader(_downloadOptions, _cookieContainer);

            attachmentsDownloader.DownloadAttachments(message);
        }
        
        private string GetGroupPageContent()
        {
            var url = string.Format("http://{0}groups.yahoo.com/group/{1}/messages", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.CodePeriodPostfix, _name);
#if DEBUG
            Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[dbg] GetGroupPageContent() :: '{0}'", url));
#endif
            return Utils.DownloadYahooWebPage(url, _cookieContainer);
        }

        #region Receiving messages
        
        public SortedList<uint, YahooGroupMessage> DownloadMessagesBundle(long startMessageNumber, long maxMessagesCount = 20)
        {
            try
            {
                // pull message list then pull each message found in the list
                var url = GetMessagesUrl(startMessageNumber, maxMessagesCount);

                string messageHeaders = Utils.DownloadYahooWebPage(url, _cookieContainer);

                if (string.IsNullOrEmpty(messageHeaders))
                {
                    return null;
                };

                var messageHeadersRoot = JObject.Parse(messageHeaders);

                int intMessageMaxThreads = Properties.Settings.Default.MessageMaxThreads;
#if !DEBUG
                intMessageMaxThreads = 1;
#else
                intMessageMaxThreads = 1;
#endif
                //Parallel.ForEach(messageHeadersRoot["ygData"]["messages"],
                //    new ParallelOptions { MaxDegreeOfParallelism = intMessageMaxThreads },
                //    message =>
                //    { }
                
                foreach (var message in messageHeadersRoot["ygData"]["messages"])
                {
                    //var localCopyOfMessage = message; // a copy for use in Task

                    // BEGIN raw message handling

                    uint thisMessageId = Convert.ToUInt32(message["messageId"]);

                    string messageDetail = "";

                    messageDetail = ServerCache.GetMessage(_appDownloadOptions, _name, thisMessageId);

                    if (string.IsNullOrEmpty(messageDetail))
                    {
                        // retrieve message from Yahoo
                        string messageBodyUrl = GetMessageUrl(thisMessageId);

                        messageDetail = Utils.DownloadYahooWebPage(messageBodyUrl, _cookieContainer);

                        //while (messageDetail != "" && messageDetail != "(SWITCH SERVER)")
                        //{
                        //    messageDetail = Utils.DownloadYahooWebPage(messageBodyUrl, _cookieContainer);
                        //}

                        //if (string.IsNullOrEmpty(messageDetail))
                        //{
                        //    // try another server before giving up
                        //    SourceCountryUrlPrefixOption.Instance.DisableCurrentCodeInUse();
                        //    SourceCountryUrlPrefixOption.Instance.SetNextEnabledCode();
                        //    messageBodyUrl = GetMessageUrl(thisMessageId);
                        //    messageDetail = Utils.DownloadYahooWebPage(messageBodyUrl, _cookieContainer);
                        //    if (string.IsNullOrEmpty(messageDetail)) continue;
                        //};

                        if (string.IsNullOrEmpty(messageDetail))
                        {
                            continue;
                        }

                        const uint submissionUserId = 0;

                        ServerCache.SubmitMessageInNewThread(_appDownloadOptions, submissionUserId, _name, thisMessageId, messageDetail);
                    }

                    JObject messageDetailRoot = JObject.Parse(messageDetail);
                    // END raw message handling

#if DEBUG
                    //File.AppendAllText(@"c:\temp\PgOffline_debug_messages.log", ">>MESSAGE" + Environment.NewLine + messageDetail + Environment.NewLine);
#endif

                    string authorName;
                    string userId;
                    string from;
                    string profile;
                    string content;
                    string topicId; 
                    string messageId;
                    string threadLevel;
                    string parentId;
                    string prevInTopic;
                    string nextInTopic;
                    string prevInTime;
                    string nextInTime;

                    authorName = (string)messageDetailRoot.SelectToken("ygData.authorName") ?? string.Empty;
                    userId = (string)messageDetailRoot.SelectToken("ygData.userId") ?? string.Empty;
                    from = (string)messageDetailRoot.SelectToken("ygData.from") ?? string.Empty;
                    profile = (string)messageDetailRoot.SelectToken("ygData.profile") ?? string.Empty;
                    content = (string)messageDetailRoot.SelectToken("ygData.messageBody") ?? string.Empty;
                    messageId = (string)messageDetailRoot.SelectToken("ygData.msgId") ?? string.Empty;
                    topicId = (string)messageDetailRoot.SelectToken("ygData.topicId") ?? "0";
                    threadLevel = (string)messageDetailRoot.SelectToken("ygData.threadLevel") ?? "0";
                    parentId = (string)messageDetailRoot.SelectToken("ygData.parent") ?? "0";
                    prevInTopic = (string)messageDetailRoot.SelectToken("ygData.prevInTopic") ?? "0";
                    nextInTopic = (string)messageDetailRoot.SelectToken("ygData.nextInTopic") ?? "0";
                    prevInTime = (string)messageDetailRoot.SelectToken("ygData.prevInTime") ?? "0";
                    nextInTime = (string)messageDetailRoot.SelectToken("ygData.nextInTime") ?? "0";

                    authorName = WebUtility.HtmlDecode((authorName)).Trim();
                    userId = WebUtility.HtmlDecode((userId)).Trim();
                    from = WebUtility.HtmlDecode((from)).Trim();
                    profile = WebUtility.HtmlDecode((profile)).Trim();
                    content = WebUtility.HtmlDecode((content)).Trim();
                    
                    // order of use: authorName, profile, from, userId
                    if (string.IsNullOrEmpty(authorName))
                    {
                        if (string.IsNullOrEmpty(profile))
                        {
                            if (string.IsNullOrEmpty(from))
                            {
                                authorName = string.IsNullOrEmpty(userId) ? "unknown" : userId;
                            }
                            else
                            {
                                authorName = from;
                            }
                        }
                        else
                        {
                            authorName = profile;
                        }
                    }

                    message.Last.AddAfterSelf(new JProperty("pgoAuthor", authorName));
                    message.Last.AddAfterSelf(new JProperty("pgoEmail", authorName));
                    message.Last.AddAfterSelf(new JProperty("pgoUserId", userId));
                    message.Last.AddAfterSelf(new JProperty("pgoFrom", from));
                    message.Last.AddAfterSelf(new JProperty("pgoProfile", profile));
                    message.Last.AddAfterSelf(new JProperty("pgoContent", content));
                    message.Last.AddAfterSelf(new JProperty("pgoTopicId", topicId));
                    message.Last.AddAfterSelf(new JProperty("pgoMessageId", messageId));
                    message.Last.AddAfterSelf(new JProperty("pgoThreadLevel", threadLevel));
                    message.Last.AddAfterSelf(new JProperty("pgoParentId", parentId));
                    message.Last.AddAfterSelf(new JProperty("pgoPrevInTopic", prevInTopic));
                    message.Last.AddAfterSelf(new JProperty("pgoNextInTopic", nextInTopic));
                    message.Last.AddAfterSelf(new JProperty("pgoPrevInTime", prevInTime));
                    message.Last.AddAfterSelf(new JProperty("pgoNextInTime", nextInTime));

#if DEBUG
                    if (intMessageMaxThreads == 1)
                    {
                        //File.AppendAllText(@"c:\temp\PgOffline_debug_messages.log", ">>MESSAGE-JSON" + Environment.NewLine + message.ToString() + Environment.NewLine);
                    }
#endif
                } // end foreach
                
                string messagesContent = messageHeadersRoot.ToString();

                SortedList<uint, YahooGroupMessage> messageBundle = new MessagesBundleParser(Name, messagesContent, _downloadOptions).Parse();

                return messageBundle;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[{0}] Message parsing error.", _name));

                throw new YahooGroupsException(String.Format("[{0}] Message parsing error", _name));
            }
        }

        private string GetMessagesUrl(long startMessageNumber, long maxMessageCount = 20)
        {
            string url = String.Format("http://groups.yahoo.com/api/v1/groups/{0}/messages?start={1}&count={2}&sortOrder=asc&direction=1&chrome=raw", Name, startMessageNumber, maxMessageCount);
#if DEBUG
            //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[dbg] GetMessagesUrl() :: '{0}'", url));
#endif
            return url;
        }

        private string GetMessageUrl(long messageNumber)
        {
            string url = String.Format("http://groups.yahoo.com/api/v1/groups/{0}/messages/{1}?chrome=raw", Name, messageNumber);
#if DEBUG
            //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[dbg] GetMessageUrl() :: '{0}'", url));
#endif
            return url;
        }

        public YahooGroupMessage GetMessageContentFromHtml(YahooGroupMessage message, string groupName)
        {
            // Our backup sever for HTML is always the main US domain
            
            string url = String.Format("https://groups.yahoo.com/neo/groups/{0}/conversations/messages/{1}", groupName);
#if DEBUG
            Trace.Write(string.Format(@"/DEBUG\ Getting HTML from backup server for {0} in {1}", message.Number, groupName));
#endif
            try
            {
                WebDownloadResponse page = Utils.GetWebPage(url, _cookieContainer);

                CQ pageContentDom = page.Content;

                message.Content = pageContentDom["div.msg-content"].Html();
#if DEBUG
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format(@" (message size {0} chars)", message.Content.Length));
#endif
            }
            catch (Exception ex)
            {
                return message;
            }
            
            return message;
        }

        #endregion

        #region Receiving members

        BackgroundWorker _receiveMembersWorker;
        
        public delegate void ReceiveMembersCompletedEventHandler();
        public event ReceiveMembersCompletedEventHandler ReceiveMembersCompleted;

        public delegate void ReceiveMembersProgressEventHandler(int progressPercentage, YahooGroupMember message);
        public event ReceiveMembersProgressEventHandler ReceiveMembersProgress;

        private string GetGroupMembersJson(int recordStart)
        {
            // get the Yahoo forced max of 100 records at a time
            string pageMembersUri = string.Format("http://groups.yahoo.com/api/v1/groups/{0}/members/confirmed?start={1}&count=100&sortBy=name&sortOrder=asc&chrome=raw", _name, recordStart);

            string groupMembersPageContents = Utils.DownloadYahooWebPage(pageMembersUri, _cookieContainer);

            return groupMembersPageContents;
        }

        private ulong GetMembersCount(JObject oGroupMembers)
        {
            var membersCount = Convert.ToUInt64(oGroupMembers["ygData"]["total"]);

            return membersCount;
        }

        private void ReceiveMembersWrapper()
        {
            ulong membersReceived = 0;

            int recordStart = 1;

            _members.Clear();

            Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Receiving members of group {0}", _name));

            // keep getting 30 records at a time until the record count is reached or there are no more "members"
            string groupMembersPageContents = GetGroupMembersJson(recordStart);

            var oGroupMembers = JObject.Parse(groupMembersPageContents);

            ulong membersCount = GetMembersCount(oGroupMembers);

            Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Members in group {0}: {1}", _name, membersCount));

            // Keep on loading pages with members info until there are no more remaining
            while (membersReceived < membersCount)
            {
                if (_receiveMembersWorker.CancellationPending)
                {
                    break;
                }

                int recordsThisLoop = oGroupMembers["ygData"]["members"].Count();

                if (recordsThisLoop == 0)
                {
                    break;
                }


                foreach (JToken member in oGroupMembers["ygData"]["members"])
                {
                    DateTime date = Utils.UnixTimeStampToDateTime((uint) member["date"]);
                    string name = (string) member["yalias"];
                    string email = (string) member["email"];
                    if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(name)) return;
                    if (string.IsNullOrEmpty(name))
                        name = string.Empty;
                    if (string.IsNullOrEmpty(email))
                        email = string.Empty;

                    var newMember = new YahooGroupMember(name, email, date);
                    _receiveMembersWorker.ReportProgress((int) ((membersReceived/(double) membersCount)*100.0f),
                        newMember);
                    //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Adding user {0} ({1})", name, email));
                    _members.Add(newMember);
                    membersReceived++;
                }

                //Parallel.ForEach(oGroupMembers["ygData"]["members"],
                //    new ParallelOptions { MaxDegreeOfParallelism = 10 },
                //    member =>
                //    {
                //        DateTime date = Utils.UnixTimeStampToDateTime((uint)member["date"]);
                //        string name = (string)member["yalias"];
                //        string email = (string)member["email"];
                //        if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(name)) return;
                //        if (string.IsNullOrEmpty(name))
                //            name = string.Empty;
                //        if (string.IsNullOrEmpty(email))
                //            email = string.Empty;

                //        var newMember = new YahooGroupMember(name, email, date);
                //        _receiveMembersWorker.ReportProgress((int)((membersReceived / (double)membersCount) * 100.0f), newMember);
                //        //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Adding user {0} ({1})", name, email));
                //        _members.Add(newMember);
                //        membersReceived++;
                //    });

                recordStart += recordsThisLoop;

                oGroupMembers = JObject.Parse(GetGroupMembersJson(recordStart));

                // handle case when no records exist at all
                if (membersReceived != 0) continue;
                Trace.WriteLine("Error. Members information unavailable.");
                return;
            }
            if (membersReceived > 0)
            {
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Added {0} valid users.", membersReceived));
            }
            Trace.WriteLine("Receiving members complete.");
        }

        public void ReceiveMembersAsync()
        {
            if (_receiveMembersWorker != null)
            {
                ReceiveMembersCancelAsync();
            }

            _receiveMembersWorker = new BackgroundWorker
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };

            _receiveMembersWorker.DoWork += new DoWorkEventHandler((sender, e) => ReceiveMembersWrapper());
            _receiveMembersWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((sender, e) => ReceiveMembersCompleted());
            _receiveMembersWorker.ProgressChanged += new ProgressChangedEventHandler((sender, e) => ReceiveMembersProgress(e.ProgressPercentage, (YahooGroupMember)e.UserState));

            _receiveMembersWorker.RunWorkerAsync();
        }

        public void ReceiveMembersCancelAsync()
        {
            if (_receiveMembersWorker != null)
            {
                _receiveMembersWorker.CancelAsync();
            }
        }

        #endregion

        #region Receiving files
        
        public IEnumerable<object> ReceiveFilesAndDirectories(BackgroundWorker worker)
        {
            var unknownDirectories = new List<YahooGroupDirectory>();

            //string pageFilesUri = string.Format("http://{0}groups.yahoo.com/group/{1}/files", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.CodePeriodPostfix, _name);
            string pageFilesUri = string.Format("http://groups.yahoo.com/group/{0}/files", _name);

            //pageFilesUri = "https://groups.yahoo.com/neo/groups/7x10minilathe/files/?noImage=true&noNavbar=true&chrome=raw";

            var rootDirectory = new YahooGroupDirectory("root", pageFilesUri, null);

            unknownDirectories.Add(rootDirectory);

            while (unknownDirectories.Count > 0)
            {
                if (worker.CancellationPending)
                    break;

                var newUnknownDirectories = new List<YahooGroupDirectory>();

                foreach (YahooGroupDirectory directory in unknownDirectories)
                {
                    newUnknownDirectories.AddRange(DiscoverDirectory(directory, worker));
                }

                unknownDirectories.Clear();

                unknownDirectories.AddRange(newUnknownDirectories);
            }

            return rootDirectory.Children;
        }

        public void DownloadAllFiles(IEnumerable<YahooGroupFile> files, BackgroundWorker worker)
        {
            int filesDownloaded = 0;

            foreach (YahooGroupFile file in files)
            {
                try
                {
                    if (worker.CancellationPending)
                    {
                        break;
                    }

                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Downloading file {0} to {1}", file.Name, file.PathOnDisk));

                    //string pageFilesRefererUri = string.Format("http://{0}groups.yahoo.com/group/{1}/files", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.CodePeriodPostfix, _name);
                    string pageFilesRefererUri = string.Format("http://groups.yahoo.com/group/{0}/files", _name);

                    Utils.DownloadFile(file.YahooUri, file.PathOnDisk, _cookieContainer, pageFilesRefererUri);

                    filesDownloaded++;

                    worker.ReportProgress((int)((float)filesDownloaded / (float)files.Count() * 100.0f));
                }
                catch (Exception ex)
                {
                    Trace.WriteLine("Error while downloading file: " + ex.Message);
                }
            }
        }

        private IEnumerable<YahooGroupDirectory> DiscoverDirectory(YahooGroupDirectory directory, BackgroundWorker worker)
        {
            string pageFilesUri = directory.Uri;

            string pageFilesContents = Utils.DownloadYahooWebPage(pageFilesUri, _cookieContainer);

            if (string.IsNullOrEmpty(pageFilesContents)) return new List<YahooGroupDirectory>();

            List<YahooGroupFile> files;

            List<YahooGroupDirectory> directories;

            GetFilesAndDirectoriesByPageContents(pageFilesContents, directory, out directories, out files, worker);

            directory.AddDirectoriesRange(directories);

            directory.AddFilesRange(files);

            return directories;
        }

        private void GetFilesAndDirectoriesByCsQueryPageContents(string pageContents, YahooGroupDirectory parentDirectory, out List<YahooGroupDirectory> discoveredDirectories, out List<YahooGroupFile> discoveredFiles, BackgroundWorker worker)
        {
            var oHtml = JObject.Parse(pageContents);

            var html = oHtml["html"].ToString().Replace(@"\""", @"""");

            CQ dom = oHtml["html"].ToString();

            CQ filesRow = dom["div.files-row"];

            discoveredDirectories = new List<YahooGroupDirectory>();

            discoveredFiles = new List<YahooGroupFile>();



            foreach (IDomObject item in filesRow.ToList())
            {
                if (worker.CancellationPending) break;

                string sFileData = string.Format(@"{{{0}}}", item.GetAttribute("data-file").Replace(@"\""", @""""));

                JObject fileData = JObject.Parse(sFileData);

                string fileType = fileData["fileType"].ToString();

                string filePath = fileData["filePath"].ToString();

                string uriString;

                // It is a directory.
                switch (fileType)
                {
                    case "d":
                        string fileDirectoryName = Utils.GetLastDirectoryNameWithinPath(filePath);
                        uriString = parentDirectory.Uri.Replace("/?", string.Format("/{0}/?", fileDirectoryName));
                        discoveredDirectories.Add(new YahooGroupDirectory(fileDirectoryName, uriString, parentDirectory));
                        Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Discovered directory {0} in directory {1}", HttpUtility.HtmlDecode(fileDirectoryName), HttpUtility.HtmlDecode(parentDirectory.Name)));
                        break;

                    case "f":
                        string fileName = Path.GetFileName(filePath);
                        uriString = string.Format("{0}/{1}", parentDirectory.Uri, fileName);
                        var PP = GetFilePathOnDisk(fileName, parentDirectory.Path);
                        discoveredFiles.Add(new YahooGroupFile(fileName, GetFilePathOnDisk(fileName, parentDirectory.Path), new Uri(uriString), parentDirectory));
                        Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Discovered file {0} in directory {1}", HttpUtility.HtmlDecode(fileName), HttpUtility.HtmlDecode(parentDirectory.Name)));
                        break;
                    default:
                        // unknown file type
                        continue;
                }
            }

            return;
        }
        private void GetFilesAndDirectoriesByPageContents(string pageContents, YahooGroupDirectory parentDirectory, out List<YahooGroupDirectory> discoveredDirectories, out List<YahooGroupFile> discoveredFiles, BackgroundWorker worker)
        {
            discoveredDirectories = new List<YahooGroupDirectory>();

            discoveredFiles = new List<YahooGroupFile>();

            var doc = new HtmlDocument();
            doc.LoadHtml(pageContents);

            IEnumerable<HtmlNode> childElementsNodes = doc.DocumentNode.QuerySelectorAll(YahooGroupsServer.GetParsingOptionsValue("fizzler_file_uri"));

            foreach (HtmlNode node in childElementsNodes)
            {
                if (worker.CancellationPending)
                    break;

                string elementUri = node.Attributes["href"].Value;

                string elementName = node.InnerText;

                // It is a directory.
                if (elementUri.EndsWith("/"))
                {
                    string uriString = string.Format("http://groups.yahoo.com{0}", elementUri);
                    discoveredDirectories.Add(new YahooGroupDirectory(elementName, uriString, parentDirectory));
                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Discovered directory {0} in directory {1}", elementName, parentDirectory.Name));
                }
                // It is a file.
                else
                {
                    discoveredFiles.Add(new YahooGroupFile(elementName, GetFilePathOnDisk(elementName, parentDirectory.Path), new Uri(elementUri), parentDirectory));

                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Discovered file {0} in directory {1}", elementName, parentDirectory.Name));
                }
            }
        }

        private string GetFilePathOnDisk(string fileName, string groupPath)
        {
            string applicationDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            string downloadPath = string.Format("{0}\\PG Offline 4\\Files\\{1}{2}{3}", applicationDataPath, _name, groupPath, fileName);

            return downloadPath;
        }

        #endregion

        #region Receiving photos
        
        public IEnumerable<YahooGroupAlbumOfPhotos> ReceiveAlbums(BackgroundWorker worker)
        {
            var albums = new List<YahooGroupAlbumOfPhotos>();

            int recordStart = 0;

            const int recordsPerCall = 1000;

            int albumsReceived = 0;

            JObject oAlbums = GetAlbumListJson(recordStart, recordsPerCall);

            if (oAlbums == null) return null;

            int albumCount = (int)oAlbums["ygData"]["total"];

            Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Receiving {0} photo albums", albumCount));

            // Keep on loading pages until there are no more records remaining
            while (albumsReceived < albumCount)
            {
                if (worker.CancellationPending)
                    break;

                int recordsThisLoop = oAlbums["ygData"]["albums"].Count();

                if (recordsThisLoop == 0)
                {
                    break;
                }

                Parallel.ForEach(oAlbums["ygData"]["albums"],
                    new ParallelOptions { MaxDegreeOfParallelism = 2 },
                    oAlbum =>
                    {
                        string albumName = oAlbum["albumName"].ToString();
                        albumName = WebUtility.HtmlDecode(albumName);
                        string albumId = oAlbum["albumId"].ToString();
                        string albumUrl = string.Format("http://groups.yahoo.com/api/v2/groups/{0}/albums/{1}?start={{0}}&count={{1}}&sortOrder=asc&orderBy=ordinal&chrome=raw", _name, albumId);
                        var album = new YahooGroupAlbumOfPhotos(albumName, albumUrl);
                        Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Discovered album {0} [{1}]", albumName, _name));
                        Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Discovered album URL {0}", albumUrl));
                        albums.Add(album);
                        albumsReceived++;
                    });
                
                recordStart += recordsThisLoop;

                oAlbums = GetAlbumListJson(recordStart, recordsPerCall);

                // handle case when no records exist at all
                if (albumsReceived != 0) continue;

                Trace.WriteLine("Error. Album information unavailable.");
            }

            Trace.WriteLine("Receiving album list complete.");

            return albums.OrderBy(x => x.Name);
        }

        private JObject GetAlbumListJson(int recordStart, int recordsPerCall)
        {
            string albumsUrl = string.Format("http://groups.yahoo.com/api/v2/groups/{0}/albums?start={1}&count={2}&orderBy=mtime&sortOrder=desc&chrome=raw", _name, recordStart, recordsPerCall);

            string pageContents = Utils.DownloadYahooWebPage(albumsUrl, _cookieContainer);

            if (string.IsNullOrEmpty(pageContents)) return null;

            var oAlbums = JObject.Parse(pageContents);

            return oAlbums;
        }

        public void DownloadPhotos(IEnumerable<YahooGroupAlbumOfPhotos> albumPhotos, BackgroundWorker worker)
        {
            if (worker.CancellationPending)
                return;

            var maxDegreeOfParallelism = 10;
#if DEBUG
            maxDegreeOfParallelism = 1;
#endif

            int photosDownloaded = 0;

            if (albumPhotos == null) return;

            foreach (var album in albumPhotos.Where(x => x.IsChecked == true))
            {
                DiscoverAlbum(album, worker);
            }

            int photosToDownload = albumPhotos.Sum(album => album.Children.Count());

            foreach (var album in albumPhotos.Where(x => x.IsChecked==true))
            {
                if (worker.CancellationPending)
                    return;

                YahooGroupAlbumOfPhotos localAlbum = album;

                Parallel.ForEach(album.Children,
                    new ParallelOptions { MaxDegreeOfParallelism = maxDegreeOfParallelism },
                    photo =>
                    {
                        if (worker.CancellationPending)
                            return;

                        DownloadPhoto(photo, localAlbum);

                        photosDownloaded++;

                        worker.ReportProgress((int) ((float) photosDownloaded/(float) photosToDownload*100.0f));
                    });
            }
        }

        private void DownloadPhoto(YahooGroupPhoto photo, YahooGroupAlbumOfPhotos localAlbum)
        {
            if (File.Exists(photo.PathOnDisk))
            {
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Photo already exists. Skipping photo '{0}' [{1}][{2}] to [{3}]", photo.Name, localAlbum.Name, _name, photo.PathOnDisk));
            }
            else
            {
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Downloading photo '{0}' [{1}][{2}] to [{3}]", photo.Name, localAlbum.Name, _name, photo.PathOnDisk));

                foreach (Uri yahooUri in photo.YahooUriList)
                {
                    if (!File.Exists(photo.PathOnDisk) || new FileInfo(photo.PathOnDisk).Length == 0)
                    {
                        Utils.DownloadFile(yahooUri, photo.PathOnDisk, _cookieContainer, photo.Referer);
                    }
                }

#if DEBUG
                //// download all
                var i = 1;
                foreach (Uri yahooUri in photo.YahooUriList)
                {
                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("IMAGE DOWNLOAD '{0}' image '{1}'", i, yahooUri));
                    Utils.DownloadFile(yahooUri, photo.PathOnDisk + "." + i, _cookieContainer, photo.Referer);
                    i++;
                }
#endif
            }
        }

        private void DiscoverAlbum(YahooGroupAlbumOfPhotos album, BackgroundWorker worker)
        {
            const int recordStart = 0;

            const int recordsPerCall = 1000;

            string applicationDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            int photosReceived = 0;

            string albumPhotosUrl = string.Format(album.Uri, recordStart, recordsPerCall);

            string pageContents = Utils.DownloadYahooWebPage(albumPhotosUrl, _cookieContainer);

            if (string.IsNullOrEmpty(pageContents)) return;

            string refererUri = albumPhotosUrl;

            var oPhotos = JObject.Parse(pageContents);

            int photosCount = (int)oPhotos["ygData"]["total"];

            album.SetPhotosCount(photosCount);

            while (photosReceived < photosCount)
            {
                int recordsThisLoop = oPhotos["ygData"]["photos"].Count();

                if (recordsThisLoop == 0)
                {
                    break;
                }

                foreach (var oPhoto in oPhotos["ygData"]["photos"])
                {
                    if (worker.CancellationPending)
                        break;

                    string imageName = (string) oPhoto["photoFilename"];

                    if (imageName == "n/a" || string.IsNullOrEmpty(imageName))
                    {
                        string imageFileExt;

                        string imageFileType = oPhoto["fileType"].ToString();

                        switch (imageFileType)
                        {
                            case "image/jpeg":
                                imageFileExt = ".jpg";
                                break;
                            case "image/png":
                                imageFileExt = ".png";
                                break;
                            case "image/bmp":
                                imageFileExt = ".bmp";
                                break;
                            case "image/x-icon":
                                imageFileExt = ".ico";
                                break;
                            case "image/gif":
                                imageFileExt = ".gif";
                                break;
                            case "image/x-pict":
                                imageFileExt = ".pct";
                                break;
                            default:
                                imageFileExt = ".unknown";
                                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Unhandled image file type '{0}' for image name '{1}'; defaulting to '.unknown'", imageFileType, imageName));
                                break;
                        }

                        imageName = string.Format("{0}_{1}{2}", (string) oPhoto["photoName"], oPhoto["photoId"], imageFileExt);
                    }

                    imageName = Utils.MakeValidFileName(imageName);

                    string downloadPath = string.Format("{0}\\PG Offline 4\\Photos\\{1}\\{2}\\{3}", applicationDataPath, _name, Utils.FixDirectoryName(album.Name), imageName);

                    //get the last image, which should be the original photo
                    var photoUri = (string) oPhoto["photoInfo"].Last()["displayURL"];

                    var downloadUri = new Uri(photoUri);

                    //get all the uri's for the image since some might not exist and we can check them
                    var downloadUris = oPhoto["photoInfo"].Children().OrderByDescending(x => x["size"]);

                    var downloadUriList = downloadUris.Select(item => new Uri(string.Format("{0}?download=1", item["displayURL"].ToString()))).ToList();

                    album.AddPhoto(new YahooGroupPhoto(imageName, downloadPath, downloadUri, downloadUriList, album, refererUri));

                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Discovered photo {0} [{1}][{2}]", imageName, _name, album.Name));
                }

                photosReceived += recordsThisLoop;
                
                // handle case when no records exist at all
                if (photosCount > 0 && photosReceived != 0) continue;

                Trace.WriteLine("Error. Photo information unavailable.");
            }
        }
        
        #endregion

        public void SetCookieContainer(CookieContainer cookieContainer)
        {
            _cookieContainer = cookieContainer;
        }

    }
}
