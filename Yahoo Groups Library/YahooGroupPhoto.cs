﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class YahooGroupPhoto : INotifyPropertyChanged
    {
        private string _name;
        private string _pathOnDisk;
        private Uri _yahooUri;
        private List<Uri> _yahooUriList;
        private string _referer;
        private bool _isChecked = true;
        private YahooGroupAlbumOfPhotos _album;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public string PathOnDisk
        {
            get
            {
                return _pathOnDisk;
            }
        }

        public Uri YahooUri
        {
            get
            {
                return _yahooUri;
            }
        }
        public List<Uri> YahooUriList
        {
            get
            {
                return _yahooUriList;
            }
        }

        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                _isChecked = value;
                _album.VerifyCheckState();
                PropertyChanged(this, new PropertyChangedEventArgs("IsChecked"));
            }
        }

        public string Referer
        {
            get
            {
                return _referer;
            }
        }

        public YahooGroupPhoto(string name, string pathOnDisk, Uri yahooUri, List<Uri> yahooUriList, YahooGroupAlbumOfPhotos album, string referer)
        {
            this._name = name;
            this._pathOnDisk = pathOnDisk;
            this._yahooUri = yahooUri;
            this._yahooUriList = yahooUriList;
            this._album = album;
            this._referer = referer;
        }

        public FileStream GetFileStream()
        {
            return File.Exists(_pathOnDisk) ? File.OpenRead(_pathOnDisk) : null;
        }
    }
}
