﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class YahooGroupAttachment
    {
        public string Type { get; set; }

        public string FileType { get; set; }

        public string Name { get; set; }

        public string PathOnDisk { get; set; }

        public Uri YahooUri { get; set; }

        public string RefererUri { get; set; }

        public uint MessageNumber { get; set; }

        public YahooGroupAttachment(string name, string pathOnDisk, Uri yahooUri, string refererUri, uint messageNumber)
        {
            Name = name;
            PathOnDisk = pathOnDisk;
            YahooUri = yahooUri;
            MessageNumber = messageNumber;
            RefererUri = refererUri;
        }

        public YahooGroupAttachment()
        {
        }

        public FileStream GetFileStream()
        {
            return File.Exists(PathOnDisk) ? File.OpenRead(PathOnDisk) : null;
        }
    }
}
