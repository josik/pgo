﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class AttachmentsDownloader
    {
        DownloadOptions _downloadOptions;

        CookieContainer _cookieContainer;

        public AttachmentsDownloader(DownloadOptions downloadOptions, CookieContainer cookieContainer)
        {
            _downloadOptions = downloadOptions;
            _cookieContainer = cookieContainer;
        }

        public void DownloadAttachments(YahooGroupMessage message)
        {
            foreach (var attachment in message.Attachments)
            {
                if (!IsAttachmentFilterSatified(attachment)) continue;

                if (File.Exists(attachment.PathOnDisk)) continue;

                var success = Utils.DownloadFile(attachment.YahooUri, attachment.PathOnDisk, _cookieContainer, attachment.RefererUri);

                if (success) continue;
#if DEBUG
                Trace.Write(string.Format("/DEBUG/[{0}]", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));
#endif
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("One or more files failed to download for message number {0}", attachment.MessageNumber));
            }
        }
        
        private bool IsAttachmentFilterSatified(YahooGroupAttachment attachment)
        {
            bool isSatisfyFilter = Utils.IsFilenameFilterSatisfied(attachment.Name, _downloadOptions.IncludeAttachmentsPattern, _downloadOptions.ExcludeAttachmentsPattern);

            int attachmentSize = Utils.GetFileSize(attachment.YahooUri, _cookieContainer, attachment.RefererUri);

            if (attachmentSize < _downloadOptions.MinimumAttachmentSize || _downloadOptions.MaximumAttachmentSize < attachmentSize)
            {
                isSatisfyFilter = false;
            }

            return isSatisfyFilter;
        }


        //public List<YahooGroupAttachment> ReceiveAttachments(string pageMessageContents, uint messageNumber)
        //{
        //    List<YahooGroupAttachment> attachments = new List<YahooGroupAttachment>();

        //    HtmlDocument doc = new HtmlDocument();

        //    doc.LoadHtml(pageMessageContents);

        //    List<HtmlNode> filesNodes = doc.DocumentNode.QuerySelectorAll(YahooGroupsServer.GetParsingOptionsValue("fizzler_attachment_node")).ToList();

        //    List<HtmlNode> photosNode = doc.DocumentNode.QuerySelectorAll(YahooGroupsServer.GetParsingOptionsValue("fizzler_photo_attachment_node")).ToList();

        //    filesNodes.AddRange(photosNode);

        //    foreach (HtmlNode node in filesNodes)
        //    {
        //        try
        //        {
        //            YahooGroupAttachment attachment = ExtractAttachment(node, messageNumber);

        //            if (attachment != null)
        //            {
        //                attachments.Add(attachment);
        //            }
        //        }
        //        catch (AttachmentDownloadException e1)
        //        {
        //            Trace.WriteLine(e1.Message);
        //        }
        //        catch (Exception)
        //        {
        //            Trace.WriteLine("Failed to download attachment");
        //        }
        //    }

        //    return attachments;
        //}

        //private YahooGroupAttachment ExtractAttachment(HtmlNode node, uint messageNumber)
        //{
        //    YahooGroupAttachment attachment = GetAttachmentFromHtml(node, messageNumber);

        //    if (!IsAttachmentFilterSatified(attachment))
        //        throw new AttachmentDownloadException(String.Format("Attachment '{0}' filtered out from download.", attachment.Name));

        //    try
        //    {
        //        string applicationDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

        //        string downloadPath = string.Format("{0}\\{1}", _downloadOptions.FolderForAttachments, attachment.Name);

        //        if (File.Exists(downloadPath)) return attachment;

        //        Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Downloading attachment '{0}'.", attachment.Name));
                
        //        //string newPath = Utils.DownloadFile(attachment.YahooUri, downloadPath, _cookieContainer, attachment.RefererUri);

        //        //attachment.PathOnDisk += newPath.Substring(newPath.LastIndexOf("."));

        //        var downloadStatus = Utils.DownloadFile(attachment.YahooUri, downloadPath, _cookieContainer, attachment.RefererUri);
                
        //        return attachment;
        //    }
        //    catch (Exception)
        //    {
        //        throw new AttachmentDownloadException(String.Format("Failed to download attachment '{0}'.", attachment.Name));
        //    }
        //}
        
        //private YahooGroupAttachment GetAttachmentFromHtml(HtmlNode node, uint messageNumber)
        //{
        //    string attachmentName = null;
        //    string attachmentUri = null;
        //    string attachmentReferer = null; ;

        //    HtmlNode aNode = node.QuerySelector("a");
        //    HtmlNode imgNode = node.QuerySelector("img");
        //    // Attachment is photo.
        //    if (aNode != null && imgNode != node)
        //    {
        //        attachmentReferer = node.QuerySelector("a").Attributes["href"].Value;
        //        attachmentUri = node.QuerySelector("img").Attributes["src"].Value;
        //        // Link to thumbnail substituted with link to original.
        //        attachmentUri = attachmentUri.Replace("/tn/", "/sn/");
        //        attachmentName = node.QuerySelector("img").Attributes["alt"].Value;
        //    }
        //    // Attachment is file.
        //    else
        //    {
        //        attachmentName = WebUtility.HtmlDecode(node.InnerText);
        //        attachmentUri = node.Attributes["href"].Value;
        //        attachmentReferer = "";
        //    }

        //    string downloadPath = string.Format("{0}\\{1}", _downloadOptions.FolderForAttachments, attachmentName);

        //    YahooGroupAttachment attachment = new YahooGroupAttachment(attachmentName, downloadPath, new Uri(attachmentUri), attachmentReferer, messageNumber);

        //    return attachment;
        //}
        

        /*
        private List<YahooGroupAttachment> ReceivePhotoAttachments(string pageMessageContents, uint messageNumber, bool downloadAttachments)
        {
            List<YahooGroupAttachment> attachments = new List<YahooGroupAttachment>();

            HtmlAgilityPack.doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(pageMessageContents);

            IEnumerable<HtmlNode> filesNodes = doc.DocumentNode.QuerySelectorAll(YahooGroupsServer.GetParsingOptionsValue("fizzler_photo_attachment_node"));
            foreach (HtmlNode node in filesNodes)
            {
                string photoReferer = node.QuerySelector("a").Attributes["href"].Value;
                string photoUri = node.QuerySelector("img").Attributes["src"].Value;
                string photoName = node.QuerySelector("img").Attributes["alt"].Value;

                bool isSatisfyFilter = Utils.IsSatisfyFilter(photoName, _downloadOptions.IncludeAttachmentsPattern, _downloadOptions.ExcludeAttachmentsPattern);
                System.Diagnostics.Trace.WriteLine("Attachment name - " + photoName + ". Condition - (" + _downloadOptions.IncludeAttachmentsPattern + ")" + ". Satisfies? " + isSatisfyFilter.ToString());

                if (isSatisfyFilter == false)
                    continue;

                int attachmentSize = Utils.GetFileSize(new Uri(photoUri), _cookieContainer, photoReferer);
                System.Diagnostics.Trace.WriteLine("Size of " + photoReferer + " - " + attachmentSize.ToString() + " kbytes, limitations (" + _downloadOptions.MinimumAttachmentSize.ToString() + " - " + _downloadOptions.MaximumAttachmentSize + ")");
                if (attachmentSize < _downloadOptions.MinimumAttachmentSize || _downloadOptions.MaximumAttachmentSize < attachmentSize)
                    continue;

                string applicationDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                string downloadPath = _downloadOptions.FolderForAttachments + "\\" + photoReferer;

                System.Diagnostics.Trace.WriteLineIf(downloadAttachments == false, "Downloading attachments disabled.");

                if (downloadAttachments)
                {
                    if (System.IO.File.Exists(downloadPath) == false)
                    {
                        System.Diagnostics.Trace.WriteLine("Downloading attachment " + downloadPath);
                        Utils.DownloadFile(new Uri(photoUri), downloadPath, _cookieContainer, String.Empty);
                    }
                }

                YahooGroupAttachment attachment = new YahooGroupAttachment(photoReferer, downloadPath, new Uri(photoUri), photoReferer, messageNumber);
                attachments.Add(attachment);
            }

            return attachments;
        }
        */

        /*
        public List<YahooGroupAttachment> GetAttachmentsList()
        {
            _attachments.Clear();

            DownloadAllAttachments();

            return _attachments;
        }
        */

        /*
        public void DownloadMessageAttachments(YahooGroupMessage message)
        {
            foreach (YahooGroupAttachment attachment in message.Attachments)
            {
                if (System.IO.File.Exists(attachment.PathOnDisk) == false)
                {
                    Utils.DownloadFile(attachment.YahooUri, attachment.PathOnDisk, _cookieContainer, String.Empty);
                }
            }
        }
        */

        /*
        private void DownloadAllAttachments()
        {
            string pageAttachmentsUri = "http://groups.yahoo.com/groups/" + _name + "/attachments/folder/0/list?mode=list";
            string pageAttachmentContents = Utils.DownloadWebPage(pageAttachmentsUri, _cookieContainer);

            // Get attachments count.
            Regex regexCount = new Regex(YahooGroupsServer.GetParsingOptionsValue("regex_attachments_count"));
            Match matchCount = regexCount.Match(pageAttachmentContents);
            string attachmentsCount = matchCount.Groups[1].Value;

            pageAttachmentsUri += "&count=" + attachmentsCount;
            pageAttachmentContents = Utils.DownloadWebPage(pageAttachmentsUri, _cookieContainer);
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(pageAttachmentContents);

            System.Diagnostics.Trace.WriteLine("Receiving attachments for group " + _name + ". Attachments count - " + attachmentsCount);

            IEnumerable<HtmlNode> messagesNodes = doc.DocumentNode.QuerySelectorAll(YahooGroupsServer.GetParsingOptionsValue("fizzler_attachment_original_message"));
            foreach (HtmlNode node in messagesNodes)
            {
                string messageUri = "http://groups.yahoo.com";
                messageUri += node.Attributes["href"].Value;
                string pageMessageContents = Utils.DownloadWebPage(messageUri, _cookieContainer);
                string messageNumberString = messageUri.Substring(messageUri.LastIndexOf('/') + 1);
                uint messageNumber = uint.Parse(messageNumberString);
                List<YahooGroupAttachment> attachments = ReceiveAttachments(pageMessageContents, messageNumber, true);
                this._attachments.AddRange(attachments);
            }
        }
        */
    }
}
