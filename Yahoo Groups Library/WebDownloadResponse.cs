﻿using System.Net;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class WebDownloadResponse
    {
        public bool IsSuccess { get; set; }

        public string ResponseCodeText { get; set; }

        public string Content { get; set; }

        public WebException WebException { get; set; }

        public CookieContainer CookieContainer { get; set; }

        public HttpStatusCode HttpStatusCode { get; set; }

        public WebDownloadResponse()
        {
            IsSuccess = false;

            Content = string.Empty;
        }
    }
}
