﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class YahooGroupMessage
    {
        private List<YahooGroupAttachment> _attachments;
        public uint Number { get; set; }

        public uint PrevInTime { get; set; }

        public uint NextInTime { get; set; }

        public uint PrevInTopic { get; set; }

        public uint NextInTopic { get; set; }

        public uint TopicId { get; set; }

        public uint ThreadLevel { get; set; }

        public uint ParentId { get; set; }

        public DateTime Date { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string Email { get; set; }

        public string Content { get; set; }

        public uint HasAttachments { get; set; }

        public List<YahooGroupAttachment> Attachments
        {
            get
            {
                return _attachments;
            }
            set
            {
                //var token = JObject.Parse(value.ToString());
                
//{[
//  {
//    "groupId": 11048845,
//    "attachmentId": 167553899,
//    "fileId": 142972897,
//    "title": "Solar Eclipse July 22nd 2009",
//    "creatorNickname": "masbagusp",
//    "modificationDate": 1248322697,
//    "referenceId": 3103,
//    "size": 48128,
//    "link": "http://xa.yimg.com/kq/groups/11048845/142972897/name/Solar+Eclipse+July+22nd+2009.doc",
//    "filename": "Solar Eclipse July 22nd 2009.doc",
//    "fileType": "application/msword",
//    "type": "file",
//    "canDelete": false,
//    "photoInfo": []
//  }
//]}
                _attachments = value;
            }
        }

        public YahooGroupMessage()
        {
            Attachments = new List<YahooGroupAttachment>();
        }

        public YahooGroupMessage(string title, string author, string email,string content, uint number, DateTime date)
        {
            this.Title = title;
            this.Author = author;
            this.Email = email;
            this.Content = content;
            this.Number = number;
            this.Date = date;
            this.Attachments = new List<YahooGroupAttachment>();
        }

    }
}
