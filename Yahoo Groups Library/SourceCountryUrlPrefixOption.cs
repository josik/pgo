﻿using System.Collections.Generic;
using System.Linq;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class SourceCountryUrlPrefixOption : Singleton<SourceCountryUrlPrefixOption>
    {

        public CountryCode GetCurrentCodeInUse = _sourceCountryCodes.FirstOrDefault(i => i.IsDefault);

        public bool SetNextEnabledCode()
        {
            CountryCode item = _sourceCountryCodes.FirstOrDefault(i => i.IsEnabled.Equals(true));

            if (item == null)
            {
                EnableAllServers();

                item = _sourceCountryCodes.FirstOrDefault(i => i.IsEnabled.Equals(true));
            }

            GetCurrentCodeInUse = item;

            return (item != null);
        }

        public void DisableCurrentCodeInUse()
        {
            GetCurrentCodeInUse.IsEnabled = false;
        }

        // http://antezeta.com/news/yahoo-search-domains

        private static List<CountryCode> _sourceCountryCodes = new List<CountryCode>()
        {
            new CountryCode { Code = "ar", IsDefault = false },
            new CountryCode { Code = "br" },
            new CountryCode { Code = "ca" },
            new CountryCode { Code = "espanol" },
            new CountryCode { Code = "dk" },
            new CountryCode { Code = "es" },
            new CountryCode { Code = "fr" },
            new CountryCode { Code = "hk" },
            new CountryCode { Code = "in" },
            new CountryCode { Code = "it" },
            new CountryCode { Code = "kr" },
            new CountryCode { Code = "mx" },
            new CountryCode { Code = "au" },
            new CountryCode { Code = "ph" },
            new CountryCode { Code = "sg" },
            new CountryCode { Code = "de", IsDefault = false },
            new CountryCode { Code = "uk", IsDefault = false, IsEnabled = true },
            new CountryCode { Code = "", IsDefault = true, IsEnabled = true }
        };
        // new CountryCode { Code = "cn", IsDefault = false, IsEnabled = false }

        public void EnableAllServers()
        {
            _sourceCountryCodes.ForEach(i => i.IsEnabled = true);
        }
        
        public void DisableAllServers()
        {
            _sourceCountryCodes.ForEach(i => i.IsEnabled = false);
        }


        public class CountryCode
        {
            public string Code { get; protected internal set; }

            public bool IsEnabled { get; protected internal set; }

            public bool IsDefault { get; protected internal set; }

            public string CodePeriodPostfix
            {
                get
                {
                    return Code == string.Empty ? string.Empty : Code + ".";
                }
            }

            public string CodePeriodPrefix
            {
                get
                {
                    return Code == string.Empty ? string.Empty : "." + Code;
                }
            }

            public CountryCode()
            {
                Code = string.Empty;

                IsEnabled = true;

                IsDefault = false;
            }
        }

    }

    

}
