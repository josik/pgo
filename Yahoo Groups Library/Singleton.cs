﻿namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class Singleton<T> where T : new()
    {
        protected Singleton()
        {
        }

        private sealed class SingletonCreator
        {
            private static readonly T _instance = new T();
            public static T instance
            {
                get
                {
                    return _instance;
                }
            }
        }

        public static T Instance
        {
            get
            {
                return SingletonCreator.instance;
            }
        }
    }
}
