﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;

namespace Asfdfdfd.Api.Yahoo.Groups.DownloadLimiter
{
    class SpeedDownloadLimiter : IDownloadLimiter
    {
        Stopwatch _minuteStopwatch = Stopwatch.StartNew();

        SpeedLimitOptions _speedLimitOptions;

        public SpeedDownloadLimiter(SpeedLimitOptions speedLimitOptions)
        {
            _speedLimitOptions = speedLimitOptions;
        }

        public void InitialDelay()
        {
        }

        public int GetLimit(int messagesPerMinute)
        {
            if (_minuteStopwatch.ElapsedMilliseconds >= 60000 && messagesPerMinute > 0)
            {
                _minuteStopwatch.Restart();
                messagesPerMinute = _speedLimitOptions.MessagesPerMinute;
            }
            else if (_minuteStopwatch.ElapsedMilliseconds < 60000 && messagesPerMinute <= 0)
            {
                Thread.Sleep((int)(60000 - _minuteStopwatch.ElapsedMilliseconds));

                _minuteStopwatch.Restart();
                messagesPerMinute = _speedLimitOptions.MessagesPerMinute;
            }

            return messagesPerMinute;
        }

        public int MessagesPerMinute
        {
            get
            {
                return _speedLimitOptions.MessagesPerMinute;
            }
        }
    }
}
