﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Asfdfdfd.Api.Yahoo.Groups.DownloadLimiter
{
    class RepeatDownloadLimiter : IDownloadLimiter
    {
        SpeedLimitOptions _speedLimitOptions;

        public RepeatDownloadLimiter(SpeedLimitOptions speedLimitOptions)
        {
            _speedLimitOptions = speedLimitOptions;
        }

        public void InitialDelay()
        {
            Thread.Sleep(_speedLimitOptions.InitialDelay * 60 * 1000);
        }

        public int GetLimit(int messagesPerMinute)
        {
            if (messagesPerMinute <= 0)
            {
                messagesPerMinute = _speedLimitOptions.Download;

                Thread.Sleep(_speedLimitOptions.ThenWait * 1000 * 60);
            }

            return messagesPerMinute;
        }

        public int MessagesPerMinute
        {
            get
            {
                return _speedLimitOptions.Download;
            }
        }
    }
}
