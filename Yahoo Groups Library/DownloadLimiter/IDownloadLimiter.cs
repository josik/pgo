﻿namespace Asfdfdfd.Api.Yahoo.Groups.DownloadLimiter
{
    interface IDownloadLimiter
    {
        void InitialDelay();

        int GetLimit(int messagesPerMinute);

        int MessagesPerMinute { get; }
    }
}
