﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class YahooGroupDirectory : INotifyPropertyChanged
    {
        string _name;
        List<YahooGroupFile> _files;
        List<YahooGroupDirectory> _childDirectories;
        YahooGroupDirectory _parentDirectory;
        bool _isRootDirectory;
        string _uri;
        string _path;
        bool? _isChecked;

        public event PropertyChangedEventHandler PropertyChanged;

        public YahooGroupDirectory(string name, string uri, YahooGroupDirectory parentDirectory)
        {
            _name = name;
            _uri = uri;
            _parentDirectory = parentDirectory;
            _isChecked = true;

            if (parentDirectory == null)
            {
                _path = "\\";
                _isRootDirectory = true;
            }
            else
            {
                _path = string.Format("{0}{1}\\", parentDirectory.Path, name);
                _isRootDirectory = false;
            }

            _childDirectories = new List<YahooGroupDirectory>();
            _files = new List<YahooGroupFile>();
        }

        public string Name
        {
            get
            {
                return string.Format("<{0}>", _name);
            }
        }

        public string Path
        {
            get
            {
                return _path;
            }
        }

        public List<object> Children
        {
            get
            {
                List<object> children = new List<object>();
                children.AddRange(_childDirectories);
                children.AddRange(_files);
                return children;
            }
        }

        public bool IsHaveChildren
        {
            get
            {
                return _files.Count > 0 || _childDirectories.Count > 0;
            }
        }

        public bool? IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                _isChecked = value;
                foreach (YahooGroupFile file in _files)
                    file.IsChecked = (bool)value;

                foreach (YahooGroupDirectory directory in _childDirectories)
                    directory.IsChecked = value;

                PropertyChanged(this, new PropertyChangedEventArgs("IsChecked"));
            }
        }

        public string Uri
        {
            get
            {
                return _uri;
            }
        }

        public bool IsRootDirectory
        {
            get
            {
                return _isRootDirectory;
            }
        }

        public List<YahooGroupFile> Files
        {
            get
            {
                return _files;
            }
        }

        public List<YahooGroupDirectory> ChildDirectories
        {
            get
            {
                return _childDirectories;
            }
        }

        //public void AddFile(YahooGroupFile file)
        //{
        //    _files.Add(file);
        //}

        //public void AddDirectory(YahooGroupDirectory directory)
        //{
        //    _childDirectories.Add(directory);
        //}

        public void AddDirectoriesRange(List<YahooGroupDirectory> directories)
        {
            _childDirectories.AddRange(directories);
        }

        public void AddFilesRange(List<YahooGroupFile> files)
        {
            _files.AddRange(files);
        }

        public void VerifyCheckState()
        {
            int checkedChildren = 0;
            int indetermineDirectories = 0;

            foreach (YahooGroupFile file in _files)
                if (file.IsChecked == true)
                    checkedChildren++;

            foreach (YahooGroupDirectory directory in _childDirectories)
            {
                if (directory.IsChecked == true)
                    checkedChildren++;

                if (directory.IsChecked == null)
                    indetermineDirectories++;
            }

            if (checkedChildren == _childDirectories.Count + _files.Count)
                _isChecked = true;
            else if (checkedChildren == 0 && indetermineDirectories == 0)
                _isChecked = false;
            else
                _isChecked = null;

            if (_parentDirectory != null)
                _parentDirectory.VerifyCheckState();

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("IsChecked"));            
        }
    }
}

