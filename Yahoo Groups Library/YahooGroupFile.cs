﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.ComponentModel;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class YahooGroupFile : INotifyPropertyChanged
    {
        private string _name;
        private string _pathOnDisk;
        private Uri _yahooUri;
        private bool _isChecked;
        private YahooGroupDirectory _directory;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public string PathOnDisk
        {
            get
            {
                return _pathOnDisk;
            }
        }

        public Uri YahooUri
        {
            get
            {
                return _yahooUri;
            }
        }

        public YahooGroupFile(string name, string pathOnDisk, Uri yahooUri, YahooGroupDirectory directory)
        {
            this._name = name;
            this._pathOnDisk = pathOnDisk;
            this._yahooUri = yahooUri;
            this._isChecked = true;
            this._directory = directory;
        }

        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                _isChecked = value;
                _directory.VerifyCheckState();
                PropertyChanged(this, new PropertyChangedEventArgs("IsChecked"));
            }
        }

        public FileStream GetFileStream()
        {
            if (File.Exists(_pathOnDisk))
            {
                return File.OpenRead(_pathOnDisk);
            }
            else
            {
                return null;
            }
        }
    }
}
