﻿using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using CsQuery.ExtensionMethods.Internal;
using CsQuery.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public static class Utils
    {
        public static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));

            string invalidReStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return Regex.Replace(name, invalidReStr, "_");
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);

            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();

            return dtDateTime;
        }
        
        # region JSON
        
        public static string DownloadJson(string uri, int timeoutInSeconds=10)
        {
            try
            {
                // Used for self-signed certs
                ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };

                var request = (HttpWebRequest) WebRequest.Create(uri);

                request.Timeout = timeoutInSeconds * 1000;

                var response = (HttpWebResponse) request.GetResponse();

                // Save the stream to string.
                Stream responseStream = response.GetResponseStream();

                if (responseStream == null) return string.Empty;

                var reader = new StreamReader(responseStream, Encoding.GetEncoding("windows-1252"));

                string text = reader.ReadToEnd();

                reader.Close();

                response.Close();

                return text;
            }
            catch (Exception ex)
            {
                return string.Empty;
                //throw;
            }
        }

        //public static async Task PostJsonAsync(string uri, JObject jsonContent, int timeoutInSeconds = 30)
        //{
        //    // DOESN'T SEEM TO WORK, IT LOSES THE jsonContent VALUE WHEN IN A TASK
        //    Thread.Sleep(3000);
        //    ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => true;

        //    try
        //    {
        //        Stream JSonStream = new MemoryStream();
        //        StreamWriter streamWriter = new StreamWriter(JSonStream);
        //        JsonSerializer JSonSerializer = new JsonSerializer();
        //        JSonSerializer.Serialize(streamWriter, jsonContent);
        //        //streamWriter.WriteAsync(JSonStream, jsonContent);
        //        JSonStream.Seek(0, SeekOrigin.Begin);
        //        HttpClient client = new HttpClient();
        //        HttpContent content = new StreamContent(JSonStream);
        //        await client.PostAsync(uri, content);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        public static Task<string> MakeAsyncRequest(string url, int timeoutInSeconds = 30)
        {
            // Used for self-signed certs
            ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => true;
            
            var request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = "PUT";

            Task<WebResponse> task = Task.Factory.FromAsync(request.BeginGetResponse, asyncResult => request.EndGetResponse(asyncResult), (object)null);

            return task.ContinueWith(t => ReadStreamFromResponse(t.Result));
        }
        public static Task<string> MakeAsyncRequest(string url, JObject jsonContent, int timeoutInSeconds = 30)
        {
            // Used for self-signed certs
            ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => true;
            //ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            var request = (HttpWebRequest)WebRequest.Create(url);

            request.ContentType = "application/json; charset=utf-8";

            request.Method = "POST";

            using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(jsonContent.ToString());
            }

            Task<WebResponse> task = Task.Factory.FromAsync(request.BeginGetResponse, asyncResult => request.EndGetResponse(asyncResult), (object)null);

            return task.ContinueWith(t => ReadStreamFromResponse(t.Result));
        }
        private static string ReadStreamFromResponse(WebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            {
                using (var sr = new StreamReader(responseStream))
                {
                    //Need to return this response 
                    string strContent = sr.ReadToEnd();
                    return strContent;
                }
            }
        } 

        public static void PostJson(string uri, JObject jsonContent, int timeoutInSeconds = 10)
        {
            try
            {
                // Used for self-signed certs
                ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => true;

                HttpWebRequest request = (HttpWebRequest) WebRequest.Create(uri);

                request.Method = "POST";

                request.ContentType = "application/json; charset=utf-8";

                request.Timeout = timeoutInSeconds * 1000;

                //DataContractJsonSerializer ser = new DataContractJsonSerializer(content.GetType());

                //MemoryStream ms = new MemoryStream();

                //ser.WriteObject(ms, jsonContent.ToString());

                //String json = Encoding.UTF8.GetString(ms.ToArray());

                using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(jsonContent.ToString());

                    writer.Flush();

                    writer.Close();

                    var httpResponse = (HttpWebResponse) request.GetResponse();

                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                    }
                }
            }
            catch (WebException we)
            {
                return;
                // noop
            }
            catch (Exception ex)
            {
                return;
                //throw;
            }
        }

        # endregion JSON

        public static WebDownloadResponse GetWebPage(string uri, CookieContainer cookieContainer, bool allowAutoRedirect = true)
        {
            int maxRetry = 5;

            int attemptCount = 0;

            do
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

                    request.AllowAutoRedirect = allowAutoRedirect;

                    request.CookieContainer = cookieContainer;

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    cookieContainer.Add(response.Cookies);

                    // Save the stream to string.
                    Stream responseStream = response.GetResponseStream();

                    if (responseStream == null) return new WebDownloadResponse{ IsSuccess = false, HttpStatusCode = response.StatusCode };

                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("windows-1252"));

                    string text = reader.ReadToEnd();

                    reader.Close();

                    response.Close();

                    return new WebDownloadResponse
                    {
                        IsSuccess = true, 
                        Content = text, 
                        CookieContainer = cookieContainer,
                        HttpStatusCode = response.StatusCode
                    };
                }
                catch (WebException we)
                {
                    // http://msdn.microsoft.com/en-us/library/system.net.httpstatuscode.aspx
                    if (attemptCount == maxRetry)
                    {
                        // locked out of server
                        var r = new WebDownloadResponse
                        {
                            IsSuccess = false,
                            HttpStatusCode = ((HttpWebResponse)we.Response).StatusCode,
                            WebException = we
                        };

                        return r;

                        //SourceCountryUrlPrefixOption.Instance.DisableCurrentCodeInUse();
                        //SourceCountryUrlPrefixOption.Instance.SetNextEnabledCode();

                    }
                }
                catch (Exception ex)
                {
                    // TODO: handle/log exception
                }

            } while (++attemptCount <= maxRetry);

            return new WebDownloadResponse { IsSuccess = false };
        }

        public static string DownloadYahooWebPage(string uri, CookieContainer cookieContainer, bool allowAutoRedirect = true)
        {
            int maxRetry = 5;

            int attemptCount = 0;

            do
            {
                try
                {
                    return DownloadWebPage(uri, cookieContainer, allowAutoRedirect);
                }
                catch (WebException we)
                {
                    if (we.Status == WebExceptionStatus.NameResolutionFailure)
                    {
                        // server domain does not exist
#if DEBUG
                        Trace.Write(string.Format(@"/DEBUG\ WARNING: [{0}] server domain does not resolve.", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));
#endif
                        return string.Empty;
                    }

                    if (we.Response == null) return string.Empty;

                    HttpStatusCode wRespStatusCode = ((HttpWebResponse)we.Response).StatusCode;

                    // not found on this server, so try the US server if this is the next-to-last attempt
                    //if (attemptCount == maxRetry && (wRespStatusCode == HttpStatusCode.NotFound || wRespStatusCode == HttpStatusCode.InternalServerError))
                    if (wRespStatusCode == HttpStatusCode.SeeOther || wRespStatusCode == HttpStatusCode.NotFound || wRespStatusCode == HttpStatusCode.InternalServerError)
                    {
                        uri = uri.Replace(string.Format("{0}.groups.yahoo.com", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code), "groups.yahoo.com");
                        try
                        {
                            return DownloadWebPage(uri, cookieContainer, allowAutoRedirect);
                        }
                        catch (Exception ex)
                        {
                            // the active server and now the US server responds with an error, so perhaps the page really doesn't seem to exist
                            // we shouldn't assume this server has us locked out
                            //Trace.WriteLine("Yahoo servers appear to be offline or your IP has been temporarily locked out.");
                            SourceCountryUrlPrefixOption.Instance.DisableCurrentCodeInUse();
                            SourceCountryUrlPrefixOption.Instance.SetNextEnabledCode();
                            throw new YahooGroupsException("Yahoo servers appear to be offline or your IP has been temporarily locked out.");
                            //return string.Empty;
                            //return "(SWITCH SERVER)";
                        }
                    }

                    // http://msdn.microsoft.com/en-us/library/system.net.httpstatuscode.aspx
                    if (attemptCount != maxRetry || wRespStatusCode != HttpStatusCode.InternalServerError) continue;

                    // locked out of server, so attempt to switch

                    SourceCountryUrlPrefixOption.Instance.DisableCurrentCodeInUse();

                    if (SourceCountryUrlPrefixOption.Instance.SetNextEnabledCode())
                    {
                        //return "(SWITCH SERVER)";
                        return "(RETRY)";
                    }

                    // all servers might now be locked
                    Trace.WriteLine("Yahoo servers appear to be offline or your IP has been temporally locked out.");
                            
                    // re-enable all servers
                    SourceCountryUrlPrefixOption.Instance.EnableAllServers();

                    return string.Empty;
                }
                catch (Exception ex)
                {
                    // TODO: log the exception
                    // ignore the exception since we'll keep retrying and then switch to other servers.
                }

            } while (++attemptCount <= maxRetry);

            return string.Empty;
        }

        private static string DownloadWebPage(string uri, CookieContainer cookieContainer, bool allowAutoRedirect)
        {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(uri);

            request.AllowAutoRedirect = allowAutoRedirect;

            request.CookieContainer = cookieContainer;

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();

            // Save the stream to string.
            Stream responseStream = response.GetResponseStream();

            if (responseStream == null) return string.Empty;

            StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("windows-1252"));

            string text = reader.ReadToEnd();

            reader.Close();

            response.Close();

            // follow javascript redirect specific for Yahoo
            if (text.Contains("Please wait while we are redirecting!"))
            {
                // window.location.href = "https://groups.yahoo.com/neo/groups/7x10minilathe/files";
            }

            return text;
        }

        public static bool DownloadFile(Uri uri, string downloadPath, CookieContainer cookieContainer, string referer)
        {
            try
            {
                var webClient = new WebClient();

                var headers = new WebHeaderCollection { { HttpRequestHeader.Referer, referer } };

                webClient.Headers = headers;

                // Create download directory if it does not exist.
                string directoryPath = downloadPath.Substring(0, downloadPath.LastIndexOf('\\') + 1);

                if (Directory.Exists(directoryPath) == false)
                {
                    Directory.CreateDirectory(directoryPath);
                }

                webClient.DownloadFile(uri, downloadPath);
            }
            catch (Exception ex)
            {
#if DEBUG
                Trace.Write(string.Format("/DEBUG/[{0}]", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));
#endif
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("File failed to download {0} to {1}. {2}", uri, downloadPath, ex.Message));

                return false;
            }

            return true;

            //if (uri.AbsoluteUri.LastIndexOf("/") > uri.AbsoluteUri.LastIndexOf("."))
            //{
            //    string contentType = webClient.ResponseHeaders.Get("Content-Type");
            //    newPath = AddExtensionToFile(downloadPath, contentType);

            //    // System.Diagnostics.Trace.WriteLine("Correct file name - " + newPath);
            //}
            
            //return newPath;
        }

        public static string AddExtensionToFile(string downloadPath, string contentType)
        {
            int indexOfSlash = contentType.IndexOf("/");

            int indexOfSemicolon = contentType.IndexOf(";");

            contentType = contentType.Substring(indexOfSlash + 1, indexOfSemicolon - indexOfSlash - 1);

            string newPath = string.Format("{0}.{1}", downloadPath, contentType);

            if (File.Exists(newPath))
            {
                int suffix = 0;

                while (File.Exists(string.Format("{0}{1}.{2}", downloadPath, suffix, contentType)) == true)
                {
                    suffix++;
                }

                newPath = string.Format("{0}{1}.{2}", downloadPath, suffix, contentType);

                File.Move(downloadPath, newPath);
            }
            else
            {
                File.Move(downloadPath, string.Format("{0}.{1}", downloadPath, contentType));
            }

            return newPath;
        }

        //public static void DownloadImage(Uri uri, CookieContainer cookieContainer, string referer)
        //{
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
        //    request.Referer = referer;
        //    request.CookieContainer = cookieContainer;

        //    HttpWebResponse response = (HttpWebResponse)(request.GetResponse());

        //    StreamReader reader = new StreamReader(response.GetResponseStream());
        //    reader.ReadToEnd();
        //    StreamWriter writer = new StreamWriter(

        //    System.Diagnostics.Trace.WriteLine(response.Headers.Get("Content-Type"));
        //    response.Close();
        //}

        public static int GetFileSize(Uri uri, CookieContainer cookieContainer, string referer)
        {
            var request = (HttpWebRequest) WebRequest.Create(uri);

            //request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/A.B (KHTML, like Gecko) Chrome/X.Y.Z.W Safari/A.B.";

            //request.Method = "HEAD";

            request.Referer = referer;

            request.CookieContainer = cookieContainer;

            try
            {
                var response = (HttpWebResponse) (request.GetResponse());

                double sizeDouble = (double)response.ContentLength / 1000;

                int size = (int)Math.Round(sizeDouble);

                //System.Diagnostics.Trace.WriteLine(response.Headers.Get("Content-Type"));

                response.Close();

                return size;
            }
            catch (Exception ex)
            {
#if DEBUG
                Trace.Write(string.Format(@"/DEBUG\ [{0}]", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower()));

                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("Can't get size of {0}. {1}", uri, ex.Message));
#endif
                return 0;
            }
        }

        public static bool IsFilenameFilterSatisfied(string filename, string includePattern, string excludePattern)
        {
            bool includeAll = includePattern.Contains("*.*");

            bool excludeAll = excludePattern.Contains("*.*");

            if (includeAll && excludeAll == false)
            {
                return true;
            }

            if (excludeAll)
            {
                return false;
            }

            var f = Path.GetExtension(filename);

            if (f == null) return false;

            string extension = f.Replace(".", "");

            var acceptedExtensions = new List<string>();

            var regex = new Regex(@"\*\.(\w*)");

            MatchCollection matchesAccepted = regex.Matches(includePattern);

            foreach (Match match in matchesAccepted)
            {
                acceptedExtensions.Add(match.Groups[1].Value);
            }

            var prohibitedExtensions = new List<string>();

            MatchCollection matchesProhibited = regex.Matches(excludePattern);

            foreach (Match match in matchesProhibited)
            {
                prohibitedExtensions.Add(match.Groups[1].Value);
            }

            bool accept = acceptedExtensions.Contains(extension);

            bool prohibit = prohibitedExtensions.Contains(extension);

            if (accept && prohibit == false)
            {
                return true;
            }

            return false;
        }

        public static string CleanString(string parsedString)
        {
            return parsedString.Trim().Replace("\n", string.Empty);
        }

        public static string CleanEmail(string email)
        {
            if (email.Contains("<") && email.Contains(">"))
            {
                return CleanString(new Regex("<(.*)>").Match(email).Groups[1].Value);
            }

            return CleanString(email);
        }

        public static string GetLastDirectoryNameWithinPath(string dirPath)
        {
            string trimPath = Path.GetFullPath(dirPath).TrimEnd(Path.DirectorySeparatorChar);
            string dirName = trimPath.Split(Path.DirectorySeparatorChar).Last();
            return dirName;
        }
        public static string FixFileName(string rawName)
        {
            char[] chars = Path.GetInvalidFileNameChars();

            rawName = rawName.Replace('/', '_');

            foreach (char c in chars)
            {
                rawName = rawName.Replace(c, '_');
            }

            return rawName;
        }

        public static string FixDirectoryName(string rawName)
        {
            char[] chars = Path.GetInvalidPathChars();

            rawName = rawName.Replace('/', '_');

            rawName = rawName.Replace(':', '_');

            foreach (char c in chars)
            {
                rawName = rawName.Replace(c, '_');
            }

            return rawName;
        }
    }
}
