﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class YahooGroupAlbumOfPhotos : INotifyPropertyChanged
    {
        string _name;
        List<YahooGroupPhoto> _photos;
        string _uri;
        bool? _isChecked = true;

        public event PropertyChangedEventHandler PropertyChanged;

        public YahooGroupAlbumOfPhotos(string name, string uri)
        {
            _name = name;
            _uri = uri;
            _photos = new List<YahooGroupPhoto>();
        }

        public string Name
        {
            get
            {
                return _name; 
            }
        }

        public string Uri
        {
            get
            {
                return _uri;
            }
        }

        public IEnumerable<YahooGroupPhoto> Children
        {
            get
            {
                return _photos;
            }
        }

        public bool? IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                _isChecked = value;
                foreach (YahooGroupPhoto photo in _photos)
                {
                    photo.IsChecked = (bool)value;
                }

                PropertyChanged(this, new PropertyChangedEventArgs("IsChecked"));
            }
        }

        public void VerifyCheckState()
        {
            bool isChecked = false;
            bool isIndeterminate = false;

            if (_photos.Count > 0)
            {
                isChecked = _photos.First().IsChecked;
            }

            foreach (YahooGroupPhoto photo in _photos)
            {
                if (isChecked != photo.IsChecked)
                {
                    isIndeterminate = true;
                    break;
                }

                isChecked = photo.IsChecked;
            }

            if (isIndeterminate)
            {
                _isChecked = null;
            }
            else
            {
                _isChecked = isChecked;
            }

            PropertyChanged(this, new PropertyChangedEventArgs("IsChecked"));
        }

        public void SetPhotosCount(int count)
        {
            if (_uri.Contains("?count="))
            { 
                int length = _uri.IndexOf("?count=", System.StringComparison.Ordinal);
                _uri = _uri.Substring(0, length);
            }

            _uri += "?count=" + count;
        }

        public void AddPhotos(IEnumerable<YahooGroupPhoto> photos)
        {
            _photos.AddRange(photos);
        }

        public void AddPhoto(YahooGroupPhoto photo)
        {
            _photos.Add(photo);
        }
    }
}
