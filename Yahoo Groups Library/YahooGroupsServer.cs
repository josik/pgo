﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using Cookie = System.Net.Cookie;
using NetFwTypeLib;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class YahooGroupsServer
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public delegate void ReceiveGroupsCompletedEventHandler(List<YahooGroup> yahooGroups);
        
        public event ReceiveGroupsCompletedEventHandler ReceiveGroupsCompleted;
        
        private CookieContainer _cookieContainer;
        
        private NameValueCollection _formInputValues;
        
        public List<YahooGroup> _userGroups;

        private SpeedLimitOptions _speedLimitOptions;
        
        private WorkflowOptions _workflowOptions;

        private bool _skipAuthorisation;

        private static Dictionary<string, string> _parsingOptions;
        
        BackgroundWorker _receiveGroupsWorker;

        public AppDownloadOptions ServerAppDownloadOptions { get; set; }


        public YahooGroupsServer(AppDownloadOptions appDownloadOptions)
        {
            ServerAppDownloadOptions = appDownloadOptions;

            _cookieContainer = new CookieContainer();

            _formInputValues = new NameValueCollection();

            _userGroups = new List<YahooGroup>();
        }

        public void SetParsingOptions(Dictionary<string, string> options)
        {
            _parsingOptions = options;
        }

        public void SetAppDownloadOptions(AppDownloadOptions appDownloadOptions)
        {
            ServerAppDownloadOptions = appDownloadOptions;

            foreach (YahooGroup group in _userGroups)
            {
                group.SetAppDownloadOptions(appDownloadOptions);
            }
        }

        public void SetSpeedLimit(SpeedLimitOptions speedLimitOptions)
        {
            _speedLimitOptions = speedLimitOptions;
            foreach (YahooGroup group in _userGroups)
            {
                group.SetSpeedLimit(_speedLimitOptions);
            }
        }

        public void SetWorkflow(WorkflowOptions workflowOptions)
        {
            _workflowOptions = workflowOptions;
            foreach (YahooGroup group in _userGroups)
            {
                group.SetWorkflow(_workflowOptions);
            }
        }

        public bool SkipAuthorization
        {
            set
            {
                _skipAuthorisation = value;
            }
        }
        
        public static string GetParsingOptionsValue(string key)
        {
            return _parsingOptions.ContainsKey(key) ? _parsingOptions[key] : string.Empty;
        }

        //public bool Login2(string username, string password)
        //{
        //    if (_skipAuthorisation) return true;

        //    _formInputValues.Clear();

        //    InputLoginAndPassword(username, password);

        //    InputStaticValues();

        //    InputServiceValues();

        //    // Send authentication request.
        //    SendLogonRequest(_formInputValues, "https://login.yahoo.com/config/login?");

        //    // Check logon success.
        //    bool logonSuccess = IsLoggedIn();

        //    if (logonSuccess == false)
        //    {
        //        throw new LoginException("Login failed");
        //    }

        //    _cookieContainer.Add(new Cookie("adult", string.Format("1{0}#lr", username), "/", ".groups.yahoo.com"));

        //    return true;
        //}

        //private bool IsLoggedIn()
        //{
        //    BrowserHelper.Browser.Navigate().Refresh();
        //    return (BrowserHelper.Browser.Url.StartsWith("https://groups.yahoo.com/neo"));
        //}

        //public bool Login(string username = "", string password = "")
        //{
        //    //if (IsLoggedIn(username, password)) return true;
        //    if (IsLoggedIn())
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        if (LoadLoginPage("https://login.yahoo.com/config/login?.src=fpctx&.intl=us&.done=https%3A%2F%2Fgroups.yahoo.com%2F"))
        //        return SendLoginRequest(username, password);
        //    }
            
        //    return false;
        //}

        //private bool SendLoginRequest(string username, string password)
        //{
        //    //IWebElement element = BrowserHelper.Browser.FindElementById("mbr-login-form");
        //    if (BrowserHelper.Browser.FindElementById("login-username").TagName=="input")
        //        BrowserHelper.Browser.FindElementById("login-username").SendKeys(username);
        //    BrowserHelper.Browser.FindElementById("login-passwd").SendKeys(password);
        //    BrowserHelper.Browser.GetScreenshot().SaveAsFile(@"C:\Users\ihor_Hadzera@epam.com\Desktop\screenshot.png", System.Drawing.Imaging.ImageFormat.Png);
        //    BrowserHelper.Browser.FindElementById("mbr-login-form").Submit();
        //    BrowserHelper.Browser.GetScreenshot().SaveAsFile(@"C:\Users\ihor_Hadzera@epam.com\Desktop\screenshot.png", System.Drawing.Imaging.ImageFormat.Png);
        //    //_cookieContainer = new CookieContainer();
        //    foreach (var _cookie in BrowserHelper.Browser.Manage().Cookies.AllCookies) 
        //    {
        //        _cookieContainer.Add(new Cookie() { 
        //            Domain = _cookie.Domain,
        //            Expires = _cookie.Expiry.Value, 
        //            Name = _cookie.Name,
        //            Path = _cookie.Path, 
        //            Secure = _cookie.Secure, 
        //            Value = _cookie.Value });}

        //    return (BrowserHelper.Browser.Url.StartsWith("https://groups.yahoo.com/neo"));
        //}

        //private bool LoadLoginPage(string LoginUri)
        //{
        //    BrowserHelper.Browser.Navigate().GoToUrl(LoginUri);
        //    return !String.IsNullOrEmpty(BrowserHelper.Browser.FindElementById("mbr-login-form").Text);
        //}


        private WebDownloadResponse SendLogonRequest(string username,string password, string requestUrl)
        {
            PhantomJSDriverService driverService = PhantomJSDriverService.CreateDefaultService();
            driverService.HideCommandPromptWindow = true;
            PhantomJSOptions driverOptions = new PhantomJSOptions();
            driverOptions.AddAdditionalCapability("phantomjs.page.settings.userAgent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0");
            PhantomJSDriver _Browser = new PhantomJSDriver(driverService, driverOptions);

            try
            {
                _Browser.Navigate().GoToUrl(requestUrl);
                if (!String.IsNullOrEmpty(_Browser.FindElementById("mbr-login-form").Text)) { 
                if (_Browser.FindElementById("login-username").TagName == "input")
                    _Browser.FindElementById("login-username").SendKeys(username);
                _Browser.FindElementById("login-passwd").SendKeys(password);
                _Browser.FindElementById("mbr-login-form").Submit();
                bool isLoggedIn = (_Browser.Manage().Cookies.AllCookies.Count > 0);

                foreach (var _cookie in _Browser.Manage().Cookies.AllCookies)
                {
                    _cookieContainer.Add(new Cookie()
                    {
                        Domain = _cookie.Domain,
                        Expires = _cookie.Expiry.Value,
                        Name = _cookie.Name,
                        Path = _cookie.Path,
                        Secure = _cookie.Secure,
                        Value = _cookie.Value
                    });
                }

                WebDownloadResponse webResponse = new WebDownloadResponse
                {
                    IsSuccess = isLoggedIn,
                    HttpStatusCode = HttpStatusCode.Found,
                    CookieContainer = _cookieContainer
                };
                return webResponse;}
            }
            catch (WebException we)
            {
                WebDownloadResponse webResponse = new WebDownloadResponse
                {
                    IsSuccess = false,
                    //HttpStatusCode = HttpStatusCode.BadRequest //((HttpWebResponse)we.Response).StatusCode
                };
                return webResponse;
            }
            catch (Exception ex)
            {
                // noop
            }
            finally
                {
                    _Browser.Quit();
                }

            return new WebDownloadResponse { IsSuccess = false };
        }





        #region Login1
        
        public bool Login(string username = "", string password = "")
        {
            if (IsLoggedIn(username, password)) return true;
            //if (LoadLoginPage("https://login.yahoo.com/config/login?.src=fpctx&.intl=us&.done=https%3A%2F%2Fgroups.yahoo.com%2Fneo"))
            do
            {
                string loginUrl = string.Format("https://login.yahoo.com/config/login?.src=fpctx&.intl={0}&.done=https%3A%2F%2F{1}groups.yahoo.com%2F",
                        SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code,
                        SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.CodePeriodPostfix);

                // Send authentication request.

                //string loginUrl = "https://login.yahoo.com/config/login?";

                WebDownloadResponse webResponse = SendLogonRequest(username,password, loginUrl);

                if (webResponse.IsSuccess)
                {
                    var groupCookieDomain = ".groups.yahoo.com";

                    groupCookieDomain = SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.CodePeriodPrefix + groupCookieDomain;

                    _cookieContainer.Add(new Cookie("adult", string.Format("1{0}#lr", username), "/", groupCookieDomain));
#if DEBUG
                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format(@"/DEBUG\ Logged in to location '{0}'", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToUpper()));
#endif
                    foreach (YahooGroup group in _userGroups)
                    {
                        group.SetCookieContainer(_cookieContainer);
                    }

                    return true;
                }
                
                if (
                    webResponse.HttpStatusCode == HttpStatusCode.Continue ||
                    webResponse.HttpStatusCode == HttpStatusCode.Found ||
                    webResponse.HttpStatusCode == HttpStatusCode.OK)
                {
                    // credentials simply failed; don't bother trying other locations
                    return false;
                }

                //if (_receiveGroupsWorker != null && _receiveGroupsWorker.CancellationPending)
                //{
                //    Trace.WriteLine("Login canceled.");
                //    return false;
                //};
                
#if DEBUG
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("/DEBUG/ Server location '{0}' may be restricting access.", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToUpper()));
#endif
                SourceCountryUrlPrefixOption.Instance.DisableCurrentCodeInUse();
#if DEBUG
                Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("/DEBUG/ Attempting to use server location '{0}'", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToUpper()));
#endif
                SourceCountryUrlPrefixOption.Instance.SetNextEnabledCode();

            } while (SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.IsEnabled);

            return true;


            //loginUrl = "https://login.yahoo.com/config/login;_ylt=AvNGZRXEFqVT_pkL4mUcqDRLQ9EF?.src=ygrp&.intl=uk&.lang=en-GB&.done=https://uk.groups.yahoo.com/neo";
            //loginUrl = "https://login.yahoo.com/config/login?.src=fpctx&.intl=uk&.done=https%3A%2F%2Fuk.groups.yahoo.com%2F";
            //SendLogonRequest(_formInputValues, "https://login.yahoo.com/config/login?");

            //if (!webResponse.IsSuccess) throw new LoginException("Login failed.");
        }

#endregion

        #region ReLogin
/*
        private void ReLogIn(string username, string password)
        {
            _cookieContainer = new CookieContainer();
            
            Login(username, password);
        }
*/
        #endregion

        private bool IsLoggedIn(string username = "", string password = "")
        {
            if (_skipAuthorisation) return true;
            
            // START CHECK if session should be expired and restarted
            
            CookieCollection cookiesForLoginDomain = _cookieContainer.GetCookies(new Uri("https://login.yahoo.com"));

            //CookieCollection cookieForCountryServerDomain = _cookieContainer.GetCookies(new Uri(string.Format("https://{0}login.yahoo.com", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower())));
            CookieCollection cookieForCountryServerDomain = _cookieContainer.GetCookies(new Uri("https://login.yahoo.com"));

            // no creds supplied so see if the current creds are logged in
            if (username == "" && password == "")
            {
                // check if we are already logged in, otherwise continue logging in
                //if (IsLoggedIn()) return true;
                return cookiesForLoginDomain.Count > 1 && cookieForCountryServerDomain.Count > 1;
            }

            // check if the requested creds are different from what might currently be in use
            if (username != Username || password != Password)
            {
                // we need to login again user new creds
                _cookieContainer = new CookieContainer();
                
                Username = username;

                Password = password;
            }

            // END CHECK if session should be expired and restarted

            cookiesForLoginDomain = _cookieContainer.GetCookies(new Uri("https://login.yahoo.com"));
            
            //cookieForCountryServerDomain = _cookieContainer.GetCookies(new Uri(string.Format("https://{0}login.yahoo.com", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code.ToLower())));
            cookieForCountryServerDomain = _cookieContainer.GetCookies(new Uri("https://login.yahoo.com"));

            return cookiesForLoginDomain.Count > 1 && cookieForCountryServerDomain.Count > 1;

            // PH T SSL

            // TODO: should be determined for each thread; right now all threads would need to be the same country src
            //string groupsPageContents = Utils.DownloadWebPage(string.Format("http://{0}groups.yahoo.com/neo", _userGroups[0].DownloadOptions.SourceCountryUrlPrefix), _cookieContainer);
            //string groupsPageContents = Utils.DownloadWebPage(string.Format("http://{0}groups.yahoo.com/neo", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.CodePeriodPostfix), _cookieContainer);
            //var r = groupsPageContents.Contains("Sign Out");
            //return r;
        }

        public YahooGroup CreateGroup(string name)
        {
            return new YahooGroup(_cookieContainer, name, _speedLimitOptions, _workflowOptions, ServerAppDownloadOptions);
        }

        #region Logout
/*
        public void Logout()
        {
            const string requestUrl = "http://login.yahoo.com/config/login?logout=1";

            var request = (HttpWebRequest)WebRequest.Create(requestUrl);

            request.CookieContainer = _cookieContainer;

            request.AllowAutoRedirect = false;

            var response = (HttpWebResponse)request.GetResponse();

            response.Close();            

            //bool isUserLoggedIn = IsLoggedIn();

            //if (isUserLoggedIn)
            //{
            //    throw new LoginException("Logout failed");            
            //}
        }
*/
        #endregion

        private void InputLoginAndPassword(string login, string password)
        {
            _formInputValues.Add("login", login);

            _formInputValues.Add("passwd", password);
        }

        private void InputStaticValues()
        {
            _formInputValues.Add("sessionId", "");
            _formInputValues.Add(".md5", "");
            _formInputValues.Add(".hash", "");
            _formInputValues.Add(".js", "");
            _formInputValues.Add(".stepid", "");
            _formInputValues.Add(".partner", "");
            _formInputValues.Add(".lang", "en-US");
            _formInputValues.Add(".yplus", "");
            _formInputValues.Add(".pd", "fpctx_ver=0&c=&ivt=&sg=");
            _formInputValues.Add(".pkg", "");
            _formInputValues.Add(".chkP", "Y");

            _formInputValues.Add(".tries", "1");
            _formInputValues.Add(".src", "ygrp");
            _formInputValues.Add(".last", "");
            _formInputValues.Add("promo", "1");
            //_formInputValues.Add(".intl", "uk");
            _formInputValues.Add(".bypass", "");
            _formInputValues.Add(".partner", "");
            _formInputValues.Add(".emailCode", "");
            _formInputValues.Add("hasMsgr", "0");
            _formInputValues.Add(".chkP", "Y");
            _formInputValues.Add(".done", "https://groups.yahoo.com/");
            _formInputValues.Add(".persistent", "y");
            _formInputValues.Add(".save", "Sign+In");
        }

        private void InputServiceValues()
        {
            //string pageContents = Utils.DownloadWebPage(string.Format("https://login.yahoo.com/config/login_verify2?.src=fpctx&.intl={0}&.done=https%3A%2F%2F{0}.groups.yahoo.com%2F", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code), _cookieContainer);

            WebDownloadResponse page = Utils.GetWebPage(string.Format("https://login.yahoo.com/config/login_verify2?.src=fpctx&.intl={0}&.done=https%3A%2F%2F{0}.groups.yahoo.com%2F", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code), _cookieContainer);

            if (!page.IsSuccess) return;

            string pageContent = page.Content;

            _cookieContainer = page.CookieContainer;

            //string pageContents = Utils.DownloadWebPage("https://login.yahoo.com/config/login_verify2?.intl=us&.src=ygrp", _cookieContainer);

            if (string.IsNullOrEmpty(pageContent)) return;

            var regexU = new Regex("<input type=\\\"hidden\\\" name=\\\"\\.u\\\" value=\\\"([^\"]*)\\\">");
            Match match = regexU.Match(pageContent);
            _formInputValues.Add(".u", match.Groups[1].Value);

            var regexV = new Regex("<input type=\\\"hidden\\\" name=\\\"\\.v\\\" value=\\\"([^\"]*)\\\">");
            match = regexV.Match(pageContent);
            _formInputValues.Add(".v", match.Groups[1].Value);

            var regexChallenge =
                new Regex("<input type=\\\"hidden\\\" name=\\\"\\.challenge\\\" value=\\\"([^\"]*)\\\">");
            match = regexChallenge.Match(pageContent);
            _formInputValues.Add(".challenge", match.Groups[1].Value);
        }

        
      
        private void ReceiveGroupsList()
        {
            _userGroups.Clear();

            string groupsPageContents = string.Empty;

            int startRecord = 1;

            int recordsThisLoop;

            do {
                groupsPageContents = Utils.DownloadYahooWebPage(
                    string.Format("http://{0}groups.yahoo.com/api/v1/user/groups/all?start={1}&sortOrder=asc&orderBy=name&chrome=raw&.intl={2}", 
                    SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.CodePeriodPostfix, 
                    startRecord, 
                    SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.Code), 
                    _cookieContainer);

                if (string.IsNullOrEmpty(groupsPageContents)) return;

                var oGroupsRoot = JObject.Parse(groupsPageContents);

                var oGroups = oGroupsRoot["ygData"]["allMyGroups"];

                recordsThisLoop = oGroups.Count();

                if (recordsThisLoop <= 0) continue;

                foreach (var oGroup in oGroups)
                {
                    string groupName = oGroup["groupName"].ToString();

                    _userGroups.Add(new YahooGroup(_cookieContainer, groupName, _speedLimitOptions, _workflowOptions, ServerAppDownloadOptions));
                }

                startRecord += recordsThisLoop;

            } while (recordsThisLoop > 0);
        }

        public void ReceiveGroupsAsync()
        {
            if (_receiveGroupsWorker != null)
            {
                ReceiveGroupsCancelAsync();
            }

            _receiveGroupsWorker = new BackgroundWorker
            {
                WorkerSupportsCancellation = true
            };

            _receiveGroupsWorker.DoWork += new DoWorkEventHandler((sender, e) => ReceiveGroupsList());

            _receiveGroupsWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((sender, e) => ReceiveGroupsCompleted(_userGroups));

            _receiveGroupsWorker.RunWorkerAsync();
        }

        private void ReceiveGroupsCancelAsync()
        {
            _receiveGroupsWorker.CancelAsync();
        }
    }
}
