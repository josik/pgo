﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using Newtonsoft.Json.Linq;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class MessagesBundleParser
    {
        string _content;
        private string _groupName;
        private DownloadOptions _downloadOptions;
        long _lastMessageNumber;

        public MessagesBundleParser(string groupName, string content, DownloadOptions downloadOptions)
        {
            _groupName = groupName;
            _content = content;
            _downloadOptions = downloadOptions;
        }

        public SortedList<uint, YahooGroupMessage> Parse()
        {
            var messages = new SortedList<uint, YahooGroupMessage>();

            try
            {
                JObject root = JObject.Parse(_content);

                var messagesContent =
                    from m in root["ygData"]["messages"]
                    select new
                    {
                        Number = (uint)m["messageId"],
                        Date = Utils.UnixTimeStampToDateTime((uint)m["date"]),
                        Email = (string)m["pgoEmail"],
                        Author = (string)m["pgoAuthor"],
                        Title = WebUtility.HtmlDecode((string)m["subject"]),
                        Content = (string)m["pgoContent"],
                        HasAttachments = (uint)m["hasAttachments"],
                        Attachments = m["attachments"],
                        TopicId = (string)m["pgoTopicId"],
                        ThreadLevel = (string)m["PgoThreadLevel"],
                        ParentId = (string)m["pgoParentId"],
                        PrevInTopic = (string)m["pgoPrevInTopic"],
                        NextInTopic = (string)m["pgoNextInTopic"],
                        PrevInTime = (string)m["pgoPrevInTime"],
                        NextInTime = (string)m["pgoNextInTime"]
                    };

                foreach (var d in messagesContent)
                {
                    var title = d.Title;
                    //title = title.Replace("[" + _groupName + "] ", string.Empty).Trim();

                    var message = new YahooGroupMessage();
                    message.Number = d.Number;
                    message.Date = d.Date;
                    message.Email = d.Email;
                    message.Author = d.Author;
                    message.Title = title;
                    message.Content = d.Content;
                    message.HasAttachments = d.HasAttachments;
                    message.TopicId = Convert.ToUInt32(d.TopicId);
                    message.ThreadLevel = Convert.ToUInt32(d.ThreadLevel);
                    message.ParentId = Convert.ToUInt32(d.ParentId);
                    message.PrevInTopic = Convert.ToUInt32(d.PrevInTopic);
                    message.NextInTopic = Convert.ToUInt32(d.NextInTopic);
                    message.PrevInTime = Convert.ToUInt32(d.PrevInTime);
                    message.NextInTime = Convert.ToUInt32(d.NextInTime);

                    // get attachments info
                    if (message.HasAttachments > 0 && d.Attachments != null)
                    {
                        foreach (JToken attachment in d.Attachments)
                        {
                            Uri fileUri = null;

                            string albumUri = null;

                            uint messageId = (uint)attachment["referenceId"];

                            string filename = attachment["filename"].ToString();

                            string downloadPath = string.Format("{0}\\{1}\\{2}", _downloadOptions.FolderForAttachments, _groupName, filename);

                            JToken attachmentFileTypeToken = attachment["fileType"];

                            JToken attachmentTypeToken = attachment["type"];
#if DEBUG
                            Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format(@"/DEBUG\ Downloading file of type '{0}'", attachmentFileTypeToken.ToString().Trim()));
#endif
                            switch (attachmentTypeToken.ToString())
                            {
                                case "photo":
                                    JToken photoToken = attachment["photoInfo"].OrderByDescending(x => x["size"]).FirstOrDefault();
                                    
                                    if (photoToken == null) continue;
                                    
                                    fileUri = new Uri(photoToken["displayURL"].ToString());
                                    
                                    albumUri = attachment["photomaticAlbumUrl"].ToString();
                                    
                                    message.Attachments.Add(new YahooGroupAttachment
                                    {
                                        Name = filename,
                                        Type = attachmentTypeToken.ToString(),
                                        FileType = attachmentFileTypeToken.ToString(),
                                        PathOnDisk = downloadPath,
                                        YahooUri = fileUri,
                                        RefererUri = albumUri,
                                        MessageNumber = messageId
                                    });
                                    
                                    break;

                                case "file":
                                    fileUri = new Uri(attachment["link"].ToString());
                                    
                                    message.Attachments.Add(new YahooGroupAttachment
                                    {
                                        Name = filename,
                                        Type = attachmentTypeToken.ToString(),
                                        FileType = attachmentFileTypeToken.ToString(),
                                        PathOnDisk = downloadPath,
                                        YahooUri = fileUri,
                                        MessageNumber = messageId
                                    });

                                    break;

                                default:
                                    // new file type encountered
#if DEBUG
                                    Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("/Debug/ New file type encountered: '{0}'", attachmentTypeToken));
#endif
                                    continue;
                            }
                        }
                    }
                    //{[
                    //  {
                    //    "groupId": 11048845,
                    //    "attachmentId": 167553899,
                    //    "fileId": 142972897,
                    //    "title": "Solar Eclipse July 22nd 2009",
                    //    "creatorNickname": "masbagusp",
                    //    "modificationDate": 1248322697,
                    //    "referenceId": 3103,
                    //    "size": 48128,
                    //    "link": "http://xa.yimg.com/kq/groups/11048845/142972897/name/Solar+Eclipse+July+22nd+2009.doc",
                    //    "filename": "Solar Eclipse July 22nd 2009.doc",
                    //    "fileType": "application/msword",
                    //    "type": "file",
                    //    "canDelete": false,
                    //    "photoInfo": []
                    //  }
                    //]}


                    messages.Add(message.Number, message);
                }

                _lastMessageNumber = messages.Count > 0 ? messages.Last().Key : long.MaxValue;
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Unable to parse message.");
                //throw;
            }

            return messages;
        }

        public long LastMessageNumber
        {
            get
            {
                return _lastMessageNumber;
            }
        }

    }
}
