﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class YahooGroupMember
    {
        private string name;
        private string email;
        private DateTime joined;

        public string Name
        {
            get
            {
                return name;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
        }

        public DateTime Joined
        {
            get
            {
                return joined;
            }
        }

        public YahooGroupMember(string name, string email, DateTime joined)
        {
            this.name = name;
            this.email = email;
            this.joined = joined;
        }
    }
}
