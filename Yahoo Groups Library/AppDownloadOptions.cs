﻿namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class AppDownloadOptions
    {
        public string SourceCountryUrlPrefix { get; set; }

        private DownloadSpeedLimitType _limitSpeedType;

        public bool IsLimitSpeedEnabled { get; private set; }
        
        public DownloadSpeedLimitType LimitSpeedType
        {
            get
            {
                return _limitSpeedType;
            }
            private set
            {
                _limitSpeedType = value;
            }
        }

        public int MessagesPerMinute { get; private set; }

        public int InitialDelay { get; private set; }

        public int Download { get; private set; }

        public int ThenWait { get; private set; }

        public bool IsCacheServerEnabled { get; private set; }

        public string CacheServerName { get; private set; }

        public string CacheServerRestApiVersion { get; private set; }

        public bool IsMessageCacheEnabled { get; private set; }

        public int MessageCacheMessagesPerRequest { get; private set; }

        public AppDownloadOptions()
        {
            IsLimitSpeedEnabled = false;
            IsCacheServerEnabled = false;
            IsMessageCacheEnabled = false;
        }

        public AppDownloadOptions(bool isLimitSpeedEnabled, int limitSpeedType, int messagesPerMinute, int initialDelay,
            int download, int thenWait, bool isCacheServerEnabled, string cacheServerName, string cacheServerRestApiVersion, 
            bool isMessageCacheEnabled, int messageCacheMessagesPerRequest)
        {
            IsLimitSpeedEnabled = isLimitSpeedEnabled;
            LimitSpeedType = limitSpeedType == 0 ? DownloadSpeedLimitType.Speed : DownloadSpeedLimitType.Repeat;
            MessagesPerMinute = messagesPerMinute;
            InitialDelay = initialDelay;
            Download = download;
            ThenWait = thenWait;
            IsCacheServerEnabled = isCacheServerEnabled;
            CacheServerName = cacheServerName;
            CacheServerRestApiVersion = cacheServerRestApiVersion;
            IsMessageCacheEnabled = isMessageCacheEnabled;
            MessageCacheMessagesPerRequest = messageCacheMessagesPerRequest;
        }

    }
}
