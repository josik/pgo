﻿using System.IO;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    /// <summary>
    /// Random string generators.
    /// </summary>
    public static class RandomUtil
    {
        /// <summary>
        /// Get random string of 11 characters.
        /// </summary>
        /// <returns>Random string.</returns>
        public static string GetRandomString()
        {
            string path = Path.GetRandomFileName().Replace(".", string.Empty);

            return path;
        }
    }
}
