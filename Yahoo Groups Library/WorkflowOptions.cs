﻿namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class WorkflowOptions
    {
        int _invalidPagesMaxCount;
        int _rerequestMessagesMaxCount;
        bool _stopAfterReceivingWrongPages;
        bool _onLockoutResumeAfterEnabled;
        int _onLockoutResumeAfterMinutes;

        public WorkflowOptions(int invalidPagesMaxCount, int rerequestMessagesMaxCount, bool stopAfterReceivingWrongPages, bool onLockoutResumeAfterEnabled, int onLockoutResumeAfterMinutes)
        {
            _invalidPagesMaxCount = invalidPagesMaxCount;
            _rerequestMessagesMaxCount = rerequestMessagesMaxCount;
            _stopAfterReceivingWrongPages = stopAfterReceivingWrongPages;
            _onLockoutResumeAfterEnabled = onLockoutResumeAfterEnabled;
            _onLockoutResumeAfterMinutes = onLockoutResumeAfterMinutes;
        }

        public int InvalidPagesMaxCount
        {
            get
            {
                return _invalidPagesMaxCount;
            }
        }

        public int RerequestMessagesMaxCount
        {
            get
            {
                return _rerequestMessagesMaxCount;
            }
        }

        public bool StopAfterReceivingWrongPages
        {
            get
            {
                return _stopAfterReceivingWrongPages;
            }
        }

        public bool OnLockoutResumeAfterEnabled
        {
            get
            {
                return _onLockoutResumeAfterEnabled;
            }
        }

        public int OnLockoutResumeAfterMinutes
        {
            get
            {
                return _onLockoutResumeAfterMinutes;
            }
        }
    }
}
