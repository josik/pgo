﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.ComponentModel;
using Asfdfdfd.Api.Yahoo.Groups.DownloadLimiter;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    class MessagesDownloader
    {
        CookieContainer _cookieContainer;

        DownloadOptions _downloadOptions;

        long _allGroupMessagesCount = 0;
        
        BackgroundWorker _receiveMessagesWorker;

        HashSet<long> _existingMessageNumbers;

        bool _downloadAttachments;

        string _name;
        
        SpeedLimitOptions _speedLimitOptions;

        WorkflowOptions _workflowOptions;

        long _prevMessageNumber = 0;

        long _lastMessageNumber = 0;

        int _messagesPerMinute = 0;

        IDownloadLimiter _downloadLimiter;

        public MessagesDownloader(string yahooGroupName, SpeedLimitOptions speedLimitOptions, WorkflowOptions workflowOptions, DownloadOptions downloadOptions, long allGroupMessagesCount, CookieContainer cookieContainer, HashSet<long> existingMessageNumbers, bool downloadAttachments)
        {
            _speedLimitOptions = speedLimitOptions;

            _workflowOptions = workflowOptions;

            _name = yahooGroupName;

            _cookieContainer = cookieContainer;

            _downloadOptions = downloadOptions;

            if (_downloadOptions.EndMessageNumber < _allGroupMessagesCount)
            {
                _allGroupMessagesCount = _downloadOptions.EndMessageNumber;
            }

            if (_downloadOptions.StartMessageNumber < 1)
            {
                _downloadOptions.StartMessageNumber = 1;
            }

            _prevMessageNumber = _downloadOptions.StartMessageNumber;

            _lastMessageNumber = _prevMessageNumber;

            _allGroupMessagesCount = allGroupMessagesCount;

            _existingMessageNumbers = existingMessageNumbers;
            
            _downloadAttachments = downloadAttachments;

            if (_speedLimitOptions.IsEnabled)
            {
                switch (_speedLimitOptions.SpeedLimitType)
                {
                    case DownloadSpeedLimitType.Repeat:
                        _downloadLimiter = new RepeatDownloadLimiter(_speedLimitOptions);
                        break;

                    case DownloadSpeedLimitType.Speed:
                        _downloadLimiter = new SpeedDownloadLimiter(_speedLimitOptions);
                        break;
                }
            }
        }
        
        public string GetMessagesUrl(long lastMessageNumber)
        {
            //string messagesUriTemplate = "http://groups.yahoo.com/group/{0}/messages/{1}?xm=1&m=e&l=1";

            string messagesUriTemplate = string.Format("http://{0}groups.yahoo.com/group/{{0}}/messages/{{1}}?xm=1&m=e&l=1", SourceCountryUrlPrefixOption.Instance.GetCurrentCodeInUse.CodePeriodPostfix);

#if DEBUG
            //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format("[dbg] GetMessagesUrl() :: '{0}'", messagesUriTemplate));
            //File.AppendAllText(@"c:\temp\PgOffline_debug_messages.log", ">> GetMessagesUrl" + Environment.NewLine + messagesUriTemplate + Environment.NewLine);
#endif

            return string.Format(messagesUriTemplate, _name, lastMessageNumber);
        }
    }
}
