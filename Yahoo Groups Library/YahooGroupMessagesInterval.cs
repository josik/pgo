﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class YahooGroupMessagesInterval
    {
        int _start;
        int _stop;

        public YahooGroupMessagesInterval(int start, int stop)
        {
            _start = start;
            _stop = stop;
        }

        public int Start
        {
            get { return _start; }
            set { _start = value; }
        }

        public int Stop
        {
            get { return _stop; }
            set { _stop = value; }
        }
    }
}
