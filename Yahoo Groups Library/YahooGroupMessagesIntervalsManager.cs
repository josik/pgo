﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    class YahooGroupMessagesIntervalsManager
    {
        List<YahooGroupMessagesInterval> _messagesIntervals;
        int _messagesCount = 0;

        public YahooGroupMessagesIntervalsManager(List<YahooGroupMessagesInterval> messagesIntervals)
        {
            _messagesIntervals = messagesIntervals;
        }

        static private int YahooGroupMessagesIntervalComparator(YahooGroupMessagesInterval interval1, YahooGroupMessagesInterval interval2)
        {
            if (interval1.Start > interval2.Start)
            {
                return 1;
            }

            if (interval1.Start < interval2.Start)
            {
                return -1;
            }

            return 0;
        }

        public int MessagesCount
        {
            get
            {
                if (_messagesCount != 0)
                {
                    return _messagesCount;
                }

                foreach (var messageInterval in _messagesIntervals)
                {
                    _messagesCount += (messageInterval.Stop - messageInterval.Start);
                }

                return _messagesCount;
            }
        }

        public IEnumerable<uint> MessagesNumbers
        {
            get
            {
                foreach (var messageInterval in _messagesIntervals)
                {
                    for (uint messageIndex = (uint)messageInterval.Start; messageIndex <= messageInterval.Stop; messageIndex++)
                    {
                        yield return messageIndex;
                    }
                }
            }
        }

        public List<YahooGroupMessagesInterval> Process()
        {
            _messagesIntervals.Sort(YahooGroupMessagesIntervalComparator);
            
            FixIntersections();
            FixInvalid();

            return _messagesIntervals;
        }

        private void FixIntersections()
        {
            // TODO. Implement
        }

        private void FixInvalid()
        {
            // TODO. Implement
        }
    }
}
