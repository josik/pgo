﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public class SpeedLimitOptions
    {
        private int _speedLimitType;

        public DownloadSpeedLimitType SpeedLimitType
        {
            get
            {
                return _speedLimitType == 0 ? DownloadSpeedLimitType.Speed : DownloadSpeedLimitType.Repeat;
            }
        }

        public int MessagesPerMinute { get; private set; }

        public int InitialDelay { get; private set; }

        public int Download { get; private set; }

        public int ThenWait { get; private set; }

        public bool IsEnabled { get; private set; }

        public SpeedLimitOptions()
        {
            IsEnabled = false;
        }

        public SpeedLimitOptions(int speedLimitType, int messagesPerMinute, int initialDelay, int download, int thenWait)
        {
            IsEnabled = true;

            _speedLimitType = speedLimitType;

            MessagesPerMinute = messagesPerMinute;

            InitialDelay = initialDelay;

            Download = download;

            ThenWait = thenWait;
        }

    }
}
