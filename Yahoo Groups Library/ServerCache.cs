﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Asfdfdfd.Api.Yahoo.Groups
{
    public static class ServerCache
    {
        private const string debug_url = "localhost";

        public static string GetMessage(AppDownloadOptions appDownloadOptions, string groupName, uint thisMessageId)
        {
            if (!appDownloadOptions.IsCacheServerEnabled 
                || string.IsNullOrEmpty(appDownloadOptions.CacheServerName) 
                || string.IsNullOrEmpty(appDownloadOptions.CacheServerRestApiVersion)) return string.Empty;

            var url = string.Format("https://{0}/{1}/groups/{2}/messages/{3}", appDownloadOptions.CacheServerName, appDownloadOptions.CacheServerRestApiVersion, groupName, thisMessageId);

#if DEBUG
            //url = string.Format("https://{0}/{1}/groups/{2}/messages/{3}", debug_url, appDownloadOptions.CacheServerRestApiVersion, groupName, thisMessageId);
#endif
            string content = Utils.DownloadJson(url);

#if DEBUG
            //Trace.WriteLine(DateTime.Now.ToLongTimeString() + " " + string.Format(@"/DEBUG\ Pull ServerCache: content length: {0} {1} ", content.Length, url));
#endif

            if (content == "[]" || content == "{}")
            {
                content = string.Empty;
            }

            return content;
        }
        
        public async static Task<bool> SubmitMessageInNewThread(AppDownloadOptions appDownloadOptions, uint submissionUserId, string groupName, uint thisMessageId, string messageDetail)
        {
            var appDownloadOptionsCopy = appDownloadOptions;
            var submissionUserIdCopy = submissionUserId;
            var groupNameCopy = groupName;
            var thisMessageIdCopy = thisMessageId;
            var messageDetailCopy = messageDetail;

            if (!appDownloadOptionsCopy.IsCacheServerEnabled
                || string.IsNullOrEmpty(appDownloadOptionsCopy.CacheServerName)
                || string.IsNullOrEmpty(appDownloadOptionsCopy.CacheServerRestApiVersion)) return false;

            var url = string.Format("https://{0}/{1}/groups/{2}/messages/{3}", appDownloadOptionsCopy.CacheServerName, appDownloadOptionsCopy.CacheServerRestApiVersion, groupNameCopy, thisMessageIdCopy);

#if DEBUG
            //url = string.Format("https://{0}/{1}/groups/{2}/messages/{3}", debug_url, appDownloadOptionsCopy.CacheServerRestApiVersion, groupNameCopy, thisMessageIdCopy);
#endif

            JObject jsonContent = new JObject
            {
                new JProperty("submission_username", submissionUserIdCopy),
                new JProperty("group_name", groupNameCopy),
                new JProperty("message_id", thisMessageIdCopy),
                new JProperty("message_json", messageDetailCopy)
            };


            Utils.MakeAsyncRequest(url, jsonContent);
            
            return true;
        }

        public static void SubmitMessageInNewThread2(AppDownloadOptions appDownloadOptions, uint submissionUserId, string groupName, uint thisMessageId, string messageDetail)
        {
            var appDownloadOptionsCopy = appDownloadOptions;
            var submissionUserIdCopy = submissionUserId;
            var groupNameCopy = groupName;
            var thisMessageIdCopy = thisMessageId;
            var messageDetailCopy = messageDetail;


            if (!appDownloadOptionsCopy.IsCacheServerEnabled
                || string.IsNullOrEmpty(appDownloadOptionsCopy.CacheServerName)
                || string.IsNullOrEmpty(appDownloadOptionsCopy.CacheServerRestApiVersion)) return;

            var url = string.Format("https://{0}/{1}/groups/{2}/messages/{3}", appDownloadOptionsCopy.CacheServerName, appDownloadOptionsCopy.CacheServerRestApiVersion, groupNameCopy, thisMessageIdCopy);

#if DEBUG
            //url = string.Format("http://{0}/{1}/groups/{2}/messages/{3}", debug_url, appDownloadOptionsCopy.CacheServerRestApiVersion, groupNameCopy, thisMessageIdCopy);
#endif

            JObject content = new JObject
            {
                new JProperty("submission_username", submissionUserIdCopy),
                new JProperty("group_name", groupNameCopy),
                new JProperty("message_id", thisMessageIdCopy),
                new JProperty("message_json", messageDetailCopy)
            };



            // spawn process and do not wait for result, do not care

            //Task task = new Task( () => DoSubmitMessage(url, content));
            //task.Start();

            //Task.Factory.StartNew(() => DoSubmitMessage(url, content));
            
            //ThreadPool.QueueUserWorkItem(o => DoSubmitMessage(url, content));

            //var t = new Action(() => DoSubmitMessage(url, content)).BeginInvoke(null, null);

            //Task.Run(() => DoSubmitMessage(url, content));


            DoSubmitMessage(url, content);
        }


        public static async Task<bool> SubmitMessageInNoNewThread(AppDownloadOptions appDownloadOptions, uint submissionUserId, string groupName, uint thisMessageId, string messageDetail)
        {
            var appDownloadOptionsCopy = appDownloadOptions;
            var submissionUserIdCopy = submissionUserId;
            var groupNameCopy = groupName;
            var thisMessageIdCopy = thisMessageId;
            var messageDetailCopy = messageDetail;

            if (!appDownloadOptionsCopy.IsCacheServerEnabled
                || string.IsNullOrEmpty(appDownloadOptionsCopy.CacheServerName)
                || string.IsNullOrEmpty(appDownloadOptionsCopy.CacheServerRestApiVersion)) return false;

            var url = string.Format("https://{0}/{1}/groups/{2}/messages/{3}", appDownloadOptionsCopy.CacheServerName, appDownloadOptionsCopy.CacheServerRestApiVersion, groupNameCopy, thisMessageIdCopy);

#if DEBUG
            //url = string.Format("http://{0}/{1}/groups/{2}/messages/{3}", debug_url, appDownloadOptionsCopy.CacheServerRestApiVersion, groupNameCopy, thisMessageIdCopy);
#endif

            JObject content = new JObject
            {
                new JProperty("submission_username", submissionUserIdCopy),
                new JProperty("group_name", groupNameCopy),
                new JProperty("message_id", thisMessageIdCopy),
                new JProperty("message_json", messageDetailCopy)
            };

            Utils.PostJson(url, content);

            return true;
        }

        private static void DoSubmitMessage(string url, JObject content)
        {
            Utils.PostJson(url, content);
            var fname = Path.GetRandomFileName();
            File.WriteAllText("c:/temp_pg4/task/" + fname , url);
            File.AppendAllText("c:/temp_pg4/task/" + fname, content.ToString());
        }

    }
}
