﻿using PGOffline.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Data.SQLite;
using PGOffline;
using PGOffline.Exceptions;

namespace UnitTests
{
    [TestClass()]
    public class OptionsTest
    {
        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestMethod()]
        public void OptionsTestInitialize()
        {
            using (TempFileManager temp = new TempFileManager())
            {
                FileStream file = File.Create(temp["options.db3"]);
                file.Close();

                string schemaPath = Path.Combine(PathResolver.GetAppDir(), @"Data\Test\options.sql");

                SQLiteConnection connection = new SQLiteConnection(@"Data source=" + file.Name);
                connection.Open();

                Options options = CreateOptionsDatabaseTest(connection, schemaPath);

                if (options != null)
                    SetStatisticsOptionsTest(options);

                connection.Close();
            }
        }

        public Options CreateOptionsDatabaseTest(SQLiteConnection connection, string schemaPath)
        {
            try
            {
                Options options = new Options(connection, schemaPath);
                return options;
            }
            catch (ExecutionEngineException)
            {
                Assert.Fail("Database isn't created.");
                return null;
            }                
        }

        public void SetStatisticsOptionsTest(Options options)
        {
            try
            {
                StatisticsOptions staticOptions = new StatisticsOptions();
                staticOptions.SetStatisticsOptions(options);
            }
            catch (StatisticsOptionsFormatException)
            {
                return;
            }

            //Assert.Fail("No exception was thrown.");
        }
    }
}