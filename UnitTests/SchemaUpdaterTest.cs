﻿using PGOffline;
using PGOffline.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Data.SQLite;
using System.IO;

namespace UnitTests
{
    /// <summary>
    ///This is a test class for SchemaUpdaterTest and is intended
    ///to contain all SchemaUpdaterTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SchemaUpdaterTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestMethod()]
        public void TryDatabaseUpgradeTest()
        {
            using (TempFileManager temp = new TempFileManager())
            {
                FileStream file = File.Create(temp["db.db3"]);
                file.Close();

                DatabaseManager dbManager = new DatabaseManager();

                SQLiteConnection connection = new SQLiteConnection(@"Data source=" + file.Name);
                connection.Open();

                try
                {
                    SchemaUpdater schemaUpdater = new SchemaUpdater(connection, file.Name);
                    schemaUpdater.TryDatabaseUpgrade();

                    //Assert.Fail("No exception was thrown.");
                }
                catch (DatabaseWrongFormatException e)
                {
                    Debug.WriteLine("Database wrong format exception.");
                    //return;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

    }
}
