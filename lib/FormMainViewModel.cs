using Asfdfdfd.Api.Yahoo.Groups;
using PGOffline.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
namespace PGOffline.ViewModel
{
	public class FormMainViewModel : ViewModelBase
	{
		private BackgroundWorker worker;
		private PGOTraceListener _traceListener;
		private YahooGroupsServer _yahooServer;
		public static AsyncObservableCollection<Message> MyMessageCollection
		{
			get;
			set;
		}
		public DiscussionGroup CurrentDiscussionGroup
		{
			get;
			private set;
		}
		public System.Collections.Generic.List<DiscussionGroup> CurrentDiscussionGroupSelectedList
		{
			get;
			private set;
		}
		public Message CurrentMessage
		{
			get;
			private set;
		}
		public long TotalMessageCount
		{
			get;
			private set;
		}
		public ICommand GroupSelectedCommand
		{
			get;
			set;
		}
		public ICommand GroupSelectedCommand_CanExecute
		{
			get;
			set;
		}
		public ICommand LoadGroupMessagesCommand
		{
			get;
			set;
		}
		public ICommand UpdateSingleGroupCommand
		{
			get;
			set;
		}
		public ICommand UpdateSingleGroupCommand_CanExecute
		{
			get;
			set;
		}
		public ICommand UpdateAllGroupCommand
		{
			get;
			set;
		}
		public ICommand UpdateAllGroupCommand_CanExecute
		{
			get;
			set;
		}
		public FormMainViewModel()
		{
			this.CurrentDiscussionGroupSelectedList = new System.Collections.Generic.List<DiscussionGroup>();
			FormMainViewModel.MyMessageCollection = new AsyncObservableCollection<Message>();
			this.SetupYahooServer();
			this.GroupSelectedCommand = new RelayCommand(new System.Action<object>(this.GroupSelected_Execute), new System.Predicate<object>(this.GroupSelected_CanExecute));
			this.UpdateSingleGroupCommand = new RelayCommand(new System.Action<object>(this.UpdateSingleGroup_Execute), new System.Predicate<object>(this.UpdateSingleGroup_CanExecute));
			this.UpdateAllGroupCommand = new RelayCommand(new System.Action<object>(this.UpdateAllGroup_Execute), new System.Predicate<object>(this.UpdateAllGroup_CanExecute));
		}
		private bool UpdateAllGroup_CanExecute(object obj)
		{
			return true;
		}
		private void UpdateAllGroup_Execute(object obj)
		{
			throw new System.NotImplementedException();
		}
		private bool UpdateSingleGroup_CanExecute(object obj)
		{
			return this.CurrentDiscussionGroupSelectedList.Count == 1;
		}
		private void UpdateSingleGroup_Execute(object obj)
		{
			if (this.CurrentDiscussionGroup == null)
			{
				return;
			}
			Trace.WriteLine(string.Format("Receiving messages for group {0}", this.CurrentDiscussionGroup.Name));
			this.worker = new BackgroundWorker();
			this.worker.WorkerReportsProgress = true;
			this.worker.WorkerSupportsCancellation = true;
			this.worker.DoWork += new DoWorkEventHandler(this.worker_DoWork);
			this.worker.ProgressChanged += new ProgressChangedEventHandler(this.worker_ProgressChanged);
			this.worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.worker_RunWorkerCompleted);
		}
		private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			throw new System.NotImplementedException();
		}
		private void worker_DoWork(object sender, DoWorkEventArgs doWorkEventArgs)
		{
			DiscussionGroupsMessagesDownloader discussionGroupsMessagesDownloader = new DiscussionGroupsMessagesDownloader(this._yahooServer);
			discussionGroupsMessagesDownloader.DownloadNewDiscussionGroupMessages(this.CurrentDiscussionGroup, 2147483647L);
		}
		public void DownloadNewDiscussionGroupsMessages(System.Collections.Generic.List<DiscussionGroup> groupsIncoming)
		{
			System.Collections.Generic.List<DiscussionGroup> list = new System.Collections.Generic.List<DiscussionGroup>(groupsIncoming);
			DiscussionGroupsMessagesDownloader discussionGroupsMessagesDownloader = new DiscussionGroupsMessagesDownloader(this._yahooServer);
			foreach (DiscussionGroup current in list)
			{
				discussionGroupsMessagesDownloader.DownloadNewDiscussionGroupMessages(current, 2147483647L);
			}
		}
		private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			Trace.WriteLine("Downloading still working...");
		}
		private bool GroupSelected_CanExecute(object sender)
		{
			return true;
		}
		private void GroupSelected_Execute(object obj)
		{
			if (obj == null)
			{
				return;
			}
			this.CurrentDiscussionGroupSelectedList = ((System.Collections.IEnumerable)obj).Cast<DiscussionGroup>().ToList<DiscussionGroup>();
			if (this.CurrentDiscussionGroupSelectedList.Count != 1)
			{
				return;
			}
			this.LoadGroupMessages(this.CurrentDiscussionGroupSelectedList.FirstOrDefault<DiscussionGroup>());
		}
		private void LoadGroupMessages(DiscussionGroup group)
		{
			if (group == null)
			{
				return;
			}
			this.CurrentDiscussionGroup = group;
			FormMainViewModel.MyMessageCollection.Clear();
			using (SQLiteCommand sQLiteCommand = Singleton<DatabaseManager>.Instance.CreateCommand("select id, number, date, subject, favorite, unread, person, charset, discussion_group, topic_id, parent_id, thread_level, prev_in_topic, next_in_topic, prev_in_time, next_in_time from group_message where discussion_group = @group"))
			{
				sQLiteCommand.Parameters.AddWithValue("@group", group.Id);
				using (SQLiteDataReader sQLiteDataReader = sQLiteCommand.ExecuteReader())
				{
					while (sQLiteDataReader.Read())
					{
						object[] array = new object[sQLiteDataReader.FieldCount];
						sQLiteDataReader.GetValues(array);
						FormMainViewModel.MyMessageCollection.Add(new Message(array));
					}
				}
			}
			this.TotalMessageCount = (long)FormMainViewModel.MyMessageCollection.Count<Message>();
		}
		private void SetupYahooServer()
		{
			this._yahooServer = new YahooGroupsServer(Utils.GetAppDownloadOptions());
			this._yahooServer.SetParsingOptions(Singleton<Options>.Instance.GetParsingOptions());
		}
	}
}
